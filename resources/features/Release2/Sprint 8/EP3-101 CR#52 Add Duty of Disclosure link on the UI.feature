Feature: CR#52 Add Duty of Disclosure link on the UI
#Meta: @issue #EP3-101 

Background: 
	Given eBusines portal is launched 
	 And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details
	
@Sprint-8
Scenario: As a broker I wnat to see the Duty of Disclousure link on the Landing page
And click Duty of Disclousure link on UI

#And click on quick indication button and continue
#And click on privacy statement link in the Indicative quote page and open PDF

@Sprint-8
Scenario: As a broker I wnat to see the Duty of Disclousure link on the indicative quote page
And click on quick indication button and continue
And click Duty of Disclousure link on UI

@Sprint-8
Scenario: As a broker I wnat to see the Duty of Disclousure link on the Full quotation page
And click I would like an full quotation
And click Duty of Disclousure link on UI