Feature: EP3-6 Chubb Direct and NAB UI changes
	
Background:

	Given evolution SIT application is launched
	And select search type as "Client"
	
	@CI   
	Scenario: EP3-Chubb Direct and NAB UI changes for Individual client
	And Create a new client as UW in "EP3_6_SC1" 
	Then Verify Chubb Direct and NAB
	@CI
	Scenario: EP3-Chubb Direct and NAB UI changes for Company client
	And Create a new company client in UW
	Then Verify Chubb Direct and NAB