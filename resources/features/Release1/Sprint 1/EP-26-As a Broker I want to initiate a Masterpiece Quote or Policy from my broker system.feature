Feature: As a Sunrise Broker I want to access the Evolution Application 
Meta: @issue #EP2-26


Background: 
	Given eBusines portal is launched 
	And enter company id as "IAA00371" 
	And enter user id as "EVOTEST" 
	And enter password "123456" 
	And click login 


@CI	
@Sprint-1
Scenario: Verify Connectivity between eBusiness Portal and Evolution via Add New Quote 
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	#And click on Add a New Quote 
	#And verify landing URL is "http://apevodweb121.aceins.com/NewQuote/index/" 
	And verify landing URL is "http://apregevwebqa121.aceins.com/NewQuote/index/"


@CI
@Sprint-1
Scenario: Verify Connectivity between eBusiness Portal and Evolution via New Business 
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add New Business in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	#And click on Add a New Quote 
	#And verify landing URL is "http://apevodweb121.aceins.com/NewQuote/index/" 
	And verify landing URL is "http://apregevwebqa121.aceins.com/NewQuote/index/"
