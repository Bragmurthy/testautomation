Feature: EP2-260-As a Broker I want to access Evolution application through WINBEAT

@Sprint-11
Scenario: 
Given WinBeat portal is launched
And login WinBeat portal with "testflow1"
And create client and Save details
And click on "TRANSACTION" by Name in Winbeat
And click on "Transaction entry" by Name in Winbeat
And click on "optSunrise" by id in WinBeat
And click on "optCreateQuote" by id in WinBeat
And click on Next> button in winBeat
And select client
And click on Next> button in winBeat
And select scheme "CBMP02"
And click on Next> button in winBeat after Scheme Selection
And click on I would like a full quotationin Winbeat
And click on "Continue" by Name in Winbeat
And click on "Update Client" by Name in Winbeat
And Update client details
And click on "Submit" by Name in Winbeat
And add address details
And Select Insurer and Add named insured
And Change Broker Contact
And click on "Next" by Name in Winbeat
And Wait for "1" minutes
And Add location And VAC and Home and contents
And click on "Next" by Name in Winbeat
And Wait for "1" minutes
And click on "Loss History" by Name in Winbeat
And select Loss History options
And click on "Calculate Premium" by Name in Winbeat
And Wait for "1" minutes
And click on "Proceed with referral" by Name in Winbeat
And Wait for "1" minutes
And click on Finish by Name in Winbeat
And logout
And Quit Winbeat session

Given evolution application is launched
And select search type as "Client"
And enter search value saved from Winbeat and search
And Save Quote Number created in WinBeat

And approve quote referred from WinBeat
    
Given WinBeat portal is launched
And login WinBeat portal with "testflow1"
And click on "TRANSACTION" by Name in Winbeat
And click on "Transaction entry" by Name in Winbeat
And click on "optResume" by id in WinBeat
And click on Next> button in winBeat
And select client
And click on "Show" by Name in Winbeat
And click on Next> button in winBeat
And click on "optProduct" by id in WinBeat
And click on Next> button in winBeat
And click on "Premium Summary" by Name in Winbeat
And click on "Finish Quote" by Name in Winbeat
And Wait for "1" minutes 
And click on Next> button in winBeat
And click on Next> button in winBeat
And click on Next> button in winBeat
And Select Invoice Type
And click on "Finish" by Name in Winbeat
And Accept ok button
And click on "cmdCloseForm" by id in WinBeat
And click on "TRANSACTION" by Name in Winbeat
And click on "Transaction entry" by Name in Winbeat
And click on "optConvert" by id in WinBeat
And click on Next> button in winBeat
And select client
And click on "Show" by Name in Winbeat
And click on Next> button in winBeat
And click on Next> button in winBeat
And click on "Binding" by Name in Winbeat
And Select deductible and confirm
And click on "Proceed To Bind" by Name in Winbeat
And click on Next> button in winBeat
And click on Next> button in winBeat
And click on Next> button in winBeat
And Save Policy Number from WinBeat
And click on "Finish" by Name in Winbeat
And Accept ok button
And click on "cmdCloseForm" by id in WinBeat


And click on "TRANSACTION" by Name in Winbeat
And click on "Transaction entry" by Name in Winbeat
And click on "optEndorse" by id in WinBeat
And click on Next> button in winBeat
And select client
And click on "Show" by Name in Winbeat
And click on Next> button in winBeat
And click on Next> button in winBeat
And Initiate Standard Endorsement in Winbeat
And update risk details
And click on "Next" by Name in Winbeat
And click on "Calculate Premium" by Name in Winbeat
And Wait for "1" minutes
And click on "Finish Mid Term Adjustment" by Name in Winbeat
And Wait for "1" minutes
And click on Next> button in winBeat
And Wait for "1" minutes
And click on Next> button in winBeat
And click on Next> button in winBeat
And click on Finish by Name in Winbeat
And logout
And Quit Winbeat session
    