Feature: As a product owner I want to see the latest Privacy Statement updated in Evolution 
Meta: @issue #EP2-561 

Background: 
	Given eBusines portal is launched 
	 And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details
	
@Sprint-10
Scenario: As a product owner I want to see the latest Privacy Statement updated in Evolution
And click on quick indication button and continue
And click on privacy statement link in the Indicative quote page and open PDF