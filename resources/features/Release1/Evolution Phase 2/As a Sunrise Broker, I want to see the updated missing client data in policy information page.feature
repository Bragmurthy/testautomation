Feature: As a Sunrise Broker, I want to see the updated missing client data in policy information page

Acceptance Criteria:
1. Display Policy Information page with the newly created Client details
2. By Default the Edit button in Client Details section should be enabled
3. When Edit button is selected display 'Edit Client' screen with below details:
a. First Name (M) - from Sunrise
b. Last Name(M) - from Sunrise
c. DOB 
d. Occupation
e. Employer
4. All above fields should be editable and default the value if passed from Sunrise
5. Save button should be enabled
6. On clicking Save - 
a. Edited details should be saved
b. Broker automatically navigated to Policy Information page
c. All Client details displayed in Client Details section in Policy Information page


@verify_PCW_Query_1 
Scenario:
verify PCW query get_TOTAL_PREMIUM_BY_PACPL displays CLIENT_ID, LOB_CODE, PRODUCER_NAME, GROSS_PREMIUM_LEDGER, GROSS_PREMIUM_USD and GROSS_COMMISSION_USD 
	Given verify PCW query get_PRODCUER_SELECTION for Producer "%AON RISK%" with policy expiration between "20170101" and "20170331" 
	
@verify_PCW_Query_2 
Scenario:
verify PCW query get_TOTAL_PREMIUM_BY_PACPL displays CLIENT_ID, LOB_CODE, PRODUCER_NAME, GROSS_PREMIUM_LEDGER AND GROSS_PREMIUM_USD 
	Given verify PCW query get_TOTAL_PREMIUM_BY_PACPL for Producer "%AON RISK%" with policy expiration between "20170101" and "20170331" 
	
@verify_PCW_Query_3 
Scenario: verify Get POLICY COUNT displays POLICY_HOLDER_ID and POLICY_COUNT 
	Given verify PCW query get_CLIENT_POLICY_COUNT with policy expiration date "20150331" 
	
@verify_PCW_Query_4 
Scenario: verify PCW query get_CLIENT_DETAILS displays CLIENT_ID and CLIENT_NAME 
	Given verify PCW query get_CLIENT_DETAILS with contact type "PH" 
	
@verify_PCW_Query_5 
Scenario:
verify PCW query get_GLOBAL_PRODUCER_DETAILS displays GLOBAL_PRODUCER_CODE and GLOBAL_PRODUCER_NAME 
	Given verify PCW query get_GLOBAL_PRODUCER_DETAILS 
	
@verify_PCW_Query_6 
Scenario:
verify PCW query get_GLOBAL_PRODUCER_DETAILS displays GLOBAL_PRODUCER_CODE and GLOBAL_PRODUCER_NAME 
	Given verify PCW query get_INDUSTRY_CODE_DESCRIPTION 
