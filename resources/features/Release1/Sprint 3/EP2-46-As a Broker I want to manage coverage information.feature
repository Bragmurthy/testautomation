Feature: EP2-46-As a Broker I want to manage coverage information

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation 

@CI
@Sprint-3	
Scenario: verify "Building Sum Insured" field only accepts integer value for location "1"
And enter "120.789" in building sum insured field for location "0" and verify value accepted by application is "120,789"

@Sprint-3
Scenario: verify "Building Sum Insured" field only accepts integer value for location "2"
And click on "Add Next Risk Location"
And enter "120.789" in building sum insured field for location "1" and verify value accepted by application is "120,789"

@CI
@Sprint-3
Scenario: verify "Content Sum Insured" defaulted to "20%" of "Building Sum Insured" for location "1" and verify "Content Sum Insured" defaulted to "20%" of "Building Sum Insured" for location "2"
And enter details for Risk Location as per test data with no google address picker "testflow7" and save quote reference number
And enter "1000001" for Building Sum Insured and verify "200,000" is displayed for Contents Sum Insured for location "0"
And click on "Add Next Risk Location"
And enter details for Risk Location as per test data "testflow10" and save quote reference number 
And enter "1000067" for Building Sum Insured and verify "200,013" is displayed for Contents Sum Insured for location "1"	

@CI
@Sprint-3
Scenario: verify default value in "Building and Contents Deductible" when type of residence is "Primary Residence" for location "1" and Building Sum Insured is less than 2 million
And verify default Building and Contents Deductible is "500" when Building Sum Insured is "1999999" and residence type is "Primary Residence" for location "0"

@Sprint-3
Scenario: verify default value in "Building and Contents Deductible" when type of residence is "Primary Residence" for location "2" and Building Sum Insured is less than 2 million
And click on "Add Next Risk Location"
And verify default Building and Contents Deductible is "500" when Building Sum Insured is "899999" and residence type is "Primary Residence" for location "1"


@Sprint-3
Scenario: verify default value in "Building and Contents Deductible" when type of residence is "Primary Residence" for location "1" and Building Sum Insured is greater than 2 million
And verify default Building and Contents Deductible is "1,000" when Building Sum Insured is "2899999" and residence type is "Primary Residence" for location "0"

@Sprint-3
Scenario: verify default value in "Building and Contents Deductible" when type of residence is "Primary Residence" for location "2" and Building Sum Insured is greater than 2 million
And click on "Add Next Risk Location"
And verify default Building and Contents Deductible is "1,000" when Building Sum Insured is "2899999" and residence type is "Primary Residence" for location "1"

@Sprint-3
Scenario: verify default value in "Building and Contents Deductible" when type of residence is "Storage" for location "1" and Building Sum Insured is greater than 2 million
And verify default Building and Contents Deductible is "500" when Building Sum Insured is "2999999" and residence type is "Storage" for location "0"

@CI
@Sprint-3
Scenario: verify default value in "Building and Contents Deductible" when type of residence is "Business Property" for location "2" and Building Sum Insured is less than 2 million
And click on "Add Next Risk Location"
And verify default Building and Contents Deductible is "500" when Building Sum Insured is "899999" and residence type is "Business Property" for location "1"

@Sprint-3
Scenario: Verify pop up error message when Contents Sum Insured is set to "$0" for location "1"
And verify pop up error message "We cannot provide standalone cover for Valuable Articles. Please enter a contents sum insured." when Building Sum Insured is "4564564" Contents Sum Insured is "0" for location "0"

@Sprint-3	
Scenario: Verify pop up error message when Contents Sum Insured is set to "$0" for location "2"
And click on "Add Next Risk Location"
And verify pop up error message "We cannot provide standalone cover for Valuable Articles. Please enter a contents sum insured." when Building Sum Insured is "4564564" Contents Sum Insured is "0" for location "1"
 
@CI 
@Sprint-3 
Scenario: Verify add and delete functionality in valuable categories for location "1"and  Verify add and delete functionality in valuable categories for location "2"
enter details for Risk Location as per test data with no google address picker "testflow1" and save quote reference number
And enter building sum insured as "444654" and verify user is able to select below valuable category from drop down for location "0"
|Jewellery         |100               |
|Jewellery In Vault|100               |
|Furs              |100               |
|Fine Arts         |100               |
|Silverware        |100               |
|Stamps            |100               |
|Collectibles      |100               |
|Musical Items     |100               |
|Wine              |100               |
|Cameras           |100               |
And delete added "10" valuable categories from location "0"
And click on "Add Next Risk Location"
And enter details for Risk Location as per test data with no google address picker "testflow10" and save quote reference number
And enter building sum insured as "444654" and verify user is able to select below valuable category from drop down for location "1"
|Valuable Category |Total Sum Insured |
|Jewellery         |100               |
|Jewellery In Vault|100               |
|Furs              |100               |
|Fine Arts         |100               |
|Silverware        |100               |
|Stamps            |100               |
|Collectibles      |100               |
|Musical Items     |100               |
|Wine              |100               |
|Cameras           |100               |
And delete added "10" valuable categories from location "1"


@Manual	
Scenario: Verify label change for location "1" when property type is set to "Unit"
And select property type as "Unit" for location "0" and verify label "Contents Deductible" is displayed


@Manual	
Scenario: Verify label change for location "2" when property type is set to "Unit"
And click on "Add Next Risk Location"
And select property type as "Unit" for location "1" and verify label "Contents Deductible" is displayed


