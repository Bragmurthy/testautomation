Feature: EP2-49-As a Broker I want to continue to obtain a Full Quote 

Background: 
	Given eBusines portal is launched 
	And login eBusiness portal with "testflow1" 
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation 

@CI
@Sprint-3	
Scenario: Continue to Full quote by entering details in Location 1 not calculating Premium 
	And enter details for Risk Location as per test data with no google address picker "testflow1" and save quote reference number 
	And click on Continue Full Quote
	And display quote status as "Status: Quote In Progress" 
	And verify URL is "http://apregevwebqa121.aceins.com/policy-info/edit" and update status in "US_49_SC1"

@Sprint-3	
Scenario: Continue to Full quote by entering details in Location 1 not calculating Premium 
	And enter details for Risk Location as per test data with no google address picker "testflow4" and save quote reference number 
	And click on Continue Full Quote 
	And verify URL is "http://apregevwebqa121.aceins.com/policy-info/edit?" and update status in "US_49_SC2"
	And display quote status as "Status: Quote In Progress" 

@Sprint-3	
Scenario: Continue to Full quote by entering details in Location 1 not calculating Premium 
	And enter details for Risk Location as per test data with no google address picker "testflow7" and save quote reference number 
	And click on Continue Full Quote
	And verify URL is "http://apregevwebqa121.aceins.com/policy-info/edit?" and update status in "US_49_SC3"
	And display quote status as "Status: Quote In Progress" 

@Sprint-3	
Scenario: Continue to Full quote by entering details in Location 1 not calculating Premium 
	And enter details for Risk Location as per test data with no google address picker "testflow10" and save quote reference number 
	And click on Continue Full Quote
	And verify URL is "http://apregevwebqa121.aceins.com/policy-info/edit?" and update status in "US_49_SC4"
	And display quote status as "Status: Quote In Progress" 

	

	
	
	
	
	
