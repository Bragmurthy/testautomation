Feature: EP2-42-As a Broker I want to add multiple Risk Location details 

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation 
	
@Sprint-3	
Scenario: Enter Risk location details for location 1 
	And enter details for Risk Location as per test data with no google address picker "testflow1" and save quote reference number 

@Sprint-3	
Scenario: Enter Risk location details for location 1 and location 2 
	And enter details for Risk Location as per test data with no google address picker "testflow1" and save quote reference number 
