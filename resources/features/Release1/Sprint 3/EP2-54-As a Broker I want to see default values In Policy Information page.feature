Feature: EP2-54-As a Broker I want to see default values In Policy Information page

Background: 
	Given eBusines portal is launched 
	And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
 
 @Sprint-3	
 Scenario: verify fileds (Quote In Progress,Required Field) in Policy information page
 And click I would like an indicative quotation 
 And enter details for Risk Location as per test data with no google address picker "testflow1" and save quote reference number 
 And click on Continue Full Quote
 Then display Quote Number from the Indicative Quote transaction
 And display quote status as "Status: Quote In Progress" 
 And display static text "* Required field"
 
 @Sprint-3	
 Scenario: verify fileds (Quote In Progress,Required Field) in Policy information page
 And click I would like an full quotation
 Then display Quote Number from the Indicative Quote transaction
 And display quote status as "Status: Quote In Progress" 
 And display static text "* Required field"

 
#@Manual
#Scenario: Verify menu alignment of fields in Policy Information
#And left menu highlighted for Policy Information navigation menu option (existing Evolution layout)
#Then display Quote Number (from the Indicative Quote transaction) left aligned to page (existing Evolution layout)
#And display quote status as "Status: Quote In Progress" right aligned to the page (existing Evolution layout)
#And display static text "* Required Field" right aligned (existing Evolution layout)