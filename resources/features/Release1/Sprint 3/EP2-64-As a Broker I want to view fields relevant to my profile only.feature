Feature: EP2-64-As a Broker I want to view fields relevant to my profile only

#Background: 
#	Given eBusines portal is launched 
#    And login eBusiness portal with "testflow1"
#	And click on Add a New Quote 
#	And enter new Quote details as below 
#		|Field         |Value                       |
#		|Client ID     |EVO1                        |
#		|Product       |Chubb MasterPiece Australia |
#		|Insured ID    |EVO1                        |
#		|Start Date    |08 Nov 2017                 |
#		|End Date      |08 Nov 2018                 |
#		|Branch        |Melbourne                   |
#	And click add risk details 
#	And click I would like an indicative quotation 
	
		
@Manual	
Scenario: Verify Policy Address section 
Then static text 'or:' should not be displayed


#@Manual	
#Scenario: Verify Named Insureds section -1
#Then 'Name to be displayed on documents:' field should not be visible
#And the Edit and Delete icons should not be displayed
#

#@Manual	
#Scenario: Verify Named Insureds section -2
#When the Named Insured is replaced
#Then text 'Replaced' should not be displayed
#

#@Manual	 
#Scenario: Verify Broker Details section
#Then button 'Edit Broker Contact Details' should be enabled for selection
#Then button 'Change Broker' should not be displayed
# 
	 
#@Manual	 
#Scenario: Verify Chubb Contact Details section
#Then fields 'Chubb Contact Name:', 'Chubb Contact Email:', 'Chubb Contact Telephone' should not be displayed
#And check box 'Do Not Renew' and 'Manual Renewal' should not be displayed
# 
 
#@Manual	 
#Scenario: verify page footer -1
#Then buttons 'Start a Note' and 'Undo' should not be displayed
#And buttons 'Save and Exit', 'Save Progress' and 'Proceed With Quote' should be displayed
#

#@Manual
#Scenario: verify page footer -2
#Then Chubb tag line "Chubb.Insured.SM" is displayed left aligned to the header block of colour margin 
#And Chubb copyright text "©2017 Chubb Insurance Australia Limited. Chubb®, its logos and Chubb.InsuredSM are protected trademarks of Chubb" should be displayed (Note: R and SM should be superscribed)
#And "Masterpiece Brochure" hyperlink to open pdf brochure document should be displayed (to reuse the link from Landing page)
#And "PDS and Policy Wording" hyperlink to open pdf wording document should be displayed (to reuse the link from Landing page)
#And "Privacy Statement" hyperlink to open pdf privacy document should be displayed (to use the link from Landing page) TBD
#

#@Manual
#Scenario: verify page header -1
#Then buttons 'Generate LAR', 'Generate Appraisal Request Form' and 'lock symbol' should not be displayed
#

#@Manual	
#Scenario: verify page header -2
#And static text 'New rating model' next to the quote number should not be displayed
#And the Breadcrumbs (i.e., page navigation path) above the quote number should not be displayed
#And Client Search field should not be displayed