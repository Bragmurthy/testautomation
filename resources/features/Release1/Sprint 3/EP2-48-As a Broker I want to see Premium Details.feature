Feature: EP2-48-As a Broker I want to see Premium Details.feature

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation 


@Manual
Scenario: Continue to Full quote by entering details in Location 1 and not calculating Premium
	And enter details for Risk Location as per test data with no google address picker "testflow2" and save quote reference number
	And click on Continue Full Quote
