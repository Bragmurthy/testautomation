Feature: EP2-53-As a Broker I want to see Policy Address default

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"

@Sprint-3	
Scenario: Address search verification for Risk Location 1 
	And click add risk details 
	And click I would like an indicative quotation 
	And enter details for Risk Location as per test data with no google address picker "testflow5" and save quote reference number 
	And click on Continue Full Quote
	And verify below details are populated after address search for Risk Location "0" 
		|Field                   |Value                        |
		|Unit No                 |                              |
		|Street No               |42                           |
		|Street Name             |Kambara Drive               |
		|Building Name           |                              |
		|Suburb/City             |Mulgrave                  |
		|Post Code               |3170                        |
		|Country                 |Australia                     |
		|State/Province          |80000002                    |

@Sprint-3	
Scenario: Address search verification for Risk Location 1 
And click add risk details 
And click I would like an full quotation 
And verify all fields are blank in Policy information page for Risk Location "0"
	