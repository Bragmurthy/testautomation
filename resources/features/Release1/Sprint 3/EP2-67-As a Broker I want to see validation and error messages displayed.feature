Feature: EP2-67-As a Broker I want to see validation and error messages displayed

Background: 
	Given eBusines portal is launched 
	And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	
@CI	
@Sprint-3		
 Scenario: Verify the Validation error message as a Broker
 And click I would like an full quotation
 And  click Next in policy information page
 Then verify all madatory fields error messages for risk location "0" 
