Feature: EP2-74-As a Broker I want to see validation and error messages displayed

Background: 
	Given eBusines portal is launched 
	And enter company id as "IAA00371" 
	And enter user id as "EVOTEST" 
	And enter password "123456" 
	And click login 
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation 
	
		
@Manual
Scenario: Verify error message in Edit location in Risk and Coverages
Given Broker clicked 'Save This New Location'
When mandatory fields missing data input
Then highlight all missing mandatory fields (existing Evolution functionality)
And cursor focus to the first missing field from top of the page

#@Sprint-3	
#@Manual
#Scenario: Verify error message in Edit location in Risk and Coverages
#Given Broker clicked 'Save This New Location'
#When all mandatory fields has valid data input
#Then display Location Summary page for the risk location(s) (existing Evolution functionality)
