Feature: EP2-77-As a Broker I want to manage Building Coverage Details

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an full quotation

@CI
@Sprint-3	
Scenario: Continue to Full quote by entering details in Location 1 not calculating Premium
And click on "Risks / Coverages"
And "add" risk location "0" and enter risk information details as per "testflow7" 
Given 'Details for House and Contents Coverage' page is displayed
And  click Next in policy information page
Then validate for missing mandatory fields for Houses and Contents
Given 'Valuable Articles Coverage - Add Valuables Coverage' page is displayed
When click on 'OK' button
Then validate for missing mandatory fields for VAC