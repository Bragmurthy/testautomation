Feature: EP2-69-As a Broker I want to see default values from Indicative Quote

Background: 
	Given eBusines portal is launched 
	And enter company id as "IAA00371" 
	And enter user id as "EVOTEST" 
	And enter password "123456" 
	And click login 
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation 

@CI
@Sprint-3	
Scenario: Verify details from indicative quote are passed to "Risk and Coverages" - Details for location screen "0" for "testflow1"
	And enter details for Risk Location as per test data with no google address picker "testflow4" and save quote reference number 
	And click on Continue Full Quote
	And click on Risks/Coverages navigation
	And Verify details for location "0" as per "testflow1"
	
@Sprint-3		
Scenario: Verify details from indicative quote are passed to "Risk and Coverages" - Details for location screen "0" for "testflow2"
	And enter details for Risk Location as per test data with no google address picker "testflow10" and save quote reference number 
	And click on Continue Full Quote
	And click on Risks/Coverages navigation
	And Verify details for location "0" as per "testflow1"
	
@Sprint-3		
Scenario: Verify details from indicative quote are passed to "Risk and Coverages" - Details for location screen "0" for "testflow3"
	And enter details for Risk Location as per test data with no google address picker "testflow7" and save quote reference number 
	And click on Continue Full Quote
	And click on Risks/Coverages navigation
	And Verify details for location "0" as per "testflow3"		
