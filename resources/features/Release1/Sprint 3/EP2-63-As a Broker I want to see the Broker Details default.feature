Feature: EP2-63-As a Broker I want to see the Broker Details default

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	
@CI	
@Sprint-3		
Scenario: Verify Broker details section in policy information page 1
	And click I would like an indicative quotation 
	And enter details for Risk Location as per test data with no google address picker "testflow1" and save quote reference number 
	And click on Continue Full Quote
	Then Fields 'Broker', 'Address', 'Country' should default as below
	|Field          |Value                                  |
	|Broker         |IAA - Sound Insurance Services PTY LTD |
	|Address        |1/247 Hawthorn Road Caulfield, VIC 3161|
	|Country        |Australia                              |

@Sprint-3			
Scenario: Verify Broker details section in policy information 2
	And click I would like an full quotation
	Then Fields 'Broker', 'Address', 'Country' should default as below
	|Field          |Value                                  |
	|Broker         |IAA - Sound Insurance Services PTY LTD |
	|Address        |1/247 Hawthorn Road Caulfield, VIC 3161|
	|Country        |Australia                              |


	
