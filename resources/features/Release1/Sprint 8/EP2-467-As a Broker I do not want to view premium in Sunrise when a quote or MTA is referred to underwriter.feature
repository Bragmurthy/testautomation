Feature: EP2-467-As a Broker I do not want to view premium in Sunrise when a quote or MTA is referred to underwriter

Background: 
	Given eBusines portal is launched
	And login eBusiness portal with "testflow1"
@Sprint-8	
Scenario: As a Broker I do not want to view premium in Sunrise when a quote or MTA is referred to underwriter

	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"

	And click add risk details
	And click I would like an indicative quotation 
	And enter details for Risk Location as per test data with no google address picker "US_467_SC1" and save quote reference number
	And enter client surname
	And click on "Calculate Premium"
	And "Save" calculated premium
	And click SaveAndExit
	And validate premium details for indicative quote
	
@CI	
@Sprint-8	
Scenario: As a Broker I do not want to view premium in Sunrise when a full quote is referred to underwriter

    And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
    
	And click add risk details 
	And click I would like an full quotation
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "36 briggs street, Mount waverly, 3142 VIC AUS" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact   |Krishna |
    And click Next in policy information page 
    And "add" risk location "0" and enter risk information details as per "US_467_SC2"
    And click "btnProceedNext" by id    
    And enter house and contents details as per "US_467_SC2" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |US_467_SC2           |
    And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_467_SC2"
	
	# EP3-191 changes Adding next 2 times
	
	And click "btnProceedNext" by id
	And click "btnProceedNext" by id
	
	And select "claimsHistoryFlag" as "No"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"

    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    Then verify user is in 'Add New Quote' page
    And click on "btnSave" button in ebusiness portal
    And Save the Our Ref Number in "US_467_SC2"
    And click on "Submit Referral" in ebusiness portal
    And accept the alert
    And approve quote in SIT with quote number saved in "US_467_SC2"
    Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
    And click on "Find quote"
    And search for quote our reference number from "US_467_SC2"
    And click on "View"
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
	And click on Premium Summary
	And save premium details from ebix
	And click "btnSaveExit" by id
	Then verify premium details from ebix