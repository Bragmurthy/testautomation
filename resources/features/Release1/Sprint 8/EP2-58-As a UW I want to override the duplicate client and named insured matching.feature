Feature: EP2-58-As a UW I want to override the duplicate client and named insured matching

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"

	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
@Sprint-8	
	Scenario: As a UW I want to validate view duplicate clients button SC1
	
	And click add risk details 	
	And click I would like an full quotation	
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	
	And search address "12 victoria square adelaide SA 5000 AU" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "US_41_SC1"
	And enter house and contents details as per "US_41_SC1" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |US_41_SC1          |
   	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_41_SC1"
	
	# EP3-191 changes Adding next 2 times
	And click "btnProceedNext" by id
	And click "btnProceedNext" by id
	
	And select "claimsHistoryFlag" as "No"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    And click on "btnSave" button in ebusiness portal
	
	And click on Add a New Quote
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation
	And enter details for Risk Location as per test data "US_58_SC1" and save quote reference number
	And click on "Calculate Premium"
	And click on "OK" button in referal dialogbox
	And click on "btnSave" button in ebusiness portal
	And Save the Our Ref Number in "US_58_SC1"
	Given evolution SIT application is launched
	And select search type as "Policy/Quote #"                                                                                               
	And search quote number "US_58_SC1"
	And validate "View Duplicate Clients" field must be displayed
	And click "btnViewDuplicateClient" by id
	And select possible duplicate client
	And click on "Use Selected Client" button in referal dialogbox
	And release lock for editing
	Given eBusines portal is launched 
	And login eBusiness portal with "testflow1"
	And click on "Find quote"
	And search for quote our reference number from "US_58_SC1"
	And click on "View"
	And click on "btnEdit" button in ebusiness portal
	And click edit risk details
	And verify below details are populated in Risk information for Risk Location "0"
         |Field	               |Value             |
         |Property Ownership   |1                 |
         |Type Of Residence	   |1                 |
         |Year Of Construction |2017              |
         |Property Type	       |1                 |
         |Burglar Alarm	       |2                 |
         |External Construction|2                 |
And verify below details are populated after address search for Risk Location "0" 
   	    |Field         |Value                     |
   	    |Unit No	   |                          |
   	    |Street No	   |9                         |
   	    |Street Name   |Ankuri Road               |
   	    |Building Name |                          |
   	    |Suburb/City   |Tarneit                   |
   	    |Post Code	   |3029                      |
   	    |Country	   |Australia                 |
   	    |State/Province|80000002                  |
And click on "Calculate Premium"
And click on Continue Full Quote
And click on "Update Client" by text
And verify below details are populated in update client
    |Field         |Value                     |
   	    |Unit No	   |                          |
   	    |Street No	   |9                         |
   	    |Street Name   |Ankuri Road               |
   	    |Building Name |                          |
   	    |Suburb/City   |Tarneit                   |
   	    |Post Code	   |3029                      |
   	    |Country	   |Australia                 |
   	    |State/Province|Victoria                  |
@Sprint-8	
Scenario: As a UW I want to override the duplicate client and named insured matching SC1

	And click add risk details 	
	And click I would like an full quotation	
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	
	And search address "12 victoria square adelaide SA 5000 AU" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "US_41_SC1"
	And enter house and contents details as per "US_58_SC1" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |US_41_SC1          |
   	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_58_SC1"
	
	# EP3-191 changes Adding next 2 times
	And click "btnProceedNext" by id
	And click "btnProceedNext" by id
	
	And select "claimsHistoryFlag" as "No"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    And click on "btnSave" button in ebusiness portal
	And Save the Our Ref Number in "US_58_SC1"
	And click on Add a New Quote
    And enter identification details in "testflow5"	
	
	And click add risk details 
	And click I would like an indicative quotation
	And enter details for Risk Location as per test data "US_58_SC1" and save quote reference number
	And click on "Calculate Premium"
	And click on "OK" button in referal dialogbox
	And click on "btnSave" button in ebusiness portal
	And Save the Our Ref Number in "US_58_SC1"
	Given evolution SIT application is launched
	And select search type as "Policy/Quote #"                                                                                               
	And search quote number "US_58_SC1"
	And validate "View Duplicate Clients" field must be displayed
	And click "btnViewDuplicateClient" by id
	And select existing client
	And click on "Use Selected Client" button in referal dialogbox
	And release lock for editing
	Given eBusines portal is launched 
	And login eBusiness portal with "testflow1"
	And click on "Find quote"
	And search for quote our reference number from "US_58_SC1"
	And click on "View"
	And click on "btnEdit" button in ebusiness portal
	And click edit risk details
	And verify below details are populated in Risk information for Risk Location "0"
	         |Field	               |Value             |
	         |Property Ownership   |1                 |
	         |Type Of Residence	   |1                 |
	         |Year Of Construction |2017              |
	         |Property Type	       |1                 |
	         |Burglar Alarm	       |2                 |
	         |External Construction|2                 |
	And verify below details are populated after address search for Risk Location "0" 
	   	    |Field         |Value                     |
	   	    |Unit No	   |                          |
	   	    |Street No	   |9                         |
	   	    |Street Name   |Ankuri Road               |
	   	    |Building Name |                          |
	   	    |Suburb/City   |Tarneit                   |
	   	    |Post Code	   |3029                      |
	   	    |Country	   |Australia                 |
	   	    |State/Province|80000002                  |
	And click on "Calculate Premium"
	And click on Continue Full Quote
	And click on "Update Client"
	And verify below details are populated in update client
    	|Field         |Value                     |
   	    |Unit No	   |                          |
   	    |Street No	   |9                         |
   	    |Street Name   |Ankuri Road               |
   	    |Building Name |                          |
   	    |Suburb/City   |Tarneit                   |
   	    |Post Code	   |3029                      |
   	    |Country	   |Australia                 |
   	    |State/Province|Victoria                  |
  	

 @Sprint-8	
	Scenario: As a UW I want to validate view update client button

	And click add risk details 	
	And click I would like an full quotation	
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	
	And search address "12 victoria square adelaide SA 5000 AU" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "US_58_SC1"
	And enter house and contents details as per "US_58_SC1" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |US_41_SC1          |
   	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_41_SC1"
	
	# EP3-191 changes Adding next 2 times
	And click "btnProceedNext" by id
	And click "btnProceedNext" by id
	
	And select "claimsHistoryFlag" as "No"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    And click on "btnSave" button in ebusiness portal
	And Save the Our Ref Number in "US_58_SC3"
	And click on Add a New Quote
    And enter identification details in "testflow5"
	
	And click add risk details 
	And click I would like an full quotation
	And Save Quote Reference Number in "US_58_SC3"
    And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And click on "OK" button in referal dialogbox
	And click on "btnSave" button in ebusiness portal
    And Save the Our Ref Number in "US_58_SC3"
   Given evolution SIT application is launched
	And select search type as "Policy/Quote #"                                                                                               
	And search quote number "US_58_SC3"
	And click "btnViewDuplicateClient" by id
	And select possible duplicate client
	And click on "Use Selected Client" button in referal dialogbox
	And release lock for editing
	Given eBusines portal is launched 
	And login eBusiness portal with "testflow1"
	And click on "Find quote"
	And search for quote our reference number from "US_58_SC3"
	And click on "View"
	And click on "btnEdit" button in ebusiness portal
	And click edit risk details
	
	And click on "Update Client" by text
	And verify below details are populated in update client
    |Field         |Value                     |
   	    |Unit No	   |                          |
   	    |Street No	   |36                         |
   	    |Street Name   |Briggs Street               |
   	    |Building Name |                          |
   	    |Suburb/City   |Mount Waverly                   |
   	    |Post Code	   |3142                      |
   	    |Country	   |Australia                 |
   	    |State/Province|Victoria                  |	
   	    

	@Sprint-8	
	Scenario: As a UW I want to validate view updated client details
	
	And click add risk details 	
	And click I would like an full quotation	
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	
	And search address "12 victoria square adelaide SA 5000 AU" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "US_41_SC1"
	And enter house and contents details as per "US_41_SC1" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |US_41_SC1          |
   	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_41_SC1"
	
	# EP3-191 changes Adding next 2 times
	And click "btnProceedNext" by id
	And click "btnProceedNext" by id
	
	And select "claimsHistoryFlag" as "No"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    And click on "btnSave" button in ebusiness portal
	And Save the Our Ref Number in "US_58_SC4"
	And click on Add a New Quote
    And enter identification details in "testflow5"
	
	And click add risk details 
	And click I would like an full quotation
	And Save Quote  Reference Number in "US_58_SC4"
    And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And click on "OK" button in referal dialogbox
	And click on "btnSave" button in ebusiness portal
    And Save the Our Ref Number in "US_58_SC4"
    Given evolution SIT application is launched
	And select search type as "Policy/Quote #"                                                                                               
	And search quote number "US_58_SC4"
	And click "btnViewDuplicateClient" by id


	And select existing client
	And click on "Use Selected Client" button in referal dialogbox
	And release lock for editing
	Given eBusines portal is launched 
	And login eBusiness portal with "testflow1"
	And click on "Find quote"
	And search for quote our reference number from "US_58_SC4"
	And click on "View"
	And click on "btnEdit" button in ebusiness portal
	And click edit risk details
  	And click on "Update Client" by text
	And verify below details are populated in update client
    |Field         |Value                     |
   	    |Unit No	   |                          |
   	    |Street No	   |9                         |
   	    |Street Name   |Ankuri Road               |
   	    |Building Name |                          |
   	    |Suburb/City   |Tarneit                   |
   	    |Post Code	   |3029                      |
   	    |Country	   |Australia                 |
   	    |State/Province|Victoria                  |	
 	    