Feature: EP2-41-As a Chubb Product Owner I want to validate indicative quote data entered by the Broker


Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
#
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
@Sprint-8    
Scenario: As a Broker I want to see duplicate client validation message in indicative quote page
	
	And click add risk details 	
	And click I would like an full quotation	
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	
	And search address "12 victoria square adelaide SA 5000 AU" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "US_41_SC1"
	And enter house and contents details as per "US_41_SC1" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |US_41_SC1          |
   	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_41_SC1"
	
	# EP3-191 changes Adding next 2 times
	And click "btnProceedNext" by id
	And click "btnProceedNext" by id
	
	And select "claimsHistoryFlag" as "No"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    And click on "btnSave" button in ebusiness portal
    
    And click on Add a New Quote
    And enter identification details in "testflow5"	
	And click add risk details 
    And click I would like an indicative quotation
	And enter details for Risk Location as per test data with no google address picker "US_41_SC1" and save quote reference number
	And click on "Calculate Premium"
	And verify Quote Validation pop up is dipslayed
	And verify Quote Validation message is displayed as "Our system indicates that we have an existing record for this client/property."
#    And verify Quote Validation message is displayed as "Please contact your Masterpiece underwriter to discuss. Your quote reference number is "
    And click on "OK" button in referal dialogbox
	Given evolution SIT application is launched
    And select search type as "Policy/Quote #"
	And search saved indicative quote number "US_41_SC1"
    And click "btnViewDuplicateClient" by id
	And select possible duplicate client
	And click on "Use Selected Client" button in referal dialogbox
	And release lock for editing
	
	Given eBusines portal is launched
	And login eBusiness portal with "testflow1"
	And click on "Find quote"
	And search for quote reference number from "US_41_SC1"
	And click on "View"
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
	And click on "Calculate Premium"
	And verify Premium Table is displayed
@Sprint-8
Scenario: As a Broker I want to see claims history validation message in indicative quote page

	And click add risk details 	
	And click I would like an full quotation	
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	
	And search address "12 victoria square adelaide SA 5000 AU" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "US_41_SC2"
	And enter house and contents details as per "US_41_SC2" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |US_41_SC2          |
   	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_41_SC2"
	
	# EP3-191 changes Adding next 2 times
	And click "btnProceedNext" by id
	And click "btnProceedNext" by id
	
	And select "claimsHistoryFlag" as "No"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    And click on "btnSave" button in ebusiness portal

	And click on Add a New Quote
    And enter identification details in "testflow5"
    
	And click add risk details 
    And click I would like an indicative quotation
	And search address "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
	And enter details for Risk Location as per test data "US_41_SC2" and save quote reference number
	And click on "Calculate Premium"
	And verify Quote Validation pop up is dipslayed
	And verify claims warning is displayed
	And verify Quote Validation message is displayed as "Please contact your Masterpiece underwriter to discuss. Your quote reference number is "
	And click on OK button of client Already exists popup
	And verify user is in 'Add New Quote' page
@Sprint-8
Scenario: As a Broker I want to see decline postcode validation message in indicative quote page
	
	And click add risk details 	
	And click I would like an full quotation	
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	
	And search address "12 victoria square adelaide SA 5000 AU" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "US_41_SC3"
	And enter house and contents details as per "US_41_SC3" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |US_41_SC3         |
   	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_41_SC3"
	
	# EP3-191 changes Adding next 2 times
	And click "btnProceedNext" by id
	And click "btnProceedNext" by id
	
	And select "claimsHistoryFlag" as "No"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    And click on "btnSave" button in ebusiness portal

	And click on Add a New Quote
    And enter identification details in "testflow5"

	And click add risk details 
    And click I would like an indicative quotation
	And search address "8 Main St, Augathella, Queensland, Australia" for Risk Location "0"
	And enter details for Risk Location as per test data "US_41_SC3" and save quote reference number

	And click on "Calculate Premium"
	And verify Quote Validation pop up is dipslayed
	And Decline Postcode warning is displayed
	And click on OK button of client Already exists popup
	And verify user is in 'Add New Quote' page

	