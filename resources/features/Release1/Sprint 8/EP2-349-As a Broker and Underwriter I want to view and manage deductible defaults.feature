Feature: EP2-349-As a Broker and Underwriter I want to view and manage deductible defaults

Background: 
	Given eBusines portal is launched
	And login eBusiness portal with "testflow1"		

	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
    And click add risk details
	
@Sprint-8	
Scenario: As a Broker and Underwriter I want to validate deductible defaults when content sum insured is greater than or equal to 2000000
	
	And click I would like an indicative quotation
	And select residence type only as per test data "US_349_SC1"
	And enter building sum insured from "US_349_SC1"
	And verify content sum insured based on building sum insured from "US_349_SC1"
@Sprint-8	
Scenario: As a Broker and Underwriter I want to validate deductible defaults  when content sum insured is les than or equal to 2000000
	
	And click I would like an indicative quotation
	And select residence type only as per test data "US_349_SC2"
	And enter building sum insured from "US_349_SC2"
	And verify content sum insured based on building sum insured from "US_349_SC2"
@Sprint-8	
Scenario: As a Broker and Underwriter I want to validate deductable defaults for more than one risk location
	
	And click I would like an indicative quotation	
	And select residence type only as per test data "US_349_SC3"
	And enter building sum insured from "US_349_SC3"	
	And verify content sum insured based on building sum insured from "US_349_SC3"
	
@CI	
@Sprint-8	
Scenario: As a Broker and Underwriter I want to validate deductible defaults when content sum insured is greater than or equal to 2000000 full quote
		
	And click I would like an full quotation
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "12 victoria street richmond vic australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page
    And "add" risk location "0" and enter risk information details as per "US_349_SC4"
    And click "btnProceedNext" by id
    And enter building sum insured from "US_349_SC4" for full quote
    And verify content sum insured based on building sum insured from "US_349_SC4" for full quote
@Sprint-8   
 Scenario: As a Broker and Underwriter I want to validate deductible defaults when content sum insured is less than 2000000 full quote
		
	And click I would like an full quotation
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "12 victoria street richmond vic australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page
    And "add" risk location "0" and enter risk information details as per "US_349_SC5"
    And click "btnProceedNext" by id
    And enter building sum insured from "US_349_SC5" for full quote
    And verify content sum insured based on building sum insured from "US_349_SC5" for full quote  
  
 @Sprint-8   
Scenario: As a Broker and Underwriter I want to validate deductible defaults for more than one risk location for full quote
		
	And click I would like an full quotation
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address "31 NewSouth HeadRd Vaucluse" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page
    And "add" risk location "0" and enter risk information details as per "US_349_SC6"
    And "add" risk location "0" and enter risk information details as per "US_349_SC6_1"
    And enter building sum insured when 2 locations as "US_349_SC6" and "US_349_SC6_1" for full quote
    And verify content sum insured based on building sum insured when 2 locations as "US_349_SC6" and "US_349_SC6_1" for full quote
    
    