Feature: EP2-360-As a Broker I want to add my contact details when obtaining a full quote

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	
	And click add risk details  
  	And click I would like an full quotation
	
@Sprint-8
Scenario: As a Broker I want to see the read only and enabeld fields 
  	And click "btnAddProducer" by id
	Then "Add Broker Contact" field must be displayed
	Then "addProducer_Branch" id field must be read only
#	Then "evo-selectize-option" class field must be read only
	Then "selectize-control single" class field must not read only
	Then "OK" field must not read only
	Then "Cancel" field must not read only
	Then "Add New Broker Contact" field must not read only
	
@Sprint-8
Scenario: As a Broker I want to check the mandatory fields from light box 
	And click "btnAddProducer" by id
	And click on "OK" button from popup
	Then validate broker contact required message from Add broker contact

@Sprint-8
Scenario: As a Broker I want to check the mandatory fields of the add new broker contact light box  

	And click "btnAddProducer" by id
	And click on "Add New Broker Contact" button from popup
	Then "First Name:" field must be mandatory
	Then "Last Name:" field must be mandatory
	Then "Email Address:" field must be mandatory
	
	Then "Office Telephone Number:" field must be optional
	Then "Mobile Telephone Number: " field must be optional
	Then validate "OK" field must be displayed
	Then validate "Cancel" button must be displayed
@Sprint-8
Scenario: As a Broker I want to add new broker1

	And add broker as per "testflow1" 
    And "add" broker contact as per "testflow1"
    And verify broker details as per "testflow1"
    And "Edit Broker Contact Details" field must be displayed
@Sprint-8    
Scenario: As a Broker I want to add new broker2
	And add broker as per "testflow1" 
    And "add" broker contact in popup as per "testflow1"
	Then verify broker contact in Add broker contact popup as per "testflow1"
@Sprint-8	
Scenario: As a Broker I want to add new broker3
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
	And click on "Edit Broker Contact Details"
	Then "First Name:" field must be mandatory
	Then "Last Name:" field must be mandatory
	Then "Email Address:" field must be mandatory
	
	Then "Office Telephone Number:" field must be optional
	Then "Mobile Telephone Number: " field must be optional
	Then validate "OK" field must be displayed
	Then validate "Cancel" button must be displayed