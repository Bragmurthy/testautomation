Feature: EP2-340-As a Broker I do not want to view Justification field for VAC categories

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
    
    
	And click add risk details 
  	And click I would like an full quotation
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
    
@CI    
@Sprint-8   
Scenario: As a Broker I do not want to view Justification field for VAC categories  1
	And "add" risk location "0" and enter risk information details as per "US_340_SC1"
	And click "btnProceedNext" by id
	And select category from "US_340_SC1" and "1" VAC for that location "0"
	And validate no Justification dropdown

@Sprint-8	
Scenario: As a Broker I do not want to view Justification field for VAC categories 2 
	And "add" risk location "0" and enter risk information details as per "US_340_SC2"
	And click "btnProceedNext" by id
	And select category from "US_340_SC2" and "1" VAC for that location "0"
	And validate no Justification dropdown

@Sprint-8	
Scenario: As a Broker I do not want to view Justification field for VAC categories3  
	And "add" risk location "0" and enter risk information details as per "US_340_SC3"
	And click "btnProceedNext" by id
	And select category from "US_340_SC3" and "1" VAC for that location "0"
	And validate no Justification dropdown
	
@Sprint-8
Scenario: As a Broker I do not want to validate no location id is displayed
	And "add" risk location "0" and enter risk information details as per "US_340_SC4"
	And validate no location id
	