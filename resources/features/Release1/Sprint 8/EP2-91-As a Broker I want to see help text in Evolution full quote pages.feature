Feature: EP2-91-As a Broker I want to see help text in Evolution full quote pages

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
    
	And click add risk details 
  	And click I would like an full quotation
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "testflow7"
	          
 @Sprint-8   
 Scenario: As a Broker I want to see help text in Evolution full quote pages
 And click on "House and Contents"
 And verify help text for House and contents fields for location "0"
 And click "btnProceedNext" by id
 And verify help text for VAC fields for location "0"