Feature: As a Sunrise Broker I want to access the Evolution Application 5

@Manual
Scenario: Verify Sample test flow 5 
    Given evolution application is launched 
    And select search type as "Client" 
    And enter search value "Krishna Nandagopal" 
    And select search result "Krishna Nandagopal, 42 Kambara Drive Mulgrave Victoria Australia 3170" 
    And click Get a Quote 
    And select current insurer "Bank Insurance" 
    And add broker as per "testflow1" 
    And "add" broker contact as per "testflow1" 
    And click proceed with Quote in policy information page 
    And add risk location and enter risk information details as per "testflow7" 
    And enter house and contents details as per "testflow1" and "1" VAC for that location "0" as per below data 
        |Value Added Coverage |
        |testflow4            |
    And click proceed with Quote in risk and coverages page 
    And Add "2" Interested Party for location "0" as per below data 
        |Interested Party |
        |testflow1        |
        |testflow2        |
    And click proceed with Quote in interested parties page 
    And enter details in liability page as per "testflow1" 
    And click proceed with Quote in liability page 
    And enter details in loss History page as per "testflow1" 
    And click proceed with Quote in loss Histroy page 
    And retrive values from rating summary page 
    And modify rating summary details for location "1" based on "testflow1" 
    And click proceed with Quote in rating summary page 
    And enter appraisal details for location "1" based on "testflow2" 
    And click proceed with Quote in appraisals page 
    And enter subjectivities details for location "1" based on "testflow1" 
    And click proceed with Quote in subjectivities and clauses page 
