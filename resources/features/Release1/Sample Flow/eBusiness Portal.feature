Feature: As a Sunrise Broker I want to access the Evolution Application 

Scenario: Verify Connectivity between eBusiness Portal and Evolution
    Given eBusines portal is launched 
    And enter company id as "INSTST" 
    And enter user id as "JANE" 
    And enter password "jane" 
    And click login 
    And click on Add a New Quote 
    And enter new Quote details as below 
        |Field         |Value                |
        |Client ID     |KRI5                 |
        |Product       |Chubb Masterpiece Australia|
        |Insured ID    |KRI5                 |
        |Start Date    |06 Sep 2017          |
        |End Date      |06 Sep 2018          |
        |Branch        |Melbourne            |
    And click add risk details 
    And verify landing URL is "http://apevodweb121.aceins.com:9999/" 
