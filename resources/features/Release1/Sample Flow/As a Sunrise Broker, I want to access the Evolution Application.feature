Feature: As a Sunrise Broker, I want to access the Evolution Application 

	Acceptance Criteria:
1. Broker should have unique Chubb 'Fxxxxxx' ID to access evolution via single sign ON
2. Display - System error message - "Invalid Evolution user" if  'Fxxxxxx' ID is missing
3. Display - System error message - if valid  'Fxxxxxx' ID is expired
4. Navigate back to Sunrise if 2 and 3 occurs
5. Display Policy information page - when authorised successfully


@Access_Evolution 
@Sprint1 
Scenario: verify Broker with valid Chubb ID is able to access evolution via single sign ON 
	Given eBusinessPortal application is launched 
	And enter user id as "JANE" 
	And enter password as "12345" 
	And click on login button 
	And Wait for page to load 
	And click on "New Business" link 
	And enter details as follows 
		|FIELD              |VALUE            |
		|CLientID           |KR15             |
		|InsuredID          |KR16             |
		|Start Date         |17 Aug 2017      |
		|End Date           |17 Aug 2018      |
	And select Product as "Chubb Masterpiece Australia" 
	And select branch as "Sydney" 
	When Broker clicks on Add Risk Details Button 
	Then Verify Broker is navigated to Policy Information page in Evolution 
	
	@Access_Evolution 
@Sprint1 
Scenario: verify Broker with invalid Chubb ID trying to access evolution via single sign ON is displayed with error message "Invalid Evolution user"
	Given eBusinessPortal application is launched 
	And enter user id as "MARK" 
	And enter password as "12345" 
	And click on login button 
	And Wait for page to load 
	And click on "New Business" link 
	And enter details as follows 
		|FIELD              |VALUE            |
		|CLientID           |KR15             |
		|InsuredID          |KR15             |
		|Start Date         |17 Aug 2017      |
		|End Date           |17 Aug 2018      |
	And select Product as "Chubb Masterpiece Australia" 
	And select branch as "Sydney" 
	When Broker clicks on Add Risk Details Button 
	Then verify error message "Invalid Evolution user" is displayed
	Then verify Broker is navigated to eBusinessPortal application 
	
	
@Access_Evolution 
@Sprint1 
Scenario: verify Broker with valid Chubb ID but expired trying to access evolution via single sign ON is displayed with error message "Invalid Evolution user"
	Given eBusinessPortal application is launched 
	And enter user id as "MARY" 
	And enter password as "12345" 
	And click on login button 
	And Wait for page to load 
	And click on "New Business" link 
	And enter details as follows 
		|FIELD              |VALUE            |
		|CLientID           |KR15             |
		|InsuredID          |KR15             |
		|Start Date         |17 Aug 2017      |
		|End Date           |17 Aug 2018      |
	And select Product as "Chubb Masterpiece Australia" 
	And select branch as "Sydney" 
	When Broker clicks on Add Risk Details Button 
	Then verify error message "Invalid Evolution user" is displayed
	Then verify Broker is navigated to eBusinessPortal application 
	
	
    