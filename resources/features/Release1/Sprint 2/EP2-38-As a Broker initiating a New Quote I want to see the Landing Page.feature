Feature: EP2-38-As a Broker initiating a New Quote I want to see the Landing Page 

Background: 
	Given eBusines portal is launched 
	And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
		
@Sprint-2
Scenario: Verify indicative quote button function in landing page 
	And click I would like an indicative quotation 
	And verify landing URL is "http://apregevwebqa121.aceins.com/IndicativeQuote/Edit/" 
	

@Sprint-2	
Scenario: Verify full quote button function in landing page 
	And click I would like an full quotation 
	And verify landing URL is "http://apregevwebqa121.aceins.com/policy-info/edit?"	

#@Manual
	
#Scenario: Verify footer link "Terms of Service"
#    And  verify footer link "Terms of Service" re-directs to "TBD" 


#@Manual	    
#Scenario: Verify footer link "PDS and Policy Wording"
#	And  verify footer link "PDS and Policy Wording" re-directs to "https://www2.chubb.com/AU-EN/_Assets/documents/Chubb17-29-1116-Masterpiece-Policy-Wording-Product-Disclosure-Statement_20161026_02A_Singles.pdf" 


#@Manual	
#Scenario: Verify footer link "Masterpiece Brochure"
#    And verify footer link "Masterpiece Brochure" re-directs to "https://www2.chubb.com/AU-EN/_Assets/documents/Chubb17-30-1116-Masterpiece-Brochure.pdf" 
#

#@Manual
#Scenario: Verify CHUBB Logo in the landing Page 
#	When Chubb Logo is verified 
#	Then the Chubb Logo should be left aligned within the block of colour 


#@Manual 
#Scenario: Verify MasterPiece title in the landing Page 
#	When title text Masterpiece is verified 
#	Then the text 'Masterpiece' should be located next to the Chubb logo with space (refer mock up) 
#	And the text base aligned to the Chubb logo base 	