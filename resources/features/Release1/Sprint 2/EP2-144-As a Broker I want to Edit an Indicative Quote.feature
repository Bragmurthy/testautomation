Feature: EP2-144 As a Sunrise Broker I want to access the Evolution Application 

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
	
@Sprint-2
@CI
Scenario: Verify Risk Information are retained in indicative quote page after during "Edit"
	And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation
	And enter details for Risk Location as per test data with no google address picker "US_144_SC1" and save quote reference number
	And click on "Save & Exit"
    And click on "btnSave" button in ebusiness portal
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
    And verify below details are populated in Risk information for Risk Location "0"
         |Field	               |Value             |
         |Property Ownership   |1                 |
         |Type Of Residence	   |1                 |
         |Year Of Construction |2017              |
         |Property Type	       |1                 |
         |Burglar Alarm	       |2                 |
         |External Construction|2                 |
    And verify below details are populated in Risk information for Risk Location "1"
         |Field	               |Value             |
         |Property Ownership   |1                 |
         |Type Of Residence	   |1                 |
         |Year Of Construction |2017              |
         |Property Type	       |1                 |
         |Burglar Alarm	       |2                 |
         |External Construction|2                 |
         
@Sprint-2	
Scenario: Verify Address Details are is retained in indicative quote page after during "Edit"
	And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation
	#And enter details for Risk Location as per test data "US_144_SC2" and save quote reference number
	And enter details for Risk Location as per test data with no google address picker "US_144_SC2" and save quote reference number
	And click on "Save & Exit"
    And click on "btnSave" button in ebusiness portal
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
   	And verify below details are populated after address search for Risk Location "0" 
   	    |Field         |Value                     |
   	    |Unit No	   |                          |
   	    |Street No	   |45                        |
   	    |Street Name   |Ferozpuru Road            |
   	    |Building Name |                          |
   	    |Suburb/City   |Ludhianaaa                |
   	    |Post Code	   |1422                      |
   	    |Country	   |Australia                 |
   	    |State/Province|80000002                  |
   	And verify below details are populated after address search for Risk Location "1" 
   	    |Field         |Value                     |
   	    |Unit No	   |                          |
   	    |Street No	   |45                        |
   	    |Street Name   |Ferozpuru Road            |
   	    |Building Name |                          |
   	    |Suburb/City   |Ludhianaaa                |
   	    |Post Code	   |1422                      |
   	    |Country	   |Australia                 |
   	    |State/Province|80000002                  |
 
 @Sprint-2
 @CI  
 Scenario: Verify Coverage Information are retained in indicative quote page after during "Edit"
	And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation
	#And enter details for Risk Location as per test data "US_144_SC3" and save quote reference number
	And enter details for Risk Location as per test data with no google address picker "US_144_SC3" and save quote reference number
	And click on "Save & Exit"
    And click on "btnSave" button in ebusiness portal
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
    And verify below details are populated in Coverage information for Risk Location "0" for property type "House"
        |Field	                | Value                        |
        |Building Sum Insured	|100,000                       |
        |Content Sum Insured	|50,000                        |
        |Deductible	            |500                           |
    And verify below details are populated in Coverage information for Risk Location "1" for property type "House"
        |Field	                | Value                        |
        |Building Sum Insured	|100,000                       |
        |Content Sum Insured	|50,000                        |
        |Deductible	            |500                           |   
 
 @Sprint-2  
 @CI                                 
 Scenario: Verify calculated premium is retained in indicative quote page after during "Edit"
	And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation
	#And enter details for Risk Location as per test data with no google address picker "US_144_SC4" and save quote reference number
	And enter details for Risk Location as per test data with no google address picker "US_144_SC4" and save quote reference number
	And enter client surname  
	And click on "Calculate Premium"
	And "save" calculated premium
	#And "verify" calculated premium
	
	                        