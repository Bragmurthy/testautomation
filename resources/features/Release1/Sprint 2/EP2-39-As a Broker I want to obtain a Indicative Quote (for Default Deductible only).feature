Feature: EP2-39-As a Broker I want to obtain a Indicative Quote (for Default Deductible only)

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation 
	
@Sprint-2	
Scenario: Verify firstname default value in the indicative quote page
	And verify default firstname from sunrise is "SPRINT"

@Sprint-2	
Scenario: Verify Surname default value in the indicative quote page
	And verify default Surname from sunrise is "TWO"
	
@Sprint-2	
Scenario: Verify "effective date" and "expiry date" default value in the indicative quote page
	And verify effective date default value is "23/03/2018" and expiry date default value is "23/03/2019"	

@CI
@Sprint-2	
Scenario: Address search verification for Risk Location 1
And enter details for Risk Location as per test data with no google address picker "US_144_SC1" and save quote reference number
And verify below details are populated after address search for Risk Location "0"
		|Field         |Value                     |
   	    |Unit No	   |                          |
   	    |Street No	   |45                        |
   	    |Street Name   |Ferozpuru Road            |
   	    |Building Name |                          |
   	    |Suburb/City   |Ludhianaaa                |
   	    |Post Code	   |1422                      |
   	    |Country	   |1			              |
   	    |State/Province|80000002                  |
	

#@Manual	
#Scenario: Verify footer link "Masterpiece Brochure"
#	And verify footer link "Masterpiece Brochure" re-directs to "https://www2.chubb.com/AU-EN/_Assets/documents/Chubb17-30-1116-Masterpiece-Brochure.pdf" 
#

#@Manual	
#Scenario: Verify footer link "PDS and Policy Wording"
#	And  verify footer link "PDS and Policy Wording" re-directs to "https://www2.chubb.com/AU-EN/_Assets/documents/Chubb17-29-1116-Masterpiece-Policy-Wording-Product-Disclosure-Statement_20161026_02A_Singles.pdf" 
#

#@Manual
#Scenario: Verify footer link "Terms of Service"
#    And  verify footer link "Terms of Service" re-directs to "TBD" 
#
 
#@Manual	
#Scenario: Verify CHUBB logo in indicative quote page
#When Chubb Logo is verified
#Then the Chubb Logo must be left aligned within the block of colour
#

#@Manual	
#Scenario: Verify "Masterpiece" title in indicative quote page
#Then the text 'Masterpiece' must be located next to the Chubb logo with space (refer mock up)
#And the text base aligned to the Chubb logo base
#

	
		
	
	
	
	
	
	
	
                                        	