Feature: As a Sunrise Broker I want to access the Evolution Application 


@Manual	
Scenario: 	Verify changes to Indicative quote screen
Given eBusines portal is launched 
And login eBusiness portal with "testflow1"
And Use Chubb Publico font in the Landing Page
And Chubb Logo size to be consistent across Evolution screens i.e., to use 250px (If enlarging the .png file compromises the logo resolution please use .eps format which is attached to EP2-135 story)
And Chubb Logo colour to be Dark Blue for all pages where the background is White. If the background is Dark Blue use White colour logo.
And Use image for Chubb tag line Chubb.Insured.SM (Marketing image attached in this story)
And Copy right text in page footer : "©2017 Chubb Insurance Australia Limited. Chubb^®, its logos and Chubb.Insured.^SM are protected trademarks of Chubb." (Note: the 'Registered' and 'SM' should be superscribed and size smaller than the normal text)
And Use ‘Short Button’ pattern from the branding guide. All broker facing buttons should be of same size i.e., length and height
And All fonts within the application to be in Black colour except the text on dark background which should be in white
Then If there is button as part of the sub-heading banner then break the banner colour and place the button at the end with a space between the banner and buttons (please see mock up in EP2-39 for sample - Risk Location section
	                    