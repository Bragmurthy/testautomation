Feature: EP2-44-As a Broker I want to manage location information

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation 


@Sprint-2	
@CI
Scenario: Address search verification for Risk Location 1 
	And enter details for Risk Location as per test data with no google address picker "US_144_SC1" and save quote reference number
	And verify text "Risk Location No.1 (Primary Residence)" for location "0"
	And verify below details are populated after address search for Risk Location "0"
		|Field         |Value                     |
   	    |Unit No	   |                          |
   	    |Street No	   |45                        |
   	    |Street Name   |Ferozpuru Road            |
   	    |Building Name |                          |
   	    |Suburb/City   |Ludhianaaa                |
   	    |Post Code	   |1422                      |
   	    |Country	   |1			              |
   	    |State/Province|80000002                  |

@Sprint-2
@CI	
Scenario: Address search verification for Risk Location 2 
	And enter details for Risk Location as per test data with no google address picker "US_144_SC1" and save quote reference number
	And verify text "Risk Location No.2" for location "1"
	And verify below details are populated after address search for Risk Location "1"
		|Field         |Value                     |
   	    |Unit No	   |                          |
   	    |Street No	   |45                        |
   	    |Street Name   |Ferozpuru Road            |
   	    |Building Name |                          |
   	    |Suburb/City   |Ludhianaaa                |
   	    |Post Code	   |1422                      |
   	    |Country	   |1			              |
   	    |State/Province|80000002                  |
		
#@Manual
#And background colour R21 G15 B150 (Hex: #150f96) across the page (refer mock up)		
		