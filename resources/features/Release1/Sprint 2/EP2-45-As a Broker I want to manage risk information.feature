Feature: EP2-45-As a Broker I want to manage risk information 

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation 
	
@CI	
@Sprint-2
Scenario: Verify default value in "Property Ownership" dropdown for Risk Location 1 
	And verify default value in "Property Ownership" dropdown for Risk Location "0" 
@CI	
@Sprint-2	
Scenario: Verify default value in "Type of Residence" dropdown for Risk Location 1 
	And verify default value in "Type of Residence" dropdown for Risk Location "0" 
@CI	
@Sprint-2	
Scenario: Verify default value in "Property Type" dropdown for Risk Location 1 
	And verify default value in "Property Type" dropdown for Risk Location "0" 
@CI	
@Sprint-2	
Scenario: Verify default value in "Burglar Alarm" dropdown for Risk Location 1 
	And verify default value in "Burglar Alarm" dropdown for Risk Location "0" 
@CI	
@Sprint-2	
Scenario: Verify default value in "External Construction" dropdown for Risk Location 1 
	And verify default value in "External Construction" dropdown for Risk Location "0" 
	
@CI	
@Sprint-2	
Scenario: Verify Other Description box is displayed for Risk Location 1 
	And verify Other Description box is displayed when Type of Residence is other for Risk Location "0" 	

@Sprint-2	
Scenario: Verify default value in "Property Ownership" dropdown for Risk Location 2 
And verify default value in "Property Ownership" dropdown for Risk Location "1" 

@Sprint-2	
Scenario:  Verify default value in "Type of Residence" dropdown for Risk Location 2 
	And verify default value in "Type of Residence" dropdown for Risk Location "1" 

@Sprint-2	
Scenario: Verify default value in "Property Type" dropdown for Risk Location 2 
	And verify default value in "Property Type" dropdown for Risk Location "1" 

@Sprint-2	
Scenario: Verify default value in "Burglar Alarm" dropdown for Risk Location 2 
	And verify default value in "Burglar Alarm" dropdown for Risk Location "1" 
	
@Sprint-2	
Scenario: Verify default value in "External Construction" dropdown for Risk Location 2 
	And verify default value in "External Construction" dropdown for Risk Location "1" 


@Sprint-2	
Scenario: Verify Other Description box is displayed for Risk Location 2 
	And verify Other Description box is displayed when Type of Residence is other for Risk Location "1" 
	
		
	
	
	
	
	
	
	
	