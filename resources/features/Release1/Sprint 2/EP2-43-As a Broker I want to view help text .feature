Feature: As a Sunrise Broker I want to access the Evolution Application 


#Background: 
#	Given eBusines portal is launched 
#   And login eBusiness portal with "testflow1"
# 	And click on Add a New Quote 
#	And enter new Quote details as below 
#		|Field           |Value                                   |
#		|Client ID     |EVO1                                    |
#		|Product       |Chubb MasterPiece Australia |
#		|Insured ID   |EVO1                                    |
#		|Start Date    |08 Nov 2017                       |
#		|End Date     |08 Nov 2018                         |
#		|Branch         |Melbourne                            |
#	And click add risk details 
#	And click I would like an indicative quotation 

	
@Manual 
Scenario: verify help text icon 
	When Help Text icon is verified 
	Then "Building Sum Insured", "Contents Sum Insured", "Building and Contents Sum Insured", "Valuables Category", "Valuables Sum Insured", "Deductible" labels should display the Help Text icon  next to the label 


@Manual 
Scenario: Verify Building Sum Insured help text
	When "Building Sum Insured" help text icon is selected 
	Then following text "Please note that our minimum building sum insured is $500,000 for owner occupied homes" should be displayed 
	And the display of the text should be between the label and the field i.e., field will dynamically align its position to move below 


@Manual 
Scenario: Verify Content Sum Insured help text
	When "Contents Sum Insured" help text icon is selected 
	Then following text "Please note that minimum contents sums insured apply" should be displayed 


@Manual 
Scenario: Verify Building and Contents Sum Insured help text
	When "Building and Contents Sum Insured" help text icon is selected 
	Then following text "Please note that minimum deductibles apply depending on the sums insured and claims history" should be displayed 


@Manual 
Scenario: Verify Valuables Category help text
	When "Valuables Category" help text icon is selected 
	Then following text "Valuable Articles Coverage is in addition to your Contents Coverage" should be displayed 
	And text "Please note that Valuable Articles Coverage can only be selected in conjunction with Contents coverage" should be displayed 


@Manual 
Scenario: Verify VAC Total Sum Insured help text
	When "VAC Total Sum Insured" help text icon is selected 
	Then following text "Please enter the sum insured required for each category of Valuable Articles Coverage" should be displayed 


@Manual 
Scenario: Verify Number of Claims (for last 3 years)help text
	When "Number of Claims (for last 3 years)" help text icon is selected 
	Then following text "Please note that during full quote stage we will review the claims history over a longer period" should be displayed