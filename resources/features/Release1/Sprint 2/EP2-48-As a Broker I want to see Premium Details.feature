Feature: As a Sunrise Broker I want to access the Evolution Application 




#Background: 

#Given broker has clicked 'Calculate Premium' button
#When mandatory fields are validated
#Then highlight all fields with missing information (use existing Evolution behavior of highlighting fields)
#And cursor focus to the first editable missing information field in the page


@Manual
Scenario: 1
When calculated premium is displayed for deductible $500 for Risk Location No. 1 or 2
Then Total Base Premium for the risk location(s) should be displayed under "Deductible $500" column within the table 
And Total Charges for the risk location(s) should be displayed under "Deductible $500" column within the table 
And Total Premium for the risk location(s) should be displayed under "Deductible $500" column within the table 


@Manual
Scenario: 2
When calculated premium is displayed for deductible $1,000 for Risk Location No. 1 or 2
Then Total Base Premium for the risk location(s) should be displayed under "Deductible $1,000" column within the table 
And Total Charges for the risk location(s) should be displayed under "Deductible $1,000" column within the table 
And Total Premium for the risk location(s) should be displayed under "Deductible $1,000" column within the table 


@Manual
Scenario: 3
When "Do you want to include Flood Coverage?" question is answered as Yes
And ELIAS mapping zone returned <null> for the risk location(s)
Then display the premium without Flood premium
And display static text "Please speak to your underwriter regarding Flood Coverage for <Risk Location Door Number and Street name>" (see mock up for placement of the text) 
And default option 'No' for question "Do you want to include Flood coverage?" for respective Risk Location(s)


@Manual
Scenario: 4
When "Do you want to include Flood Coverage?" question is answered as Yes
And ELIAS mapping returned a value of 3 , 4 or 4+ for the risk location(s)
Then display the premium without Flood premium
And display static text "Please speak to your underwriter regarding Flood Coverage for <Risk Location Door Number and Street name>" (see mock up for placement of the text) 
And default option 'No' for question "Do you want to include Flood coverage?" for respective Risk Location(s)


@Manual
Scenario: 5
When "Do you want to include Flood Coverage?" question is answered as Yes
And ELIAS mapping returned a value of <null> or 3 , 4 or 4+ for Risk Location 1 and 2
Then display the premium without Flood premium
And display static text "Please speak to your underwriter regarding Flood Coverage for <Risk Location 1 Door Number and Street name> and <Risk Location 2 Door Number and Street name>" (see mock up for placement of the text) 
And default option 'No' for question "Do you want to include Flood coverage?" for respective Risk Location(s)


@Manual
Scenario: 6
Given Premium Details are displayed
When mouse hover-over the Total Base Premium field under "Deducitble $500" or "Deductible $1,000" column
Then display Coverage wise base premium break-down per risk location


@Manual
Scenario: 7
When mouse hover-over the Total Charges field under default deductible i.e., "Deductible $500" or "Deductible $1,000" column
Then display break-down of charges per risk location


@Manual
Scenario: 8
When mouse hover-over the Total Premium field under default deductible i.e., "Deductible $500" or "Deductible $1,000" column
Then display total premium break-down per risk location


@Manual
Scenario: 9
When Quote Reference number is verified
Then display Quote Reference number between buttons Calculate Premium and Continue Full Quote
And set the transaction status to "Indicative Quote In Progress" (new status)


@Manual
Scenario: 10
When static text messages are verified
Then display static text "Personal Liability and Family Protection coverage included" just below the premium details (see mock up for placement of the text) 
Then display static text related to sanctions check " xxxxxxxxxxxxxxxxxxxxxxxxxxxxx" just above the page footer (see mock up for placement of the text) 
And display next static text "This indication is a non binding offer until a Full Quote is offered" just above the page footer (see mock up for placement of the text)