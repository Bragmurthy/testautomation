Feature: EP2-50-As a Broker I want to Save & Exit Indicative Quote

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
    And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details
	And click I would like an indicative quotation 
	And enter details for Risk Location as per test data with no google address picker "US_50_SC1" and save quote reference number

@CI
@Sprint-2	
Scenario: 	Verify Quote Status In Ebusiness Portal After Calculating Premium On Clicking Save & Exit
	And enter client surname
	And click on "Calculate Premium"
	And click on "Save & Exit"
	And verify quote status in ebusiness portal is "Incomplete"

@CI
@Sprint-2		
Scenario: 	Verify Quote Status In Ebusiness Portal Before Calculating Premium On Clicking Save & Exit
	And click on "Save & Exit"
	And verify quote status in ebusiness portal is "Incomplete"