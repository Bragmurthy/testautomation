Feature: EP2-604-As a Broker I should able to perform a Term Change transaction 

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
  	And click I would like an full quotation
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "testflow7"
	And enter house and contents details as per "testflow1" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |testflow3            |
   	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_604_SC1"
	And click on "Loss History"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "claimsHistoryFlag" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    Then verify user is in 'Add New Quote' page
    And click on "btnSave" button in ebusiness portal
    And Save the Our Ref Number in "US_604_SC1"
    And click on "Submit Referral" in ebusiness portal
    And accept the alert
    And approve quote in SIT with quote number saved in "US_604_SC1"
    Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
    And click on "Find quote"
    And search for quote our reference number from "US_604_SC1"
    And click on "View"
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
    And click on Premium Summary
    And click on "Finish"
    And click on "btnSave" button in ebusiness portal    
    And click on "btnConvert" button in ebusiness portal
    And click on "Convert Risk Details" in ebusiness portal
 	And Wait for "1" minutes
 	
 	#EP3-191 Changes

	And click on "Premium Summary"
	And click on "Finish Quote"   
	
# 	And click on "Binding"
#    And verify user is able to select deductible option for "$500"
#    And select confirmation checkbox
#    And click on "Proceed To Bind"
    And click on SaveAndAccept button in ebusiness portal
    And wait for "5" minutes
    And Save Policy Number in "US_604_SC1"

	
 @CI	 
 @Sprint-12 
 Scenario: As a Broker I should able to perform a Term Change transaction   

    And click on "Find policy"
    And search for policy reference number from Ebix "US_604_SC1"
	
    And click View in Ebix policy page
 	And click on "btnModify" button in ebusiness portal
 	And Update End Date in ebusiness portal to later date
    And click on "Modify Risk Details" in ebusiness portal
    And Wait for "1" minutes
    And Verify Date Fields and create Termchange Endorsement
    
    And click on "Premium Summary"   
    And validate in Premium summary page
    And click on "Submit for Referral" in ebusiness portal
    
    And approve policy in SIT with quote number saved in "US_604_SC1"
    
    Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
     And click on "Find policy"
    And search for policy reference number from Ebix "US_604_SC1"	
    And click View in Ebix policy page
    
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
    And click on "Premium Summary"
    And click "btnFinish" by id
    And Wait for "2" minutes
    And click on SaveAndAccept button in ebusiness portal
    And wait for "5" minutes
    And click view risk details
    And policy should be in New line Inforce