Feature: EP2-27-As a Broker I should be validated when selecting a incorrect type of residence and coverage combination
	
Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 

@CI
@Sprint-4	
Scenario: Verify Error message displayed for invalid combination of risk location
    And click I would like an indicative quotation 
    And enter details for Risk Location as per test data with no google address picker "testflow28" and save quote reference number
    And enter client surname
    And click on "Calculate Premium"
    And verify error message "Invalid combination of Property Type, Ownership and Type of Residence" is displayed for invalid combination of risk location 