Feature: EP2-75-As a Underwriter I want to see default values

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 

	   
@Manual
Scenario: Continue to Full quote by entering details in Location 1 not calculating Premium
    And click I would like an full quotation
    And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And Wait for "1" minutes
	And edit "1" Named Insured as per "testflow1"
	
 	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "testflow7"
	And enter house and contents details as per "testflow1" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |testflow3            |
	And click on Risks/Coverages navigation
	And Verify details for location "0" as per "testflow1"

@Manual 	
Scenario: Verify details from indicative quote are passed to "Risk and Coverages" - Details for location screen "0" for "testflow2"
    And click I would like an indicative quotation 
	And enter details for Risk Location as per test data with no google address picker "testflow6" and save quote reference number 
	And click on Continue Full Quote
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And Wait for "1" minutes
	And edit "1" Named Insured as per "testflow1"
	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
	And click on Risks/Coverages navigation
	And Verify details for location "0" as per "testflow6"

@Manual	
Scenario: Verify details from indicative quote are passed to "Risk and Coverages" - Details for location screen "0" for "testflow3"
    And click I would like an indicative quotation 
	#And Verify details for location "0" as per "testflow3"
	And enter details for Risk Location as per test data with no google address picker "testflow7" and save quote reference number 
	And click on Continue Full Quote
	And click on Risks/Coverages navigation
	And Verify Flood Zone rating is "1"
	And Verify Protected value is "Yes"
