Feature: EP2-114-As a UW I want to view Evolution generate schedule and cover letter


Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an full quotation
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And Wait for "1" minutes
	And edit "1" Named Insured as per "testflow1"
 	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "testflow7"
	And enter house and contents details as per "testflow1" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |testflow3            |
	And click on "Save and Exit"
	And Save Quote Number in "US_114_SC1"
	And Save write quote Number to field Reference Number in "US_114_SC1"
	
@Sprint-4	
Scenario: Verify send Email is displayed for underwriter view 
    Given evolution SIT application is launched
    And select search type as "Policy/Quote #"
    And search quote number "US_114_SC1"
    And click on "Quote Summary"
    And click on "Generate Quote Evaluation"
    And click on "Click here to return to Client Summary"
    And approve quote in SIT with quote number saved in "US_114_SC1"
    Given evolution SIT application is launched
    And select search type as "Policy/Quote #"
    And search quote number "US_114_SC1"
    And click on "Quote Summary"
    And click on "btnIssueQuote" button
    And Verify "Send Email" is displayed in side navigation