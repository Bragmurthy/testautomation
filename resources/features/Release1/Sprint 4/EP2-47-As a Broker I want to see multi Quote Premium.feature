Feature: EP2-111-As a Broker I want to see showcase session feedback implemented
Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation 


@Manual	
Scenario: Verify Premium Display for "$1000"	
Given Calculate Premium button is clicked
And default deductible is $500 for Risk Location No. 1 and 2
And no validation errors or message displayed
Then display Premium for the next higher deductible which is $1,000


@Manual	
Scenario: Verify Premium Display for "$2,500"	
Given Calculate Premium button is clicked
And default deductible is $1,000 for Risk Location No. 1 and 2
And no validation errors or message displayed
Then display Premium for the next higher deductible which is $2,500


@Manual	
Scenario: Verify Base premium break down for deductible "$2,500"	
Given Premium Details are displayed
When mouse hover-over the Total Base Premium field under "Deducitble $2,500" column
Then display Coverage wise base premium break-down for each risk location


@Manual	
Scenario: Verify changes break down for deductible "$2,500"	
When mouse hover-over the Total Charges field under "Deducitble $2,500" column
Then display Coverage wise Charges break-down for each risk location


@Manual	
Scenario: Verify Total Premium break down for deductible "$2,500"	
When mouse hover-over the Total Premium field under "Deducitble $2,500" column
Then display Total Premium break-down for each risk location
