Feature: EP2-112-As a Broker I do not want to see Email menu option

Background: 
Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"

@Sprint-4	
Scenario: Verify send Email is not displayed for broker view
	And click add risk details 
	And click I would like an indicative quotation 
	And enter details for Risk Location as per test data with no google address picker "testflow1" and save quote reference number
	And click on Continue Full Quote
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And Wait for "1" minutes
	And edit "1" Named Insured as per "testflow1"
	And select current insurer "Bank Insurance" 
    And add broker as per "testflow1" 
    And "add" broker contact as per "testflow1" 
    And click on "Next"
	And click on "btnProceedNext" button

#EP3-191 Changes	

	And click on "btnProceedNext" button
	And click on "btnProceedNext" button
	
	And select "claimsHistoryFlag" as "No"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"
    And click on "Calculate Premium"
    And Verify "Send Email" is not displayed in side navigation
 