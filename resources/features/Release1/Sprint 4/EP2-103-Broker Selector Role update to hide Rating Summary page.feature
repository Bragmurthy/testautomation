Feature: EP2-103-Broker Selector Role update to hide Rating Summary page
	
Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	
@CI
@Sprint-4	
Scenario: Verify "Rating Summary" navigation not displayed for broker view - flow - IQ-continue full quote
 And click I would like an indicative quotation 
 And enter details for Risk Location as per test data with no google address picker "testflow4" and save quote reference number
 And click on Continue Full Quote
 And Verify "Rating Sumamry" is not displayed in side navigation

@Sprint-4
Scenario: Verify "Rating Summary" navigation not displayed for broker view - flow - IQ-calculate premium-continue full quote
 And click I would like an indicative quotation 
 And enter details for Risk Location as per test data with no google address picker "testflow4" and save quote reference number
 And enter client surname
 And click on "Calculate Premium"
 And click on Continue Full Quote 
 And Verify "Rating Sumamry" is not displayed in side navigation

@Sprint-4 
Scenario: Verify "Rating Summary" navigation not displayed for broker view - flow - full quote
 And click I would like an full quotation
 And Verify "Rating Sumamry" is not displayed in side navigation
