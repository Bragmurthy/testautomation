Feature: EP2-83-As a Broker I want to Save & Exit Evolution application

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	  And click I would like an indicative quotation
	  # And click I would like an full quotation

@CI
@Sprint-4 
Scenario: Continue to Full quote by entering details in Location 1 not calculating Premium
 	And enter details for Risk Location as per test data with no google address picker "testflow4" and save quote reference number
	And click on Continue Full Quote
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And Wait for "1" minutes
	And edit "1" Named Insured as per "testflow1"
	And select current insurer "Bank Insurance"
	And change broker contact
	|Field     		|Value|
	|contact       	|Krishna |
	And click Next in policy information page 
	And "add" risk location "0" and enter risk information details as per "testflow5"
	And enter house and contents details as per "testflow1" and "0" VAC for that location "2" as per below data for "broker"
        |Value Added Coverage |
        |testflow7            |
	And Verify field "Next" button is enabled
	And verify footer fields in the page
	And click on "Save and Exit"
	And verify user is in 'Add New Quote' page
	And Verify Quote status is "Incomplete"
	
				  