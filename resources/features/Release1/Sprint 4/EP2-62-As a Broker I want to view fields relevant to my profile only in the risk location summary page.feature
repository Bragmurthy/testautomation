Feature: EP2-62-As a Broker I want to view fields relevant to my profile only in the risk location summary page

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an full quotation
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And Wait for "1" minutes
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0" 
	And select current insurer "Bank Insurance"
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page

@CI
@Sprint-4
Scenario: Continue to Full quote by entering details in Location 1 not calculating Premium
And "add" risk location "0" and enter risk information details as per "testflow7"
And field "Re-plumbed" should not be displayed
And field "EditJewelleryIndexiation" should not be displayed
And field "Protected" should not be displayed 
And field "StartNote" should not be displayed
And field "Decline" should not be displayed
And field "GenerateLar" should not be displayed
And field "AddCoverage" should not be displayed
And field "Edit" should not be displayed
And field "Delete" should not be displayed
And field "Fluvial" should not be displayed
And field "Pluvial" should not be displayed
And field "LatestSystemFloodRating" should not be displayed
And field "OverrideFloodRating" should not be displayed
And field "GenerateAppraisalRequest" should not be displayed
And display buttons 'Add Coverage', 'Edit' and 'Delete' with existing Evolution functionality
