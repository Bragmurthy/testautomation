Feature: EP2-111-As a Broker I want to see showcase session feedback implemented
Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation 

@CI
@Sprint-4	
Scenario: Verify default drop down value for "Ownership Type" for Risk Location 1
 And verify default value for field "property_0_OwnershipType" is empty	
 And verify default value for field "property_0_ResidenceType" is empty	
 And verify default value for field "property_0_PropertyType" is empty	
 And verify default value for field "property_0_BurglarAlarmDetection" is empty	
 And verify default value for field "property_0_ExternalConstruction" is empty	
 And select property type as "Unit" for location "0" and verify Contents Sum Insured is ""

@Sprint-4 
 Scenario: Verify default drop down value for "Ownership Type" for Risk Location 2
 And click on "Add Next Risk Location"
 And verify default value for field "property_1_OwnershipType" is empty	
 And verify default value for field "property_1_ResidenceType" is empty	
 And verify default value for field "property_1_PropertyType" is empty	
 And verify default value for field "property_1_BurglarAlarmDetection" is empty	
 And verify default value for field "property_1_ExternalConstruction" is empty	
 And select property type as "Unit" for location "1" and verify Contents Sum Insured is ""

@Sprint-4 
 Scenario: Verify broker is able to edit Name and Date section in indicative quote page
 And verify broker is able to edit Name and Date section in indicative quote page
 |Field          |Value     |
 |FirstName      |hello     |
 |LastName       |welcome   |
 |Effective Date |16/03/2018|
 |Expiry Date    |16/03/2019|	

@Sprint-4
 Scenario: Verify "Next" Button is present in policy information page
 	And enter details for Risk Location as per test data "testflow4" and save quote reference number
	And click on Continue Full Quote  
	And select current insurer "AIG"
	And click Next in policy information page
