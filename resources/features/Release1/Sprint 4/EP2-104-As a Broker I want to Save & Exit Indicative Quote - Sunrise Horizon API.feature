Feature: EP2-104-As a Broker I want to Save & Exit Indicative Quote - Sunrise Horizon API

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation 

@Sprint-4	
Scenario: Verify Quote Status in eBusiness Portal before calculating premium on Clicking Save & Exit
 And enter details for Risk Location as per test data with no google address picker "testflow1" and save quote reference number
 And click on "Save & Exit"
 And verify quote status in ebusiness portal is "Incomplete"	

@CI
@Sprint-4	
Scenario: Verify Quote Status in eBusiness Portal after calculating premium on Clicking Save & Exit
 And enter details for Risk Location as per test data with no google address picker "testflow1" and save quote reference number
 And enter client surname
 And click on "Calculate Premium"
 And click on "Save & Exit"
 And verify quote status in ebusiness portal is "Incomplete"

@Sprint-4 
Scenario: Verify Quote status in Evolution after Save and Exit function for indicative quote
 And enter details for Risk Location as per test data with no google address picker "testflow1" and save quote reference number
 And click on "Save & Exit"
 And Save Quote Number in "US_104_SC3"
 Given evolution SIT application is launched
 And select search type as "Policy/Quote #" 
 And search quote number "US_104_SC3"
 And verify quote status is "Indicative Quote In Progress"
