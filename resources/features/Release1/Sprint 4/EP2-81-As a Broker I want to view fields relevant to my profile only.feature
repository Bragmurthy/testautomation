Feature: EP2-81-As a Broker I want to view fields relevant to my profile only

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 


@Manual	
Scenario: Address search verification for Risk Location 1 
	And click add risk details 
	And click I would like an indicative quotation
	And enter details for Risk Location as per test data with no google address picker "testflow1" and save quote reference number
	And click on Continue Full Quote
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And Wait for "1" minutes
	And edit "1" Named Insured as per "testflow1"
	And select current insurer "Bank Insurance"
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page
	And "add" risk location "0" and enter risk information details as per "testflow7"
	And verify "flood coverage" button is selected by default
	And enter house and contents details as per "testflow1" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |testflow3            |
