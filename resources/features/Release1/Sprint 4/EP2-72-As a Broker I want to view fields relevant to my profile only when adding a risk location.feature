Feature: EP2-72-As a Broker I want to view fields relevant to my profile only when adding a risk location 

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an full quotation 
	
@Sprint-4 
Scenario: Continue to Full quote by entering details in Location 1 and not calculating Premium 
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And Wait for "1" minutes
	And edit "1" Named Insured as per "testflow1"

	And search address with no google address picker "42 kambara drive Mulgrave" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
	And click Next in policy information page 
	And click on "Add Location" 
	Then field "DistanceToFireStation" should not be displayed 
	Then field "TypeOfFireService" should not be displayed 
	Then field "DistanceFromFireHydrant" should not be displayed 
	And field "DoesThePropertyHaveBoxGutters" should not be displayed 
	And field "HasAllWorkBeenCompletedWithNoTradesperson" should not be displayed 
	And field "HasThePropertyBeenRewired" should not be displayed 
	And field "Re-plumbed" should not be displayed 
	And field "Re-roofed" should not be displayed 
	Then field "GatedCommunity" should not be displayed 
	And field "SecurityProtection" should not be displayed 
	And field "CaretakerOnPremise" should not be displayed 
	And field "SignalContinuityProtection" should not be displayed 
	And field "BackupGenerator" should not be displayed 
	And field "HowFrequentlyIsThePropertyVisited" should not be displayed 
	Then field "IsTheResidenceFireResistant" should not be displayed