Feature: EP2-50-As a Broker I want to Save & Exit Indicative Quote

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation 
	




@Sprint-4
Scenario: Verify Quote Status in eBusiness Portal before calculating premium on Clicking Save & Exit
 And enter details for Risk Location as per test data with no google address picker "testflow4" and save quote reference number
 And click on "Save & Exit"
 And verify quote status in ebusiness portal is "Incomplete"	

@Sprint-4	
Scenario: Verify Quote Status in eBusiness Portal after calculating premium on Clicking Save & Exit
 And enter details for Risk Location as per test data with no google address picker "testflow1" and save quote reference number
 And enter client surname
 And click on "Calculate Premium"
 And click on "Save & Exit"
 And verify quote status in ebusiness portal is "Incomplete"
 

#@manual
#Scenario: Verify all the missing fields are highlighted on clicking save and Exit
#When missing field validation is performed
#Then highlight all fields with missing input (existing Evolution functionality of highlighting fields)
#And cursor focus to the first field missing value from the top of the page
 
