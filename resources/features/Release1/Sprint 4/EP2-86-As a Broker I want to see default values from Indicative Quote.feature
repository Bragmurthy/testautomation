Feature: EP2-86-As a Broker I want to see default values from Indicative Quote

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"

@CI
@Sprint-4 	
Scenario: Address search verification for Risk Location 1 
	And click add risk details 
	And click I would like an indicative quotation 
	And enter details for Risk Location as per test data with no google address picker "testflow4" and save quote reference number
	And click on Continue Full Quote
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And Wait for "1" minutes
	And edit "1" Named Insured as per "testflow1"
	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
	And click Next in policy information page
	And click on "Loss History"
	#Then question "Has the proposer and/or the insured had" should default to "Yes"
	Then the "Has any person who will be covered by this insurance" insurance history questions should have no defaults
	Then the "Has any person who will be covered by this insurance ever been charged " insurance history questions should have no defaults
	Then the "Has any person who will be covered ever filed " insurance history questions should have no defaults
	And one row displayed for 'Pre Policy Non Chubb Losses' section
	
@Sprint-4	
Scenario: Address search verification for Risk Location 1 
And click add risk details 
And click I would like an full quotation
And click on update client button in Policy Information page
When client details are edited
And click on "Submit"
And Wait for "1" minutes
And edit "1" Named Insured as per "testflow1"
And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
And click on "Loss History"	
#Then question "Has the proposer and/or the insured had" should default to "No"
Then the "Has any person who will be covered by this insurance" insurance history questions should have no defaults
Then the "Has any person who will be covered by this insurance ever been charged " insurance history questions should have no defaults
Then the "Has any person who will be covered ever filed " insurance history questions should have no defaults

    