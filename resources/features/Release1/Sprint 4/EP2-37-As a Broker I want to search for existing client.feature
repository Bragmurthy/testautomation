Feature: EP2-37-As a Broker I want to search for existing client

Background: 

Given eBusines portal is launched 
And login eBusiness portal with "testflow1"

@Sprint-4	
Scenario: Verify "CUST001" is displayed in client search
And click on "Find customer" 
When customer is searched using Find Customer "CUST001" menu option Then display matching search "CUST001" results list 

@Sprint-4
Scenario: Verify "No records found." is displayed in client search - Negative scenario
And click on "Find customer" 
When customer is searched using Find Customer "TestABCD" menu option Then display matching search "No records found" results list 