Feature: EP2-33-As a Broker I want to see Evolution calculated premium details in eBusiness Portal

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation 
	
@Sprint-4	
Scenario: Verify Premium details in ebusiness Portal after Save and Exit from Indicative quote
 And enter details for Risk Location as per test data with no google address picker "testflow1" and save quote reference number
 And enter client surname
 And click on "Calculate Premium"
 And click on "Save & Exit" and verify premium details in ebusiness portal