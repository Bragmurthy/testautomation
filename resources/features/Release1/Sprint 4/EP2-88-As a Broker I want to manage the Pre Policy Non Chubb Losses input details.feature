Feature: EP2-88-As a Broker I want to manage the Pre Policy Non Chubb Losses input details

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"


@Sprint-4 	
Scenario: Loss History comments text boxes verification if the slected option is "Yes" 
	And click add risk details 
	And click I would like an indicative quotation 
    And enter details for Risk Location as per test data with no google address picker "testflow4" and save quote reference number
	And click on Continue Full Quote
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And Wait for "1" minutes
	And edit "1" Named Insured as per "testflow1"
	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
	And click Next in policy information page 
	And click on "Loss History"
	And select "claimsHistoryFlag" as "No"
    And select "insuranceRefusalCancelRejected" as 'Yes' and verify textbox is displayed
    And select "chargeConvicted" as 'Yes' and verify textbox is displayed
    And select "bankruptcyFiled" as 'Yes' and verify textbox is displayed