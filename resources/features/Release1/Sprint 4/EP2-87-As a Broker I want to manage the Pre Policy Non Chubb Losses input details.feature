Feature: EP2-87-As a Broker I want to manage the Pre Policy Non Chubb Losses input details 

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
		
@CI	
@Sprint-4 
Scenario: As a Broker I want to manage the Pre Policy Non Chubb Losses input details
	And click add risk details 
	And click I would like an indicative quotation
	And enter details for Risk Location as per test data with no google address picker "testflow4" and save quote reference number 
	And click on Continue Full Quote
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And Wait for "1" minutes
	And edit "1" Named Insured as per "testflow1"
	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
	And click Next in policy information page 
	And click on "Loss History" 
	And Add claim "1" and verify values 
		|Field           	|Value            |
		|Loss Date     		|12/11/1985       |
		|Loss Description   |Loss Description |
		|Loss Type   		|CAT              |
		|Loss Amount    	|10,000.00    	  |
    