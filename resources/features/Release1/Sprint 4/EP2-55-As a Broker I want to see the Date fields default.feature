Feature: EP2-55-As a Broker I want to see the Date fields default

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
	And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 

@CI
@Sprint-4
Scenario: Verify Date section in Policy information page when default dates are not edited in indicative quote page
 And click I would like an indicative quotation 
 And enter details for Risk Location as per test data with no google address picker "testflow4" and save quote reference number
 And click on Continue Full Quote  
 And "EffectiveDate" , "ExpiryDate" and "SubmissionReceivedDate" fields should be non editable
 Then fields 'Effective Date', 'Expiry Date' and 'Submission Received Date' should default from Sunrise or edited value from indicative quote as below
 	    
 
@Sprint-4 
 Scenario: Verify Date section in Policy information page for a full quote option in landing page
 And click I would like an full quotation
 And "EffectiveDate" , "ExpiryDate" and "SubmissionReceivedDate" fields should be non editable
 Then fields 'Effective Date', 'Expiry Date' and 'Submission Received Date' should default from Sunrise or edited value from indicative quote as below
 	 