Feature: EP2-316-As a Broker I should be able to view Workers Compensation related text in Indicative Quote page

Background: 
Given eBusines portal is launched
    And login eBusiness portal with "testflow1"
    And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
  
@Sprint-7
Scenario: As a Broker I should be able to view Workers Compensation related text in Indicative Quote page
And click add risk details 
And click I would like an indicative quotation
And enter details for Risk Location as per test data "US_316_SC1" and save quote reference number
And enter client surname
And click on "Calculate Premium"
And verify text "Worker's Compensation coverage included" is displayed
