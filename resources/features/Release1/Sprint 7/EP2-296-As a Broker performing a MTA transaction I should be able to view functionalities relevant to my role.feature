Feature: EP2-296-As a Broker performing a MTA transaction on House and Contents or VAC I should be able to edit fields relevant to my role

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
  	And click I would like an full quotation
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "testflow7"
	And enter house and contents details as per "testflow1" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |testflow3            |
   	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_296_SC1"
	
	# EP3-191 changes Adding next 2 times
	And click "btnProceedNext" by id
	And click "btnProceedNext" by id
	
	And select "claimsHistoryFlag" as "No"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    Then verify user is in 'Add New Quote' page
    And click on "btnSave" button in ebusiness portal
     And Save the Our Ref Number in "US_296_SC1"
    And click on "Submit Referral" in ebusiness portal
    And accept the alert
    And approve quote in SIT with quote number saved in "US_296_SC1"
    Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
    And click on "Find quote"
    And search for quote our reference number from "US_296_SC1"
    And click on "View"
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
    And click on Premium Summary
    And click on "Finish"
    And click on "btnSave" button in ebusiness portal    
    And click on "btnConvert" button in ebusiness portal
    And click on "Convert Risk Details" in ebusiness portal
    And Wait for "1" minutes
    
    #EP3-191 Changes
    
	And click on "Premium Summary"
	And click on "Finish Quote"    
	
# 	And click on "Binding"
#    And verify user is able to select deductible option for "$500"
#    And select confirmation checkbox
#    And click on "Proceed To Bind"
    And click on SaveAndAccept button in ebusiness portal
    And wait for "5" minutes
    And Save Policy Number in "US_296_SC1"

# Below code is commented because of auto closing
  
#    And Save Quote Reference Number	in "US_296_SC1"
#    And click on "Proceed To Bind"
#    And click on "btnSaveAndAccept" button in ebusiness portal
#    And Save Policy Number in "US_296_SC1"
#
#	Given evolution SIT application is launched
#	And select search type as "Policy/Quote #"
#	And search saved policy number "US_296_SC1"                                                                                               
#	And click on "Closing"
#    And close the quote as UW "US_296_SC1"
#    And click "btnBookAndIssue" by id
#    And wait for Document Generation
#    And click "btnProceed" by id
#    And wait for "5" minutes
# 
 @Sprint-7   
 Scenario: As a Broker I want to perform Endorsement and not display referral  
  	Given eBusines portal is launched    
    And login eBusiness portal with "testflow1"
    And click on "Find policy"
    And search for policy reference number from Ebix "US_296_SC1"
    And click View in Ebix policy page
 	And click on "btnModify" button in ebusiness portal
    And Update Attachment Date in ebusiness portal with "4" days Later date
    And click on "Modify Risk Details" in ebusiness portal
    And Wait for "1" minutes
    And Create Standard Endorsement  
  
#    And change broker contact
#	|Field     		|Value|
#	|contact       |rachana |
    And click "btnProceedNext" by id  
    And Add VAC in MTA
    And click "btnProceedNext" by id 
    And click on "Loss History"
    And click on "Calculate Premium"
   	And field "SubmitReferral" should not be displayed
   	And click "btnFinish" by id
 
 @CI   
@Sprint-7   
Scenario: As a Broker I want to perform Endorsement and display referral  
    Given eBusines portal is launched    
    And login eBusiness portal with "testflow1"
    And click on "Find policy"
    And search for policy reference number from Ebix "US_296_SC1"
    And click View in Ebix policy page
 	And click on "btnModify" button in ebusiness portal
    And Update Attachment Date in ebusiness portal with "4" days Later date
    And click on "Modify Risk Details" in ebusiness portal
    And Wait for "1" minutes
    And Create Standard Endorsement
    And click "btnProceedNext" by id  
    And click on "House and Contents"
    And verify "bsi" is editable by passing value "30000000"
    And click "btnProceedNext" by id 
    And click "btnProceedNext" by id 
    And click on "Loss History"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
  
    
    #@Manual
   # Sanctions