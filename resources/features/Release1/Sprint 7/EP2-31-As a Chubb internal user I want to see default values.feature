Feature: EP2-31-As a Chubb internal user I want to see default values

Background: 
Given eBusines portal is launched
And login eBusiness portal with "testflow1"
And click on Add new Customer
And enter new customer details
And save Customer Id
And click on Add a New Quote in Add new Customer page
And enter identification details in "testflow5"
And click add risk details

@CI
@Sprint-7
Scenario: Verify client Details in Policy information page
And click I would like an full quotation
And click on update client button in Policy Information page
And client details are edited
And click on "Submit"
#And click on OK button of client Already exists popup
And Remove existing named insured
And "Ädd" Named Insured as per "testflow1"
And change broker contact
|Field     |Value|
|contact       |Krishna |
And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
And select current insurer "Bank Insurance"
And Save Ebix policy information details in "US_31_SC1"
And Save Quote Reference Number in "US_31_SC1"
And click "btnSaveExit" by id 
And click on "btnSave" button in ebusiness portal

Given evolution SIT application is launched
	And select search type as "Policy/Quote #"
	And search saved indicative quote number "US_31_SC1" 
    And verify policy default values as UW in "US_31_SC1" 