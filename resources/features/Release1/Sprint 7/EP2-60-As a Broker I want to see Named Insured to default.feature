Feature: EP2-60-As a Broker I want to see Named Insured to default

Background: 
	Given eBusines portal is launched
    And login eBusiness portal with "testflow1"
    And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details
 
@Sprint-7 	
Scenario: Verify named insured default in policy information page 1
	And click I would like an indicative quotation 
	And enter details for Risk Location as per test data "testflow4" and save quote reference number 
	And click on Continue Full Quote
	Then verify default values in named insured section