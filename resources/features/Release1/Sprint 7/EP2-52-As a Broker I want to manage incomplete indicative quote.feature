Feature: EP2-52-As a Broker I want to manage incomplete indicative quote

Background: 
	Given eBusines portal is launched
	And login eBusiness portal with "testflow1"
	And click on Add a New Quote 
	And enter new Quote details as below 
		|Field         |Value                           |
		|Client ID     |FDIADO                          |
		|Product       |Chubb Masterpiece Australia SIT |
		|Insured ID    |FDIADO                          |
		|Start Date    |10 Apr 2018                     |
		|End Date      |10 Mar 2019                     |
		|Branch        |Melbourne                       |
	
# below is the method to update the STart date to Current system date, Didn't remove the data table to select the existing client to get duplicate client pop-up message

	And Update Start Date in ebusiness portal to current system date
	And click add risk details
	And click I would like an indicative quotation 
	And enter details for Risk Location as per test data with no google address picker "US_52_SC1" and save quote reference number
    And click on "Calculate Premium"    
    And verify Quote Validation message is displayed as "Our system indicates that we have an existing record for this client/property."
    And click on "OK" button in referal dialogbox
	And click on "btnSave" button in ebusiness portal
    And Save the Our Ref Number in "US_52_SC1"

@Sprint-7
Scenario: As a Broker I want to override duplicate client by UW
	Given evolution SIT application is launched
	And select search type as "Policy/Quote #" 
	And search saved indicative quote number "US_52_SC1"  
	And click "btnViewDuplicateClient" by id
	And select possible duplicate client
	And click on "Use Selected Client" button in referal dialogbox
	And release lock for editing
	
	Given eBusines portal is launched
	And login eBusiness portal with "testflow1"
	And click on "Find quote"
	And search for quote our reference number from "US_52_SC1"
    And click on "View"
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
    And validate Quote validation popup not displayed

@Sprint-7
Scenario: As a Broker I want to see duplicate client pop-up
	And click on "Find quote"
	And search for quote our reference number from "US_52_SC1"
    And click on "View"
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
    And verify Quote Validation message is displayed as "Our system indicates that we have an existing record for this client/property."