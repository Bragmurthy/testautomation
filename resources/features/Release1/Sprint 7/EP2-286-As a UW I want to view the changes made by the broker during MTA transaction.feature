Feature: EP2-286-As a UW I want to view the changes made by the broker during MTA transaction

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
		
@Manual			
Scenario: As a Underwriter I want to see default values
	And click add risk details 
    And click I would like an indicative quotation
    And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
	And enter details for Risk Location as per test data "testflow2" and save quote reference number
	And click on Continue Full Quote
	And select current insurer "Bank Insurance"
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page
 	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_286_SC1"
	And select "claimsHistoryFlag" as "No"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    Then verify user is in 'Add New Quote' page
    And click on "btnSave" button in ebusiness portal
    And Save the Our Ref Number in "US_286_SC1"
    And click on "Submit Referral" in ebusiness portal
    And accept the alert
    And approve quote in SIT with quote number saved in "US_286_SC1"
    Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
    And click on "Find quote"
    And search for quote reference number from "testflow4" and verify status of quote is "Awaiting Referral"
    And click edit risk details
    And click on "Finish Quote"
    And click on "btnSave" button in ebusiness portal
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
    And click "btnProceedNext" by id
    And click "btnProceedNext" by id
    And verify user is able to select deductible option for "$500"
    And select confirmation checkbox
    And click on "Proceed To Bind"
    #And verify quote status in ebusiness portal is "NB Auto Close Accepted"
    And click on "btnSave" button in ebusiness portal
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
    And click on "Binding"
    And verify details are saved in Binding page
    And verify quote status is "Newline Bound"
    
    
    Given evolution SIT application is launched
    And select search type as "Policy/Quote #"                                                                                               
    And search saved indicative quote number "testflow4"
    And verify quote status is "Newline Bound" in UW view for the Quote in "testflow4" 
    
    # here we need to book the policy and click on MTA
    
    Given eBusines portal is launched 
    And click on "Find quote"
    And search for quote reference number from "testflow4" and verify status of quote is "Completed"
    And click on "Modify" in ebusiness portal
    And click on "Modify Risk Details" in ebusiness portal
    
    # need to verify chubb producer code and authorization
    
    And verify user is on Policy Information page
    And verify quote is editable in ebusiness potal
    And change insurer
    |Field     |Value|
	|Insurer       |AIG |
	
	Given evolution application is launched
    And select search type as "Policy/Quote #"                                                                                               
    And search saved indicative quote number "testflow4"
	
	 And verify insurer
    |Field     |Value|
	|Insurer       |AIG |
    
    