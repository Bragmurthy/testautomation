Feature: EP2-56-As a Broker I want to see Client Details default 

Background: 
	Given eBusines portal is launched 
	And login eBusiness portal with "testflow1" 
	And click on Add new Customer 
	And enter new customer details 
	And save Customer Id 
	And click on Add a New Quote in Add new Customer page 
	And enter identification details in "testflow5" 
	And click add risk details 

@Sprint-7	
Scenario: Verify client Details in Policy information page 
	And click I would like an full quotation 
	Then Client Name First and Surname should default from Sunrise 
	And Email,Retired fields should be mandatory and fields are non editable 

@Sprint-7	
Scenario: Address search verification for Risk Location 1 
	And click I would like an indicative quotation 
	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0" 
	And enter details for Risk Location as per test data "testflow1" and save quote reference number 
	And click on Continue Full Quote 
	Then Client Name First and Surname should default from Sunrise 
	And Email,Retired fields should be mandatory and fields are non editable 
	
	#@Manual
	#Scenario: verify field is alignment
	
	
	
	
	
