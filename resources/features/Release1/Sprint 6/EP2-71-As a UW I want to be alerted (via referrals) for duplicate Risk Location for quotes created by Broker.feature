Feature: EP2-71-As a UW I want to be alerted (via referrals) for duplicate Risk Location for quotes created by Broker

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"

@Sprint-6
Scenario: Verify user is able to add a Risk location through Continue Full Quote Option
	And click add risk details 
	And click I would like an full quotation
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "testflow3"
#   And "add" risk location "0" and enter risk information details as per "testflow3"
    And verify user is on Risks and Coverages page

@CI	
@Sprint-6
Scenario: Verify details entered for the Risk Location "0" in indicative quote page can be edited in Risk and coverage section
   And click add risk details 
	And click I would like an indicative quotation
	And enter details for Risk Location as per test data with no google address picker "testflow4" and save quote reference number
	And click on Continue Full Quote
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And select current insurer "Bank Insurance"
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page
 	And "add" risk location "0" and enter risk information details as per "testflow3"
#   And "add" risk location "0" and enter risk information details as per "testflow3"
    And verify user is on Risks and Coverages page
