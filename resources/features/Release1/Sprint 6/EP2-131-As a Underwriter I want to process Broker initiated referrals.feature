Feature: EP2-131-As a Underwriter I want to process Broker initiated referrals

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
    
@CI
@Sprint-6	
Scenario: EP2-131-As a Underwriter I want to process Broker initiated referrals
	And click add risk details 
    And click I would like an indicative quotation
	And enter details for Risk Location as per test data with no google address picker "testflow1" and save quote reference number
	And click on Continue Full Quote
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And select current insurer "Bank Insurance"
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page
 	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_131_SC1"

#EP3-191-Changes
 
   And click "btnProceedNext" by id
   And click "btnProceedNext" by id 	
	
	 And select "claimsHistoryFlag" as "No"
	 And select "bankruptcyFiled" as "No"
	 And select "insuranceRefusalCancelRejected" as "No"
	 And select "chargeConvicted" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    And click on "btnSave" button in ebusiness portal
    And click on "Submit Referral" in ebusiness portal
    And accept the alert
    And approve quote in SIT with quote number saved in "US_131_SC1"
   #And approve quote with quote number saved in "US_131_SC1"
    Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
    And click on find Quote
    And search for quote reference number from "US_131_SC1" and verify status of quote is "Awaiting Referral"
     
@Sprint-6   
Scenario: EP2-131-As a Underwriter I want to process Broker initiated referrals
	And click add risk details 
    And click I would like an indicative quotation
	And enter details for Risk Location as per test data "testflow1" and save quote reference number
	And click on Continue Full Quote
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And select current insurer "Bank Insurance"
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page
 	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_131_SC1"

#EP3-191-Changes
 
   And click "btnProceedNext" by id
   And click "btnProceedNext" by id 	
	
	And select "claimsHistoryFlag" as "No"
	 And select "bankruptcyFiled" as "No"
	 And select "insuranceRefusalCancelRejected" as "No"
	 And select "chargeConvicted" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    And click on "btnSave" button in ebusiness portal
    And click on "Submit Referral" in ebusiness portal
    And accept the alert
    And decline quote in SIT with quote number saved in "US_131_SC1"
   # And decline quote with quote number saved in "US_131_SC1"
    Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
    And click on find Quote
    And search for quote reference number from "US_131_SC1" and verify status of quote is "Awaiting Referral"
  
  