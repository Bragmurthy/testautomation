Feature: EP2-142-As a Broker I should be able to Edit the quote even after it is offered


Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	
	And click add risk details 
  	And click I would like an full quotation
  	And Save Quote Reference Number in "US_142_SC1"
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
    
	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "testflow7"
	And enter house and contents details as per "testflow1" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |testflow3            |
   	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_142_SC1"

#EP3-191-Changes
 
   And click "btnProceedNext" by id
   And click "btnProceedNext" by id 	
	
	 And select "claimsHistoryFlag" as "No"
	 And select "bankruptcyFiled" as "No"
	 And select "insuranceRefusalCancelRejected" as "No"
	 And select "chargeConvicted" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    Then verify user is in 'Add New Quote' page
    And click on "btnSave" button in ebusiness portal
    And Save the Our Ref Number in "US_142_SC1"
    And click on "Submit Referral" in ebusiness portal
    And accept the alert
    And approve quote in SIT with quote number saved in "US_142_SC1"
    #And approve quote with quote number saved in "US_142_SC1"
 
 @CI
 @Sprint-6
 Scenario: verifying status in Evolution
    Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
    And click on "Find quote"
    And search for quote reference number from "US_142_SC1"
    And click on "View"
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
    And click on Premium Summary
    And click on "Finish"
    And click on "btnSave" button in ebusiness portal    
    And click on "btnConvert" button in ebusiness portal
    And click on "Convert Risk Details" in ebusiness portal
    And click on "Premium Summary"
	And click on "Finish Quote"   
	
 	And verify quote status is "Quote In Progress"
	And verify user is on Policy Information page
	And change insurer
    |Field     |Value|
	|Insurer       |AIG |
	And click "btnSaveExit" by id

# Commenting the below code as "Amend Quote" button is removed in CR#512
 	
# 	And click on "Amend Quote"
#	And click on "Yes" button in referal dialogbox
#	And click on "Binding"

