Feature: EP2-228-As a Broker I should be able to continue with quote if it is Approved via referral action

Background: 
  Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
    And click I would like an indicative quotation
	And enter details for Risk Location as per test data with no google address picker "testflow1" and save quote reference number
	And click on Continue Full Quote
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And select current insurer "Bank Insurance"
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page
 	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_228_SC1"

 #EP3-191-Changes
 
   And click "btnProceedNext" by id
   And click "btnProceedNext" by id 	
	
	 And select "claimsHistoryFlag" as "No"
	 And select "bankruptcyFiled" as "No"
	 And select "insuranceRefusalCancelRejected" as "No"
	 And select "chargeConvicted" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    Then verify user is in 'Add New Quote' page
    And click on "btnSave" button in ebusiness portal
    And Save the Our Ref Number in "US_228_SC1"
    And click on "Submit Referral" in ebusiness portal
    And accept the alert
    And approve quote in SIT with quote number saved in "US_228_SC1"
    #And approve quote with quote number saved in "US_228_SC1"

@Sprint-6
Scenario: As a Broker when trying to edit the quote I want to navigate to Policy Information page
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
    And click on "Find quote"
    And search for quote reference number from "US_228_SC1" and verify status of quote is "Awaiting Referral"
    And click on "View"
	And click on "btnEdit" button in ebusiness portal
	And click edit risk details
	And verify quote is editable in ebusiness potal
	