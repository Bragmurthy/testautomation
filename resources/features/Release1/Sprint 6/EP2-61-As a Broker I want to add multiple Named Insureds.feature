Feature: EP2-61-As a Broker I want to add multiple Named Insureds

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 

@Sprint-6
Scenario: EP2-61-As a Broker I want to add multiple Named Insureds
	And click I would like an full quotation 
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
 	And click on "Add Named Insured"
	Then verify fields in "Named Insured" page
	And click on "Cancel" button in referal dialogbox
	And "Ädd" Named Insured as per "testflow1"
	And verify Named Insured added succesfully "testflow1"
	And edit "2" Named Insured as per "testflow1"
	Then verify user is on Policy Information page
	
	
