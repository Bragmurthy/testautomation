Feature: EP2-232-As a UW I should be able to search and view the quotes referred by broker in Policy Administration

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	
	And click add risk details 
	And click I would like an indicative quotation	
	And enter details for Risk Location as per test data with no google address picker "testflow1" and save quote reference number
	And click on Continue Full Quote
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And select current insurer "Bank Insurance"
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page
    And verify user is on Risks and Coverages page	
 	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_232_SC1"

 #EP3-191-Changes
 
   And click "btnProceedNext" by id
   And click "btnProceedNext" by id 	
	
	 And select "claimsHistoryFlag" as "No"
	 And select "bankruptcyFiled" as "No"
	 And select "insuranceRefusalCancelRejected" as "No"
	 And select "chargeConvicted" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    Then verify user is in 'Add New Quote' page
    And click on "btnSave" button in ebusiness portal
    And click on "Submit Referral" in ebusiness portal
    And accept the alert  
    
@Sprint-6
Scenario: As a UW I should able to view quote referred in Sunrise
	Given evolution SIT application is launched
    And select search type as "Policy/Quote #"                                                                                               
    And search saved quote number "US_232_SC1"
    And verify transaction channel is "Broker"
    And verify quote status is "Quote Referred" in UW view for the Quote in "US_232_SC1" 
    