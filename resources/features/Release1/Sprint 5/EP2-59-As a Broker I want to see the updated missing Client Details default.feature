Feature: EP2-59-As a Broker I want to see the updated missing Client Details default

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"

@Sprint-5		
Scenario: As a Broker I want to see the updated missing Client Details default
	And click add risk details 
	And click I would like an full quotation 
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	#And click on "OK" button in referal dialogbox
	Then all client details should be saved
