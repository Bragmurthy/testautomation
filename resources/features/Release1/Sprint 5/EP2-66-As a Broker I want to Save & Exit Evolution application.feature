Feature: EP2-66-As a Broker I want to Save & Exit Evolution application

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
		
@Sprint-5		
Scenario: Verify Save and Exit in Policy information page for a "continue to full quote" option
	And click I would like an full quotation 
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
	And click on "Save and Exit"
	And verify user is in 'Add New Quote' page

@Sprint-5	
Scenario: Verify Save and Exit in Policy information page for a "Indicative quote" option
	And click I would like an indicative quotation 
	And enter details for Risk Location as per test data "testflow4" and save quote reference number
	And click on Continue Full Quote
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
	And click on "Save and Exit"
	And verify user is in 'Add New Quote' page