Feature: EP2-76-As a Broker I want to see default values from Indicative Quote

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	
@Sprint-5	
Scenario: Verify "Building Coverage" for both Risk Locations matches with the details from Indicative Quote page
    And click I would like an indicative quotation 
	And enter details for Risk Location as per test data "testflow4" and save quote reference number
	And click on Continue Full Quote
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
	And click on House and Contents for Risk Location "0"
	And "verify" Building coverage for Risk Location "0"
	|Field                      |Value        |
	|Building Sum Insured       |100,000      |
	|Building Deductible        |500          |
	|Other Permanent Structures |30,000      |
   And click on House and Contents for Risk Location "1"
   And "verify" Building coverage for Risk Location "1"
	|Field                      |Value        |
	|Building Sum Insured       |100,000      |
	|Building Deductible        |500          |
	|Other Permanent Structures |30,000      |	
	
@Sprint-5
Scenario: Verify "Contents Coverage" for both Risk Locations matches with the details from Indicative Quote page
	And click I would like an indicative quotation 
	And enter details for Risk Location as per test data "testflow4" and save quote reference number
	And click on Continue Full Quote
	And click on "Risks / Coverages"
	And click on House and Contents for Risk Location "0"
	And "verify" Contents coverage for Risk Location "0"
	|Field                      |Value        |
	|Contents Sum Insured       |110,000      |
	|Contents Deductible        |500          |
	And click on House and Contents for Risk Location "1"
	And "verify" Contents coverage for Risk Location "1"
	|Field                      |Value        |
	|Contents Sum Insured       |132,000      |
	|Contents Deductible        |500          |

@Sprint-5	   
Scenario: Verify "Building Deductible" and "Contents Deductible" for both the Risk Locations
	And click I would like an indicative quotation 
	And enter details for Risk Location as per test data "testflow4" and save quote reference number
	And click on Continue Full Quote
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
	And click on House and Contents for Risk Location "0"
	And verify Building Deductible and Conntents Deductible for location "0" has same value    
    And click on House and Contents for Risk Location "1"
    And verify Building Deductible and Conntents Deductible for location "1" has same value 

@Sprint-5	
Scenario: Verify "Building and Contents Coverage" has no value for Risk Location "0" for a full quote option
	And click I would like an full quotation 
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
	And "add" risk location "0" and enter risk information details as per "testflow7"
    And click on House and Contents for Risk Location "0"
  	And "verify" Building coverage for Risk Location "0"
	|Field                      |Value        |
	|Building Sum Insured       |             |
	|Building Deductible        |             |
	|Other Permanent Structures |             |
	And "verify" Contents coverage for Risk Location "0"
	|Field                      |Value        |
	|Contents Sum Insured       |             |
	|Contents Deductible        |             |	
	
	
# This scenario is Out of scope, As Home Business property field is hidden for broker	

#Scenario: Verify "Extended Replacement Cost Type" and "Business Property Coverage" for both the Risk Locations
#	And click I would like an indicative quotation 
#	And enter details for Risk Location as per test data "testflow4" and save quote reference number
#	And click on Continue Full Quote
#	And click on "Risks / Coverages"
#	And click on House and Contents for Risk Location "0"
#	And select Home Business Property Coverage as "Yes" and Home Business Property Sum Insured as "1000" for risk location "0"
#    And select Home Business Property Coverage as "No" and Home Business Property Sum Insured as "" for risk location "0"	
#    And click on House and Contents for Risk Location "1"
#	And select Home Business Property Coverage as "Yes" and Home Business Property Sum Insured as "1000" for risk location "1"
#    And select Home Business Property Coverage as "No" and Home Business Property Sum Insured as "1000" for risk location "1"
 