Feature: EP2-57-As a Broker I want to update  missing Client details

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"

@CI
@Sprint-5		
Scenario: As a Broker I want to update  missing Client details
	And click add risk details 
	And click I would like an full quotation
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "testflow7"
	And enter house and contents details as per "testflow1" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |testflow3            |
	And click "btnProceedNext" by id
#EP3-191 Changes	
	And click "btnProceedNext" by id
    And select checkbox for Adding Appraisal Contact
    And click on "Add Appraisal Contact"
    And verify fields in Add Apprisal Contact page
	 And select "claimsHistoryFlag" as "No"
 And select "bankruptcyFiled" as "No"
 And select "insuranceRefusalCancelRejected" as "No"
 And select "chargeConvicted" as "No"
    And click on "Calculate Premium"
    And click on "Finish Quote"
    And click on "btnSave" button in ebusiness portal
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
    


    
    
    

	

