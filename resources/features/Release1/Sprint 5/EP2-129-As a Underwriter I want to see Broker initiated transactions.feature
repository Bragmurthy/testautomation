Feature: EP2-129-As a Underwriter I want to see Broker initiated transactions

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 

@Sprint-5
Scenario: Verify Policy information page is displayed in UW view for broker initiated transaction
 And click I would like an indicative quotation 
 And enter details for Risk Location as per test data "testflow4" and save quote reference number
 And click on Continue Full Quote
 And click on update client button in Policy Information page
 When client details are edited
 And click on "Submit"
 And edit "1" Named Insured as per "testflow1"
 And select current insurer "Bank Insurance" 
 And change broker contact
	|Field     |Value|
	|contact       |Krishna |
 And Save Quote Reference Number in "US_129_SC1"
 And click on "Save and Exit"
 And click on "btnSave" button in ebusiness portal
 Given evolution SIT application is launched 
 And select search type as "Policy/Quote #" 
 And search saved indicative quote number "US_129_SC1"
 #And verify landing URL is "http://apregevwebqa121.aceins.com/policy-info/edit?"	
 And verify landing URL is "http://apregevwebqa121:6666/policy-info/edit?"

@CI
@Sprint-5
Scenario: verify Quote status in Policy information for UW view when quote saved in indicative quote page
 And click I would like an indicative quotation 
 And enter details for Risk Location as per test data "testflow4" and save quote reference number
 And click on "Save & Exit"
 And Save Quote Number in "US_129_SC2"
 Given evolution SIT application is launched 
 And select search type as "Policy/Quote #" 
 And search quote number "US_129_SC2"
 And verify quote status is "Indicative Quote In Progress"
 
 Scenario: verify Quote status in Policy information for UW view when quote saved in indicative quote page
 And click I would like an indicative quotation 
 And enter details for Risk Location as per test data "testflow4" and save quote reference number
 And click on Continue Full Quote
 And Save Quote Reference Number in "US_129_SC3"
 And select current insurer "Bank Insurance" 
 And change broker contact
	|Field     |Value|
	|contact       |Krishna |
 And click on "Save and Exit"
 And click on "btnSave" button in ebusiness portal
 Given evolution SIT application is launched
 And select search type as "Policy/Quote #" 
 And search saved indicative quote number "testflow4"
 And verify clientName is "Evolution Client"
 
@Manual
 Scenario: verify Quote status in Policy information for UW view when quote saved in indicative quote page
 And click I would like an indicative quotation 
 And enter details for Risk Location as per test data "testflow4" and save quote reference number
 And click on Continue Full Quote
 And Save Quote Reference Number in "US_127_SC4"
 And select current insurer "Bank Insurance" 
And change broker contact
	|Field     |Value|
	|contact       |Krishna |
 And click on "Save and Exit"
 And click on "btnSave" button in ebusiness portal
 Given evolution application is launched 
 And select search type as "Named Insured" 
 And search saved indicative quote number "testflow4"
 And verify na is "Evolution Client"
 
  
		
@Manual	
Scenario: Verify Client Details page is displayed when Broker initiated transaction are searched in UW view
When broker initiated transactions from Sunrise are searched based on Client name
Then display search results in Client Details page (as per existing Evolution functionality)

