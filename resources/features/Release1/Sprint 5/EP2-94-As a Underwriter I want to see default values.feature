Feature: EP2-94-As a UnderwriterI want to see default values	

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation 
	And enter details for Risk Location as per test data "testflow4" and save quote reference number
	And click on Continue Full Quote
	And click on update client button in Policy Information page
	 When client details are edited
	 And click on "Submit"
	 And edit "1" Named Insured as per "testflow1"
	 And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0" 
	 And select current insurer "Bank Insurance" 
	 And change broker contact
	|Field     |Value   |
	|contact   |Krishna |
  And click Next in policy information page 
  And click "btnProceedNext" by id
 
 #EP3-191-Changes
 
   And click "btnProceedNext" by id
   And click "btnProceedNext" by id 
  
	And Add claim "1" and verify values
    |Field       |Values      |
    |Date        |01/12/2017  |
    |Description |Loss        |
    |Type of Loss|CAT         |
    |Amount Paid |1,000       | 
    
  
@Sprint-5  
Scenario: Verify Underwriter is able to click on "Proceed with QUote" in loss history page
    And select "claimsHistoryFlag" as "No"
   And select "bankruptcyFiled" as "No"
   And select "insuranceRefusalCancelRejected" as "No"
   And select "chargeConvicted" as "No"
    And click on "Save and Exit"
	And click on "btnSave" button in ebusiness portal
    Given evolution SIT application is launched
    And select search type as "Policy/Quote #"
    And search saved indicative quote number "testflow4"
    And click on "Proceed with Quote" by text
    And click on "Loss History"
    And click "btnProceed" by id

@Sprint-5    
 Scenario: Verify Underwriter is able to click on "Decline" in loss history page
    And select "claimsHistoryFlag" as "No"
   And select "bankruptcyFiled" as "No"
   And select "insuranceRefusalCancelRejected" as "No"
   And select "chargeConvicted" as "No"
    And click on "Save and Exit"
	And click on "btnSave" button in ebusiness portal
    Given evolution SIT application is launched
    And select search type as "Policy/Quote #"
    And search saved indicative quote number "testflow4"
    And click on "Proceed with Quote" by text
    And click on "Loss History"
    And click "btnDecline" by id   
 
@Sprint-5   
Scenario: Address search verification for Risk Location 1 for a Underwriter View
    And select "claimsHistoryFlag" as "Yes"
   And select "bankruptcyFiled" as "No"
   And select "insuranceRefusalCancelRejected" as "No"
   And select "chargeConvicted" as "No"
    And click on "Save and Exit"
	And click on "btnSave" button in ebusiness portal
    Given evolution SIT application is launched
    And select search type as "Policy/Quote #"
    And search saved indicative quote number "testflow4"
    And click on "Proceed with Quote" by text
    And click on "Loss History"
    And Add claim "2" and verify values
    |Field       |Values      |
    |Date        |01/12/2017  |
    |Description |Loss        |
    |Type of Loss|CAT         |
    |Amount Paid |1,000       | 

@Sprint-5	
Scenario: Verify Text box is displayed in loss history page for underwriter when option "Yes" is checked
	And select "insuranceRefusalCancelRejected" as "Yes"
	And select "chargeConvicted" as "Yes"
    And select "bankruptcyFiled" as "Yes"
    And click "btnSaveProgress" by id
 
@Sprint-5	  
Scenario: Verify Text box is displayed in loss history page for underwriter when option "Yes" is checked and verify
    And select "insuranceRefusalCancelRejected" as 'Yes' and verify textbox is displayed
    And select "chargeConvicted" as 'Yes' and verify textbox is displayed
    And select "bankruptcyFiled" as 'Yes' and verify textbox is displayed