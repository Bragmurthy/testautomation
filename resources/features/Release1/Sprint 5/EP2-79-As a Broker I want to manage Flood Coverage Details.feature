Feature: EP2-79-As a Broker I want to manage Flood Coverage Details

#Background: 
#	Given eBusines portal is launched 
#    And login eBusiness portal with "testflow1"
#	And click on Add new Customer
#    And enter new customer details
#    And save Customer Id
#    And click on Add a New Quote in Add new Customer page
#    And enter identification details in "testflow5"

	
@Manual	
Scenario: Address search verification for Risk Location 1 
	And click add risk details 
	And click I would like an full quotation 
	#And enter details for Risk Location as per test data "testflow4" and save quote reference number
 	#And Enter Data in mandatory fields 'Year of Construction' as "2015" , 'External Construction' as "Brick Veneer", 'Building Sum Insured' as "1,500,000" for Risk Location "0"
	#And click on "Continue Full Quote"
	And click on "Risks / Coverages"
	And "add" risk location "0" and enter risk information details as per "testflow7"
	And verify "flood coverage" button is selected by default
	And enter house and contents details as per "testflow1" and "1" VAC for that location "0" as per below data for "broker"
       |Value Added Coverage |
       |testflow7    |
   Then the "Yes/No" radio button should be read only
   
@Manual	   
Scenario: Address search verification for Risk Location 1 
And click add risk details 
And click I would like an full quotation
And click on "Loss History"	
Then question "Has the proposer and/or the insured had" should default to "No"
Then the "3" insurance history questions should have no defaults 
   