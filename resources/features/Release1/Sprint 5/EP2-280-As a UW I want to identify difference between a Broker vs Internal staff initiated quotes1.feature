Feature: EP2-280-As a UW I want to identify difference between a Broker vs Internal staff initiated quotes

#Background: 
#	Given eBusines portal is launched 
#    And login eBusiness portal with "testflow1"
#	And click on Add new Customer
#    And enter new customer details
#    And save Customer Id
#    And click on Add a New Quote in Add new Customer page
#    And enter identification details in "testflow5"
#	And click add risk details 
		
	
@Manual	
Scenario: Verify status of the Broker and Underwriter initiated quotes in UW view
Given a internal user role is logged in
And when a broker initiated transaction is searched in Policy Admin
When search is initiated based on Client or Named Insured or Risk Location or Policy/Quote search criteria
Then display the search results with a new column 'Transaction Channel' 
And display new column next to existing Policy/Quote number or Quote/Policy number column (in respective search result pages)
And display text 'Broker' for transactions initiated by broker
And display text 'Internal' for transactions initiated by Chubb internal staff
    
