Feature: EP2-78-As a Broker I want to manage Contents Coverage Details

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation 
	And enter details for Risk Location as per test data "testflow4" and save quote reference number
	And click on Continue Full Quote
	And click on "Risks / Coverages"

@Sprint-5	
Scenario: Verify Broker is able to update the details for "Contents Coverage" for Risk Location "0"
	And click on House and Contents for Risk Location "0"
	And "edit" Contents coverage for Risk Location "0"
	|Field                      |Value        |
	|Contents Sum Insured       |550,000      |
	|Contents Deductible        |$250         |
	And "verify" Contents coverage for Risk Location "0"
	|Field                      |Value        |
	|Contents Sum Insured       |550,000      |
	|Contents Deductible        |250          |

@Sprint-5	
Scenario: Verify Broker is able to update the details for "Contents Coverage" for Risk Location "1"
	And click on House and Contents for Risk Location "1"
	And "edit" Contents coverage for Risk Location "1"
	|Field                      |Value        |
	|Contents Sum Insured       |550,000      |
	|Contents Deductible        |$250         |
	And "verify" Contents coverage for Risk Location "1"
	|Field                      |Value        |
	|Contents Sum Insured       |550,000      |
	|Contents Deductible        |250          |
		
@Sprint-5		
Scenario: Verify "Building Deductible" and "Contents Deductible" for location "0" has same value
	And click on House and Contents for Risk Location "0"
	And verify Building Deductible and Conntents Deductible for location "0" has same value
	
@Sprint-5	
Scenario: Verify "Building Deductible" and "Contents Deductible" for location "1" has same value
	And click on House and Contents for Risk Location "1"
	And verify Building Deductible and Conntents Deductible for location "1" has same value


