Feature: EP2-126-As a Broker I do not want to see Notes screen

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 

@Sprint-5	 
Scenario: Verify "Notes" navigation not displayed for broker view - flow - full quote
 And click I would like an full quotation
 And Verify "Notes" is not displayed in side navigation