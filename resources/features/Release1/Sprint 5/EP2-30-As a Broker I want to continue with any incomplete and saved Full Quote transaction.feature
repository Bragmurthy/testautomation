Feature: EP2-30-As a Broker I want to continue with any incomplete and saved Full Quote transaction

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation 

@Sprint-5	
Scenario: Verify Address details are retained in indicative quote page after during "Edit"
 And enter details for Risk Location as per test data "testflow4" and save quote reference number
 And click SaveAndExit
 And click on "btnSave" button in ebusiness portal
 And click on "btnEdit" button in ebusiness portal
 And click edit risk details 
 And verify below details are populated after address search for Risk Location "0" 
		|Field                   |Value                   |
		|Unit No                 |                        |
		|Street No               |9                       |
		|Street Name             |Ankuri Road             |
		|Building Name           |                        |	
		|Suburb/City             |Tarneit                 |
		|Post Code               |3029                    |
		|Country                 |Australia               |
		|State/Province          |80000002                |
 And verify below details are populated after address search for Risk Location "1" 
		|Field                   |Value                   |
		|Unit No                 |                        |
		|Street No               |9                       |
		|Street Name             |Ankuri Road             |
		|Building Name           |                        |
		|Suburb/City             |Tarneit                 |
		|Post Code               |3029                    |
		|Country                 |Australia               |
		|State/Province          |80000002                |

@Sprint-5
Scenario: Verify Risk Information are retained in indicative quote page after during "Edit"
 And enter details for Risk Location as per test data "testflow4" and save quote reference number
 And click SaveAndExit
 And click on "btnSave" button in ebusiness portal
 And click on "btnEdit" button in ebusiness portal
 And click edit risk details 
 And verify below details are populated in Risk information for Risk Location "0" 
	    |Field                   |Value             |
		|Property Ownership      |1                 |
		|Type Of Residence       |1                 |
		|Year Of Construction    |2017              |
		|Property Type           |1                 |
		|Burglar Alarm           |2                 |
		|External Construction   |2                 |
		
 And verify below details are populated in Risk information for Risk Location "1" 
	    |Field                   |Value             |
		|Property Ownership      |1                 |
		|Type Of Residence       |1                 |
		|Year Of Construction    |2017              |
		|Property Type           |1                 |
		|Burglar Alarm           |2                 |
		|External Construction   |2                 |
		
		
@Sprint-5		
Scenario: Verify Coverage Information are retained in indicative quote page after during "Edit"
 And enter details for Risk Location as per test data "testflow4" and save quote reference number
 And click SaveAndExit
 And click on "btnSave" button in ebusiness portal
 And click on "btnEdit" button in ebusiness portal
 And click edit risk details 
 And verify below details are populated in Coverage information for Risk Location "0" for property type "House"
	    |Field                   |Value           |
		|Building Sum Insured    |100,000         |
		|Content Sum Insured     |50,000          |
	    |Deductible              |500             |
 And verify below details are populated in Coverage information for Risk Location "1" for property type "House"
	    |Field                   |Value           |
		|Building Sum Insured    |100,000         |
		|Content Sum Insured     |50,000          |
	    |Deductible              |500             |

@Sprint-5	
Scenario: Verify Risk Information are retained in indicative quote page after during "Edit"
 And enter details for Risk Location as per test data "testflow4" and save quote reference number
 And click SaveAndExit
 And click on "btnSave" button in ebusiness portal
 And click on "btnEdit" button in ebusiness portal
 And click edit risk details 
 And verify below details are populated in Risk information for Risk Location "0" 
	    |Field                   |Value             |
		|Property Ownership      |1                 |
		|Type Of Residence       |1                 |
		|Year Of Construction    |2017              |
		|Property Type           |1                 |
		|Burglar Alarm           |2                 |
		|External Construction   |2                 |
		
 And verify below details are populated in Risk information for Risk Location "1" 
	    |Field                   |Value             |
		|Property Ownership      |1                 |
		|Type Of Residence       |1                 |
		|Year Of Construction    |2017              |
		|Property Type           |1                 |
		|Burglar Alarm           |2                 |
		|External Construction   |2                 |
		
@CI	
@Sprint-5			
Scenario: Verify Coverage Information are retained in indicative quote page after during "Edit"
 And enter details for Risk Location as per test data "testflow4" and save quote reference number
 And click SaveAndExit
 And click on "btnSave" button in ebusiness portal
 And click on "btnEdit" button in ebusiness portal
 And click edit risk details 
 And verify below details are populated in Coverage information for Risk Location "0" for property type "House"
	    |Field                   |Value           |
		|Building Sum Insured    |100,000         |
		|Content Sum Insured     |50,000          |
	    |Deductible              |500             |
 And verify below details are populated in Coverage information for Risk Location "1" for property type "House"
	    |Field                   |Value           |
		|Building Sum Insured    |100,000         |
		|Content Sum Insured     |50,000          |
	    |Deductible              |500             |

@Sprint-5	
Scenario: Verify Risk Information are retained in indicative quote page after during "Edit"
 And enter details for Risk Location as per test data "testflow4" and save quote reference number
 And click SaveAndExit
 And click on "btnSave" button in ebusiness portal
 And click on "btnEdit" button in ebusiness portal
 And click edit risk details 
 And verify below details are populated in Risk information for Risk Location "0" 
	    |Field                   |Value             |
		|Property Ownership      |1                 |
		|Type Of Residence       |1                 |
		|Year Of Construction    |2017              |
		|Property Type           |1                 |
		|Burglar Alarm           |2                 |
		|External Construction   |2                 |
		
 And verify below details are populated in Risk information for Risk Location "1" 
	    |Field                   |Value             |
		|Property Ownership      |1                 |
		|Type Of Residence       |1                 |
		|Year Of Construction    |2017              |
		|Property Type           |1                 |
		|Burglar Alarm           |2                 |
		|External Construction   |2                 |

@Sprint-5			
Scenario: Verify Coverage Information are retained in indicative quote page after during "Edit"
 And enter details for Risk Location as per test data "testflow4" and save quote reference number
 And click SaveAndExit
 And click on "btnSave" button in ebusiness portal
 And click on "btnEdit" button in ebusiness portal
 And click edit risk details 
 And verify below details are populated in Coverage information for Risk Location "0" for property type "House"
	    |Field                   |Value           |
		|Building Sum Insured    |100,000         |
		|Content Sum Insured     |50,000          |
	    |Deductible              |500             |
 And verify below details are populated in Coverage information for Risk Location "1" for property type "House"
	    |Field                   |Value           |
		|Building Sum Insured    |100,000         |
		|Content Sum Insured     |50,000          |
	    |Deductible              |500             |

@Sprint-5	
Scenario: Verify Calculated Premium table is retained in indicative quote page after during "Edit"
 And enter details for Risk Location as per test data "testflow4" and save quote reference number
 And click on "Calculate Premium"
 And "save" calculated premium
 And "verify" calculated premium
