Feature: EP2-77-As a Broker I want to manage Building Coverage Details


Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
	And click I would like an indicative quotation 
	And enter details for Risk Location as per test data "testflow4" and save quote reference number
	And click on Continue Full Quote
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
	And click Next in policy information page 

@CI
@Sprint-5	
Scenario: Verify Broker is able to update the details for "Contents Coverage" for Risk Location "0"
	And click on House and Contents for Risk Location "0"
	And "edit" Building coverage for Risk Location "0"
	|Field                      |Value        |
	|Building Sum Insured       |550,000      |
	|Building Deductible        |$250         |
	|Other Permanent Structures |165,000      |
	And "verify" Building coverage for Risk Location "0"
	|Field                      |Value        |
	|Building Sum Insured       |550,000      |
	|Building Deductible        |250          |
	|Other Permanent Structures |165,000      |

@Sprint-5	
Scenario: Verify Broker is able to update the details for "Contents Coverage" for Risk Location "1"
	And click on House and Contents for Risk Location "1"
	And "edit" Building coverage for Risk Location "1"
	|Field                      |Value        |
	|Building Sum Insured       |550,000      |
	|Building Deductible        |$250         |
	|Other Permanent Structures |170,000      |
	And "verify" Building coverage for Risk Location "1"
	|Field                      |Value        |
	|Building Sum Insured       |550,000      |
	|Building Deductible        |250          |
	|Other Permanent Structures |165,000      |	

#Outof Scope Now
#Scenario: Verify "Extended Replacement Cost Type" and "Business Property Coverage" for location "0"
#	And click on House and Contents for Risk Location "0"
#	And select Home Business Property Coverage as "Yes" and Home Business Property Sum Insured as "1000" for risk location "0"
#    And select Home Business Property Coverage as "No" and Home Business Property Sum Insured as "" for risk location "0"
#
#Scenario: Verify "Extended Replacement Cost Type" and "Business Property Coverage" for location "1"
#	And click on House and Contents for Risk Location "1"
#	And select Home Business Property Coverage as "Yes" and Home Business Property Sum Insured as "1000" for risk location "1"
#    And select Home Business Property Coverage as "No" and Home Business Property Sum Insured as "1000" for risk location "1"
