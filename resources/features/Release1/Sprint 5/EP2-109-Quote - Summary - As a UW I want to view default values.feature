Feature: EP2-109-Quote - Summary - As a UW I want to view default values

# Background: 
#	Given eBusines portal is launched
#    And login eBusiness portal with "testflow1"
#	And click on Add new Customer
#    And enter new customer details
#    And save Customer Id
#    And click on Add a New Quote in Add new Customer page
#    And enter identification details in "testflow5"
#	And click add risk details 


@manual
Scenario: Verify Changes to Deductible and Policy Commission Rate in Premium Summary is reflected after Save & Exit for UW
 And click I would like an indicative quotation 
 And enter details for Risk Location as per test data "testflow5" and save quote reference number
 And click on "Calculate Premium"
 And click on Continue Full Quote
 And click on update client button in Policy Information page
When client details are edited
And click on "Submit"
And edit "1" Named Insured as per "testflow1"
 And select current insurer "Bank Insurance" 
 And change broker contact
	|Field     |Value   |
	|contact   |Krishna |
 And click Next in policy information page 
 And click "btnProceedNext" by id
 
 #EP3-191 Changes	

And click on "btnProceedNext" button
And click on "btnProceedNext" button
	
 And enter details in loss History page as per "testflow1" 
 And click on "Calculate Premium"
 And click on "Add Quote Option"
And edit policy commission rate "5.0"
 And click on "Calculate"
 And click on "Save and Exit"
 And Verify Quote status is "Incomplete"
 And click on "btnSave" button in ebusiness portal
 Given evolution application is launched
 And select search type as "Policy/Quote #"                                                                                               
 And search saved indicative quote number "testflow5"
 And click on "Quote Summary"
 And verify new deductible "$2,500" is displayed

 
 
 
 
    