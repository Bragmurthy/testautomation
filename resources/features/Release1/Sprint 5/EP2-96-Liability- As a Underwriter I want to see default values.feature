Feature: EP2-96-Liability- As a Underwriter I want to see default values

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
		
@Sprint-5
Scenario: Verify Liability in Underwriter View 
 And click I would like an indicative quotation 
 And enter details for Risk Location as per test data "testflow4" and save quote reference number
 And click on Continue Full Quote
 And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
	And Save Quote Reference Number in "US_96_SC1"
	And click on "Save and Exit"
    And click on "btnSave" button in ebusiness portal
   	Given evolution SIT application is launched 
    And select search type as "Policy/Quote #" 
    And search saved indicative quote number "US_96_SC1"
    And click on "Liability"
    And verify default values in Liability and sum insured is "30,000,000"
