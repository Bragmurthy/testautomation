Feature: EP2-80 As a Broker I want to manage VAC details input.feature

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details

@CI
Scenario: Verify VAC added in indicative quote is displayed in Risk and coverages for Risk Location "0"
And click I would like an indicative quotation 
And enter details for Risk Location as per test data "testflow5" and save quote reference number
And click on Continue Full Quote
And click on update client button in Policy Information page
When client details are edited
And click on "Submit"
And Wait for "1" minutes
And edit "1" Named Insured as per "testflow1"
And select current insurer "Bank Insurance" 
And change broker contact
|Field     |Value|
|contact       |Krishna |
And click Next in policy information page 
And enter building sum insured as "444654" and verify user is able to select below valuable category from drop down for location "0"
|VAC               |Value             |
|Jewellery         |100               |
|Jewellery In Vault|100               |
|Furs              |100               |
|Fine Arts         |100               |
|Silverware        |100               |
|Stamps            |100               |
|Collectibles      |100               |
|Musical Items     |100               |
|Wine              |100               |
|Cameras           |100               |
And click on Continue Full Quote
And click on "Risks / Coverages"
And click on VAC for Risk Location "0"
And verify VAC display in Risk and Coverages
|VAC               |Value             |
|Cameras           |100               |
|Collectibles      |100               |
|Fine Arts         |100               |
|Furs              |100               |
|Jewellery         |100               |
|Jewellery In Vault|100               |
|Musical Items     |100               |
|Silverware        |100               |
|Stamps            |100               |
|Wine              |100               |

Scenario: Verify VAC added in indicative quote is displayed in Risk and coverages for Risk Location "1"
And click I would like an indicative quotation 
And enter details for Risk Location as per test data "testflow4" and save quote reference number
And enter building sum insured as "444654" and verify user is able to select below valuable category from drop down for location "1"
|VAC               |Value             |
|Jewellery         |100               |
|Jewellery In Vault|100               |
|Furs              |100               |
|Fine Arts         |100               |
|Silverware        |100               |
|Stamps            |100               |
|Collectibles      |100               |
|Musical Items     |100               |
|Wine              |100               |
|Cameras           |100               |
And click on Continue Full Quote
And click on "Risks / Coverages"
And click on VAC for Risk Location "1"
And verify VAC display in Risk and Coverages
|VAC               |Value             |
|Cameras           |100               |
|Collectibles      |100               |
|Fine Arts         |100               |
|Furs              |100               |
|Jewellery         |100               |
|Jewellery In Vault|100               |
|Musical Items     |100               |
|Silverware        |100               |
|Stamps            |100               |
|Wine              |100               |

@Sprint-5
Scenario: As a Broker be able to add  VAC for Risk Location "0" through Full quote option
	And click I would like an full quotation
	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field         |Value   |
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "testflow7"
	And enter house and contents details as per "testflow1" and "10" VAC for that location "0" as per below data for "broker"
	    |Value Added Coverage |
        |testflow1            |
        |testflow5            |
        |testflow4            |
        |testflow2            |
        |testflow3            |
        |testflow9            |
        |testflow10           |
        |testflow11           |
        |testflow12           |
        |testflow13           |

@Sprint-5
Scenario: As a Broker be able to add other VAC for Risk Location "0" through Full quote option
	And click I would like an full quotation 
	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field         |Value   |
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "testflow7"
	And enter house and contents details as per "testflow1" and "2" VAC for that location "0" as per below data for "broker"
	    |Value Added Coverage |
        |testflow2            |
        |testflow3            |

   