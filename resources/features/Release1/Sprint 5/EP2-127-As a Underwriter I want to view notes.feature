Feature: EP2-127-As a Underwriter I want to view notes

#Background: 
#	Given eBusines portal is launched 
#    And login eBusiness portal with "testflow1"
#	And click on Add new Customer
#    And enter new customer details
#    And save Customer Id
#    And click on Add a New Quote in Add new Customer page
#    And enter identification details in "testflow5"

@Manual		
Scenario: As a Underwriter I want to view notes 
	And click add risk details 
	And click I would like an full quotation 
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "testflow7"
	And enter house and contents details as per "testflow1" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |testflow3            |
	And click "btnProceedNext" by id
	And select "claimsHistoryFlag" as "No"
 	And select "bankruptcyFiled" as "No"
 	And select "insuranceRefusalCancelRejected" as "No"
 	And select "chargeConvicted" as "No"
    And Save Quote Reference Number in "US_127_SC1"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    And click on "btnSave" button in ebusiness portal
    And click on "Submit Referral" in ebusiness portal
    And accept the alert
    And approve quote in SIT with quote number saved in "US_127_SC1"
    #And approve quote with quote number saved in "US_127_SC1"
    Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
    And click on "Find quote"
    And search for quote reference number from "US_127_SC1"
    And click on "View"
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
    And click Next in policy information page 
    And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_127_SC1"
	And click on Premium Summary
    And click on "Finish"
    And click on "btnSave" button in ebusiness portal
    Given evolution application is launched
   	And select search type as "Policy/Quote #"                                                                                               
   	And search saved indicative quote number "US_127_SC1"
    And Verify "Notes" is displayed in side navigation