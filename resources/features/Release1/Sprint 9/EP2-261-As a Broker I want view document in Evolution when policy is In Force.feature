Feature: EP2-261-As a Broker I want view document in Evolution when policy is In Force
	
Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
  	And click I would like an full quotation
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And Wait for "1" minutes
	And edit "1" Named Insured as per "testflow1"
	And search address "9 ankuri road, Tarneit, Victoria, Australia" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "testflow7"
	And enter house and contents details as per "testflow1" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |testflow3            |
   	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_261_SC1"
	
	# EP3-191 changes Adding next 2 times
	And click "btnProceedNext" by id
	And click "btnProceedNext" by id
	
	And select "claimsHistoryFlag" as "No"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    And click on "btnSave" button in ebusiness portal
    And Save the Our Ref Number in "US_261_SC1"
    And click on "Submit Referral" in ebusiness portal
    And accept the alert
    And approve quote in SIT with quote number saved in "US_261_SC1"
    Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
    And click on "Find quote"
    And search for quote our reference number from "US_261_SC1"
    And click on "View"
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
    And click on Premium Summary
    And click on "Finish"  
	And click on "btnSave" button in ebusiness portal
	
	@Sprint-9    
	Scenario: As a Broker I want to view Documents generation when Quote offered
	And click view risk details
    And click on "Policy Documents"
	And Select Quotation
	And Select Record of Answers 
	And click on "Generate Document"
	And Wait for "2" minutes
	And click on "Policy Information"
	And Wait for "1" minutes
	And click on "Exit"
  
    And click on "btnConvert" button in ebusiness portal
    And click on "Convert Risk Details" in ebusiness portal
 	And Wait for "1" minutes
 	
 	#EP3-191 Changes

	And click on "Premium Summary"
	And click on "Finish Quote"    
	
# 	And click on "Binding"
#    And verify user is able to select deductible option for "$500"
#    And select confirmation checkbox
#    And Save Quote Reference Number in "US_261_SC1"
#    And click on "Proceed To Bind"    

    And click on "Convert Risk Details" in ebusiness portal
    And Wait for "1" minutes
    And click on "Policy Documents"
    And Select New Business Policy and Cover Letter checkbox
    And Select Record of Answers 
    And Select Certificate of Currency radio button
    And click on "Generate Document"
    And Wait for "2" minutes
    And click on "Policy Information"
	And Wait for "1" minutes
	And click on "Exit"
	And click on "btnSaveAndAccept" button in ebusiness portal
	And Save Policy Number in "US_261_SC1"
#	Given evolution SIT application is launched
#	And select search type as "Policy/Quote #"
#	And search saved policy number "US_261_SC1"                                                                                               
#	And click on "Closing"
#    And close the quote as UW "testflow4"
#    And click "btnBookAndIssue" by id
#    And wait for Document Generation
#    And click "btnProceed" by id
    And Wait for "5" minutes

    Given eBusines portal is launched    
    And login eBusiness portal with "testflow1"
    And click on "Find policy"
    And search for policy reference number from Ebix "US_261_SC1"
    And click View in Ebix policy page
    And click view risk details
    And click on "Policy Documents"
    And Select New Business Policy and Cover Letter checkbox
    And Select Certificate of Currency radio button
    And click on "Generate Document"
    And Wait for "2" minutes
    