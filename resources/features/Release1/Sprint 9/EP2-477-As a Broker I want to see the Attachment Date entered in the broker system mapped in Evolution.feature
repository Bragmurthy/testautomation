Feature: EP2-477-As a Broker I want to see the Attachment Date entered in the broker system mapped in Evolution

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
       
 @Sprint-9
Scenario: As a Broker I want to see the Attachment Date entered As Start date in Evolution

    And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
  	And click I would like an full quotation
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And Wait for "1" minutes
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "42 kambara drive Mulgrave" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page

	And "add" risk location "0" and enter risk information details as per "US_477_SC1"
	And enter house and contents details as per "US_477_SC1" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |US_477_SC1           |
   	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_477_SC1"
	
	# EP3-191 changes Adding next 2 times
	And click "btnProceedNext" by id
	And click "btnProceedNext" by id
	
	And select "claimsHistoryFlag" as "No"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    And click on "btnSave" button in ebusiness portal
    And Save the Our Ref Number in "US_477_SC1"
    And click on "Submit Referral" in ebusiness portal
    And accept the alert
    And approve quote in SIT with quote number saved in "US_477_SC1"
    Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
    And click on "Find quote"
    And search for quote our reference number from "US_477_SC1"
    And click on "View"
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
	And click on Premium Summary
	And click on "Finish"
	And click on "btnSave" button in ebusiness portal
	And click on "Convert To New Business" in ebusiness portal
	And click on "Convert Risk Details" in ebusiness portal
	And Wait for "1" minutes
	
	#EP3-191 Changes

	And click on "Premium Summary"
	And click on "Finish Quote"    
    
#	And click on "Binding"
#    And verify user is able to select deductible option for "$500"
#    And select confirmation checkbox
#    And Save Quote Reference Number in "US_477_SC1"
#    And click on "Proceed To Bind"

    And click on "btnSaveAndAccept" button in ebusiness portal
    And Save Policy Number in "US_477_SC1"
    And wait for "2" minutes
    Given evolution SIT application is launched
    
#	And select search type as "Policy/Quote #"
#	And search saved policy number "US_477_SC1"                                                                                               
#	And click on "Closing"
#    And close the quote as UW "US_477_SC1"
#    And click "btnBookAndIssue" by id
#    And wait for Document Generation
#    And click "btnProceed" by id
    And search saved policy number "US_477_SC1"
    And verify effective date from policy admin as below
  
@Manual
Scenario: As a Broker I want to see validation error messages in Sunrise as Please update the Policy Attachment Date' is < the current system date

	And click on "Find policy"
    And search for policy reference number from Ebix "US_477_SC1"
    And click View in Ebix policy page
	And click on "btnModify" button in ebusiness portal

 	And Update Attachment Date in ebusiness portal to Previous date
 	And click on "Modify Risk Details" in ebusiness portal
 	And get alert text for attachment date prior to start date
	