Feature: EP2-113-As a Broker I want to Edit an Indicative Quote after duplicate client override by underwriter

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add a New Quote 
	And enter new Quote details as below 
		|Field           |Value                                   |
		|Client ID       |EPTS9                                   |
		|Product         |Chubb Masterpiece Australia SIT         |
		|Insured ID      |EPTS9                                   |
		|Start Date      |30 Apr 2018                             |
		|End Date        |01 Apr 2019                             |
		|Branch          |Melbourne                               |
		
# below is the method to update the STart date to Current system date, Didn't remove the data table to select the existing client to get duplicate client pop-up message

	And Update Start Date in ebusiness portal to current system date
	And click add risk details
	
@CI	
@Sprint-9
	Scenario: As a UW I want to validate view duplicate clients button
	 
	And click I would like an indicative quotation
	And enter details for Risk Location as per test data with no google address picker "US_113_SC1" and save quote reference number
	And click on "Calculate Premium"
	And click on "OK" button in referal dialogbox
	And click on "btnSave" button in ebusiness portal
	Given evolution SIT application is launched
	And select search type as "Policy/Quote #"                                                                                               
	And search quote number "US_113_SC1"
	And validate "View Duplicate Clients" field must be displayed
	And click "btnViewDuplicateClient" by id
	And select possible duplicate client
	And click on "Use Selected Client" button in referal dialogbox
	And release lock for editing
	Given eBusines portal is launched 
	And login eBusiness portal with "testflow1"
	And click on "Find quote"
	And search for quote reference number from "US_113_SC1"
	And click on "View"
	And click on "btnEdit" button in ebusiness portal
	And click edit risk details
	And verify below details are populated in Risk information for Risk Location "0"
         |Field	               |Value             |
         |Property Ownership   |1                 |
         |Type Of Residence	   |1                 |
         |Year Of Construction |2017              |
         |Property Type	       |1                 |
         |Burglar Alarm	       |2                 |
         |External Construction|2                 |
And verify below details are populated after address search for Risk Location "0" 
   	    |Field         |Value                     |
   	    |Unit No	   |                          |
   	    |Street No	   |45                        |
   	    |Street Name   |Ferozpuru Road            |
   	    |Building Name |                          |
   	    |Suburb/City   |Ludhianaaa                |
   	    |Post Code	   |1422                      |
   	    |Country	   |Australia                 |
   	    |State/Province|80000002                  |
And click on "Calculate Premium"
And click on Continue Full Quote
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
And select current insurer "Bank Insurance" 
And change broker contact
	|Field     |Value   |
	|contact   |Krishna |
And click Next in policy information page
