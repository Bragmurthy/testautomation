Feature: EP2-35-As a Broker I want to Decline a Masterpiece transaction

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
    
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details 
  	And click I would like an full quotation
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "42 kambara drive Mulgrave" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page
    
@CI
@Sprint-9  	
Scenario: As a Broker I want to Decline a Masterpiece transaction for Incomplete quote

   	And "add" risk location "0" and enter risk information details as per "US_35_SC1"
	And enter house and contents details as per "US_35_SC1" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |US_35_SC1            |
   	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_35_SC1"
	
	# EP3-191 changes Adding next 2 times
	And click "btnProceedNext" by id
	And click "btnProceedNext" by id
	
	And select "claimsHistoryFlag" as "No"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"
   And click on "Save and Exit"
    
    
    And click on "btnSave" button in ebusiness portal
    And click on "btnDelete" button in ebusiness portal
    And accpet the alert for delete from Ebusiness portal
        
     Given evolution SIT application is launched 
	 And select search type as "Policy/Quote #" 
	 And search saved indicative quote number "US_35_SC1"
     And verify quote status is "Quote Rejected"

@Sprint-9
Scenario: As a Broker I want to Decline a Masterpiece transaction for complete quote

	 
   	And "add" risk location "0" and enter risk information details as per "US_35_SC2"
	And enter house and contents details as per "US_35_SC2" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |US_35_SC2            |
   	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_35_SC2"
	
	# EP3-191 changes Adding next 2 times
	And click "btnProceedNext" by id
	And click "btnProceedNext" by id
	
	And select "claimsHistoryFlag" as "No"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    And click on "btnSave" button in ebusiness portal
    And Save the Our Ref Number in "US_35_SC2"
    And click on "Submit Referral" in ebusiness portal
    And accept the alert
    And approve quote in SIT with quote number saved in "US_35_SC2"
    Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
    And click on "Find quote"
    And search for quote our reference number from "US_35_SC2"
    And click on "View"
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
	And click on Premium Summary
	And click on "Finish"
	And click on "btnSave" button in ebusiness portal
    And click on "btnDelete" button in ebusiness portal
    And accpet the alert for delete from Ebusiness portal
   
     Given evolution SIT application is launched 
	 And select search type as "Policy/Quote #" 
	 And search saved indicative quote number "US_35_SC2"
     And verify quote status is "Quote Rejected"
     
#@Sprint-9 
#    Scenario: As a Broker I want to Decline a Masterpiece transaction for converted quote
#
#   	And "add" risk location "0" and enter risk information details as per "US_35_SC3"
#	And enter house and contents details as per "US_35_SC3" and "1" VAC for that location "0" as per below data for "broker"
#        |Value Added Coverage |
#        |US_35_SC3           |
#   	And click "btnProceedNext" by id
#	And Save Quote Reference Number in "US_35_SC3"
#	And select "claimsHistoryFlag" as "No"
#	And select "insuranceRefusalCancelRejected" as "No"
#	And select "chargeConvicted" as "No"
#    And select "bankruptcyFiled" as "No"
#    And click on "Calculate Premium"
#    And click on "Proceed with referral" button in referal dialogbox
#    And click on "btnSave" button in ebusiness portal
#    And Save the Our Ref Number in "US_35_SC3"
#    And click on "Submit Referral" in ebusiness portal
#    And accept the alert
#    And approve quote in SIT with quote number saved in "US_35_SC3"
#    Given eBusines portal is launched 
#    And login eBusiness portal with "testflow1"
#    And click on "Find quote"
#    And search for quote our reference number from "US_35_SC3"
#    And click on "View"
#    And click on "btnEdit" button in ebusiness portal
#    And click edit risk details
#	And click on Premium Summary
#	And click on "Finish"
#	And click on "btnSave" button in ebusiness portal
#	And click on "Convert To New Business" in ebusiness portal
#	And click on "Convert Risk Details" in ebusiness portal
#	And Wait for "1" minutes
#    And click on "Binding"
#    And verify user is able to select deductible option for "$500"
#    And select confirmation checkbox
#    And click on "Proceed To Bind"
#    And click on "btnSaveAndAccept" button in ebusiness portal
#    And click on "Find quote"
#    And search for quote our reference number from "US_35_SC3"
#    And click on "View"
#    And click on "btnDelete" button in ebusiness portal
#    And accpet the alert for delete from Ebusiness portal
#    
#     Given evolution SIT application is launched 
#	 And select search type as "Policy/Quote #" 
#	 And search saved indicative quote number "US_35_SC3"
#     And verify quote status is "Quote Rejected"    