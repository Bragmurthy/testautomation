Feature: EP2-28-As a Broker I want to see validation error messages in Sunrise

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
    
	
 @Sprint-9  	
Scenario: As a Broker I want to see validation error messages in Sunrise as Policy period must be between < nine or > twelve months

   	And click on Add a New Quote 
	And enter new Quote details as below 
		|Field         |Value                       |
		|Client ID     |3DXC28                        |
		|Product       |Chubb Masterpiece Australia SIT |
		|Insured ID    |3DXC28                        |
		|Start Date    |01 Apr 2018                             |
		|End Date      |01 Jun 2018               |
		|Branch        |Melbourne                   |
    And click add risk details
    And validate "policy date" error message in ebusiness portal

@Invalid(@Sprint-9)
Scenario: As a Broker I want to see validation error messages in Sunrise as Please update the Policy Effective Date to today's date or later before proceeding to Bind
     And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
    And Update Start Date in ebusiness portal to previous date 
	And click add risk details 
  	And click I would like an full quotation
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And Wait for "1" minutes
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "42 kambara drive Mulgrave" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page

	And "add" risk location "0" and enter risk information details as per "US_28_SC2"
	And enter house and contents details as per "US_28_SC2" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |US_28_SC2           |
   	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_28_SC2"
	And select "claimsHistoryFlag" as "No"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    And click on "btnSave" button in ebusiness portal
    And Save the Our Ref Number in "US_28_SC2"
    And click on "Submit Referral" in ebusiness portal
    And accept the alert
    And approve quote in SIT with quote number saved in "US_28_SC2"
    Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
    And click on "Find quote"
    And search for quote our reference number from "US_28_SC2"
    And click on "View"
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
	And click on Premium Summary
	And click on "Finish"
	And click on "btnSave" button in ebusiness portal
	And click on "Convert To New Business" in ebusiness portal
	And click on "Convert Risk Details" in ebusiness portal
	And Wait for "1" minutes
	And validate "policy start date" error message in ebusiness portal
    
    @Manual
    Scenario: As a Broker I want to see validation error messages in Sunrise as  Quote valid days expired, please re-edit insurer product.  Quote Valid Days = "45"

    And click on Add a New Quote 
	And enter new Quote details as below 
		|Field         |Value                       |
		|Client ID     |3DXC28                        |
		|Product       |Chubb Masterpiece Australia SIT |
		|Insured ID    |3DXC28                        |
		|Start Date    |01 Jan 2018	                 |
		|End Date      |01 Jan 2019                 |
		|Branch        |Melbourne                   |
	And enter policy start date as below
	|Field         |Value                       |
	|policy start date    |01 Jan 2018          |
	And click add risk details 
  	And click I would like an full quotation
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "42 kambara drive Mulgrave" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page

	And "add" risk location "0" and enter risk information details as per "US_28_SC3"
	And enter house and contents details as per "US_28_SC3" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |US_35_SC3           |
   	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_28_SC3"
	And select "claimsHistoryFlag" as "No"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    And click on "btnSave" button in ebusiness portal
    And Save the Our Ref Number in "US_28_SC3"
    And click on "Submit Referral" in ebusiness portal
    And accept the alert
    And approve quote in SIT with quote number saved in "US_28_SC3"
    Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
    And click on "Find quote"
    And search for quote our reference number from "US_28_SC3"
    And click on "View"
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
	And click on Premium Summary
	And click on "Finish"
	And click on "btnSave" button in ebusiness portal
	And click on "Convert To New Business" in ebusiness portal
	And Wait for "1" minutes
	And click on "Convert Risk Details" in ebusiness portal	 
   And validate "quote validity expire" error message in ebusiness portal
 @Sprint-9
Scenario: As a Broker I want to see validation of evolution binding page
    And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
    And Update Start Date in ebusiness portal to later date
	And click add risk details 
  	And click I would like an full quotation
  	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And Wait for "1" minutes
	And edit "1" Named Insured as per "testflow1"
	And search address with no google address picker "42 kambara drive Mulgrave" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page

	And "add" risk location "0" and enter risk information details as per "US_28_SC4"
	And enter house and contents details as per "US_28_SC4" and "1" VAC for that location "0" as per below data for "broker"
        |Value Added Coverage |
        |US_28_SC4           |
   	And click "btnProceedNext" by id
	And Save Quote Reference Number in "US_28_SC4"
	
	# EP3-191 changes Adding next 2 times
	And click "btnProceedNext" by id
	And click "btnProceedNext" by id
	
	And select "claimsHistoryFlag" as "No"
	And select "insuranceRefusalCancelRejected" as "No"
	And select "chargeConvicted" as "No"
    And select "bankruptcyFiled" as "No"
    And click on "Calculate Premium"
    And click on "Proceed with referral" button in referal dialogbox
    And click on "btnSave" button in ebusiness portal
    And Save the Our Ref Number in "US_28_SC4"
    And click on "Submit Referral" in ebusiness portal
    And accept the alert
    And approve quote in SIT with quote number saved in "US_28_SC4"
    Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
    And click on "Find quote"
    And search for quote our reference number from "US_28_SC4"
    And click on "View"
    And click on "btnEdit" button in ebusiness portal
    And click edit risk details
	And click on Premium Summary
	And click on "Finish"
	And click on "btnSave" button in ebusiness portal
	And click on "Convert To New Business" in ebusiness portal
	And Wait for "1" minutes
	 And click on "Convert Risk Details" in ebusiness portal
	And validate quote offered page