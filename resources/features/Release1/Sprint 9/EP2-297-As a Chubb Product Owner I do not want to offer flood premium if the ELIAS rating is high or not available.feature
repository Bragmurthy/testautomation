Feature: EP2-297-As a Chubb Product Owner I do not want to offer flood premium if the ELIAS rating is high or not available

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
@Sprint-9    
Scenario: As a Chubb Product Owner I do not want to offer flood premium if the ELIAS rating is 3 or more for indicative quote 

    And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details
  	And click I would like an indicative quotation
  	And enter details for Risk Location as per test data "US_297_SC1" and save quote reference number
  	And select "FloodCoverageFlag" as "Yes"
	And click on "Calculate Premium"
	And verify popup for Flood cover not applicable
    And field "PremiumTable" should not be displayed

 @Sprint-9   
Scenario: As a Chubb Product Owner I do not want to offer flood premium if the ELIAS rating is blank for indicative quote  

	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details
  	And click I would like an indicative quotation
  	And enter details for Risk Location as per test data "US_297_SC2" and save quote reference number
  	And select "FloodCoverageFlag" as "Yes"
	And click on "Calculate Premium"
	And verify popup for Flood cover not applicable
    And field "PremiumTable" should not be displayed
    
@CI
@Sprint-9	
Scenario: As a Chubb Product Owner I do not want to offer flood premium if location is QLD and pincode not valid for indicative quote
	
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details
  	And click I would like an indicative quotation
  	And enter details for Risk Location as per test data "US_297_SC3" and save quote reference number
  	And select "FloodCoverageFlag" as "Yes"
	And click on "Calculate Premium"
	And verify popup for Flood cover not applicable
    And field "PremiumTable" should not be displayed
@Sprint-9
Scenario: As a Chubb Product Owner I do not want to offer flood premium if the ELIAS rating is 3 or more full quote

	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details
	And click I would like an full quotation
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address "85 sydenham road marrickville New South Wales 2204" for Risk Location "0"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
   	And "add" risk location "0" and enter risk information details as per "US_297_SC4"
   	And click on "House and Contents"
   	And verify text Flood coverage not available text
   	And verify Flood coverage Radio Buttons are disabled
   	
@Sprint-9   	
Scenario: As a Chubb Product Owner I do not want to offer flood premium if the ELIAS rating is blank for full quote 
		
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details
	And click I would like an full quotation
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address "10 Queensland Road, Murwillumbah, NSW 2484" for Risk Location "0"
	And Change pincode of the risk location as "5000"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page 
  	And "add" risk location "0" and enter risk information details as per "US_297_SC5"
	And click on "House and Contents"
   	And verify text Flood coverage not available text
   	And verify Flood coverage Radio Buttons are disabled
  
@Sprint-9   	
Scenario: As a Chubb Product Owner I do not want to offer flood premium if location is QLD and pincode not valid for full quote
	
	
	And click on Add new Customer
    And enter new customer details
    And save Customer Id
    And click on Add a New Quote in Add new Customer page
    And enter identification details in "testflow5"
	And click add risk details
	And click I would like an full quotation
	And click on update client button in Policy Information page
	When client details are edited
	And click on "Submit"
	And edit "1" Named Insured as per "testflow1"
	And search address "189 Elliott Rd, Banyo QLD 4014, Australia" for Risk Location "0"
	And Change pincode of the risk location as "5000"
 	And select current insurer "Bank Insurance" 
	And change broker contact
	|Field     |Value|
	|contact       |Krishna |
    And click Next in policy information page
	And Save Quote Reference Number in "US_297_SC6"
   	And "add" risk location "0" and enter risk information details as per "US_297_SC6"
    And click on "House and Contents"
   	And verify text Flood coverage not available text
   	And verify Flood coverage Radio Buttons are disabled