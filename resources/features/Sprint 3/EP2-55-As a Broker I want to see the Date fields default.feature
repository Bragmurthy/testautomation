Feature: EP2-55-As a Broker I want to see the Date fields default

Background: 
	Given eBusines portal is launched 
	And enter company id as "IAA00371" 
	And enter user id as "EVOTEST" 
	And enter password "123456" 
	And click login 
	And click on Add a New Quote 
	And enter new Quote details as below 
	    |Field         |Value                       |
		|Client ID     |CUST001                        |
		|Product       |Chubb MasterPiece Australia |
		|Insured ID    |EVO1                        |
		|Start Date    |14 Nov 2017                 |
		|End Date      |14 Nov 2018                 |
		|Branch        |Melbourne                   |
	And click add risk details 
	And click on "I would like a Full Quotation"

Scenario: Navigate to Full Quotation Page and Verify values are prepoulated from  
 Then fields "Effective Date", "Expiry Date" and "Submission Received Date" should default from Sunrise
 And "EffectiveDate", "ExpiryDate" and "SubmissionReceivedDate" fields should be non editable
	