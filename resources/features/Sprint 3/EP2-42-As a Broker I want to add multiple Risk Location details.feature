Feature: EP2-42-As a Broker I want to add multiple Risk Location details 

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add a New Quote 
	And enter new Quote details as below 
		|Field         |Value                       |
		|Client ID     |EVO1                        |
		|Product       |Chubb MasterPiece Australia |
		|Insured ID    |EVO1                        |
		|Start Date    |08 Nov 2017                 |
		|End Date      |08 Nov 2018                 |
		|Branch        |Melbourne                   |
	And click add risk details 
	And click I would like an indicative quotation 
	
Scenario: Enter Risk location details for location 1 
	And enter details for Risk Location as per test data "testflow1" and save quote reference number 
	
Scenario: Enter Risk location details for location 1 and location 2 
	And enter details for Risk Location as per test data "testflow2" and save quote reference number 
