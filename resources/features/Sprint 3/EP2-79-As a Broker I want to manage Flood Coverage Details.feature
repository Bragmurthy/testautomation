Feature: EP2-79-As a Broker I want to manage Flood Coverage Details

Background: 
	Given eBusines portal is launched 
	And enter company id as "IAA00371" 
	And enter user id as "EVOTEST" 
	And enter password "123456" 
	And click login 
	And click on Add a New Quote 
	And enter new Quote details as below 
	    |Field         |Value                       |
		|Client ID     |EVO1                        |
		|Product       |Chubb MasterPiece Australia |
		|Insured ID    |EVO1                        |
		|Start Date    |08 Nov 2017                 |
		|End Date      |08 Nov 2018                 |
		|Branch        |Melbourne                   |
	And click add risk details 
	And click I would like an indicative quotation 

Scenario: Continue to Full quote by entering details in Location 1 and not calculating Premium
	And enter details for Risk Location as per test data "testflow2"
	And click on "Continue Full Quote"
	
