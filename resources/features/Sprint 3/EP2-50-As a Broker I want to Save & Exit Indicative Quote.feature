Feature: EP2-50-As a Broker I want to Save & Exit Indicative Quote 

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add a New Quote 
	And enter new Quote details as below 
		|Field         |Value                       |
		|Client ID     |EVO1                        |
		|Product       |Chubb MasterPiece Australia |
		|Insured ID    |EVO1                        |
		|Start Date    |08 Nov 2017                 |
		|End Date      |08 Nov 2018                 |
		|Branch        |Melbourne                   |
	And click add risk details 
	And click I would like an indicative quotation 
	
Scenario: Verify "Save & Exit" indicative quote functionality after calculating Premiun for Risk location 1 
	And enter details for Risk Location as per test data "testflow1" and save quote reference number
	And click on "Calculate Premium" 
	And click on "Save & Exit" 
	Given eBusines portal is launched 
	And login eBusiness portal with "testflow1"
	And click on find Quote
	And search for quote reference number from "testflow1" and verify status of quote is "Incomplete"
	
	