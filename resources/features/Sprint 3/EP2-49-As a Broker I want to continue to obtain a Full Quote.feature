Feature: EP2-49-As a Broker I want to continue to obtain a Full Quote

Background: 
	Given eBusines portal is launched 
    And login eBusiness portal with "testflow1"
	And click on Add a New Quote 
	And enter new Quote details as below 
	    |Field         |Value                       |
		|Client ID     |EVO1                        |
		|Product       |Chubb MasterPiece Australia |
		|Insured ID    |EVO1                        |
		|Start Date    |08 Nov 2017                 |
		|End Date      |08 Nov 2018                 |
		|Branch        |Melbourne                   |
	And click add risk details 
	And click I would like an indicative quotation 

Scenario: Continue to Full quote by entering details in Location 1 and not calculating Premium
	And enter details for Risk Location as per test data "testflow1" and save quote reference number 
	And click on "Continue Full Quote"
	And verify URL is "http://apevodweb121.aceins.com/policy-info/edit"    
	
Scenario: Continue to Full quote by entering details in Location 1 and 2 and not calculating Premium
	And enter details for Risk Location as per test data "testflow2" and save quote reference number 
	And click on "Continue Full Quote"
	And verify URL is "http://apevodweb121.aceins.com/policy-info/edit"    
	
Scenario: Continue to Full quote by entering details in Location 1 after calculating Premium
	And enter details for Risk Location as per test data "testflow1" and save quote reference number 
	And click on "Calculate Premium"
	And click on "Continue Full Quote"
	And verify URL is "http://apevodweb121.aceins.com/policy-info/edit"    
	
Scenario: Continue to Full quote by entering details in Location 1 and 2 after calculating Premium
	And enter details for Risk Location as per test data "testflow2" and save quote reference number 
	And click on "Calculate Premium"
	And click on "Continue Full Quote"
	And verify URL is "http://apevodweb121.aceins.com/policy-info/edit"    
	