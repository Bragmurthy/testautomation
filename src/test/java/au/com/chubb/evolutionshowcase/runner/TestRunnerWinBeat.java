package au.com.chubb.evolutionshowcase.runner;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

/**
 * Created by Bragadeesh Murthy
 */

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = {

		"./resources/features/Release1/Sprint 11/EP2-260-As a Broker I want to access Evolution application through WINBEAT.feature"

},

		plugin = { "json:target/cucumber-parallel/1.json", "html:target/cucumber-parallel/1.html", "pretty" }, glue = {
				"au.com.chubb.evolutionshowcase.pages.stepDefinitions",
				"au.com.chubb.evolutionshowcase.hooks" }, tags = { "@Sprint-11" }, monochrome = true

)
public class TestRunnerWinBeat {

}
