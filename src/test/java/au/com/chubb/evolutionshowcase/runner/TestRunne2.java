package au.com.chubb.evolutionshowcase.runner;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

/**
 * Created by Bragadeesh Murthy
 */

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = {

		"./resources/features/Release1/Sprint 2/EP2-26-As a Broker I want to initiate a Masterpiece Quote or Policy from my broker system.feature",
		
		"./resources/features/Release1/Sprint 2/EP2-40-As a Broker I want the ability to choose type of Quote.feature",
		"./resources/features/Release1/Sprint 2/EP2-50-As a Broker I want to Save & Exit Indicative Quote.feature"
		

},

		plugin = { "json:target/cucumber-parallel/1.json", "html:target/cucumber-parallel/1.html", "pretty" }, glue = {
				"au.com.chubb.evolutionshowcase.pages.stepDefinitions",
				"au.com.chubb.evolutionshowcase.hooks" }, tags = { "@Sprint-2" }, monochrome = true

)
public class TestRunne2 {

}
