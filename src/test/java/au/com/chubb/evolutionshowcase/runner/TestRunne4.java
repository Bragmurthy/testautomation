package au.com.chubb.evolutionshowcase.runner;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

/**
 * Created by Bragadeesh Murthy
 */

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = {

		"./resources/features/Sprint 2/EP2-39-As a Broker I want to obtain a Indicative Quote (for Default Deductible only).feature"

},

		plugin = { "json:target/cucumber-parallel/1.json", "html:target/cucumber-parallel/1.html", "pretty" }, glue = {
				"au.com.chubb.evolutionshowcase.pages.stepDefinitions",
				"au.com.chubb.evolutionshowcase.hooks" }, tags = { "@Sprint-2" }, monochrome = true

)
public class TestRunne4 {

}
