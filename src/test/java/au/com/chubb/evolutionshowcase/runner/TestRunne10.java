package au.com.chubb.evolutionshowcase.runner;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

/**
 * Created by Bragadeesh Murthy
 */

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = {

		"./resources/features/Release1/Sprint 10/Ep2-561-As a product owner I want to see the latest Privacy Statement updated in Evolution.feature"

},

		plugin = { "json:target/cucumber-parallel/1.json", "html:target/cucumber-parallel/1.html", "pretty" }, glue = {
				"au.com.chubb.evolutionshowcase.pages.stepDefinitions",
				"au.com.chubb.evolutionshowcase.hooks" }, tags = { "@Sprint-10" }, monochrome = true

)
public class TestRunne10 {

}
