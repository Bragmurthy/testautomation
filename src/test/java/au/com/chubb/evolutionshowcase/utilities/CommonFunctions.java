package au.com.chubb.evolutionshowcase.utilities;

import java.io.FileInputStream;
import java.util.Properties;
import net.thucydides.core.pages.PageObject;

public class CommonFunctions extends PageObject {

	public Properties ObjectRepository = new Properties();
	public Properties configprop = new Properties();

	String root = System.getProperty("user.dir");

	// *****Common Functions for the
	// application***************************************/
	public void launch_eBusiness_portal_app() {

		TestSettings ts = TestSettings.getInstance();
		Properties config = ts.getConfigProp();
		String thisURL = config.getProperty("eBusinessPortal_TEST");
		System.out.println("URL to launch " + thisURL);
		getDriver().get(thisURL);

	}
	
	
	public void launch_evolution_app() throws InterruptedException {

		TestSettings ts = TestSettings.getInstance();
		Properties config = ts.getConfigProp();
		String thisURL = config.getProperty("BaseURL_TEST");
		System.out.println("URL to launch " + thisURL);
		getDriver().get(thisURL);
		Thread.sleep(2000);

	}
	public void launch_evolution_app_in_SIT() throws InterruptedException {

		TestSettings ts = TestSettings.getInstance();
		Properties config = ts.getConfigProp();
		String thisURL = config.getProperty("SIT_Evolution_URL");
		System.out.println("URL to launch " + thisURL);
		getDriver().get(thisURL);
		Thread.sleep(15000);

	}
	
	public void launch_SITWorkManager() {

		TestSettings ts = TestSettings.getInstance();
		Properties config = ts.getConfigProp();
		String thisURL = config.getProperty("SIT_EvolutionWorkManager_URL");
		System.out.println("URL to launch " + thisURL);
		getDriver().get(thisURL);
	}

}
