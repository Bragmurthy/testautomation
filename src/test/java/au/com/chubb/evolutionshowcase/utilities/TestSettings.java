package au.com.chubb.evolutionshowcase.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

public class TestSettings {
	public static TestSettings instance;
	public static HashMap<String, String> _inputMap;
	public static Properties configprop = new Properties();
	String root = System.getProperty("user.dir");

	public static TestSettings getInstance() {
		if (instance == null) {
			instance = new TestSettings();
		}
		return instance;
	}

	// ************LOAD Credentials Properties file
	public void loadCredentialsProperties() {
		try {
			FileInputStream fs = new FileInputStream(root + "/configuration/CONFIG.properties");
			configprop.load(fs);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public TestSettings() {
		loadCredentialsProperties();
	}

	public Properties getConfigProp() {
		return configprop;
	}

	public static void main(String[] args) throws IOException {
		TestSettings ts = TestSettings.getInstance();
	}
}
