package au.com.chubb.evolutionshowcase.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.PageObject;

public class DynamicSeleniumFunctions extends PageObject {

	// *****************************SEND KEYS INPUT FUNCTIONS
	// ****************************************************************************************************************************************//
	public static Properties OR=new Properties();
	String root = System.getProperty("user.dir");
	
	public DynamicSeleniumFunctions SendKeys(String ParentClass, String DescendantTag, String FieldLabel,
			String FieldValue, String TagValue) throws InterruptedException

	{
		Thread.sleep(2000);
		WebElement Element_Name = getDriver().findElement(By.xpath(".//*[@class=\"" + ParentClass
				+ "\" and descendant::" + DescendantTag + "[normalize-space()=\"" + FieldLabel + "\"]]"));
		Element_Name.findElement(By.tagName(TagValue)).click();
		Thread.sleep(2000);
		Element_Name.findElement(By.tagName(TagValue)).sendKeys(FieldValue);
		Thread.sleep(7000);
		Element_Name.findElement(By.tagName(TagValue)).sendKeys(Keys.ENTER);
		Element_Name = null;
		return null;
	}

	public DynamicSeleniumFunctions ClearAndSendKeys(String ParentClass, String DescendantTag, String FieldLabel,
			String TagValue, String FieldValue) throws InterruptedException

	{
		Thread.sleep(1000);
		WebElement Element_Name = getDriver().findElement(By.xpath(".//*[@class=\"" + ParentClass
				+ "\" and descendant::" + DescendantTag + "[normalize-space()=\"" + FieldLabel + "\"]]"));
		Element_Name.findElement(By.tagName(TagValue)).clear();
		Thread.sleep(1000);
		Element_Name.findElement(By.tagName(TagValue)).sendKeys(FieldValue);
		return null;
	}

	public DynamicSeleniumFunctions SendKeysWithoutClear(String ParentClass, String FieldValue, String TagValue)
			throws InterruptedException

	{
		Thread.sleep(5000);
		WebElement Element_Name = getDriver().findElement(By.xpath(".//*[@class=\"" + ParentClass + "\"]"));
		Element_Name.findElement(By.tagName(TagValue)).clear();
		Thread.sleep(2000);
		Element_Name.findElement(By.tagName(TagValue)).sendKeys(FieldValue);
		return null;
	}

	public DynamicSeleniumFunctions SendKeysByNgModelValue(String NgModel_value, String FieldValue)
			throws InterruptedException

	{

		WebElement Element_Name = getDriver().findElement(By.xpath(".//*[@ng-model=\"" + NgModel_value + "\"]"));
		getDriver().switchTo().activeElement();
		Element_Name.clear();
		Element_Name.sendKeys(FieldValue);
		Element_Name = null;
		return null;
	}

	// ***************************************************** SELECT
	// FUNCTIONS**********************************************************************************************************************//

	public DynamicSeleniumFunctions SelectByPlaceHolderAndVisbileText(String Placeholer_value, String VisibleTextValue)
			throws InterruptedException

	{
		WebElement Element_Name = getDriver()
				.findElement(By.xpath(".//*[@data-placeholder=\"" + Placeholer_value + "\"]"));
		new Select(Element_Name).selectByVisibleText(VisibleTextValue);
		Element_Name = null;
		return null;
	}

	public DynamicSeleniumFunctions SelectByNgModelValueAndVisbileText(String NgModel_value, String VisibleTextValue)
			throws InterruptedException

	{
		WebElement Element_Name = getDriver().findElement(By.xpath(".//*[@ng-model=\"" + NgModel_value + "\"]"));
		new Select(Element_Name).selectByVisibleText(VisibleTextValue);
		Element_Name = null;
		return null;
	}

	// ************************************************** CLICK FUNCTIONS
	// ***********************************************************************************************************************//

	public DynamicSeleniumFunctions ClickByTag(String ParentClass, String DescendantTag, String FieldLabel,
			String TagValue) throws InterruptedException

	{
		Thread.sleep(2000);
		WebElement Element_Name = getDriver().findElement(By.xpath(".//*[@class=\"" + ParentClass
				+ "\" and descendant::" + DescendantTag + "[normalize-space()=\"" + FieldLabel + "\"]]"));
		Element_Name.findElement(By.tagName(TagValue)).click();
		return null;
	}

	public DynamicSeleniumFunctions ClickByContainsText(String ParentClass, String DescendantTag, String FieldLabel)
			throws InterruptedException

	{
		Thread.sleep(2000);
		WebElement Element_Name = getDriver().findElement(By.xpath(".//*[contains(concat(' ', @class , ' '),\""
				+ ParentClass + "\")]//" + DescendantTag + "[contains(text(),\"" + FieldLabel + "\")]"));
		Element_Name.click();
		Element_Name = null;
		return null;
	}

	// ######
	public DynamicSeleniumFunctions ClickByContainsText(String FieldLabel) throws InterruptedException

	{
		Thread.sleep(2000);
		getDriver().findElement(By.xpath(".//*[contains(text(), " + FieldLabel + ")]")).click();
		return null;
	}

	// #####
	public DynamicSeleniumFunctions SendKeysByContainsText(String FieldValue, String FieldLabel)
			throws InterruptedException

	{
		Thread.sleep(2000);
		getDriver().findElement(By.xpath("//*[contains(text(), " + FieldLabel + ")]")).sendKeys(FieldValue);
		return null;
	}

	public DynamicSeleniumFunctions ClickByContainsTextSameLevel(String ParentClass, String DescendantTag,
			String FieldLabel) throws InterruptedException

	{
		Thread.sleep(2000);
		WebElement Element_Name = getDriver().findElement(By.xpath(".//*[contains(concat(' ', @class , ' '),\""
				+ ParentClass + "\")]:" + DescendantTag + "[contains(text(),\"" + FieldLabel + "\")]"));
		Element_Name.click();
		Element_Name = null;
		return null;
	}

	// *************************************************************************************
	// GET FUNCTIONS
	// ***************************************************************************************//
	public String GetTextByContainsText(String FieldLabel, String TagValue) throws InterruptedException

	{
		Thread.sleep(2000);
		WebElement Element_Name = getDriver().findElement(By.xpath("//*[contains(text(), " + FieldLabel + ")]"));
		String Text = Element_Name.findElement(By.tagName(TagValue)).getText();
		return Text;
	}

	public String GetText(String ParentClass, String DescendantTag, String FieldLabel, String TagValue)
			throws InterruptedException

	{
		Thread.sleep(5000);
		WebElement Element_Name = getDriver().findElement(By.xpath(".//*[@class=\"" + ParentClass
				+ "\" and descendant::" + DescendantTag + "[normalize-space()=\"" + FieldLabel + "\"]]"));
		String Text = Element_Name.findElement(By.tagName(TagValue)).getText();
		Thread.sleep(2000);
		return Text;
	}
	
	public boolean verifyFieldNonEditable(String ObjectName) throws IOException
	{
		boolean isTrue = false;
		
			FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
			OR.load(fo);
			getDriver().findElement(By.xpath(OR.getProperty(ObjectName))).sendKeys("Value");
			String textEntered_APP=getDriver().findElement(By.xpath(OR.getProperty(ObjectName))).getText();
			if(!textEntered_APP.equalsIgnoreCase("Value"))
			{
				isTrue=true;
			}

		return isTrue;
		
	}
	@Step
	public void verifyFieldNonEditableForDates(String ObjectName) throws IOException
	{
		boolean flag = false;
		
			FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
			OR.load(fo);
			try{
				getDriver().findElement(By.xpath(OR.getProperty(ObjectName))).sendKeys("Value");
			}
			catch (Exception e)
			{
				if(e.getMessage().contains("Element is not enabled"))			
					flag=true;
			}
			if (flag)
			{
				Assert.assertTrue(""+ObjectName+" Element is readonly", true);
			}else
			{
				Assert.assertTrue(""+ObjectName+"  Element is readonly", false);
			}		
	
	}
	
	public boolean verifyFieldEditable(String ObjectName, String value) throws IOException
	{
		boolean isTrue = false;
		
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);
		getDriver().findElement(By.xpath(OR.getProperty(ObjectName))).clear();
		getDriver().findElement(By.xpath(OR.getProperty(ObjectName))).sendKeys(value);
		String textEntered_APP=getDriver().findElement(By.xpath(OR.getProperty(ObjectName))).getAttribute("value");
		textEntered_APP.replace(",", "");
		if(textEntered_APP.equalsIgnoreCase(value))
		{
			isTrue=true;
		}

		return isTrue;
		
	}

	// ********************** Wait till element is Clickable
	// ************************************************************************************************************************************//


}
