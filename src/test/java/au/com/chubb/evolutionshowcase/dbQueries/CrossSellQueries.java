package au.com.chubb.evolutionshowcase.dbQueries;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import net.thucydides.core.pages.PageObject;

public class CrossSellQueries extends PageObject {

	// ************************************************ Connect to Cross Sell DB
	// ********************************************************************************//
	public static Connection getDB_Connection()
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		// CROSS - SELL DEV DB Connection Credentials
		Connection connection = null;
		String DB_TYPE = "sqlserver";
		String DB_SERVER_NAME = "ausshldevdbpilot";
		String DB_NAME = "CrossSell_Test";
		String DB_PORT = "1433";
		String DB_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
		String DB_userName = "crossselluser";
		String DB_password = "Friday123";
		String url = "jdbc:" + DB_TYPE + "://" + DB_SERVER_NAME + ":" + DB_PORT + ";databasename=" + DB_NAME + "";

		Class.forName(DB_DRIVER);// .newInstance();
		connection = DriverManager.getConnection(url, DB_userName, DB_password);
		return connection;

	}

	// ***STORE - PROCEDURE to retrieve PACL mapping results *********//
	public static ResultSet executestoreprocedure_PACL_Mapping()
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getDB_Connection();
		CallableStatement myStoredProcedureCall = connection.prepareCall("{call [dbo].[usp_GetPacplMapping]}");
		ResultSet rs = myStoredProcedureCall.executeQuery();
		return rs;

	}

	public static ResultSet executestoreprocedure_Get_Industries()
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getDB_Connection();
		CallableStatement myStoredProcedureCall = connection.prepareCall("{call [dbo].[usp_GetIndustries]}");
		ResultSet rs = myStoredProcedureCall.executeQuery();
		return rs;

	}

	public static ResultSet executestoreprocedure_Get_Appetite()
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getDB_Connection();
		CallableStatement myStoredProcedureCall = connection.prepareCall("{call [dbo].[usp_GetAppetite]}");
		ResultSet rs = myStoredProcedureCall.executeQuery();
		return rs;

	}

}