package au.com.chubb.evolutionshowcase.dbQueries;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import net.thucydides.core.pages.PageObject;

public class CrossSellDEVQueries extends PageObject {

	// ************************************************ Connect to Cross Sell DB
	// ********************************************************************************//
	public static Connection getDB_Connection()
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		// CROSS - SELL DEV DB Connection Credentials
		Connection connection = null;
		String DB_TYPE = "sqlserver";
		String DB_SERVER_NAME = "ausshldevdbpilot";
		String DB_NAME = "CrossSell_DEV";
		String DB_PORT = "1433";
		String DB_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
		String DB_userName = "bmurthy";
		String DB_password = "Hello123";

		String url = "jdbc:" + DB_TYPE + "://" + DB_SERVER_NAME + ":" + DB_PORT + ";databasename=" + DB_NAME + "";
		Class.forName(DB_DRIVER);// .newInstance();
		connection = DriverManager.getConnection(url, DB_userName, DB_password);
		return connection;

	}

	// ***STORE - PROCEDURE for Client Search *********//
	public static void executestoreprocedure_PACL_Mapping()
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getDB_Connection();
		CallableStatement myStoredProcedureCall = connection.prepareCall("{call [dbo].[usp_GetPacplMapping]}");

		ResultSet rs = myStoredProcedureCall.executeQuery();

		if (!rs.next()) {
			System.out.println("No Data in Result Set");
		} else
			do {
				System.out.println(rs.getString("Key") + "   " + rs.getString("Value"));
			} while (rs.next());

	}

	// ***STORE - PROCEDURE for Client Search *********//
	public static ResultSet executestoreprocedure(String SearchClientName)
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getDB_Connection();
		CallableStatement myStoredProcedureCall = connection.prepareCall("{call dbo.cs_Client_Search(?)}");
		myStoredProcedureCall.setString(1, SearchClientName);
		ResultSet rs = myStoredProcedureCall.executeQuery();
		return rs;

	}

	public static String getInsuredNameId_WithClientID(String DBTableName, String ColName1, String ColName2,
			String ClientIdChubb, String InsuredName)
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getDB_Connection();
		String query = "select * from " + DBTableName + " where " + ColName1 + "=" + " '" + ClientIdChubb + "'" + ""
				+ " and " + ColName2 + "=" + " '" + InsuredName + "'";
		PreparedStatement preparedStatement = connection.prepareStatement(query);
		ResultSet rs = preparedStatement.executeQuery();
		rs.next();
		String InsuredNameId = rs.getString("Id");
		return InsuredNameId;

	}

	public static String getOfficeCountryId(String OfficeName, String CountryName)
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getDB_Connection();
		String query = "select oc.id from officecountry oc" + " left join dbo.office o" + " on oc.officeid = o.id"
				+ " left join dbo.country c" + " on oc.countryid = c.id" + " where o.value = " + "'" + OfficeName + "'"
				+ "" + " and c.value = " + "'" + CountryName + "'" + "";
		PreparedStatement preparedStatement = connection.prepareStatement(query);
		ResultSet rs = preparedStatement.executeQuery();
		rs.next();
		String OfficeCountryId = rs.getString("id");
		return OfficeCountryId;

	}

	public static ResultSet GetProductCounts_DB(String InsuredNameId, String ProducerId, String OfficeCountryId)
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getDB_Connection();

		String query = "select p.ProductId, pt.Value, COUNT(p.ProductId) as 'Count' from Policy p"
				+ " left join dbo.InsuredName ins" + " on p.InsuredNameId = ins.Id" + " left join dbo.Product pt"
				+ " on p.ProductId = pt.Id" + " left join dbo.PolicyStatus ps" + " on ps.Id = p.PolicyStatusId"
				+ " where ins.Id = " + "'" + InsuredNameId + "'" + "" + " and p.ProducerId = " + "'" + ProducerId + "'"
				+ "" + " and p.OfficeCountryId = " + "'" + OfficeCountryId + "'" + "" + " and ps.Value in" + " ("
				+ " 'Pending'," + " 'Lapsed - single risk only'," + " 'Issued'," + " 'Bound - Run-off'," + " 'Lapsed',"
				+ " 'Ren Auth Complete'," + " 'Bound pending closings'," + " 'Bound'," + " 'In Force'," + " 'Expiring',"
				+ " 'Referred'," + " 'Bind requested'," + " 'Extended'," + " 'Bound - Awaiting subjectivity',"
				+ " 'Cancelled'" + ")" + " and pt.Value" + "<>" + "'undefined'"
				+ " group by p.ProductId, pt.Value ORDER BY" + " 'Count'" + " DESC";

		PreparedStatement preparedStatement = connection.prepareStatement(query);
		ResultSet rs = preparedStatement.executeQuery();
		return rs;

	}

	public static String[] executestoreprocedure_GetTopUnderWriters(int ProductId)
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getDB_Connection();
		CallableStatement GetTopUnderWriters = connection
				.prepareCall("{call dbo.cs_Underwriter_GetTopByProduct(?,?,?)}");
		GetTopUnderWriters.setInt(1, ProductId);
		GetTopUnderWriters.setInt(2, 0);
		GetTopUnderWriters.setInt(3, 0);
		ResultSet rs = GetTopUnderWriters.executeQuery();
		String[] TopUnderwriter = new String[3];

		for (int i = 0; i < 3; i++) {

			if (!rs.next()) {
				System.out.println("no data");
			}

			else {
				TopUnderwriter[i] = rs.getString("Underwriter");
				System.out.println(TopUnderwriter[i]);
			}

		}

		return TopUnderwriter;

	}

	public static String getProducerId(String DBTableName, String ColName, String QueryValue)
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getDB_Connection();
		String query = "select * from " + DBTableName + " where " + ColName + " =?";
		System.out.println(query);
		System.out.println(QueryValue);
		PreparedStatement preparedStatement = connection.prepareStatement(query);
		preparedStatement.setString(1, QueryValue);
		ResultSet rs = preparedStatement.executeQuery();
		rs.next();
		String ProducerId = rs.getString("Id");
		return ProducerId;

	}

	public static String getUnderWriterId(String DBTableName, String ColName, String QueryValue)
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getDB_Connection();
		String query = "select * from " + DBTableName + " where " + ColName + " =?";
		PreparedStatement preparedStatement = connection.prepareStatement(query);
		preparedStatement.setString(1, QueryValue);
		ResultSet rs = preparedStatement.executeQuery();
		rs.next();
		String UnderWriterId = rs.getString("Id");
		return UnderWriterId;

	}

	public static int getProductId(String DBTableName, String ColName, String QueryValue)
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getDB_Connection();
		String query = "select * from " + DBTableName + " where " + ColName + " =?";
		PreparedStatement preparedStatement = connection.prepareStatement(query);
		preparedStatement.setString(1, QueryValue);
		ResultSet rs = preparedStatement.executeQuery();
		rs.next();
		int ProductId = rs.getInt("Id");
		return ProductId;

	}

	public static boolean getProducerSearchResults(String DBTableName, String ColName1, String ColName2,
			String ColName3, String ColName4, ArrayList<String> producer_ID, ArrayList<String> office_Country_ID,
			ArrayList<String> policyStatusId, String UnderWriterId)
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getDB_Connection();
		boolean policy_count = false;
		int counter = 0;
		System.out.println("Control reached DB query");

		String formattedOfficeCountryID = office_Country_ID.toString().replace("[", "") // remove
																						// the
																						// right
																						// bracket
				.replace("]", "") // remove the left bracket
				.trim();

		String formattedPolicyStatusID = policyStatusId.toString().replace("[", "") // remove
																					// the
																					// right
																					// bracket
				.replace("]", "") // remove the left bracket
				.trim();

		String formattedProducerID = producer_ID.toString().replace("[", "") // remove
																				// the
																				// right
																				// bracket
				.replace("]", "") // remove the left bracket
				.trim();

		String query = null;

		if (policyStatusId.size() > 0 && office_Country_ID.size() > 0) {
			if (UnderWriterId == null) {
				query = "select * from " + DBTableName + " where " + ColName1 + " IN" + " (" + formattedProducerID + ")"
						+ "" + " and " + ColName2 + " IN" + " (" + formattedOfficeCountryID + ")" + "" + " and "
						+ ColName3 + " IN  (" + formattedPolicyStatusID + ")";
			} else {
				query = "select * from " + DBTableName + " where " + ColName1 + " IN" + " (" + formattedProducerID + ")"
						+ "" + " and " + ColName2 + " IN" + " (" + formattedOfficeCountryID + ")" + "" + " and "
						+ ColName3 + " IN  (" + formattedPolicyStatusID + ")" + "" + " and " + ColName4 + "=" + " '"
						+ UnderWriterId + "'";
			}

		} else if (policyStatusId.size() > 0 && office_Country_ID.size() == 0)

		{
			if (UnderWriterId == null) {
				query = "select * from " + DBTableName + " where " + ColName1 + " IN" + " (" + formattedProducerID + ")"
						+ "" + " and " + ColName3 + " IN  (" + formattedPolicyStatusID + ")";
			} else {
				query = "select * from " + DBTableName + " where " + ColName1 + " IN" + " (" + formattedProducerID + ")"
						+ "" + " and " + ColName3 + " IN  (" + formattedPolicyStatusID + ")" + "" + " and " + ColName4
						+ "=" + " '" + UnderWriterId + "'";
			}

		}
		// 1 - Producer Id, 2- Office_country, 3- Policy_status_ID, 4 -
		// Underwriter ID

		else if ((policyStatusId.size() == 0 && office_Country_ID.size() > 0)) {

			if (UnderWriterId == null) {

				query = "select * from " + DBTableName + " where " + ColName1 + " IN" + " (" + formattedProducerID + ")"
						+ "" + " and " + ColName2 + " IN" + " (" + formattedOfficeCountryID + ")";
			} else {
				query = "select * from " + DBTableName + " where " + ColName1 + " IN" + " (" + formattedProducerID + ")"
						+ "" + " and " + ColName2 + " IN" + " (" + formattedOfficeCountryID + ")" + "" + " and "
						+ ColName4 + "=" + " '" + UnderWriterId + "'";
			}
		} else if ((policyStatusId.size() == 0 && office_Country_ID.size() == 0)) {
			if (UnderWriterId == null) {
				query = "select * from " + DBTableName + " where " + ColName1 + " IN" + " (" + formattedProducerID
						+ ")";
			} else {
				query = "select * from " + DBTableName + " where " + ColName1 + " IN" + " (" + formattedProducerID + ")"
						+ "" + " and " + ColName4 + "=" + " '" + UnderWriterId + "'";

			}
		}

		System.out.println(query);

		PreparedStatement stmt = connection.prepareStatement(query);
		ResultSet rs = stmt.executeQuery();

		if (!rs.next()) {
			System.out.println("No Data in Result Set");
		} else
			do {
				policy_count = true;
				System.out.println(rs.getString("PolicyNumber") + "   " + rs.getString("PolicyStatusId"));
				counter++;
			} while (rs.next());

		System.out.println("Total Policies " + counter);
		System.out.println(policy_count);

		return policy_count;

	}

}
