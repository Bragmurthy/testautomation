package au.com.chubb.evolutionshowcase.dbQueries;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class PCWQueries {

	// ==================LOCAL PCW DB=================================//
	public static Connection getNetezza_Connection_local()
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		Connection connection = null;
		String DB_TYPE = "netezza";
		String DB_DRIVER = "org.netezza.Driver";
		String DB_SERVER_NAME = "UK01DNEZ009";
		String DB_NAME = "DB_Q_DM_PCW";
		String DB_userName = "bxmurt";
		String DB_password = "Google@1";
		String DB_PORT = "5480";

		try {
			Class.forName(DB_DRIVER);
			connection = DriverManager.getConnection(
					"jdbc:" + DB_TYPE + "://" + "" + DB_SERVER_NAME + ":" + DB_PORT + "" + "/" + DB_NAME + "",
					"" + DB_userName + "", "" + DB_password + "");
			if (connection != null) {
				System.out.println("Connected with connection #1");
			}

		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		return connection;

	}

	// ==================PROD PCW DB=================================//
	public static Connection getNetezza_Connection()
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		Connection connection = null;
		String DB_TYPE = "netezza";
		String DB_DRIVER = "org.netezza.Driver";
		String DB_SERVER_NAME = "UK01PNEZ015";
		String DB_NAME = "DB_Q_DM_PCW";
		String DB_userName = "bxmurt";
		String DB_password = "Google@1";
		String DB_PORT = "5480";

		try {
			Class.forName(DB_DRIVER);
			connection = DriverManager.getConnection(
					"jdbc:" + DB_TYPE + "://" + "" + DB_SERVER_NAME + ":" + DB_PORT + "" + "/" + DB_NAME + "",
					"" + DB_userName + "", "" + DB_password + "");
			if (connection != null) {
				System.out.println("Connected with connection #1");
			}

		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		return connection;

	}

	// ************************PCW OLD
	// QUERIES**********************************************************************************************************//

	public static void get_Limit_100_POLICY()
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getNetezza_Connection();
		Statement statement = null;
		String query = "SELECT * FROM DB_Q_DM_PCW.DB_Q_DM_PCW_OWNER.DIM_POLICY LIMIT 100;";
		statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		System.out.println(
				"*************************************POLICY TABLE LIMIT 100...*************************************************************************");
		if (!resultSet.next()) {
			System.out.println("No results for -- get_Limit_100_CONTACT");
		} else {
			do {
				System.out.println("POLICY_KEY:" + resultSet.getString("POLICY_KEY") + ",  POLICY_NUMBER: "
						+ resultSet.getString("POLICY_NUMBER") + ",  CURRENT_POLICY_NUMBER:"
						+ resultSet.getString("CURRENT_POLICY_NUMBER") + ",  POLICY_EXPIRATION_DATE:"
						+ resultSet.getString("POLICY_EXPIRATION_DATE") + ",  POLICY_HOLDER_ID:"
						+ resultSet.getString("POLICY_HOLDER_ID") + ",  POLICY_LINE_OF_BUSINESS:"
						+ resultSet.getString("POLICY_LINE_OF_BUSINESS") + ",  PRODUCER_NAME:"
						+ resultSet.getString("PRODUCER_NAME"));
			} while (resultSet.next());
		}
		connection.close();

	}

	public static void get_Limit_100_CONTACT()
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getNetezza_Connection();
		Statement statement = null;
		String query = "SELECT * FROM DB_Q_DM_PCW.DB_Q_DM_PCW_OWNER.DIM_CONTACT LIMIT 100;";
		statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		System.out.println(
				"****************************************CONTACT TABLE LIMIT 100...**************************************************************************");
		if (!resultSet.next()) {
			System.out.println("No results for -- get_Limit_100_CONTACT");
		} else {
			do {
				System.out.println("CONTACT_KEY:" + resultSet.getString("CONTACT_KEY") + ",  CONTACT_CODE: "
						+ resultSet.getString("CONTACT_CODE") + ",  CONTACT_TYPE:" + resultSet.getString("CONTACT_TYPE")
						+ ",  ORIGINATING_BUSINESS_UNIT:" + resultSet.getString("ORIGINATING_BUSINESS_UNIT")
						+ ",  COUNTRY_CODE:" + resultSet.getString("COUNTRY_CODE"));
			} while (resultSet.next());
		}
		connection.close();

	}

	public static void get_Limit_100_GLOBAL_PRODUCER()
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getNetezza_Connection();
		Statement statement = null;
		String query = "SELECT * FROM DB_Q_DM_PCW.DB_Q_DM_PCW_OWNER.DIM_GLOBAL_PRODUCER LIMIT 100;";
		statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		System.out.println(
				"****************************************GLOBAL PRODUCER TABLE LIMIT 100...**************************************************************************");
		if (!resultSet.next()) {
			System.out.println("No results for -- get_Limit_100_GLOBAL_PRODUCER");
		} else {

			do {
				System.out.println("CODE:" + resultSet.getString("CODE") + ",  DESCRIPTION: "
						+ resultSet.getString("DESCRIPTION") + ",  GROUP_CODE:" + resultSet.getString("GROUP_CODE")
						+ ",  CATEGORY_CODE:" + resultSet.getString("CATEGORY_CODE") + ", SOURCE_SYSTEM_ID:"
						+ resultSet.getString("SOURCE_SYSTEM_ID"));
			} while (resultSet.next());
		}
		connection.close();

	}

	public static ResultSet get_PRODUCER_SEARCH_RESULTS(String contactType, String producerName, String officeFilterSet,
			List<String> offices)
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getNetezza_Connection();
		Statement statement = null;
		String formattedOffices = offices.toString().replace("[", "") // remove
																		// the
																		// right
																		// bracket
				.replace("]", "") // remove the left bracket
				.trim();
		String query = null;
		if (officeFilterSet.equalsIgnoreCase("NO")) {
			query = "SELECT c.CONTACT_CODE AS ProducerId, NVL(c.NAME_1, '') || NVL(c.NAME_2, '') AS ProducerName, 'Group TBD' AS ProducerGroup, c.field_Office AS Office, COUNT(*) OVER() AS MaxResults FROM DB_Q_DM_PCW.DB_D1_DWH_PCW_OWNER.DIM_CONTACT c WHERE Originating_business_unit IN ('35001') AND contact_type ="
					+ "'" + contactType + "'" + "" + " AND ProducerName LIKE " + "'" + producerName + "'"
					+ " ORDER BY ProducerName ASC LIMIT 100";
		} else {
			query = "SELECT c.CONTACT_CODE AS ProducerId, NVL(c.NAME_1, '') || NVL(c.NAME_2, '') AS ProducerName, 'Group TBD' AS ProducerGroup, c.field_Office AS Office, COUNT(*) OVER() AS MaxResults FROM DB_Q_DM_PCW.DB_D1_DWH_PCW_OWNER.DIM_CONTACT c WHERE Originating_business_unit IN ('35001') AND contact_type ="
					+ "'" + contactType + "'" + "" + " AND ProducerName LIKE " + "'" + producerName + "'"
					+ " AND field_office IN " + "(" + formattedOffices + ")" + "  ORDER BY ProducerName ASC LIMIT 100";
		}
		System.out.println("Control reached query: " + query);

		statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		connection.close();
		return resultSet;
	}

	public static void get_DISTINCT_PRODUCERS_IN_TRANSACTION()
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getNetezza_Connection();
		Statement statement = null;
		String query = "SELECT DISTINCT PTF.ORIGINATING_BUSINESS_UNIT, " + " P.PRODUCER_CODE," + " P.PRODUCER_NAME "
				+ " FROM DIM_POLICY P, DIM_POLICY_COVERAGE PC, FCT_PREMIUM_TRANSACTION PTF "
				+ " WHERE P.POLICY_KEY = PTF.POLICY_KEY" + " AND PC.POLICY_COVERAGE_KEY = PTF.POLICY_COVERAGE_KEY"
				+ " AND PTF.ORIGINATING_BUSINESS_UNIT = '35001' -- Country filter - DIM_BUSINESS_UNIT_GEOGRAPHIC can be used for reference data."
				+ " AND PTF.DETAIL_LEVEL_INDICATOR = 'Y' -- Used to only extract data that has come from a front-end system, as opposed to a backfeed for example."
				+ " AND PTF.DIRECT_ASSUMED_CEDED_CODE IN ( '1', '2', '3', '4' ) -- Used to differenciate between Direct/Assumed/Ceded/Retro-Ceded business respectively."
				+ " AND PTF.DATA_SOURCE_ID NOT IN ( 'FT_AUE' ) -- Used to determine which source systems are included in the extract - FT_AUE is for Endeavour, for Combined."
				+ " AND substr(P.POLICY_EFFECTIVE_DATE,1,4) = '2015'"
				+ " GROUP BY PTF.ORIGINATING_BUSINESS_UNIT, P.PRODUCER_CODE, P.PRODUCER_NAME ";
		statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		System.out.println(
				"****************************************AUSTRALIAN PRODUCERS ...**************************************************************************");
		if (!resultSet.next()) {
			System.out.println("No results for -- get_DISTINCT_PRODUCERS_IN_TRANSACTION");
		} else {
			do {
				System.out.println("ORIGINATING_BUSINESS_UNIT:" + resultSet.getString("ORIGINATING_BUSINESS_UNIT")
						+ ",  PRODUCER_CODE: " + resultSet.getString("PRODUCER_CODE") + ",  PRODUCER_NAME:"
						+ resultSet.getString("PRODUCER_NAME"));
			} while (resultSet.next());
		}
		connection.close();

	}

	public static void get_POLICY_TRANSACTIONS_FOR_PRODUCER()
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getNetezza_Connection();
		Statement statement = null;
		String query = "SELECT PTF.ORIGINATING_BUSINESS_UNIT, PTF.ACCOUNTING_PERIOD, P.PRODUCER_CODE, P.PRODUCER_NAME, P.POLICY_NUMBER, P.POLICY_HOLDER_NAME, P.POLICY_INCEPTION_DATE, P.POLICY_EFFECTIVE_DATE, P.POLICY_EXPIRATION_DATE, P.POLICY_LINE_OF_BUSINESS, PC.CURRENT_PRODUCT, PR.DESCRIPTION, PC.COVERAGE_CODE, P.CURRENT_INDUSTRY_CODE, P.CURRENT_FULL_INDUSTRY_CODE, IC.EXPLANATION, PTF.TRANS_GLOBAL_PRODUCER_CODE, GP.DESCRIPTION, P.CURRENT_OPERATING_UNIT, OP.DESCR, sum(PTF.PREMIUM_IN_LEDGER_CURRENCY) as PREMIUM_LEDGER"
				+ " FROM DIM_POLICY P, DIM_POLICY_COVERAGE PC, FCT_PREMIUM_TRANSACTION PTF, DIM_PRODUCT PR, DIM_INDUSTRY_CODE IC, DIM_GLOBAL_PRODUCER GP, DIM_PS_OPERATING_UNIT OP"

				+ " WHERE P.POLICY_KEY = PTF.POLICY_KEY AND PC.POLICY_COVERAGE_KEY = PTF.POLICY_COVERAGE_KEY AND PC.CURRENT_PRODUCT = PR.PROD AND P.CURRENT_FULL_INDUSTRY_CODE = IC.INDUSTRY_CODE AND PTF.TRANS_GLOBAL_PRODUCER_CODE = GP.CODE AND P.CURRENT_OPERATING_UNIT = OP.OPERATING_UNIT AND PTF.ORIGINATING_BUSINESS_UNIT = '35001' AND PTF.DETAIL_LEVEL_INDICATOR = 'Y' AND PTF.DIRECT_ASSUMED_CEDED_CODE IN ( '1', '2', '3', '4' ) AND PTF.DATA_SOURCE_ID NOT IN ( 'FT_AUE' ) AND substr(P.POLICY_EFFECTIVE_DATE,1,4) = '2015' AND P.PRODUCER_CODE = '7101262'"
				+ " GROUP BY PTF.ORIGINATING_BUSINESS_UNIT, PTF.ACCOUNTING_PERIOD, P.PRODUCER_CODE, P.PRODUCER_NAME, P.POLICY_NUMBER, P.POLICY_HOLDER_NAME, P.POLICY_INCEPTION_DATE, P.POLICY_EFFECTIVE_DATE, P.POLICY_EXPIRATION_DATE, P.POLICY_LINE_OF_BUSINESS, PC.CURRENT_PRODUCT, PR.DESCRIPTION, PC.COVERAGE_CODE, P.CURRENT_INDUSTRY_CODE, P.CURRENT_FULL_INDUSTRY_CODE, IC.EXPLANATION, PTF.TRANS_GLOBAL_PRODUCER_CODE, GP.DESCRIPTION, P.CURRENT_OPERATING_UNIT, OP.DESCR ORDER BY P.POLICY_NUMBER";
		statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		System.out.println(
				"****************************************POLICY TRANSACTIONS FOR PRODUCERS ...**************************************************************************");
		if (!resultSet.next()) {
			System.out.println("No results for -- get_POLICY_TRANSACTIONS_FOR_PRODUCER");
		} else {
			do {
				System.out.println("ORIGINATING_BUSINESS_UNIT:" + resultSet.getString("ORIGINATING_BUSINESS_UNIT")
						+ ",  PRODUCER_CODE: " + resultSet.getString("PRODUCER_CODE") + ",  POLICY_NUMBER:"
						+ resultSet.getString("POLICY_NUMBER") + "POLICY_HOLDER_NAME:"
						+ resultSet.getString("POLICY_HOLDER_NAME") + ",  POLICY_LINE_OF_BUSINESS: "
						+ resultSet.getString("POLICY_LINE_OF_BUSINESS") + ",  DESCRIPTION: "
						+ resultSet.getString("DESCRIPTION"));
			} while (resultSet.next());
		}
		connection.close();

	}

	public static void get_POLICY_CLAIM_TRANSACTIONS()
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getNetezza_Connection();
		Statement statement = null;
		String query = "SELECT PTF.ORIGINATING_BUSINESS_UNIT, P.POLICY_NUMBER, P.POLICY_EFFECTIVE_DATE, C.CLAIM_NUMBER, CL.CLAIMANT_NUMBER, CL.CLAIMANT_NAME, C.CLAIM_EVENT_DATE, C.CLAIM_REPORT_DATE, C.CLAIM_EVENT_DESCRIPTION, sum(PTF.PREMIUM_IN_LEDGER_CURRENCY) as PREMIUM_LEDGER, sum(PTF.PREMIUM_IN_US_DOLLARS) as PREMIUM_USD, --CTF.TRANSACTION_CODE, sum(CTF.PAID_EXP_AMOUNT_LEDGER) as PAID_LEDGER, sum(CTF.PAID_EXP_AMOUNT_US) PAID_USD, sum(CTF.OSLR_CHANGE_LEDGER) as OSLR_LEDGER, sum(CTF.OSLR_CHANGE_US) as OSLR_USD"

				+ " FROM DB_Q_DM_PCW.DB_P_DM_PCW_OWNER.DIM_POLICY P, DB_Q_DM_PCW.DB_P_DM_PCW_OWNER.FCT_PREMIUM_TRANSACTION PTF, DB_Q_DM_PCW.DB_P_DM_PCW_OWNER.DIM_CLAIM C, DB_Q_DM_PCW.DB_P_DM_PCW_OWNER.DIM_CLAIMANT CL, DB_Q_DM_PCW.DB_P_DM_PCW_OWNER.FCT_CLAIM_TRANSACTION CTF"
				+ " WHERE P.POLICY_KEY = PTF.POLICY_KEY AND P.POLICY_NUMBER = C.POLICY_NUMBER AND P.POLICY_EFFECTIVE_DATE = C.POLICY_EFFECTIVE_DATE AND P.ORIGINATING_BUSINESS_UNIT = C.ORIGINATING_BUSINESS_UNIT AND C.CLAIM_KEY = CTF.CLAIM_KEY AND CL.CLAIMANT_KEY = CTF.CLAIMANT_KEY AND P.POLICY_LINE_OF_BUSINESS = C.POLICY_LINE_OF_BUSINESS AND PTF.ORIGINATING_BUSINESS_UNIT = '35001' AND P.POLICY_NUMBER = '01CE526602' AND PTF.DIRECT_ASSUMED_CEDED_CODE IN ( '1', '2', '3', '4' ) AND CTF.DIRECT_ASSUMED_CEDED_CODE IN ( '1', '2', '3', '4' ) AND CTF.TRANSACTION_CODE NOT IN ( 'OSR', 'OSX', 'NF' ) AND PTF.DETAIL_LEVEL_INDICATOR = 'Y' AND CTF.DETAIL_LEVEL_INDICATOR = 'Y' AND PTF.DATA_SOURCE_ID NOT IN ( 'FT_AUE' )"
				+ " GROUP BY PTF.ORIGINATING_BUSINESS_UNIT, P.POLICY_NUMBER, C.CLAIM_NUMBER, CL.CLAIMANT_NUMBER, CL.CLAIMANT_NAME, C.CLAIM_EVENT_DATE, C.CLAIM_REPORT_DATE, C.CLAIM_EVENT_DESCRIPTION, P.POLICY_EFFECTIVE_DATE --,CTF.TRANSACTION_CODE";
		statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		System.out.println(
				"****************************************POLICY and CLAIMS TRANSACTIONS FOR PRODUCERS ...**************************************************************************");
		if (!resultSet.next()) {
			System.out.println("No results for -- get_POLICY_CLAIM_TRANSACTIONS");
		} else {
			do {
				System.out.println("ORIGINATING_BUSINESS_UNIT:" + resultSet.getString("ORIGINATING_BUSINESS_UNIT")
						+ ",  PRODUCER_CODE: " + resultSet.getString("PRODUCER_CODE") + ",  POLICY_NUMBER:"
						+ resultSet.getString("POLICY_NUMBER") + "POLICY_HOLDER_NAME:"
						+ resultSet.getString("POLICY_HOLDER_NAME") + ",  POLICY_LINE_OF_BUSINESS: "
						+ resultSet.getString("POLICY_LINE_OF_BUSINESS") + ",  DESCRIPTION: "
						+ resultSet.getString("DESCRIPTION"));
			} while (resultSet.next());
		}
		connection.close();

	}

	public static ResultSet get_ClientDashboard_ProducerSearch(List<String> producerIDs, String businessUnit,
			String dateFilterSet, int startDate, int endDate)
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getNetezza_Connection();
		Statement statement = null;
		System.out.println("EXECUTING QUERY --------");
		String formattedProducerIDs = producerIDs.toString().replace("[", "") // remove
																				// the
																				// right
																				// bracket
				.replace("]", "") // remove the left bracket
				.trim();
		String query = null;

		if (dateFilterSet.equalsIgnoreCase("YES")) {

			query = " SELECT ROW_NUMBER() OVER(ORDER BY DETAILS.Policy_Number) AS Id, "
					+ " DETAILS.PRODUCER_CODE AS ProducerId, " + " DETAILS.Contact_Producer_Name AS ProducerName, "
					+ " DETAILS.Global_Producer_Name AS ProducerGroup, "
					+ " DETAILS.Global_Producer_Code AS ProducerGroupId, " + " DETAILS.POLICY_NUMBER, "
					+ " DETAILS.Client_ID AS ClientId, " + " DETAILS.Contact_Client_Name AS ClientName, "
					+ " DETAILS.NAIC_Code AS NAICCode, " + " DETAILS.NAIC_Description AS NAICExpl, "
					+ " POL_COUNT.POLICY_COUNT as PolicyCount, " + " DETAILS.PACPL_Code AS ProductCode, "
					+ " DETAILS.PACPL_Desc, " + " sum( DETAILS.Gross_Premium_Ledger ) as Gross_Premium_Ledger, "
					+ " sum( DETAILS.Gross_Commission_Ledger ) as Gross_Commission_Ledger, "
					+ " sum( DETAILS.Gross_Premium_USD ) as Gross_Premium_USD, "
					+ " sum( DETAILS.Gross_Commission_USD ) as Gross_Commission_USD "

					+ "FROM ( " + " SELECT " + "   P.POLICY_KEY as Policy_Key, "
					+ "   P.PRODUCER_CODE as Producer_Code, " + "   P.PRODUCER_NAME as Producer_Name, "
					+ "   nvl(CPR.NAME_1 || ' ' || CPR.NAME_2, 'UNKNOWN') as Contact_Producer_Name, "
					+ "   PTF.TRANS_GLOBAL_PRODUCER_CODE as Global_Producer_Code, "
					+ "   nvl(GP.DESCRIPTION, 'UNKNOWN') as Global_Producer_Name, "
					+ "   P.POLICY_NUMBER as Policy_Number, "
					+ "   nvl(CPH.NAME_1 || ' ' || nvl(CPH.NAME_2,''), 'UNKNOWN') as Contact_Client_Name, "
					+ "   P.POLICY_HOLDER_ID as Client_ID, " + "   P.POLICY_EFFECTIVE_DATE as Policy_Effective_Date, "
					+ "   P.POLICY_LINE_OF_BUSINESS as PACPL_Code, " + "   LOB.PACPL_DESC as PACPL_Desc, "
					+ "   P.CURRENT_FULL_INDUSTRY_CODE as NAIC_Code, " + "   IC.EXPLANATION as NAIC_Description, "
					+ "   PTF.TRANSACTION_CODE as Transaction_Code, "
					+ "   sum( PTF.PREMIUM_IN_LEDGER_CURRENCY ) as Gross_Premium_Ledger, "
					+ "   sum( PTF.COMMISSION_IN_LEDGER_CURRENCY ) as Gross_Commission_Ledger, "
					+ "   sum( PTF.PREMIUM_IN_US_DOLLARS ) as Gross_Premium_USD, "
					+ "   sum( PTF.COMMISSION_IN_US_DOLLARS ) as Gross_Commission_USD "

					+ "          FROM DB_Q_DM_PCW.DB_Q_DM_PCW_OWNER.FCT_PREMIUM_TRANSACTION PTF "
					+ "          INNER JOIN DB_Q_DM_PCW.DB_P_DM_PCW_OWNER.DIM_POLICY P "
					+ "          ON P.POLICY_KEY = PTF.POLICY_KEY "
					+ "          AND P.ORIGINATING_BUSINESS_UNIT = PTF.ORIGINATING_BUSINESS_UNIT "
					+ "          INNER JOIN DB_Q_DM_PCW.DB_Q_DM_PCW_OWNER.DIM_PACPL_TO_SUPR_LINE LOB "
					+ "          ON P.POLICY_LINE_OF_BUSINESS = LOB.PACPL_CODE "
					+ "          INNER JOIN DB_Q_DM_PCW.DB_Q_DM_PCW_OWNER.DIM_CONTACT CPH "
					+ "          ON P.POLICY_HOLDER_ID = CPH.CONTACT_CODE "
					+ "          AND P.ORIGINATING_BUSINESS_UNIT = CPH.ORIGINATING_BUSINESS_UNIT "
					+ "          AND CPH.CONTACT_TYPE = 'PH' "
					+ "          LEFT OUTER JOIN DB_Q_DM_PCW.DB_Q_DM_PCW_OWNER.DIM_CONTACT CPR "
					+ "          ON P.PRODUCER_CODE = CPR.CONTACT_CODE "
					+ "          AND P.ORIGINATING_BUSINESS_UNIT = CPR.ORIGINATING_BUSINESS_UNIT "
					+ "          AND CPR.CONTACT_TYPE = 'PR' "
					+ "          INNER JOIN DB_Q_DM_PCW.DB_Q_DM_PCW_OWNER.DIM_GLOBAL_PRODUCER GP "
					+ "          ON PTF.TRANS_GLOBAL_PRODUCER_CODE = GP.CODE "
					+ "          LEFT OUTER JOIN DB_P_DM_PCW_OWNER.DIM_INDUSTRY_CODE IC "
					+ "          ON P.CURRENT_FULL_INDUSTRY_CODE = IC.INDUSTRY_CODE "

					+ "       WHERE P.ORIGINATING_BUSINESS_UNIT IN (" + "'" + businessUnit + "'" + " ) "
					+ "  AND PTF.DETAIL_LEVEL_INDICATOR = 'Y' AND PTF.DATA_SOURCE_ID NOT IN ('FT_AUE')  AND PTF.DIRECT_ASSUMED_CEDED_CODE IN ('1','2') AND P.PRODUCER_CODE IN "
					+ "(" + formattedProducerIDs + ")"
					+ "       GROUP BY P.POLICY_KEY, P.PRODUCER_CODE, P.PRODUCER_NAME, CPR.NAME_1, CPR.NAME_2, PTF.TRANS_GLOBAL_PRODUCER_CODE, GP.DESCRIPTION, P.POLICY_NUMBER, CPH.NAME_1, CPH.NAME_2, P.POLICY_HOLDER_ID, P.POLICY_EFFECTIVE_DATE, "
					+ "       P.POLICY_LINE_OF_BUSINESS, P.CURRENT_FULL_INDUSTRY_CODE, IC.EXPLANATION, PTF.TRANSACTION_CODE, LOB.PACPL_DESC ) as DETAILS, "
					+ "  (    SELECT   P.POLICY_HOLDER_ID, count( distinct P.POLICY_NUMBER ) as POLICY_COUNT FROM DB_Q_DM_PCW.DB_Q_DM_PCW_OWNER.DIM_POLICY P "
					+ "       WHERE P.ORIGINATING_BUSINESS_UNIT IN (" + "'" + businessUnit + "'" + " ) "
					+ "       AND P.Policy_Effective_Date BETWEEN " + startDate + " AND  " + endDate
					+ "       GROUP BY P.POLICY_HOLDER_ID " + " ) as POL_COUNT "

					+ " WHERE DETAILS.Client_ID = POL_COUNT.POLICY_HOLDER_ID "
					+ " AND DETAILS.Policy_Effective_Date BETWEEN " + startDate + " AND  " + endDate
					+ " GROUP BY DETAILS.PRODUCER_CODE, DETAILS.Contact_Producer_Name, DETAILS.Global_Producer_Code, DETAILS.Global_Producer_Name, DETAILS.POLICY_NUMBER, DETAILS.Contact_Client_Name, DETAILS.NAIC_Code, DETAILS.NAIC_Description, POL_COUNT.POLICY_COUNT, DETAILS.PACPL_Code, DETAILS.PACPL_Desc, DETAILS.Client_ID "
					+ " ORDER BY DETAILS.Policy_Number ";

		} else {
			query = " SELECT ROW_NUMBER() OVER(ORDER BY DETAILS.Policy_Number) AS Id, "
					+ " DETAILS.PRODUCER_CODE AS ProducerId, " + " DETAILS.Contact_Producer_Name AS ProducerName, "
					+ " DETAILS.Global_Producer_Name AS ProducerGroup, "
					+ " DETAILS.Global_Producer_Code AS ProducerGroupId, " + " DETAILS.POLICY_NUMBER, "
					+ " DETAILS.Client_ID AS ClientId, " + " DETAILS.Contact_Client_Name AS ClientName, "
					+ " DETAILS.NAIC_Code AS NAICCode, " + " DETAILS.NAIC_Description AS NAICExpl, "
					+ " POL_COUNT.POLICY_COUNT as PolicyCount, " + " DETAILS.PACPL_Code AS ProductCode, "
					+ " DETAILS.PACPL_Desc, " + " sum( DETAILS.Gross_Premium_Ledger ) as Gross_Premium_Ledger, "
					+ " sum( DETAILS.Gross_Commission_Ledger ) as Gross_Commission_Ledger, "
					+ " sum( DETAILS.Gross_Premium_USD ) as Gross_Premium_USD, "
					+ " sum( DETAILS.Gross_Commission_USD ) as Gross_Commission_USD "

					+ "FROM ( " + " SELECT " + "   P.POLICY_KEY as Policy_Key, "
					+ "   P.PRODUCER_CODE as Producer_Code, " + "   P.PRODUCER_NAME as Producer_Name, "
					+ "   nvl(CPR.NAME_1 || ' ' || CPR.NAME_2, 'UNKNOWN') as Contact_Producer_Name, "
					+ "   PTF.TRANS_GLOBAL_PRODUCER_CODE as Global_Producer_Code, "
					+ "   nvl(GP.DESCRIPTION, 'UNKNOWN') as Global_Producer_Name, "
					+ "   P.POLICY_NUMBER as Policy_Number, "
					+ "   nvl(CPH.NAME_1 || ' ' || nvl(CPH.NAME_2,''), 'UNKNOWN') as Contact_Client_Name, "
					+ "   P.POLICY_HOLDER_ID as Client_ID, " + "   P.POLICY_EFFECTIVE_DATE as Policy_Effective_Date, "
					+ "   P.POLICY_LINE_OF_BUSINESS as PACPL_Code, " + "   LOB.PACPL_DESC as PACPL_Desc, "
					+ "   P.CURRENT_FULL_INDUSTRY_CODE as NAIC_Code, " + "   IC.EXPLANATION as NAIC_Description, "
					+ "   PTF.TRANSACTION_CODE as Transaction_Code, "
					+ "   sum( PTF.PREMIUM_IN_LEDGER_CURRENCY ) as Gross_Premium_Ledger, "
					+ "   sum( PTF.COMMISSION_IN_LEDGER_CURRENCY ) as Gross_Commission_Ledger, "
					+ "   sum( PTF.PREMIUM_IN_US_DOLLARS ) as Gross_Premium_USD, "
					+ "   sum( PTF.COMMISSION_IN_US_DOLLARS ) as Gross_Commission_USD "

					+ "          FROM DB_Q_DM_PCW.DB_Q_DM_PCW_OWNER.FCT_PREMIUM_TRANSACTION PTF "
					+ "          INNER JOIN DB_Q_DM_PCW.DB_P_DM_PCW_OWNER.DIM_POLICY P "
					+ "          ON P.POLICY_KEY = PTF.POLICY_KEY "
					+ "          AND P.ORIGINATING_BUSINESS_UNIT = PTF.ORIGINATING_BUSINESS_UNIT "
					+ "          INNER JOIN DB_Q_DM_PCW.DB_Q_DM_PCW_OWNER.DIM_PACPL_TO_SUPR_LINE LOB "
					+ "          ON P.POLICY_LINE_OF_BUSINESS = LOB.PACPL_CODE "
					+ "          INNER JOIN DB_Q_DM_PCW.DB_Q_DM_PCW_OWNER.DIM_CONTACT CPH "
					+ "          ON P.POLICY_HOLDER_ID = CPH.CONTACT_CODE "
					+ "          AND P.ORIGINATING_BUSINESS_UNIT = CPH.ORIGINATING_BUSINESS_UNIT "
					+ "          AND CPH.CONTACT_TYPE = 'PH' "
					+ "          LEFT OUTER JOIN DB_Q_DM_PCW.DB_Q_DM_PCW_OWNER.DIM_CONTACT CPR "
					+ "          ON P.PRODUCER_CODE = CPR.CONTACT_CODE "
					+ "          AND P.ORIGINATING_BUSINESS_UNIT = CPR.ORIGINATING_BUSINESS_UNIT "
					+ "          AND CPR.CONTACT_TYPE = 'PR' "
					+ "          INNER JOIN DB_Q_DM_PCW.DB_Q_DM_PCW_OWNER.DIM_GLOBAL_PRODUCER GP "
					+ "          ON PTF.TRANS_GLOBAL_PRODUCER_CODE = GP.CODE "
					+ "          LEFT OUTER JOIN DB_P_DM_PCW_OWNER.DIM_INDUSTRY_CODE IC "
					+ "          ON P.CURRENT_FULL_INDUSTRY_CODE = IC.INDUSTRY_CODE "

					+ "       WHERE P.ORIGINATING_BUSINESS_UNIT IN (" + "'" + businessUnit + "'" + " ) "
					+ "  AND PTF.DETAIL_LEVEL_INDICATOR = 'Y' AND PTF.DATA_SOURCE_ID NOT IN ('FT_AUE')  AND PTF.DIRECT_ASSUMED_CEDED_CODE IN ('1','2') AND P.PRODUCER_CODE IN "
					+ "(" + formattedProducerIDs + ")"
					+ "       GROUP BY P.POLICY_KEY, P.PRODUCER_CODE, P.PRODUCER_NAME, CPR.NAME_1, CPR.NAME_2, PTF.TRANS_GLOBAL_PRODUCER_CODE, GP.DESCRIPTION, P.POLICY_NUMBER, CPH.NAME_1, CPH.NAME_2, P.POLICY_HOLDER_ID, P.POLICY_EFFECTIVE_DATE, "
					+ "       P.POLICY_LINE_OF_BUSINESS, P.CURRENT_FULL_INDUSTRY_CODE, IC.EXPLANATION, PTF.TRANSACTION_CODE, LOB.PACPL_DESC ) as DETAILS, "
					+ "  (    SELECT   P.POLICY_HOLDER_ID, count( distinct P.POLICY_NUMBER ) as POLICY_COUNT FROM DB_Q_DM_PCW.DB_Q_DM_PCW_OWNER.DIM_POLICY P "
					+ "       WHERE P.ORIGINATING_BUSINESS_UNIT IN (" + "'" + businessUnit + "'" + " ) "
					// +" AND P.Policy_Effective_Date BETWEEN 20150101 AND
					// 20150331 "
					+ "       GROUP BY P.POLICY_HOLDER_ID " + " ) as POL_COUNT "

					+ " WHERE DETAILS.Client_ID = POL_COUNT.POLICY_HOLDER_ID "
					// +" AND DETAILS.Policy_Effective_Date BETWEEN "+ startDate
					// +" AND " + endDate
					+ " GROUP BY DETAILS.PRODUCER_CODE, DETAILS.Contact_Producer_Name, DETAILS.Global_Producer_Code, DETAILS.Global_Producer_Name, DETAILS.POLICY_NUMBER, DETAILS.Contact_Client_Name, DETAILS.NAIC_Code, DETAILS.NAIC_Description, POL_COUNT.POLICY_COUNT, DETAILS.PACPL_Code, DETAILS.PACPL_Desc, DETAILS.Client_ID "
					+ " ORDER BY DETAILS.Policy_Number ";

		}

		statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		/*
		 * if (!resultSet.next()) { System.out.
		 * println("No results for -- get_Client_Dashboard_Producer_Search"); }
		 * else { do { System.out.println("PRODUCERID:" +
		 * resultSet.getString("PRODUCERID") + ",  PRODUCERNAME: " +
		 * resultSet.getString("PRODUCERNAME") + ",  PRODUCERGROUP:" +
		 * resultSet.getString("PRODUCERGROUP") + "PRODUCERGROUPID:" +
		 * resultSet.getString("PRODUCERGROUPID") + ",  POLICY_NUMBER: " +
		 * resultSet.getString("POLICY_NUMBER") + ",  CLIENTID:" +
		 * resultSet.getString("CLIENTID") + "CLIENTNAME:" +
		 * resultSet.getString("CLIENTNAME") + ",  NAICCODE: " +
		 * resultSet.getString("NAICCODE") + ",  POLICYCOUNT:" +
		 * resultSet.getString("POLICYCOUNT")); }while (resultSet.next()); }
		 */
		connection.close();
		return resultSet;

	}

	// *************************************************END OF PCW OLD
	// QUERIES****************************************************************************//

	// ************************************************************* PCW NEW
	// QUERIES
	// *******************************************************************//

	/*
	 * Query 1 - PRODUCER SELECTION -- RETURNS PRODUCER_CODE, PRODUCER_NAME,
	 * CURRENT_GLOBAL_PRODUCER_CODE,PRODUCER_GROUP, CLIENT_ID,LOB_CODE,
	 * NAIC_CODE, GROSS_PREMIUM_LEDGER,GROSS_PREMIUM_USD, GROSS_COMMISSION_USD
	 */

	public static void get_PRODCUER_SELECTION(String Producer_Name, String Start_Date, String End_Date)
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		System.out.println("Control Reached producer selection");
		Connection connection = getNetezza_Connection();
		Statement statement = null;
		String query = "SELECT P.PRODUCER_CODE as Producer_Code, " + " P.PRODUCER_NAME as Producer_Name, "
				+ " P.CURRENT_GLOBAL_PRODUCER_CODE, " + " 'Group TBD' as PRODUCER_GROUP, "
				+ " P.POLICY_NUMBER as Policy_Number, " + " P.POLICY_HOLDER_ID as Client_ID, "
				+ " P.POLICY_LINE_OF_BUSINESS as LOB_Code, " + " P.CURRENT_FULL_INDUSTRY_CODE as NAIC_Code, "
				+ " sum( PTF.PREMIUM_IN_LEDGER_CURRENCY ) as Gross_Premium_Ledger, "
				+ " sum( PTF.COMMISSION_IN_LEDGER_CURRENCY ) as Gross_Commission_Ledger, "
				+ " sum( PTF.PREMIUM_IN_US_DOLLARS ) as Gross_Premium_USD, "
				+ " sum( PTF.COMMISSION_IN_US_DOLLARS ) as Gross_Commission_USD "

				+ " FROM FCT_PREMIUM_TRANSACTION PTF " + " INNER JOIN DIM_POLICY P "
				+ " ON P.POLICY_KEY = PTF.POLICY_KEY "
				+ " AND P.ORIGINATING_BUSINESS_UNIT = PTF.ORIGINATING_BUSINESS_UNIT "

				+ " WHERE P.ORIGINATING_BUSINESS_UNIT = '35001' " + " AND PTF.DETAIL_LEVEL_INDICATOR = 'Y' "
				+ " AND PTF.DATA_SOURCE_ID NOT IN ( 'FT_AUE' )  "
				+ " AND PTF.DIRECT_ASSUMED_CEDED_CODE IN ( '1', '2' ) " + " AND P.PRODUCER_NAME LIKE ( " + "'"
				+ Producer_Name + "'" + " )  "
				+ " AND ( P.CANCELLATION_REASON IS NULL OR P.CANCELLATION_REASON = ' ' ) "
				+ " AND P.POLICY_EXPIRATION_DATE between " + Start_Date + " and  " + End_Date + " "

				+ " GROUP BY P.PRODUCER_CODE, " + " P.PRODUCER_NAME, " + " P.CURRENT_GLOBAL_PRODUCER_CODE, "
				+ " PRODUCER_GROUP, " + " P.POLICY_NUMBER, " + " P.POLICY_HOLDER_ID, " + " P.POLICY_LINE_OF_BUSINESS, "
				+ " P.CURRENT_FULL_INDUSTRY_CODE; ";

		statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		System.out.println(
				"****************************************get_PRODCUER_SELECTION**************************************************************************");
		int counter = 0;
		if (!resultSet.next()) {
			System.out.println("No results for -- get_PRODCUER_SELECTION");
		} else {
			do {
				System.out.println("PRODUCER_CODE:" + resultSet.getString("PRODUCER_CODE") + ",  PRODUCER_NAME: "
						+ resultSet.getString("PRODUCER_NAME") + ",  CURRENT_GLOBAL_PRODUCER_CODE: "
						+ resultSet.getString("CURRENT_GLOBAL_PRODUCER_CODE") + ",  PRODUCER_GROUP: "
						+ resultSet.getString("PRODUCER_GROUP") + ",  POLICY_NUMBER: "
						+ resultSet.getString("POLICY_NUMBER") + ",  CLIENT_ID: " + resultSet.getString("CLIENT_ID")
						+ ",  LOB_CODE: " + resultSet.getString("LOB_CODE") + ",  NAIC_CODE: "
						+ resultSet.getString("NAIC_CODE") + ",  GROSS_PREMIUM_LEDGER: "
						+ resultSet.getString("GROSS_PREMIUM_LEDGER") + ",  GROSS_PREMIUM_USD: "
						+ resultSet.getString("GROSS_PREMIUM_USD") + ",  GROSS_COMMISSION_USD: "
						+ resultSet.getString("GROSS_COMMISSION_USD"));
				counter++;
			} while (resultSet.next());
		}
		System.out.println("Total number of rows in get_PRODCUER_SELECTION " + counter);

		connection.close();

	}

	/*
	 * Query 2 - TOTAL PREMIUM BY PACPL -- RETURNS CLIENT_ID, LOB_CODE,
	 * PRODUCER_NAME, GROSS_PREMIUM_LEDGER AND GROSS_PREMIUM_USD
	 */

	public static void get_TOTAL_PREMIUM_BY_PACPL(String Producer_Name, String Start_Date, String End_Date)
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getNetezza_Connection();
		Statement statement = null;
		String query = "SELECT P.POLICY_HOLDER_ID as Client_ID, " + " P.POLICY_LINE_OF_BUSINESS as LOB_Code,"
				+ " P.PRODUCER_NAME, " + " sum( PTF.PREMIUM_IN_LEDGER_CURRENCY ) as Gross_Premium_Ledger, "
				+ " sum( PTF.PREMIUM_IN_US_DOLLARS ) as Gross_Premium_USD "

				+ " FROM DB_P_DM_PCW_OWNER.DIM_POLICY P, " + " DB_P_DM_PCW_OWNER.FCT_PREMIUM_TRANSACTION PTF "

				+ " WHERE P.POLICY_KEY = PTF.POLICY_KEY " + " AND P.ORIGINATING_BUSINESS_UNIT = '35001' "
				+ " AND PTF.DETAIL_LEVEL_INDICATOR = 'Y' " + " AND PTF.DATA_SOURCE_ID NOT IN ( 'FT_AUE' )  "
				+ " AND PTF.DIRECT_ASSUMED_CEDED_CODE IN ( '1', '2' ) " + " AND P.PRODUCER_NAME LIKE ( " + "'"
				+ Producer_Name + "'" + " )  "
				+ " AND ( P.CANCELLATION_REASON IS NULL OR P.CANCELLATION_REASON = ' ' ) "
				+ " AND P.POLICY_EXPIRATION_DATE between  " + Start_Date + " and  " + End_Date + " "

				+ " GROUP BY P.POLICY_HOLDER_ID, " + " P.POLICY_LINE_OF_BUSINESS, " + " P.PRODUCER_NAME; ";

		statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		System.out.println(
				"****************************************get_TOTAL_PREMIUM_BY_PACPL**************************************************************************");
		int counter = 0;
		if (!resultSet.next()) {
			System.out.println("No results for -- get_TOTAL_PREMIUM_BY_PACPL");
		} else {
			do {
				System.out.println("CLIENT_ID:" + resultSet.getString("CLIENT_ID") + ",  LOB_CODE: "
						+ resultSet.getString("LOB_CODE") + ",  PRODUCER_NAME: " + resultSet.getString("PRODUCER_NAME")
						+ ",  GROSS_PREMIUM_LEDGER: " + resultSet.getString("GROSS_PREMIUM_LEDGER")
						+ ",  GROSS_PREMIUM_USD: " + resultSet.getString("GROSS_PREMIUM_USD"));
				counter++;
			} while (resultSet.next());
		}
		System.out.println("Total number of rows in get_TOTAL_PREMIUM_BY_PACPL " + counter);

		connection.close();

	}

	/*
	 * Query 3 - CLIENT POLICY COUNT -- RETURNS POLICY_HOLDER_ID AND POL_COUNT
	 */
	public static void get_CLIENT_POLICY_COUNT(String Expiration_Date)
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getNetezza_Connection();
		Statement statement = null;
		String query = "SELECT P.POLICY_HOLDER_ID, " + " count( distinct P.POLICY_NUMBER ) as POL_COUNT "
				+ " FROM DIM_POLICY P, FCT_PREMIUM_TRANSACTION PTF "

				+ " WHERE P.POLICY_KEY = PTF.POLICY_KEY "
				+ " AND P.ORIGINATING_BUSINESS_UNIT = PTF.ORIGINATING_BUSINESS_UNIT "
				+ " AND PTF.ORIGINATING_BUSINESS_UNIT = '35001' " + " AND PTF.DETAIL_LEVEL_INDICATOR = 'Y' "
				+ " AND PTF.DATA_SOURCE_ID NOT IN ( 'FT_AUE' )  "
				+ " AND PTF.DIRECT_ASSUMED_CEDED_CODE IN ( '1', '2' ) "
				+ " AND ( P.CANCELLATION_REASON IS NULL OR P.CANCELLATION_REASON = ' ' ) "
				+ " AND P.POLICY_EXPIRATION_DATE >  " + Expiration_Date + " "

				+ " GROUP BY P.POLICY_HOLDER_ID, " + " P.CANCELLATION_REASON " + " ORDER BY P.POLICY_HOLDER_ID ASC; ";

		statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		System.out.println(
				"****************************************POLICY COUNT BY CLIENT IDs**************************************************************************");
		int counter = 0;
		if (!resultSet.next()) {
			System.out.println("No results for -- get_POLICY_COUNT by CLIENT IDs");
		} else {
			do {
				System.out.println("POLICY_HOLDER_ID:" + resultSet.getString("POLICY_HOLDER_ID") + ",  POL_COUNT: "
						+ resultSet.getString("POL_COUNT"));
				counter++;
			} while (resultSet.next());
		}
		System.out.println("Total number of rows in get POLICY COUNT " + counter);

		connection.close();

	}

	/* Query 4 - CLIENT DETAILS -- Returns CLIENT_ID and CLIENT_NAME */
	public static void get_CLIENT_DETAILS(String Contact_Type)
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getNetezza_Connection();
		Statement statement = null;
		String query = "SELECT C.CONTACT_CODE as Client_ID, " + " C.NAME_1 || ' ' || nvl(C.NAME_2,'') as Client_Name "
				+ " FROM DB_P_DM_PCW_OWNER.DIM_CONTACT C "

				+ " WHERE C.CONTACT_TYPE = " + "'" + Contact_Type + "'" + " "
				+ " AND C.ORIGINATING_BUSINESS_UNIT = '35001' "

				+ " GROUP BY C.CONTACT_CODE, " + " C.NAME_1, " + " C.NAME_2; ";

		statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		System.out.println(
				"****************************************GET CLIENT DETAILS**************************************************************************");
		int counter = 0;
		if (!resultSet.next()) {
			System.out.println("No results for -- get_CLIENT_DETAILS");
		} else {
			do {
				System.out.println("CLIENT_ID:" + resultSet.getString("CLIENT_ID") + ",  CLIENT_NAME: "
						+ resultSet.getString("CLIENT_NAME"));
				counter++;
			} while (resultSet.next());
		}
		System.out.println("Total number of rows in get CLIENT DETAILS " + counter);
		connection.close();

	}

	/*
	 * Query 5 - GLOBAL PRODUCER DETAILS -- RETURNS GLOBAL_PRODUCER_CODE AND
	 * GLOBAL_PRODUCER_NAME
	 */
	public static void get_GLOBAL_PRODUCER_DETAILS()
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getNetezza_Connection();
		Statement statement = null;
		String query = "SELECT GP.CODE as Global_Producer_Code, " + " GP.DESCRIPTION as Global_Producer_Name "
				+ " FROM DIM_GLOBAL_PRODUCER GP "

				+ " GROUP BY GP.CODE, " + " GP.DESCRIPTION "

				+ " ORDER BY GP.CODE ASC; ";

		statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		System.out.println(
				"****************************************GET GLOBAL PRODUCER DETAILS**************************************************************************");
		int counter = 0;
		if (!resultSet.next()) {
			System.out.println("No results for -- get_GLOBAL_PRODUCER_DETAILS");
		} else {
			do {
				System.out.println("GLOBAL_PRODUCER_CODE:" + resultSet.getString("GLOBAL_PRODUCER_CODE")
						+ ",  GLOBAL_PRODUCER_NAME: " + resultSet.getString("GLOBAL_PRODUCER_NAME"));
				counter++;
			} while (resultSet.next());
		}
		System.out.println("Total number of rows in get_GLOBAL_PRODUCER_DETAILS  " + counter);
		connection.close();

	}

	/*
	 * Query 6 - INDUSTRY CODE DESCRIPTION -- RETURNS INDUSTRY_CODE AND
	 * EXPLANATION
	 */

	public static void get_INDUSTRY_CODE_DESCRIPTION()
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		Connection connection = getNetezza_Connection();
		Statement statement = null;
		String query = "SELECT IC.INDUSTRY_CODE, " + " IC.EXPLANATION "

				+ " FROM DIM_INDUSTRY_CODE IC "

				+ " GROUP BY IC.INDUSTRY_CODE, " + " IC.EXPLANATION "

				+ " ORDER BY IC.INDUSTRY_CODE ASC; ";

		statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		System.out.println(
				"****************************************GET INDUSTRY CODE DESCRIPTION**************************************************************************");
		int counter = 0;
		if (!resultSet.next()) {
			System.out.println("No results for -- get_INDUSTRY_CODE_DESCRIPTION");
		} else {
			do {
				System.out.println("INDUSTRY_CODE:" + resultSet.getString("INDUSTRY_CODE") + ",  EXPLANATION: "
						+ resultSet.getString("EXPLANATION"));
				counter++;
			} while (resultSet.next());
		}
		System.out.println("Total number of rows in get_INDUSTRY_CODE_DESCRIPTION " + counter);
		connection.close();

	}

}
