package au.com.chubb.evolutionshowcase.hooks;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import au.com.chubb.evolutionshowcase.utilities.TestSettings;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import net.thucydides.core.annotations.Managed;

public class Hooks {

	// @Managed(uniqueSession = true)
	@Managed(uniqueSession = true, driver = "IExplorer")
	WebDriver driver;
	public static TestSettings testSettings;
	public static Properties credentials;
	String root = System.getProperty("user.dir");
	public static Properties CONFIG = null;

	public Hooks() {
		System.out.println("--------------------INITIALIZE HOOKS----------------------");
		testSettings = TestSettings.getInstance();
		credentials = testSettings.getConfigProp();
	}

	@SuppressWarnings("deprecation")
	@Before
	public void BeforeScenario() throws IOException {
		System.out.println("--------------------BEFORE SCENARIO----------------------");

			CONFIG = new Properties();
		FileInputStream fs = new FileInputStream(root + "/configuration/CONFIG.properties");
		CONFIG.load(fs);
		System.setProperty("webdriver.ie.driver", (root + CONFIG.getProperty("IE_Driver_Path")));
		DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
		ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		ieCapabilities.setCapability(CapabilityType.BROWSER_NAME, "internet explorer");
		ieCapabilities.setCapability(CapabilityType.PAGE_LOAD_STRATEGY, "normal");
		ieCapabilities.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
		ieCapabilities.setCapability(CapabilityType.VERSION, "11");
		ieCapabilities.setCapability(CapabilityType.PLATFORM, "WINDOWS");
		ieCapabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, "dismiss");

/*
		InternetExplorerOptions ieOptions = new InternetExplorerOptions()
		        .enablePersistentHovering().ignoreZoomSettings()
		         .enableNativeEvents().addCommandSwitches("");
				 		
		ieCapabilities.setCapability("se:ieOptions", ieOptions); 
	*/	
		/*
		ieCapabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
		ieCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		ieCapabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		ieCapabilities.setCapability(CapabilityType.PROXY, true);
		ieCapabilities.setCapability(CapabilityType.SUPPORTS_ALERTS, true);
		ieCapabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, true);
		ieCapabilities.setCapability(CapabilityType.UNHANDLED_PROMPT_BEHAVIOUR, true);
		ieCapabilities.setCapability(CapabilityType.SUPPORTS_NETWORK_CONNECTION, true);
*/
	
		
		
		//driver = new InternetExplorerDriver(ieCapabilities);
		driver.manage().window().maximize();
	}

	@After
	public void AfterScenario() {
		System.out.println("--------------------AFTER SCENARIO----------------------");
		driver.quit();
		//driver.close();
	}

}
