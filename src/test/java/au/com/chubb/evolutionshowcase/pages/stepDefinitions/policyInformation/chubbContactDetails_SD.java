package au.com.chubb.evolutionshowcase.pages.stepDefinitions.policyInformation;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.policyInformation.chubbContactDetails_Steps;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

public class chubbContactDetails_SD {

	@Steps
	chubbContactDetails_Steps chubbContactDetails;

	
	@And("^select chubb contact name \"([^\"]*)\"$")
	public void select_chubb_contact_name(String insurer_name) throws Throwable {
		chubbContactDetails.select_chubb_contact_name(insurer_name);
	}
	
	@And("^check do not renew with justification reason \"([^\"]*)\"$")
	public void do_not_renew(String justificationReason) throws Throwable {
		chubbContactDetails.do_not_renew(justificationReason);
	}
	
	@And("^check manual renewal with justification reason \"([^\"]*)\"$")
	public void manual_renewal(String justificationReason) throws Throwable {
		chubbContactDetails.manual_renewal(justificationReason);
	}
	
	
	@And("^click Next in policy information page$")
	public void click_next_button() throws Throwable {
		chubbContactDetails.click_next_button();
	}
	
	@And("^click on Risks/Coverages navigation$")
	public void click_risk_and_coverages() throws Throwable {
		chubbContactDetails.click_risk_and_coverages();
	}

	
	
	
}
