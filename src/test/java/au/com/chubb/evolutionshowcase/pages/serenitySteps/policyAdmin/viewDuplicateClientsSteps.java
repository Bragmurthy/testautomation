package au.com.chubb.evolutionshowcase.pages.serenitySteps.policyAdmin;


import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebElement;

import au.com.chubb.evolutionshowcase.pages.objectModel.policyAdmin.policyAdminWelcomePage;
import au.com.chubb.evolutionshowcase.utilities.CommonFunctions;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;


@RunWith(SerenityRunner.class)
public class viewDuplicateClientsSteps extends PageObject {

	CommonFunctions common_functions;
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	@Step
	public void selectExistingClient() {
	getDriver().findElement(By.xpath("//table[preceding-sibling::p[contains(text(),'Existing client details in Evolution:')]]//input[@name='existing-client-selector']")).click();}
	
	@Step
	public void selectPossibleDuplicateClient() throws InterruptedException {
	getDriver().findElement(By.xpath("//table[preceding-sibling::p[contains(text(),'Possible duplicate client created by broker:')]]//input[@name='existing-client-selector']")).click();
	Thread.sleep(5000);
	}
	
}