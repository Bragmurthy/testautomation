package au.com.chubb.evolutionshowcase.pages.stepDefinitions.riskAndCoverages;


import au.com.chubb.evolutionshowcase.pages.serenitySteps.riskAndCoverages.VerifyLocationDetailsFromIndicativeQuote_Steps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class verifyLocationDetailsFromIndicativeQuote {

	@Steps
	VerifyLocationDetailsFromIndicativeQuote_Steps verifyDetails;

	
	@And("^Verify details for location \"([^\"]*)\" as per \"([^\"]*)\"$")
	public void verify_risk_location_details(String location, String testData) throws Throwable {
		verifyDetails.verify_risk_location_details(location,testData);
	}
	
	@And("^click on edit quote for location \"([^\"]*)\"$")
	public void click_edit_quote(String location) throws Throwable {
		verifyDetails.click_edit_quote(location);
	}
	@And("^Verify Protected value is \"([^\"]*)\"$")
	public void verify_Protected_value_is(String expectedText) throws Throwable {
		verifyDetails.verifyProtectedFiledValue(expectedText);
	}
	
	@And("^Verify Flood Zone rating is \"([^\"]*)\" and reason is \"([^\"]*)\"$")
	public void verify_FloodZone_value_is(String EliasValue, String OverrideReason) throws Throwable {
		verifyDetails.verifyFloodZoneValue(EliasValue, OverrideReason);
	}
	
	@Given("^verify \"([^\"]*)\" button is selected by default$")
	public void verify_button_is_selected_by_default(String fieldName) throws Throwable {
		verifyDetails.verifyFieldSelectedByDefault(fieldName);
	}
	
	@And("^verify \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" are non editable during MTA$")
	public void RiskLocationDetailsarenoneditable(String address,String property,String ownership, String residence){
		
		
	}
}
