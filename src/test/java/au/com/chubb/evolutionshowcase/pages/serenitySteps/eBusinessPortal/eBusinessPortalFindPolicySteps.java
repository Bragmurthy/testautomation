package au.com.chubb.evolutionshowcase.pages.serenitySteps.eBusinessPortal;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import au.com.chubb.evolutionshowcase.pages.objectModel.eBusinessPortal.eBusinessPortalFindPolicyPage;
import au.com.chubb.evolutionshowcase.utilities.CommonFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class eBusinessPortalFindPolicySteps extends PageObject {
	String root = System.getProperty("user.dir");
	static eBusinessPortalFindPolicyPage eBusinessPortalFindPolicyPage;
	static CommonFunctions common_functions;

		
	@Step
	public void click_find_policy() throws InterruptedException {
		eBusinessPortalFindPolicyPage.click_find_policy();
	}
	
	@Step
	public void enter_policy_number(String policynumber) throws Throwable {
		eBusinessPortalFindPolicyPage.enter_policy_number(policynumber);
	}
	
	@Step
	public void select_transaction_type(String transaction) throws Throwable {
		eBusinessPortalFindPolicyPage.select_transaction_type(transaction);
	}
	
	@Step
	public void searchPolicy(String testData) throws InterruptedException {
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
	String sheetName = "IndicativeQuoteRiskLocation";
	Thread.sleep(1000);

	int Row_Count = XLS.getRowCount(sheetName);
	int rowNum = 0;

	innerloop: for (int row = 2; row <= Row_Count; row++) {

		System.out.println("test data --" + testData);
		if (testData.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
			rowNum = row;
			break innerloop;
		}
	}

	String policyNumber_excel=XLS.getCellData(sheetName, "PolicyNumber", rowNum);
	System.out.println("Quote Reference Excel -- " +policyNumber_excel );


	loop:
	for (int i=0;i<100;i++)
	{
		try
		{
			getDriver().findElement(By.name("txtPolicyNo")).sendKeys(policyNumber_excel);
	        //getDriver().findElement(By.xpath("html/body/table/tbody/tr[2]/td[2]/form/table[2]/tbody/tr[2]/td[4]/input")).sendKeys(quoteReferenceNumber_excel);
	        Thread.sleep(1000);
	        getDriver().findElement(By.id("Search")).click();
	        Thread.sleep(5000);
	      
	        break loop;

			
		}catch (Exception e)
		{
			Thread.sleep(1000);

		}

	}
		
	}
	@Step
	public void click_View_Ebix() throws Throwable {
		getDriver().findElement(By.className("StdLinkOver")).click();
		
	}
}