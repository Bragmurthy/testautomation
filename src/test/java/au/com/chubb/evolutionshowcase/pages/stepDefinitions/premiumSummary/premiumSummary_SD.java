package au.com.chubb.evolutionshowcase.pages.stepDefinitions.premiumSummary;


import au.com.chubb.evolutionshowcase.pages.serenitySteps.premiumSummary.premiumSummary_Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class premiumSummary_SD {

	@Steps
	premiumSummary_Steps premiumSummarySteps;
	
	
	@Given("^verify user is on Premium Summary Page$")
	public void change_broker_contact() throws Throwable {
		premiumSummarySteps.userOnPremiumPage();
	}
	
	@Given("^Save Quote Reference Number in \"([^\"]*)\"$")
	public void saveQuoteNumber(String testdata) throws Throwable {
		premiumSummarySteps.saveQuoteNumber(testdata);
	}
	@Given("^Save the Our Ref Number in \"([^\"]*)\"$")
	public void saveOurRefNumber(String testdata) throws Throwable {
		premiumSummarySteps.saveOurRefNumber(testdata);
	}
	
//EP3-191 change
	

	@Given("^Select the Deductible option$")
	public void SelectDeductibleOption() throws Throwable {
		premiumSummarySteps.SelectDeductibleOption();
	}
//	@Given("^save the Our Ref Number in \"([^\"]*)\" from the button$")
//	public void saveOurRefNumberFomButton(String testdata) throws Throwable {
//		premiumSummarySteps.saveOurRefNumberFomButton(testdata);
//	}
	
}
