package au.com.chubb.evolutionshowcase.pages.stepDefinitions.eBusinessPortal;


import au.com.chubb.evolutionshowcase.pages.serenitySteps.eBusinessPortal.eBusinessPortalAddNewQuoteSteps;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class eBusinessPortalAddNewQuoteStepDefinition {

	@Steps
	eBusinessPortalAddNewQuoteSteps add_new_quote;

	@And("^click on Add a New Quote$")
	public void click_add_new_quote() throws Throwable {
		add_new_quote.click_add_new_quote();
		}
	
	@And("^click on New Business$")
	public void click_new_business() throws Throwable {
		add_new_quote.click_new_business();
		}

	@And("^enter new Quote details as below$")
	public void enter_company_id(DataTable arg1) throws Throwable {
		add_new_quote.enter_new_quote_details(arg1);
	}
	
	@And("^enter policy start date as below$")
	public void enter_policy_start_date_details(DataTable arg1) throws Throwable {
		add_new_quote.enter_policy_start_date_details(arg1);
	}

	@And("^click add risk details$")
	public void click_add_risk_details() throws Throwable {
		add_new_quote.click_add_risk_details();
	}
	
	@Given("^click edit risk details$")
	public void click_edit_risk_details() throws Throwable {
		add_new_quote.click_edit_risk_details();
	}
	
	@Given("^click view risk details$")
	public void click_view_risk_details() throws Throwable {
		add_new_quote.click_view_risk_details();
	}
			
	@And("^verify landing URL is \"([^\"]*)\"$")
	public void verify_landing_URL(String URL) throws Throwable {
		add_new_quote.verify_landing_URL(URL);
	}
	
	@And("^Verify Quote status is \"([^\"]*)\"$")
	public void verify_Quote_status_is(String quoteStatus) throws Throwable {
		add_new_quote.verify_Quote_status_is(quoteStatus);
	
	}	
	@And("^click on \"([^\"]*)\" button in ebusiness portal$")
		public void click_button(String button_name) throws Throwable {
			add_new_quote.click_button(button_name);
		}	
	
	@And("^click on SaveAndAccept button in ebusiness portal$")
	public void click_SaveAndAccept() throws InterruptedException{
		add_new_quote.click_SaveAndAccept();
	}
	
	@And("^click \"([^\"]*)\" by id$")
	public void click_ButtonByID(String button_id) throws Throwable {
		add_new_quote.click_ButtonByID(button_id);
	}	
	
	@And("^validate no Justification dropdown$")
	public void validate_no_justification_dropdown() throws Throwable {
		add_new_quote.validate_no_justification_dropdown();
	}
	
	@And("^validate no location id$")
	public void validate_no_location_id() throws Throwable {
		add_new_quote.validate_no_location_id();
	}
	
	@And("^click on \"([^\"]*)\" by text$")
	public void click_by_text(String button_text) throws Throwable {
		add_new_quote.click_by_text(button_text);
	}
	
	@And("^click on \"([^\"]*)\" in ebusiness portal$")
	public void click_onbutton(String button_name) throws Throwable {
		add_new_quote.click_onbutton(button_name);
	}	
	
	@And("^accpet the alert for delete from Ebusiness portal$")
	public void click_Delete_then_ok_button() throws Throwable {
		add_new_quote.click_Delete_then_ok_button();
	}
	
	@And("^accpet the alert for Cancel from Ebusiness portal$")
	public void click_Cancel_then_ok_button() throws Throwable {
		add_new_quote.click_Cancel_then_ok_button();
	}
	
	@And("^accept the alert$")
	public void acceptAlert() throws Throwable {
		add_new_quote.acceptAlert();
	}
	
	@And("^click on accept from the popup$")
	public void acceptFrompopup() throws Throwable {
		add_new_quote.acceptFrompopup();
	}
	
	@And("^accept GetAcceptance alert$")
	public void acceptanceAlert() throws Throwable {
		add_new_quote.acceptAcceptanceAlert();
	}
	
	@Given("^verify transaction channel is \"([^\"]*)\"$")
	public void verify_transaction_channel(String text) throws Throwable
	{
		add_new_quote.verify_transaction_channelshouldbroker(text);
	}
	@Given("^verify quote status is \"([^\"]*)\" in UW view for the Quote in \"([^\"]*)\"$")
	public void verify_Quote_Search(String quotestatus, String text) throws Throwable
	{
		add_new_quote.verify_Quote_Search(quotestatus,text);
	}

	@Given("^wait for \"([^\"]*)\" minutes$")
	public void waitforQuoteToUnlock(String waitTime) throws Throwable
	{
		add_new_quote.waitforQuoteToUnlock();
	}
	
	@Given("^Wait for \"([^\"]*)\" minutes$")
	public void WaitFor(String text) throws Throwable
	{
		add_new_quote.waitForTimeInMinutes(text);
	}
	
	@Given("^Save Ebix policy information details in \"([^\"]*)\"$")
	public void save_policy_quote_details(String testdata) throws Throwable
	{
		add_new_quote.save_policy_quote_details(testdata);
	}
	
	@Given("^Save Quote Number in \"([^\"]*)\"$")
	public void saveQuoteNumber(String testdata) throws Throwable {
		add_new_quote.saveQuoteNumber(testdata);
	}
	@Given("^Save write quote Number to field Reference Number in \"([^\"]*)\"$")
	public void SavetoReferenceNumber(String testdata) throws Throwable {
		add_new_quote.SavetoReferenceNumber(testdata);
	}
	
	@Given("^verify policy status is \"([^\"]*)\"$")
	public void verifyPolicyStatus(String policystatus) throws Throwable {
		add_new_quote.verifyPolicyStatus(policystatus);
	}
	
	@And("^validate \"([^\"]*)\" error message in ebusiness portal$")
	public void verify_policy_period_error_message(String errorType) throws Throwable {
		add_new_quote.verify_policy_period_error_message(errorType);
	}
	@And("clear mandatory details in appraisal contact page$")
	public void clear_mandatorydetails_in_Appraisalcontactspage()
	{
		add_new_quote.clear_mandatorydetails_in_Appraisalcontactspage();
	}
}
