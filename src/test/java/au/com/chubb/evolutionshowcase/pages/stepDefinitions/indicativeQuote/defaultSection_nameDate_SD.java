package au.com.chubb.evolutionshowcase.pages.stepDefinitions.indicativeQuote;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote.defaultSection_nameDate_Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class defaultSection_nameDate_SD {

	@Steps
	defaultSection_nameDate_Steps nameDate;
	
	@And("^verify default firstname from sunrise is \"([^\"]*)\"$")
	public void default_firstname(String firstname) throws Throwable {
		nameDate.default_firstname(firstname);
	}
	

	@And("^verify default Surname from sunrise is \"([^\"]*)\"$")
	public void default_surname(String Surname) throws Throwable {
		nameDate.default_surname(Surname);
	}
	
	@And("^verify effective date default value is \"([^\"]*)\" and expiry date default value is \"([^\"]*)\"$")
	public void default_surname(String effectiveDate, String expiryDate) throws Throwable {
		nameDate.default_dates(effectiveDate, expiryDate);
	}
	
	@Given("^verify user is in 'Add New Quote' page$")
	public void verify_user_is_in_Add_New_Quote_page() throws Throwable {
		nameDate.verifyUserIsOnNewQuotePage();
	}
	
	@And("^verify broker is able to edit Name and Date section in indicative quote page$")
	public void edit_name_date(DataTable arg1) throws Throwable {
		nameDate.edit_name_date(arg1);
	}


}
