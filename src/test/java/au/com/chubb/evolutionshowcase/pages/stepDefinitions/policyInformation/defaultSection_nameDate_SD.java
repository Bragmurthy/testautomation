package au.com.chubb.evolutionshowcase.pages.stepDefinitions.policyInformation;

import java.io.IOException;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.policyInformation.defaultSection_nameDate_Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class defaultSection_nameDate_SD {

	@Steps
	defaultSection_nameDate_Steps nameDate;
	
	
	@Then("^fields 'Effective Date', 'Expiry Date' and 'Submission Received Date' should default from Sunrise or edited value from indicative quote as below$")
	public void default_dates() {
		nameDate.default_dates();
	}

	@And("^\"([^\"]*)\" , \"([^\"]*)\" and \"([^\"]*)\" fields should be non editable$")
	public void and_fields_should_be_non_editable(String effectiveDate, String expiryDate, String submissionRecivedDate) throws IOException{
		nameDate.fieldsNotEditable(effectiveDate, expiryDate, submissionRecivedDate);
	
	}
	
	@And("^\"([^\"]*)\" , \"([^\"]*)\" and \"([^\"]*)\" fields should be non editable In Newline InForce$")
	public void and_fields_should_be_non_editableInNewlineInforce(String effectiveDate, String expiryDate, String submissionRecivedDate) throws IOException{
		nameDate.fieldsNotEditableInInforce(effectiveDate, expiryDate, submissionRecivedDate);
	}
	
	@And("^display quote status as \"([^\"]*)\"$")
	public void display_quote_status_as_right_aligned_to_the_page_existing_Evolution_layout(String labelQuoteStatus) throws Throwable {
		nameDate.verifyQuoteStatus(labelQuoteStatus);
	}

	@And("^display static text \"([^\"]*)\"$")
	public void display_static_text_right_aligned_existing_Evolution_layout(String labelrequireField) throws Throwable {
		nameDate.verifyRequiredFieldLabel(labelrequireField);
	}
	
	@Then("^display Quote Number from the Indicative Quote transaction$")
	public void display_QuoteNumber() {
		nameDate.verifyQuoteNumber();
	}
	
	@Then("^verify all madatory fields error messages for risk location \"([^\"]*)\"$")
	public void verify_all_madatory_fields_error_messages_for_risk_location(String riskLocationNumber){
		nameDate.verifyAllMandatoryFiledsErrorMessage(riskLocationNumber);
	}
}


