package au.com.chubb.evolutionshowcase.pages.serenitySteps.policyDocuments;



import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class policyDocuments_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public policyDocuments_Steps() throws IOException {
	
	}
	
	
	
	@Step
	public void verify_RequiredFiledErrorMessage_AppraisalContactPage() {
		
		getDriver().findElements(By.xpath("//input[@id='appraisalContact_Name']")).clear();
		List<WebElement> errorMessageList=getDriver().findElements(By.xpath("//span[contains(text(),'This field is required.')]"));
		String errorMessage="This field is required.";
		int noOfErros=errorMessageList.size();
		boolean flag=true;
		//String contactNameError=getDriver().findElement(By.xpath("//span[@id='appraisalContact_Name-error']")).getText();
		String mobilePhoneError=getDriver().findElement(By.xpath("//span[@id='appraisalContact_Mobile-error']")).getText();
			
		if(!mobilePhoneError.equalsIgnoreCase(errorMessage) 
				)
		{
			flag=false;
		}
		if(noOfErros==1 && flag)
		{
			Assert.assertTrue("Appraisal Contact page field required error messages are displayed", true);
		}
		else
		 Assert.assertTrue("Appraisal Contact page field required error messages are displayed", false);
	}



	@Step
	public void select_CertificateOfCurrency() {
		
		getDriver().findElement(By.xpath("//input[@name='coc']")).click();
	}


@Step
	public void select_PolcyAndCoverLetterCheckBox() {
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'nbPolicyDocuments')]")).click();
	}


	@Step
	public void select_Checkbox_policy_documents_click_genereate_documents() throws Exception {
		
		getDriver().findElement(By.xpath("//*[text()='Quotation']/..//input")).click();
		Thread.sleep(2000);
		getDriver().findElement(By.xpath("//*[text()='Record of Answers']/..//input")).click();
		Thread.sleep(2000);
//		clicking on Generate documents
		getDriver().findElement(By.xpath("//button[@id='btnGeneratePolicyDocs']")).click();
		
		
	}
	
	@Step
	public void validate_docs_Quote_offered(String test_data) throws Exception {
		
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop:
			for (int row = 2; row <= Row_Count; row++) {

			System.out.println("test data --" + test_data);
			if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				break innerloop;
			}
		}
		String quote=XLS.getCellData(sheetName, "Quote_Reference_Number", rowNum);
		
//		String expDocsName ="Quote "+quote+" - Masterpiece New Business Quote v 1.pdf";
//		Boolean isQuoteDocsDisplayed = getDriver().findElement(By.xpath("//*[text()='"+expDocsName+"']")).isDisplayed();
//		Thread.sleep(2000);
//		String expDocsName1 ="Quote "+quote+" - Record of Answers 1 [DD MM YYYY HH MM SS sss].pdf";
//		Boolean isQuoteDocs1Displayed = getDriver().findElement(By.xpath("//*[text()='"+expDocsName1+"']")).isDisplayed();
		

		Boolean isQuoteDocsDisplayed = getDriver().findElement(By.xpath("(//*[@class='control-label attachment']//span)[1]")).isDisplayed();
		System.out.println(getDriver().findElement(By.xpath("(//*[@class='control-label attachment']//span)[1]")).getText());
		Boolean isQuoteDocs1Displayed = getDriver().findElement(By.xpath("(//*[@class='control-label attachment']//span)[2]")).isDisplayed();
		System.out.println(getDriver().findElement(By.xpath("(//*[@class='control-label attachment']//span)[2]")).getText());

		if(isQuoteDocsDisplayed==true && isQuoteDocs1Displayed == true)
			Assert.assertTrue("Documents are displayed as expeced", true);
		else
			Assert.assertTrue("Documents are not displayed as expeced", false);
		
		
		
	}
	
	
	@Step
	public void select_Checkbox_revised_coverage_summary() throws Exception {
		Thread.sleep(10000);
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'revisedCoverageSummary')]")).click();
		Thread.sleep(2000);		
	}
	
	@Step
	public void validate_docs_Revised_coverage_summary(String test_data) throws Exception {
		
		Boolean isQuoteDocsDisplayed = getDriver().findElement(By.xpath("(//*[@class='control-label attachment']//span)[1]")).isDisplayed();
		System.out.println(getDriver().findElement(By.xpath("(//*[@class='control-label attachment']//span)[1]")).getText());

		if(isQuoteDocsDisplayed==true )
			Assert.assertTrue("Documents are displayed as expeced", true);
		else
			Assert.assertTrue("Documents are not displayed as expeced", false);
	}
		
		
	@Step
	public void select_Record_of_Answers()
	{
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'recordOfAnswersDocuments().')]")).click();
	}	
	
	@Step
	public void select_Quotation() throws InterruptedException
	{	Thread.sleep(10000);
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'quotationDocuments')]")).click();
	}
	
	@Step
	public void wait_Documents_Generated()
	{
		WebDriverWait wait=new WebDriverWait(getDriver(), 300);
		wait.until(ExpectedConditions.textToBe(By.xpath("//div[@class='alert alert-warning alert-inline']"), "The document(s) have been generated."));
	}
	
} 
		





