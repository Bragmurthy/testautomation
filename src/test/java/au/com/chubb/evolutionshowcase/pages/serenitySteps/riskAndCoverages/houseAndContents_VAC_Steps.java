package au.com.chubb.evolutionshowcase.pages.serenitySteps.riskAndCoverages;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.*;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class houseAndContents_VAC_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public houseAndContents_VAC_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);

	}

	public void enter_house_and_contents_details(String house_content_test_data, String VAC_Count, String Location_Identifier, String User_Profile, DataTable arg1)
			throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		int Location_Number=Integer.parseInt(Location_Identifier);
		int vac_count = Integer.parseInt(VAC_Count);
		Thread.sleep(8000);
		
		

		
		if (vac_count > 0)

		{

			if(getDriver().findElement(By.id("selectProperty_"+Location_Number+"_VAC_Coverage")).isDisplayed())
			{			
			getDriver().findElement(By.id("selectProperty_"+Location_Number+"_VAC_Coverage")).click();
			Thread.sleep(3000);}
			else
			{
			getDriver().findElement(By.id("btnAddCoverage")).click();
			Thread.sleep(3000);
			getDriver().findElement(By.id("checkVacCoverageGroup")).click();
			getDriver().findElement(By.xpath("//button[contains(text(),'Add') and contains(@class,'btn btn-critical')]")).click();
			Thread.sleep(3000);
			getDriver().findElement(By.id("selectProperty_"+Location_Number+"_VAC_Coverage")).click();
			Thread.sleep(3000);
			}
			
			for (int i = 1; i <= vac_count; i++) {

				String sheetName = "VAC_Coverage";
				int Row_Count = XLS.getRowCount(sheetName);
				int rowNum = 0;

				List<List<String>> data = arg1.raw();
						

				innerloop: for (int row = 2; row <= Row_Count; row++) {
					String vac_test_data = data.get(i).toString().replace("[", "").replace("]", "").trim();

					System.out.println("Vac test data --" + vac_test_data);
					if (vac_test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
						rowNum = row;
						break innerloop;
					}
				}
				System.out.println("Value of row number " + rowNum);

				try {
					// Add Coverage
					//WebElement Add_Coverage = getDriver().findElement(By.xpath(".//*[@id='cov_VAC']/div[2]/div["+Location_Identifier+1+"]/div/span[1]/button"));
					List<WebElement> Add_Coverage = getDriver().findElements(By.xpath("//button[contains(@data-bind,'openAddVacCategoryDlg')]"));
					WebElement element=Add_Coverage.get(Location_Number);
					((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", element);

					Thread.sleep(2000);
					System.out.println("Clicked - Add Coverage Button");
				} catch (Exception e) {
				}

				try {

					Thread.sleep(100);

					String Category = XLS.getCellData(sheetName, "Category", rowNum);
					// Select Category***********************************//
					new Select(getDriver().findElement(By.id("vacEdit_Category"))).selectByVisibleText(Category);
					Thread.sleep(100);

					// Select Category Deductible************************//
					if (User_Profile.equalsIgnoreCase("underwriter"))
					{
					try {
						new Select(getDriver().findElement(By.id("vacEdit_CategoryDeductible")))
								.selectByVisibleText(XLS.getCellData(sheetName, "Category Deductible", rowNum));

						Thread.sleep(100);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				

					// Floating Limit************************************//
					if (Category.equalsIgnoreCase("Jewellery In Vault")
							|| Category.equalsIgnoreCase("Jewellery In Safe")) {

						try {
							getDriver().findElement(By.id("vacEdit_FloatLimit"))
									.sendKeys(XLS.getCellData(sheetName, "Floating Limit", rowNum));
							Thread.sleep(100);
						} catch (Exception e) {

						}

					}
					
					if (Category.equalsIgnoreCase("Jewellery In Vault")) {
						// Temporary Removal Clause ********************//

						try {
							if (XLS.getCellData(sheetName, "Temporary Removal Clause", rowNum)
									.equalsIgnoreCase("Yes")) {
								getDriver().findElement(By.id("vacEdit_TempRemoval")).click();
								Thread.sleep(100);
							}
						} catch (Exception e) {

						}
						Thread.sleep(100);
					}
					
					}
						// Bank Financial Institution********************//
					if (Category.equalsIgnoreCase("Jewellery In Vault")) {
						try {

							String Financial_Institution = XLS.getCellData(sheetName, "Bank Financial Institution",
									rowNum);
							System.out.println("Bank Financial Institution --" + Financial_Institution);
							new Select(getDriver().findElement(By.id("vacEdit_Bank")))
									.selectByVisibleText(Financial_Institution);
							Thread.sleep(100);
							

						} catch (Exception e) {

						}
						Thread.sleep(1000);
					}
					

					// ******************************Justification********************************************//
					if (Category.equalsIgnoreCase("Jewellery In Safe") || Category.equalsIgnoreCase("Jewellery In Home")
							|| Category.equalsIgnoreCase("Coins")) {

						getDriver().findElement(By.id("vacEdit_Justification"))
								.sendKeys(XLS.getCellData(sheetName, "Justification", rowNum));
					}

					// ******************************Other********************************************//
					if (Category.equalsIgnoreCase("Other")) {

						getDriver().findElement(By.id("vacEdit_OtherDesc"))
								.sendKeys(XLS.getCellData(sheetName, "Other", rowNum));
					}

					Thread.sleep(100);

					// *******************Blanket
					// Coverage**********************************************//

					if (Category.equalsIgnoreCase("Jewellery") || Category.equalsIgnoreCase("Furs")
							|| Category.equalsIgnoreCase("Fine Arts") || Category.equalsIgnoreCase("Silverware")
							|| Category.equalsIgnoreCase("Cameras") || Category.equalsIgnoreCase("Stamps")
							|| Category.equalsIgnoreCase("Coins") || Category.equalsIgnoreCase("Collectibles")
							|| Category.equalsIgnoreCase("Musical Items") || Category.equalsIgnoreCase("Wine")) {

						try {

							Thread.sleep(1000);
							System.out.println("Before clicking Add Cover Button -- Blanket Coverage");
							Thread.sleep(1000);

							try {
							
								WebElement ActiveElement=getDriver().switchTo().activeElement();
								ActiveElement.sendKeys(Keys.TAB);
								Thread.sleep(500);
								ActiveElement=getDriver().switchTo().activeElement();
								Thread.sleep(500);
								ActiveElement.click();


							} catch (Exception e) {
								System.out.println("Blanket add cover " + e);
							}

							Thread.sleep(1000);
							System.out.println("After clicking Add Cover Button -- Blanket Coverage");
							
							getDriver().findElement(By.id("vacEdit_BlanketSum")).clear();
							Thread.sleep(500);
							getDriver().findElement(By.id("vacEdit_BlanketSum"))
							.sendKeys(XLS.getCellData(sheetName, "Blanket Sum", rowNum));
					        Thread.sleep(500);

						} catch (Exception e) {

						}
					}

					// *******************Itemised
					// Coverage**********************************************//

					Thread.sleep(1000);
					System.out.println("Before clicking Add Cover Button - Itemised Coverage");

					try {
						
						if (Category.equalsIgnoreCase("Jewellery") || Category.equalsIgnoreCase("Furs")
								|| Category.equalsIgnoreCase("Fine Arts") || Category.equalsIgnoreCase("Silverware")
								|| Category.equalsIgnoreCase("Cameras") || Category.equalsIgnoreCase("Stamps")
								|| Category.equalsIgnoreCase("Coins") || Category.equalsIgnoreCase("Collectibles")
								|| Category.equalsIgnoreCase("Musical Items") || Category.equalsIgnoreCase("Wine")) {
						Thread.sleep(1000);
						WebElement ActiveElement=getDriver().findElement(By.id("vacEdit_BlanketSum"));
						ActiveElement.click();
						ActiveElement.sendKeys(Keys.TAB);
						Thread.sleep(500);
						ActiveElement=getDriver().switchTo().activeElement();
						Thread.sleep(500);
						ActiveElement.click();
						}
						else
						{
							WebElement ActiveElement=getDriver().switchTo().activeElement();
							ActiveElement.sendKeys(Keys.TAB);
							Thread.sleep(500);
							ActiveElement=getDriver().switchTo().activeElement();
							Thread.sleep(500);
							ActiveElement.click();
						}
						
					} catch (Exception e) {
						System.out.println("Blanket add cover " + e);
					}

					Thread.sleep(4000);
					System.out.println("After clicking Add Cover Button - Itemised Coverage");

					if (XLS.getCellData(sheetName, "Include Valuable Articles Schedule", rowNum).equalsIgnoreCase("No"))

					{

						System.out.println("Control reacched VAC EDIT TSI");
					
						try {
							//getDriver().findElement(By.xpath("//input[@id='vacEdit_TSI']")).sendKeys(XLS.getCellData(sheetName, "Total Sum Insured", rowNum));
							getDriver().findElement(By.id("vacEdit_TSI")).click();
							Thread.sleep(500);
							getDriver().findElement(By.id("vacEdit_TSI"))
									.sendKeys(XLS.getCellData(sheetName, "Total Sum Insured", rowNum));
							Thread.sleep(500);
						} catch (Exception e) {
							System.out.println("vacEdit_TSI " + e);
						}
						try {
							getDriver().findElement(By.id("vacEdit_MostValuable")).click();
							Thread.sleep(500);

							getDriver().findElement(By.id("vacEdit_MostValuable"))
									.sendKeys(XLS.getCellData(sheetName, "Most Valuable Item Cost", rowNum));
							Thread.sleep(500);
							getDriver().findElement(By.id("vacEdit_MostValuable")).sendKeys(Keys.TAB);
							Thread.sleep(500);

						} catch (Exception e) {
							System.out.println("vacEdit_MostValuable " + e);
						}

					}

					else if (XLS.getCellData(sheetName, "Include Valuable Articles Schedule", rowNum)
							.equalsIgnoreCase("Yes"))

					{
						// Click Include Valuable articles Schedule Toggle
						getDriver().findElement(By.id("vacEdit_IncludeSchedule")).sendKeys(Keys.ARROW_RIGHT);
						Thread.sleep(500);

						String Item_Count = XLS.getCellData(sheetName, "ItemCoverage_Count", rowNum);
						int item_coverage_count = Integer.parseInt(Item_Count);

						for (int k = 0; k < item_coverage_count; k++) {

							getDriver().findElement(By.id("vacEdit_Items_" + k + "_ItemDescription")).clear();
							getDriver().findElement(By.id("vacEdit_Items_" + k + "_ItemDescription"))
									.sendKeys(XLS.getCellData(sheetName, "ItemDescription_" + k, rowNum));

							getDriver().findElement(By.id("vacEdit_Items_" + k + "_ItemCoverage"))
									.sendKeys(XLS.getCellData(sheetName, "SumInsured_" + k, rowNum));
							getDriver().findElement(By.id("vacEdit_Items_" + k + "_ItemValuationDate"))
									.sendKeys(XLS.getCellData(sheetName, "LastValuationDate_" + k, rowNum));
							getDriver().findElement(By.id("vacEdit_Items_" + k + "_ItemValuationDate"))
									.sendKeys(Keys.ENTER);
							Thread.sleep(500);

							if (XLS.getCellData(sheetName, "AsPerSchedule_" + k, rowNum).equalsIgnoreCase("Yes")) {
								getDriver().findElement(By.id("vacEdit_Items_" + k + "_ItemAsPerSchedule")).click();
							}

							if (XLS.getCellData(sheetName, "DescriptionToFollow_" + k, rowNum)
									.equalsIgnoreCase("Yes")) {
								getDriver().findElement(By.id("vacEdit_Items_" + k + "_ItemDescriptionToFollow"))
										.click();
								Thread.sleep(500);

							}

						}

						if (item_coverage_count == 2) {

							getDriver().findElement(By.id("vacEdit_Items_2_Remove")).click();
							Thread.sleep(500);

						} else if (item_coverage_count == 1) {
							getDriver().findElement(By.id("vacEdit_Items_1_Remove")).click();
							Thread.sleep(500);
							getDriver().findElement(By.id("vacEdit_Items_2_Remove")).click();
							Thread.sleep(500);

						}

					}

				} catch (Exception e) {
				}
				Thread.sleep(500);

				try {

					if (getDriver().findElement(By.id("vacEdit_AddVacItem")).isDisplayed()) {

						WebElement AddVACItem = getDriver().findElement(By.id("vacEdit_AddVacItem"));
						AddVACItem.sendKeys(Keys.TAB);
						Thread.sleep(500);
					} else if (getDriver().findElement(By.id("vacEdit_MostValuable")).isDisplayed()) {
						WebElement MostValuable = getDriver().findElement(By.id("vacEdit_MostValuable"));
						MostValuable.sendKeys(Keys.TAB);
						Thread.sleep(500);
					} else {

					}

				} catch (Exception e) {

				}

				WebElement Cancel_Button = getDriver().switchTo().activeElement();
				Thread.sleep(500);
				Cancel_Button.sendKeys(Keys.TAB);
				WebElement OK_Button = getDriver().switchTo().activeElement();
				Thread.sleep(500);
				OK_Button.click();

				Thread.sleep(4000);
			}

		}

		// ********************************-------------------------HOUSE AND
		// CONTENTS

		System.out.println("Trying to click Next Button");

		getDriver().switchTo().defaultContent();
		Thread.sleep(1000);
		getDriver().findElement(By.id("selectProperty_" +Location_Number+"_Home_Coverage")).click();
		Thread.sleep(8000);
		Thread.sleep(2000);
		System.out.println("Next Button Clicked");

		Thread.sleep(3000);

		String sheetName = "HouseAndContents";
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		loop: for (int row = 2; row <= Row_Count; row++) {
			if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(house_content_test_data)) {
				rowNum = row;
				break loop;
			}

		}

		System.out.println("Value of Row number " + rowNum);

		String PropertyType = XLS.getCellData(sheetName, "PropertyType", rowNum);
		if (!PropertyType.equalsIgnoreCase("Unit")) {

			System.out.println("control reached try catch");
//			// Building Coverage
//			int locater = 0;
//
//			loop: for (int i = 0; i <= 9; i++) {
//
//				if (getDriver().findElement(By.id("RiskLocation_" + i + "_BuildingSumInsured")).isDisplayed()) {
//
//					locater = i;
//					break loop;
//				}
//
//			}
			//System.out.println("Value of locater " + locater);

			try {
				getDriver().findElement(By.id("RiskLocation_" + Location_Number + "_BuildingSumInsured"))
						.sendKeys(XLS.getCellData(sheetName, "BuildingSumInsured", rowNum));
				System.out.println("value is "+getDriver().findElement(By.id("RiskLocation_" + Location_Number + "_BuildingSumInsured")).getAttribute("value"));
			} catch (Exception e) {
			}

			try {
				new Select(getDriver().findElement(By.id("RiskLocation_" + Location_Number + "_BuildingDeductible")))
						.selectByVisibleText(XLS.getCellData(sheetName, "BuildingDeductible", rowNum));
				Thread.sleep(500);
			} catch (Exception e) {
			}
			try {
				
				getDriver().findElement(By.id("RiskLocation_" + Location_Number + "_OPS")).clear();
				Thread.sleep(500);
				getDriver().findElement(By.id("RiskLocation_" + Location_Number + "_OPS"))
						.sendKeys(XLS.getCellData(sheetName, "OtherPermanentStructures", rowNum));
				Thread.sleep(500);

			} catch (Exception e) {
			}
			try {

			} catch (Exception e) {
				new Select(getDriver().findElement(By.id("RiskLocation_" + Location_Number + "_ReplacementCostType")))
						.selectByVisibleText(XLS.getCellData(sheetName, "BuildingReplacementCostType", rowNum));
			}

			// Contents Coverage
			try {
				getDriver().findElement(By.id("RiskLocation_" + Location_Number + "_ContentsSumInsured")).clear();
				getDriver().findElement(By.id("RiskLocation_" + Location_Number + "_ContentsSumInsured")).clear();
				Thread.sleep(2000);
				System.out.println(XLS.getCellData(sheetName, "ContentsSumInsured", rowNum));
				getDriver().findElement(By.id("RiskLocation_" + Location_Number + "_ContentsSumInsured"))
						.sendKeys(XLS.getCellData(sheetName, "ContentsSumInsured", rowNum));
				Thread.sleep(500);
			} catch (Exception e) {
			}

			try {
				new Select(getDriver().findElement(By.id("RiskLocation_" + Location_Number + "_ContentsDeductible")))
						.selectByVisibleText(XLS.getCellData(sheetName, "ContentsDeductible", rowNum));
				Thread.sleep(500);
			} catch (Exception e) {
			}

			Thread.sleep(500);

//			List<WebElement> FloodCoverage = getDriver().findElements(By.name("FloodCoverageFlag"));
//			if (XLS.getCellData(sheetName, "FloodCoverage", rowNum).equalsIgnoreCase("Yes")) {
//				FloodCoverage.get(0).click();
//				Thread.sleep(500);
//
//			} else if (XLS.getCellData(sheetName, "FloodCoverage", rowNum).equalsIgnoreCase("No")) {
//				FloodCoverage.get(1).click();
//				Thread.sleep(500);}}
			}
	}

			/*List<WebElement> BusinessPropertyCoverage = getDriver()
					.findElements(By.name("HomeBusinessPropertyCoverageFlag"));
			if (XLS.getCellData(sheetName, "BusinessPropertyCoverage", rowNum).equalsIgnoreCase("Yes")) {
				BusinessPropertyCoverage.get(0).click();
				BusinessPropertyCoverage.get(0).sendKeys(Keys.TAB);
				Thread.sleep(500);
				WebElement ActiveElement = getDriver().switchTo().activeElement();
				ActiveElement.sendKeys(XLS.getCellData(sheetName, "BusinessPropertySumInsured", rowNum));

				;

			} else if (XLS.getCellData(sheetName, "BusinessPropertyCoverage", rowNum).equalsIgnoreCase("No")) {
				BusinessPropertyCoverage.get(1).click();
				Thread.sleep(500);

			}*/

			//getDriver().findElement(By.id("btnSaveProgress")).click();
			//Thread.sleep(10000);

		

	

	@Step
	public void click_proceed_with_Quote() throws InterruptedException {
		
		getDriver().findElement(By.xpath("//button[contains(.,'Proceed With Quote')]")).click();
		Thread.sleep(4000);
		
	}

	public void verifyDefaultBuildingDectibleValueForAllLocations(String locationExpected, String locationActual) throws InterruptedException {
		int expectedLocation=Integer.parseInt(locationExpected);
		int actualLocation=Integer.parseInt(locationActual);
		
		getDriver().findElement(By.xpath("//a[@id='selectProperty_"+expectedLocation+"_Home_Coverage']")).click();
		Thread.sleep(1000);
		
		String expectedDeductible=new Select(getDriver().findElement(By.xpath("//select[@name='RiskLocation_"+expectedLocation+"_BuildingDeductible']"))).getFirstSelectedOption().getText();
		
		getDriver().findElement(By.xpath("//a[@id='selectProperty_"+actualLocation+"_Home_Coverage']")).click();
		Thread.sleep(1000);
		String actualDeductible_APP=new Select(getDriver().findElement(By.xpath("//select[@name='RiskLocation_"+expectedLocation+"_BuildingDeductible']"))).getFirstSelectedOption().getText();
		
		if (expectedDeductible.equalsIgnoreCase(actualDeductible_APP)) {
			Assert.assertTrue("Deductible value is displayed as same in All Locations"+actualDeductible_APP, true);
		} else {
			System.out.println("test Failed ");
			Assert.assertTrue("Deductible value is not displayed as same in All Locations Actual "+actualDeductible_APP+"and Expected is "+expectedDeductible, false);

		}
		
	}
	@Step
	public void verifyDefaultBuildingReplcaementCostType(String extendedReplacementCost,String location_identifier) {
		int location=Integer.parseInt(location_identifier);
		String replacementCostType_APP=new Select(getDriver().findElement(By.xpath("//select[@name='RiskLocation_"+location+"_ReplacementCostType']"))).getFirstSelectedOption().getText();
		System.out.println("Replacement cost type "+replacementCostType_APP);
		if (extendedReplacementCost.equalsIgnoreCase(replacementCostType_APP)) {
			Assert.assertTrue("Deductible value is displayed as same in All Locations"+replacementCostType_APP, true);
		} else {
			System.out.println("test Failed ");
			Assert.assertTrue("Deductible value is not displayed as same in All Locations Actual "+replacementCostType_APP+"and Expected is "+extendedReplacementCost, false);
		}
		
	}
@Step
	public void clickAddCoverage() throws InterruptedException {
		clickVAC();
		getDriver().findElement(By.xpath("//button[contains(@data-bind,'openAddVacCategoryDlg')]")).click();
		Thread.sleep(15000);
		
	}
	
public void clickVAC() throws InterruptedException {
		
		getDriver().findElement(By.xpath("//a[@id='selectProperty_0_VAC_Coverage']")).click();
		Thread.sleep(4000);
		
	}

@Step
public void validateMandatoryFiledsForVAC() throws InterruptedException {
	boolean value=verifyVACCategoryRequired();
	verifyRequiredFieldErrorMessageForAllCategories();
	if (value) {
		Assert.assertTrue("VAC field required error message is displayed", true);
	} else {
		Assert.assertTrue("VAC field required error message is not displayed", false);
	}
	
}
@Step
public void verifyRequiredFieldErrorMessageForAllCategories() throws InterruptedException {
	String [] categories = {"Jewellery", "Jewellery In Vault", "Jewellery In Safe", "Jewellery In Home", "Furs","Fine Arts","Silverware","Cameras","Stamps","Coins","Collectibles","Musical Items","Wine","Other"};
	for(int i=0;i<categories.length;i++)
	{
		verifyRequiredFieldErrorMessage(categories[i]);
	}
}

private void verifyRequiredFieldErrorMessage(String category) throws InterruptedException {
	switch(category)
	{    
	case "Jewellery": 
		selectCategoryAndClickOkbutton(category);
    verifyVACCoverErrorMessage();
	break;   
	case "Jewellery In Vault":  
		selectCategoryAndClickOkbutton(category);
	verifyVACCoverErrorMessage();
	 break;  
	case "Jewellery In Safe":
		selectCategoryAndClickOkbutton(category);
		verifyVACCoverErrorMessage();
		//verifyVACJusticationFieldRequiredError();
		 break;  
	case "Jewellery In Home": 
		selectCategoryAndClickOkbutton(category);
		verifyVACCoverErrorMessage();
		//verifyVACJusticationFieldRequiredError();
		 break;  
	case "Furs":  
		selectCategoryAndClickOkbutton(category);
		verifyVACCoverErrorMessage();
		 break;  
	case "Fine Arts":
		selectCategoryAndClickOkbutton(category);
		verifyVACCoverErrorMessage();
		 break;  
	case "Silverware":  
		selectCategoryAndClickOkbutton(category);
		verifyVACCoverErrorMessage();
		 break;  
	case "Cameras":
		selectCategoryAndClickOkbutton(category);
		verifyVACCoverErrorMessage();
		 break;  
	case "Stamps": 
		selectCategoryAndClickOkbutton(category);
		verifyVACCoverErrorMessage();
		 break;  
	case "Coins": 
		selectCategoryAndClickOkbutton(category);
		verifyVACCoverErrorMessage();
		//verifyVACJusticationFieldRequiredError();
		 break;  
	case "Collectibles":
		selectCategoryAndClickOkbutton(category);
		verifyVACCoverErrorMessage();
		 break;  
	case "Musical Items":
		selectCategoryAndClickOkbutton(category);
		verifyVACCoverErrorMessage();
		 break;  
	case "Wine":
		selectCategoryAndClickOkbutton(category);
		verifyVACCoverErrorMessage();
		 break;  
	case "Other":
		selectCategoryAndClickOkbutton(category);
		verifyOtherFielRequiredError();
		 break;
}     
	
}

public void selectCategoryAndClickOkbutton(String category) throws InterruptedException {
	new Select(getDriver().findElement(By.xpath("//select[@id='vacEdit_Category']"))).selectByVisibleText(category);
	getDriver().findElement(By.xpath("//button[contains(text(),'OK')]")).click();
	Thread.sleep(5000);
}

@Step
public void verifyVACCoverErrorMessage()
{
	if (getDriver().findElement(By.xpath("//span[contains(text(),'VAC cover must include at least one of either Blanket or Itemised coverages')]")).isDisplayed()) {
		Assert.assertTrue("Required fileds error after Selecting Jewellery is displayed", true);

	} else {
		Assert.assertTrue("Required fileds error after Selecting Jewellery is not displayed", false);
		}
}

@Step
public void verifyVACJusticationFieldRequiredError()
{
	if (getDriver().findElement(By.xpath("//span[@id='vacEdit_Justification-error']")).getText().equalsIgnoreCase("This field is required.")) {
		Assert.assertTrue("Required fileds error for Justification field is displayed", true);
	} else {
		Assert.assertTrue("Required fileds error for Justification field is not displayed", false);
	}
}

@Step
public void verifyOtherFielRequiredError()
{
	if (getDriver().findElement(By.xpath("//span[@id='vacEdit_OtherDesc-error']")).getText().equalsIgnoreCase("This field is required.")) {
		Assert.assertTrue("Required fileds error for Other field is displayed", true);

	} else {
		Assert.assertTrue("Required fileds error for Other field is not displayed", false);
	}
}

@Step
private boolean verifyVACCategoryRequired() {
	return getDriver().findElement(By.xpath("//span[@id='vacEdit_Category-error']")).getText().equalsIgnoreCase("This field is required.");
}

@Step
public void clickHousesAndContents() throws InterruptedException {
	getDriver().findElement(By.xpath("//a[span[contains(text(),'House and Contents')]]")).click();
	Thread.sleep(4000);
}

@Step
public void validateMandatoryFiledsForHousesAndContents() throws InterruptedException {
	getDriver().findElement(By.xpath("//a[span[contains(text(),'House and Contents')]]")).click();
	Thread.sleep(4000);
	List<WebElement> errorMessageList=getDriver().findElements(By.xpath("//span[contains(text(),'This field is required.')]"));
	String errorMessage="This field is required.";
	int noOfErros=errorMessageList.size();
	boolean flag=true;
	String buildingsSumInsuredfieldError=getDriver().findElement(By.xpath("//span[contains(text(),'This field is required.')]")).getText();
	String contentsSumInsuredErrorMessage=getDriver().findElement(By.xpath("//span[@id='RiskLocation_0_ContentsSumInsured-error']")).getText();	
	
	if(buildingsSumInsuredfieldError.equalsIgnoreCase(errorMessage) &&contentsSumInsuredErrorMessage.equalsIgnoreCase(errorMessage))
	{
		flag=false;
	}
	if(noOfErros==6 && flag)
	{
		Assert.assertTrue("Houses and Contents field required error messages are displayed", true);
	}
	else
	 Assert.assertTrue("Houses and Contents field required error messages are not displayed", true);
}
@Step
public void clickonOKButton() throws InterruptedException {
	getDriver().findElement(By.xpath("//span[button[contains(text(),'OK')]]")).click();
	Thread.sleep(4000);
	
}

@Step
public void click_button(String button_name) throws InterruptedException {
	// TODO Auto-generated method stub
	getDriver().findElement(By.id(button_name)).click();
	Thread.sleep(15000);
}

@Step
public void edit_verify_building_coverage(String action, String Location_Identifier, DataTable arg1) throws InterruptedException {
	// TODO Auto-generated method stub
	
	int Location_Number=Integer.parseInt(Location_Identifier);
	List<List<String>> data = arg1.raw();
	Thread.sleep(3000);
		
	
		if (action.equalsIgnoreCase("edit"))
		{
			
		getDriver().findElement(By.id("RiskLocation_"+Location_Number+"_BuildingSumInsured")).clear();
		Thread.sleep(2000);
		System.out.println("INPUT ");
		System.out.println(data.get(1).get(1));
		System.out.println(data.get(2).get(1));
		System.out.println(data.get(3).get(1));

		getDriver().findElement(By.id("RiskLocation_"+Location_Number+"_BuildingSumInsured")).sendKeys(data.get(1).get(1));
		Thread.sleep(2000);
		getDriver().findElement(By.xpath("//select[@id='RiskLocation_"+Location_Number+"_BuildingDeductible']")).click();
		Thread.sleep(2000);

		new Select(getDriver().findElement(By.xpath("//select[@id='RiskLocation_"+Location_Number+"_BuildingDeductible']"))).selectByVisibleText(data.get(2).get(1));

	
		Thread.sleep(2000);

		/*getDriver().findElement(By.id("RiskLocation_"+Location_Number+"_OPS")).clear();
		Thread.sleep(2000);

		getDriver().findElement(By.id("RiskLocation_"+Location_Number+"_OPS")).sendKeys(data.get(3).get(1));
		Thread.sleep(2000);*/

		}
		
		else if (action.equalsIgnoreCase("verify"))
		{
		
		System.out.println("OUTPUT");

		String Building_Sum_Insured_APP=getDriver().findElement(By.id("RiskLocation_"+Location_Number+"_BuildingSumInsured")).getAttribute("value");
		System.out.println(Building_Sum_Insured_APP);
		String Building_Deductible_APP=	getDriver().findElement(By.id("RiskLocation_"+Location_Number+"_BuildingDeductible")).getAttribute("value");
		System.out.println(Building_Deductible_APP);
		String OPS_APP=	getDriver().findElement(By.id("RiskLocation_"+Location_Number+"_OPS")).getAttribute("value");	
		System.out.println(OPS_APP);
		
		
		if (Building_Sum_Insured_APP.equalsIgnoreCase(data.get(1).get(1)) &&
				Building_Deductible_APP.equalsIgnoreCase(data.get(2).get(1)) &&
				OPS_APP.equalsIgnoreCase(data.get(3).get(1)))
		{
			Assert.assertTrue("Able to edit the Building Coverage details ", true);
		}
		else
		{
			Assert.assertTrue("Able to edit the Building Coverage details ", false);

		}
		
		
		}
		else
		{
			
		}
		
	
}

@Step
public void verify_home_business_property_coverage(String selection, String value, String location) throws InterruptedException {
	// TODO Auto-generated method stub

	Thread.sleep(1000);
	
	if (selection.equalsIgnoreCase("Yes")) {
		
		System.out.println("YES Radio Button");
		getDriver().findElement(By.xpath(".//*[@id='RiskLocation_"+location+"_HBPCoverage']/label[1]")).click();;
		Thread.sleep(1000);
        getDriver().findElement(By.id("RiskLocation_"+location+"_HBPSumInsured")).sendKeys(value);
		

	} else if (selection.equalsIgnoreCase("No")) {
		getDriver().findElement(By.xpath(".//*[@id='RiskLocation_"+location+"_HBPCoverage']/label[2]")).click();;
		Thread.sleep(500);

	}

}

@Step
public void click_house_and_contents(String location) {
	// TODO Auto-generated method stub
	try
	{
	getDriver().findElement(By.xpath(".//*[@id='selectProperty_"+location+"_Home_Coverage']/span[1]")).click();
	Thread.sleep(3000);
	}catch (Exception e)
	{
		
	}
	
}

@Step
public void edit_verify_contents_coverage(String action, String Location_Identifier, DataTable arg1) throws InterruptedException {
	// TODO Auto-generated method stub

	int Location_Number=Integer.parseInt(Location_Identifier);
	List<List<String>> data = arg1.raw();
	Thread.sleep(3000);
		
	
		if (action.equalsIgnoreCase("edit"))
		{
			
		getDriver().findElement(By.id("RiskLocation_"+Location_Number+"_ContentsSumInsured")).clear();
		Thread.sleep(2000);
		System.out.println("INPUT ");
		System.out.println(data.get(1).get(1));
		System.out.println(data.get(2).get(1));

		getDriver().findElement(By.id("RiskLocation_"+Location_Number+"_ContentsSumInsured")).sendKeys(data.get(1).get(1));
		Thread.sleep(2000);
		getDriver().findElement(By.xpath("//select[@id='RiskLocation_"+Location_Number+"_ContentsDeductible']")).click();
		Thread.sleep(2000);

		new Select(getDriver().findElement(By.xpath("//select[@id='RiskLocation_"+Location_Number+"_ContentsDeductible']"))).selectByVisibleText(data.get(2).get(1));
		Thread.sleep(2000);

		

		}
		
		else if (action.equalsIgnoreCase("verify"))
		{
		
		System.out.println("OUTPUT");

		String Contents_Sum_Insured_APP=getDriver().findElement(By.id("RiskLocation_"+Location_Number+"_ContentsSumInsured")).getAttribute("value");
		System.out.println(Contents_Sum_Insured_APP);
		String Contents_Deductible_APP=	getDriver().findElement(By.id("RiskLocation_"+Location_Number+"_ContentsDeductible")).getAttribute("value");
		System.out.println(Contents_Deductible_APP);
				
		
		if (Contents_Sum_Insured_APP.equalsIgnoreCase(data.get(1).get(1)) &&
				Contents_Deductible_APP.equalsIgnoreCase(data.get(2).get(1)))
		{
			Assert.assertTrue("Able to edit the Contents Coverage details ", true);
		}
		else
		{
			Assert.assertTrue("Able to edit the Contents Coverage details ", false);

		}
		
		
		}
		else
		{
			
		}
}

@Step
public void verify_building_contents_deductible(String Location_Number) {
	// TODO Auto-generated method stub
	
	
	String Building_Deductible_APP=	getDriver().findElement(By.id("RiskLocation_"+Location_Number+"_BuildingDeductible")).getAttribute("value");
	String Contents_Deductible_APP=	getDriver().findElement(By.id("RiskLocation_"+Location_Number+"_ContentsDeductible")).getAttribute("value");

	
	if (Building_Deductible_APP.equalsIgnoreCase(Contents_Deductible_APP))
	{
		Assert.assertTrue("Building and Contents Deductible has same value  " +Building_Deductible_APP, true);

	}
	else
	{
		Assert.assertTrue("Building " +Building_Deductible_APP +"and Contents Deductible " +Contents_Deductible_APP +" has same value  ",  false);

	}
	
	
}

@Step
public void click_VAC(String location) {

	try
	{
	getDriver().findElement(By.xpath(".//*[@id='selectProperty_"+location+"_VAC_Coverage']/span[1]")).click();
	Thread.sleep(5000);
	}catch (Exception e)
	{
		
	}
}	

@Step
public void select_category_from_VAC_popup(String house_content_test_data, String VAC_Count, String Location_Identifier)
		throws InterruptedException {

	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
	int Location_Number=Integer.parseInt(Location_Identifier);
	int vac_count = Integer.parseInt(VAC_Count);
	Thread.sleep(8000);
	
	

	
	if (vac_count > 0)

	{

		if(getDriver().findElement(By.id("selectProperty_"+Location_Number+"_VAC_Coverage")).isDisplayed())
		{			
		getDriver().findElement(By.id("selectProperty_"+Location_Number+"_VAC_Coverage")).click();
		Thread.sleep(3000);}
		else
		{
		getDriver().findElement(By.id("btnAddCoverage")).click();
//			getDriver().findElement(By.xpath("//*[text()='Add Coverage']")).click();
		Thread.sleep(3000);
		getDriver().findElement(By.id("checkVacCoverageGroup")).click();
		getDriver().findElement(By.xpath("//button[contains(text(),'Add') and contains(@class,'btn btn-critical')]")).click();
		Thread.sleep(3000);
		getDriver().findElement(By.id("selectProperty_"+Location_Number+"_VAC_Coverage")).click();
		Thread.sleep(3000);
		}
		
		for (int i = 1; i <= vac_count; i++) {

			String sheetName = "VAC_Coverage";
			int Row_Count = XLS.getRowCount(sheetName);
			int rowNum = 0;

			
			try {
				// Add Coverage
				WebElement Add_Coverage = getDriver()
						.findElement(By.xpath(".//*[@id='cov_VAC']/div[2]/div["+Location_Identifier+1+"]/div/span[1]/button"));
				((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", Add_Coverage);

				Thread.sleep(2000);
				System.out.println("Clicked - Add Coverage Button");
			} catch (Exception e) {
			}

			try {

				Thread.sleep(100);

				String Category = XLS.getCellData(sheetName, "Category", rowNum);
				// Select Category***********************************//
				new Select(getDriver().findElement(By.id("vacEdit_Category"))).selectByVisibleText(Category);
				Thread.sleep(100);

				// Select Category Deductible************************//
//				if (User_Profile.equalsIgnoreCase("underwriter"))
//				{
//				try {
//					new Select(getDriver().findElement(By.id("vacEdit_CategoryDeductible")))
//							.selectByVisibleText(XLS.getCellData(sheetName, "Category Deductible", rowNum));
//
//					Thread.sleep(100);
//				} catch (Exception e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				}
			

				// Floating Limit************************************//
				if (Category.equalsIgnoreCase("Jewellery In Vault")
						|| Category.equalsIgnoreCase("Jewellery In Safe")) {

					try {
						getDriver().findElement(By.id("vacEdit_FloatLimit"))
								.sendKeys(XLS.getCellData(sheetName, "Floating Limit", rowNum));
						Thread.sleep(100);
					} catch (Exception e) {

					}

				}
				
				if (Category.equalsIgnoreCase("Jewellery In Vault")) {
					// Temporary Removal Clause ********************//

					try {
						if (XLS.getCellData(sheetName, "Temporary Removal Clause", rowNum)
								.equalsIgnoreCase("Yes")) {
							getDriver().findElement(By.id("vacEdit_TempRemoval")).click();
							Thread.sleep(100);
						}
					} catch (Exception e) {

					}
					Thread.sleep(100);
				}
				
//				}
					// Bank Financial Institution********************//
				if (Category.equalsIgnoreCase("Jewellery In Vault")) {
					try {

						String Financial_Institution = XLS.getCellData(sheetName, "Bank Financial Institution",
								rowNum);
						System.out.println("Bank Financial Institution --" + Financial_Institution);
						new Select(getDriver().findElement(By.id("vacEdit_Bank")))
								.selectByVisibleText(Financial_Institution);
						Thread.sleep(100);
						

					} catch (Exception e) {

					}
					Thread.sleep(1000);
				}
//				
//
//				// ******************************Justification********************************************//
//				try{if ((getDriver().findElement(By.id("vacEdit_Justification")).isDisplayed())) {
//					System.out.println("element is displayed so validation got failed");
//					Assert.assertTrue(false);
//					}
//					else{
//						System.out.println("element is not displayed so validation passed");
//						Assert.assertTrue(true);
//					}
//				}
//				catch(Exception e)
//				{
//					System.out.println("element is not displayed so validation pass");
//					Assert.assertTrue(true);
//				}
//
//				// ******************************Other********************************************//
//				if (Category.equalsIgnoreCase("Other")) {
//
//					getDriver().findElement(By.id("vacEdit_OtherDesc"))
//							.sendKeys(XLS.getCellData(sheetName, "Other", rowNum));
//				}

				Thread.sleep(10000);

				// *******************Blanket
				// Coverage**********************************************//
							
			}finally{
				
			}
		}
		}
		
	}


@Step
public void verify_VAC_from_indicative_quote(DataTable arg1) throws InterruptedException {

	List<List<String>> data = arg1.raw();
	int i=1;
	int j=1;
	int match_counter=0;
	System.out.println("Input Data " +data.size());
	for (i=1;i<data.size();i++)
	{
	
	try
	{
	System.out.println("i " +i);	
	System.out.println("j " +j);
	String category_name=data.get(i).get(0);
	System.out.println(category_name);
	getDriver().findElement(By.xpath(".//*[@id='root_VACTable']/tbody/tr["+j+"]/td[1]/a/strong")).click();    
	Thread.sleep(2000);
	getDriver().findElement(By.id("vacEdit_TSI")).click();
	Thread.sleep(1000);
	String Total_Sum_Insured_APP=getDriver().findElement(By.id("vacEdit_TSI")).getAttribute("value");
	
	if (Total_Sum_Insured_APP.equalsIgnoreCase(data.get(i).get(1)))
			{
		      match_counter ++;
			}
	
	
    System.out.println("Total Sum Insured " +getDriver().findElement(By.id("vacEdit_TSI")).getAttribute("value"));
	Thread.sleep(1000);
	getDriver().findElement(By.id("vacEdit_MostValuable")).click();
	getDriver().findElement(By.id("vacEdit_MostValuable")).sendKeys(Keys.TAB);
	WebElement Cancel_Button = getDriver().switchTo().activeElement();
	Thread.sleep(500);
	Cancel_Button.click();
	j=j+2;
    Thread.sleep(2000);
    JavascriptExecutor jse = (JavascriptExecutor)getDriver();
    jse.executeScript("window.scrollBy(0,50)", "");
	}catch (Exception e)
	{
	   System.out.println(e);	
	}
	
	   System.out.println("Match counter " +match_counter);
	   System.out.println("Category types " +(data.size()-1));
	
    
	}


}
	@Step
	public void verify_helptext_houseandcontents(String location) throws InterruptedException {
		
		String bsitext = "Please note that our minimum building sum insured is $500,000 for owner occupied homes.";
		String bsideductibletext = "Please note that minimum deductibles apply depending on the sums insured and claims history.";
		String csitext = "Please note that minimum contents sums insured apply.";
		String csideductibletext = "Please note that minimum deductibles apply depending on the sums insured and claims history.";
		
		getDriver().findElement(By.id("property_"+location+"_bSIHelp")).click();
		String bsiHelpText_APP=getDriver().findElement(By.xpath("//label[contains(@data-bind,'showBSIHelp')]")).getText();
		Thread.sleep(5000);
		getDriver().findElement(By.id("property_"+location+"_deductibleHelp")).click();
		String bsiDeductibleHelpText_APP=getDriver().findElement(By.xpath("//label[contains(@data-bind,'showBDeductibleHelp')]")).getText();
		Thread.sleep(5000);
		getDriver().findElement(By.id("property_"+location+"_cSIHelp")).click();
		String csiHelpText_APP=getDriver().findElement(By.xpath("//label[contains(@data-bind,'showCSIHelp')]")).getText();
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//a[contains(@data-bind,'showCDeductibleHelp')]")).click();
		String csiDeductibleHelpText_APP=getDriver().findElement(By.xpath("//label[contains(@data-bind,'showCDeductibleHelp')]")).getText();
		
		if(bsiHelpText_APP.equals(bsitext) 
				&& bsiDeductibleHelpText_APP.equals(bsideductibletext)
				&& csiHelpText_APP.equals(csitext)
				&& csiDeductibleHelpText_APP.equals(csideductibletext))
			Assert.assertTrue("Help Text for Building Coverage Screen are as expected", true);
		else
			Assert.assertTrue("Help Text for Building Coverage Screen are as expected", false);	
		
		getDriver().findElement(By.id("RiskLocation_"+location+"_BuildingSumInsured")).sendKeys("20000");
//		getDriver().findElement(By.id("RiskLocation_"+location+"_ContentsSumInsured")).clear();
//		getDriver().findElement(By.id("RiskLocation_"+location+"_ContentsSumInsured")).sendKeys("20000");
		
	}
	
	@Step
	public void verify_helptext_vac(String location) throws InterruptedException {
		
		WebElement Add_Coverage = getDriver()
				.findElement(By.xpath(".//*[@id='cov_VAC']/div[2]/div[1]/div/span[1]/button"));
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", Add_Coverage);

		Thread.sleep(2000);
		
		String vaccategorgyhelptext = "Valuable Articles Coverage is in addition to your Contents Coverage."+"\n"+"Please note that Valuable Articles Coverage can only be selected in conjunction with Contents coverage.";
		String Blankethelptext = "Please note that limits per item may apply.";
		
		
		getDriver().findElement(By.xpath("//a[contains(@data-bind,'showNewVacCatHelp')]")).click();
		String vacCategoryHelpText_APP=getDriver().findElement(By.xpath("//label[contains(@data-bind,'showNewVacCatHelp')]")).getText();
		
		
		new Select(getDriver().findElement(By.id("vacEdit_Category"))).selectByVisibleText("Jewellery");
		
		getDriver().findElement(By.xpath("//a[contains(@data-bind,'showBlanketCovHelp')]")).click();
		String BlanketHelpText_App=getDriver().findElement(By.xpath("//label[contains(@data-bind,'showBlanketCovHelp')]")).getText();
		
				
		if(vacCategoryHelpText_APP.equals(vaccategorgyhelptext) 
				&& BlanketHelpText_App.equals(Blankethelptext))
			Assert.assertTrue("Help Text for VAC Screen are as expected", true);
		else
			Assert.assertTrue("Help Text for VAC Screen are as expected", false);		
		
	}
	
	@Step
	public void overrideEliasRating(String EliasValue, String OverrideReason) throws InterruptedException {
		// TODO Auto-generated method stub
		getDriver().findElement(By.xpath("//span[@class='fa fa-pencil']")).click();
		Thread.sleep(2000);
		new Select(getDriver().findElement(By.id("override-flood-rating-id"))).selectByVisibleText(EliasValue);	
		Thread.sleep(2000);
		new Select(getDriver().findElement(By.id("override-flood-rating-reasonid"))).selectByVisibleText(OverrideReason);
		Thread.sleep(2000);
		getDriver().findElement(By.xpath("//button[contains(text(),'Save')]")).click();
		Thread.sleep(2000);
	}
	@Step
	public void selectFloodCoverageAs(String value) throws InterruptedException {
		// TODO Auto-generated method stub
		getDriver().findElement(By.xpath("//span[contains(text(),'"+ value +"')/preceding::input[@name='FloodCoverageFlag'")).click();
	}
	
	@Step
	public void virifyFloodCoverageDisabled(String value) throws InterruptedException {
		// TODO Auto-generated method stub
		String property = getDriver().findElement(By.xpath("//div[@id='RiskLocation_"+ value +"_FloodCoverage']")).getAttribute("data-bind");
		if(property.contains("disable"))
			Assert.assertTrue("Flood Coverage is disabled as expected", true);
		else
			Assert.assertTrue("Flood Coverage is not disabled as expected", false);
		
	}
	
	@Step
	public void virifyFloodCoverageSelecteded(String value) throws InterruptedException {
		Thread.sleep(5000);
		String property = getDriver().findElement(By.xpath("//div[@id='RiskLocation_"+ value +"_FloodCoverage']//input[text()='"+value+"']")).getAttribute("value");
		if(property.contains("disable"))
			Assert.assertTrue("Flood Coverage option selected is "+value+" as expected", true);
		else
			Assert.assertTrue("Flood Coverage is not selected "+value+" as expected", false);
		
	}
	@Step
	public void verifyFloodCoverageText() {
		// TODO Auto-generated method stub
		String FloodCoverageStaticText_APP = getDriver().findElement(By.xpath("//button[contains(text(),'Elias')]")).getText();
		String ActualFloodCoveragetext = "Flood";
		if(FloodCoverageStaticText_APP.equalsIgnoreCase(ActualFloodCoveragetext))
		{
			Assert.assertTrue("Flood coverage statis text displayed is correct"+ FloodCoverageStaticText_APP, true);
		}
		else
		{
			Assert.assertTrue("Flood coverage statis text displayed is not correct"+ FloodCoverageStaticText_APP, false);
		}
		
	}
	@Step
	public void verifyFloodCoverageTextNotDisplayed() {
		// TODO Auto-generated method stub
		WebElement Flood_Text= getDriver().findElement(By.xpath("//button[contains(text(),'Elias')]"));
		
		if(!(Flood_Text.isDisplayed()))
		{
			Assert.assertTrue("Flood coverage text is not dipslayed", true);
		}
		else
		{
			Assert.assertTrue("Flood coverage text is displayed"+Flood_Text, false);
		}
	}
	
	@Step
	public void enter_building_sum_insured_full_quote(String test_data) throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "HouseAndContents";
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		loop: for (int row = 2; row <= Row_Count; row++) {
			if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(test_data)) {
				rowNum = row;
				break loop;
			}

		}

		System.out.println("Value of Row number " + rowNum);
		
		getDriver().findElement(By.id("selectProperty_0_Home_Coverage")).click();
		
		Thread.sleep(5000);
		
			String building_sum_insured = XLS.getCellData(sheetName, "BuildingSumInsured", rowNum);
			getDriver().findElement(By.id("RiskLocation_0_BuildingSumInsured")).clear();
			getDriver().findElement(By.id("RiskLocation_0_BuildingSumInsured")).sendKeys(building_sum_insured);
			System.out.println("entered building summary is : "+building_sum_insured);
			getDriver().findElement(By.id("RiskLocation_0_BuildingSumInsured")).sendKeys(Keys.TAB);
			Thread.sleep(5000);

	}
	
	@Step
	public void verify_building_sum_insured_default_full_quote(String test_data) throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "HouseAndContents";
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		loop: for (int row = 2; row <= Row_Count; row++) {
			if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(test_data)) {
				rowNum = row;
				break loop;
			}

		}
		String building_sum_insured_string = XLS.getCellData(sheetName, "BuildingSumInsured", rowNum);
		int building_sum_insured = Integer.parseInt(building_sum_insured_string);

				String content = getDriver().findElement(By.name("RiskLocation_0_BuildingDeductible")).getAttribute("value");
				System.out.println(content);
				if(building_sum_insured>=2000000 )
				{
					if(content.contains("1000"))
						Assert.assertTrue("Content sum insured is as expected", true);
					else
						Assert.assertTrue("Content sum insured is as expected", true);
					
				}
				else if(building_sum_insured<2000000 )
				{
					if(content.contains("500"))
						Assert.assertTrue("Content sum insured is as expected", true);
					else
						Assert.assertTrue("Content sum insured is as expected", true);
					
				}		
		
	}
	
	@Step
	public void enter_building_sum_insured_full_quote_two_locations(String test_data, String test_data1) throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "HouseAndContents";
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		loop: for (int row = 2; row <= Row_Count; row++) {
			if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(test_data)) {
				rowNum = row;
				break loop;
			}

		}
		String building_sum_insured = XLS.getCellData(sheetName, "BuildingSumInsured", rowNum);
		
		loop: for (int row = 2; row <= Row_Count; row++) {
			if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(test_data1)) {
				rowNum = row;
				break loop;
			}

		}
		String building_sum_insured1 = XLS.getCellData(sheetName, "BuildingSumInsured", rowNum);
		
		getDriver().findElement(By.id("selectProperty_0_Home_Coverage")).click();
		Thread.sleep(5000);
		getDriver().findElement(By.name("RiskLocation_0_BuildingSumInsured")).sendKeys(building_sum_insured);
		Thread.sleep(5000);
		getDriver().findElement(By.id("selectProperty_1_Home_Coverage")).click();
		Thread.sleep(5000);
		getDriver().findElement(By.name("RiskLocation_1_BuildingSumInsured")).sendKeys(building_sum_insured1);
		Thread.sleep(5000);

	}
	
	@Step
	public void verify_building_sum_insured_default_full_quote_two_locations(String test_data, String test_data1) throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "HouseAndContents";
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		loop: for (int row = 2; row <= Row_Count; row++) {
			if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(test_data)) {
				rowNum = row;
				break loop;
			}

		}
		String building_sum_insured_string = XLS.getCellData(sheetName, "BuildingSumInsured", rowNum);		
		int building_sum_insured = Integer.parseInt(building_sum_insured_string);
		
		loop: for (int row = 2; row <= Row_Count; row++) {
			if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(test_data1)) {
				rowNum = row;
				break loop;
			}

		}
		String building_sum_insured_string1 = XLS.getCellData(sheetName, "BuildingSumInsured", rowNum);		
		int building_sum_insured1 = Integer.parseInt(building_sum_insured_string1);
		System.out.println(building_sum_insured_string);
		System.out.println(building_sum_insured_string1);
		if(building_sum_insured<building_sum_insured1)
			building_sum_insured=building_sum_insured1;
		for (int i=0;i<2;i++)
		{
		
			getDriver().findElement(By.id("selectProperty_"+i+"_Home_Coverage")).click();
			Thread.sleep(5000);
			String content = getDriver().findElement(By.name("RiskLocation_"+i+"_BuildingDeductible")).getAttribute("value");
			System.out.println(content);
			if(building_sum_insured>=2000000 )
			{
				if(content.contains("1000"))
					Assert.assertTrue("Content sum insured is as expected", true);
				else
					Assert.assertTrue("Content sum insured is as expected", true);
					
			}
			else if(building_sum_insured<2000000 )
			{
				if(content.contains("500"))
					Assert.assertTrue("Content sum insured is as expected", true);
				else
					Assert.assertTrue("Content sum insured is as expected", true);
					
			}
		}
		
	}
	
	@Step
	public void verify_upload_and_download_VAC_schedule_buttons_are_available() throws InterruptedException {
		clickVAC();
		Thread.sleep(5000);
		try{
			
			Assert.assertTrue(getDriver().findElement(By.cssSelector("span.fa.fa-upload")).isDisplayed());
			Assert.assertTrue(getDriver().findElement(By.cssSelector("span.fa.fa-download")).isDisplayed());
		}catch(NoSuchElementException e){
			System.out.println("Element no found:"+e.getMessage());
		}
		
	}

	@Step
	public void verify_upload_schedule_button_functionality() throws InterruptedException {
		Thread.sleep(5000);
		try{
			getDriver().findElement(By.cssSelector("span.fa.fa-upload")).click();
			Thread.sleep(3000);
			getDriver().findElement(By.id("FileUpload")).sendKeys(System.getProperty("user.dir")+"\\src\\test\\java\\au\\com\\chubb\\evolutionshowcase\\Images\\VAC Schedule_JIV.xlsx");
			Thread.sleep(5000);
			getDriver().findElement(By.cssSelector("div.checkbox>label")).click();
			Thread.sleep(5000);
			getDriver().findElement(By.xpath("//button[text()='Upload file']")).click();
			Thread.sleep(5000);
			
			WebElement worksheet = getDriver().findElement(By.id("ddlWorksheet"));
			Select sel = new Select(worksheet);
			sel.selectByIndex(1);
			Thread.sleep(7000);
			
			WebElement item_number = getDriver().findElement(By.xpath("//select[@name='ko_unique_6']"));
			Select item_dropdown = new Select(item_number);
			item_dropdown.selectByIndex(1);
			Thread.sleep(7000);
			
			WebElement category = getDriver().findElement(By.xpath("//select[@name='ko_unique_7']"));
			Select cat_dropdown = new Select(category);
			cat_dropdown.selectByIndex(2);
			Thread.sleep(7000);
			
			WebElement suminsured = getDriver().findElement(By.xpath("//select[@name='ko_unique_8']"));
			Select si_dropdown = new Select(suminsured);
			si_dropdown.selectByIndex(3);
			Thread.sleep(7000);
			
			WebElement description = getDriver().findElement(By.xpath("//select[@name='ko_unique_9']"));
			Select descriptiondrop = new Select(description);
			descriptiondrop.selectByIndex(5);
			Thread.sleep(7000);
			
			WebElement lvd = getDriver().findElement(By.xpath("//select[@name='ko_unique_10']"));
			Select lvdDrop = new Select(lvd);
			lvdDrop.selectByIndex(4);

			Thread.sleep(4000);
			getDriver().findElement(By.xpath("//button[text()='Process file']")).click();
			Thread.sleep(5000);
		}catch(NoSuchElementException e){
			System.out.println("Element no found:"+e.getMessage());
		}
	}	
		
    @Step
	public void verify_download_schedule_button_functionality() throws InterruptedException {
		Thread.sleep(5000);
		try{
			getDriver().findElement(By.cssSelector("span.fa.fa-download")).click();
			Thread.sleep(3000);
		}catch(NoSuchElementException e){
			System.out.println("Element no found:"+e.getMessage());
		}
		
	}
}