package au.com.chubb.evolutionshowcase.pages.stepDefinitions.policyAdmin;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.policyAdmin.policyAdminWelcomeSteps;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class policyAdminWelcomeStepDefinition {

	@Steps
	policyAdminWelcomeSteps policy_admin_welcome_steps;

	@Given("^evolution application is launched$")
	public void launch_evolution_app() throws Throwable {
		policy_admin_welcome_steps.launch_evolution_app();
		Thread.sleep(1000);
	}
	@Given("^evolution SIT application is launched$")
	public void launch_SIT_evolution_app() throws Throwable {
		policy_admin_welcome_steps.launch_evolution_app_in_SIT();
	}
	
	@And("^Create a new client as UW in \"([^\"]*)\"$")
	public void crate_new_client(String testdata) throws Throwable {
		policy_admin_welcome_steps.crate_new_client(testdata);
	}
	
	@And("^Get Quote as UW in \"([^\"]*)\"$")
	public void get_Quote(String testdata) throws Throwable {
		policy_admin_welcome_steps.get_Quote(testdata);
	}
	
	@And("^Create a new company client in UW$")
	public void crate_new_company_client() throws Throwable {
		policy_admin_welcome_steps.crate_new_company_client();
	}
	
	@Then("^Verify Chubb Direct and NAB$")
	public void verify_ChubbDirectandNAB() throws InterruptedException {
		policy_admin_welcome_steps.verify_ChubbDirectandNAB();
	}
	
	@And("^select search type as \"([^\"]*)\"$")
	public void select_search_type(String searchType) throws Throwable {
		policy_admin_welcome_steps.select_search_type(searchType);
	}

	@And("^enter search value \"([^\"]*)\"$")
	public void enter_search_value(String searchValue) throws Throwable {
		policy_admin_welcome_steps.enter_search_value(searchValue);
	}

	@And("^select search result \"([^\"]*)\"$")
	public void select_search_result(String searchResult) throws Throwable {
		policy_admin_welcome_steps.select_search_result(searchResult);
	}
	
	@And("^click search$")
	public void click_search() throws Throwable {
		policy_admin_welcome_steps.click_search();
	}

	@And("^click Get a Quote$")
	public void click_get_a_quote() throws Throwable {
		policy_admin_welcome_steps.click_get_a_quote();
	}
		
	@And("^search saved indicative quote number \"([^\"]*)\"$")
	public void search_saved_indicative_quote(String test_flow) throws Throwable {
		policy_admin_welcome_steps.search_saved_indicative_quote(test_flow);
	}
	
	
	@And("^search quote number \"([^\"]*)\"$")
	public void search_QuoteNumber(String test_flow) throws Throwable {
		policy_admin_welcome_steps.search_QuoteNumber(test_flow);
	}
	
	
	@And("^search saved quote number \"([^\"]*)\"$")
	public void search_saved_quote(String test_flow) throws Throwable {
		policy_admin_welcome_steps.search_saved_quote(test_flow);
	}
	
	@And("^search saved quote with our ref number \"([^\"]*)\"$")
	public void search_saved_quote_OurRef(String test_flow) throws Throwable {
		policy_admin_welcome_steps.search_quote_OurRef(test_flow);
	}
	
	@And("^wait for Document Generation$")
	public void waitForDocumentGeneration() throws Throwable {
		policy_admin_welcome_steps.waitForDocumentGeneration();
	}
	
	@And("^search saved policy number \"([^\"]*)\"$")
	public void search_saved_policy(String test_flow) throws Throwable {
		policy_admin_welcome_steps.search_saved_policy(test_flow);
	}
	
	@And("^release lock for editing$")
	public void releaseLockForEditing() throws Throwable {
		policy_admin_welcome_steps.releaseEditLock();
	}
	
	@And("^click on quote number from \"([^\"]*)\"$")
	public void click_policy_number(String test_flow) throws Throwable {
		policy_admin_welcome_steps.click_policy_number(test_flow);
	}
	
	@And("^verify effective date from policy admin as below$")
	public void verify_policy_effective_date() throws Throwable {
		policy_admin_welcome_steps.verify_policy_effective_date();
	}
	
	@And("^enter search value saved from Winbeat and search$")
	public void searchClientNameFromWinBeat() throws Throwable {
		policy_admin_welcome_steps.searchClientNameFromWinBeat();
	}
	
	@And("^Save Quote Number created in WinBeat$")
	public void saveQuoteNumber() throws Throwable {
		policy_admin_welcome_steps.saveQuoteNumber();
	}
	
	@And("^search quote and close the quote created in Winbeat$")
	public void searchAndCloseTheQuote() throws Throwable {
		policy_admin_welcome_steps.searchAndCloseTheQuote();
	}

}