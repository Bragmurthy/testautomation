package au.com.chubb.evolutionshowcase.pages.stepDefinitions.policyAdmin;


import java.io.IOException;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.policyAdmin.workManagerSteps;
import au.com.chubb.evolutionshowcase.utilities.CommonFunctions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class workManagerStepdefinition {
	@Steps
	workManagerSteps workManagerSteps;
	CommonFunctions common_functions;
	@Given("^approve quote with quote number saved in \"([^\"]*)\"$")
	public void ApproveQuote(String testdata) throws Throwable {
		workManagerSteps.approveQuote(testdata);
	}
	@Given("^approve quote in SIT with quote number saved in \"([^\"]*)\"$")
	public void ApproveQuoteInSIT(String testdata) throws Throwable {
		workManagerSteps.approveQuoteinSIT(testdata);
	}
	@Given("^decline quote with quote number saved in \"([^\"]*)\"$")
	public void DeclineQuote(String testdata) throws Throwable {
		workManagerSteps.declineQuote(testdata);
	}
	@Given("^decline quote in SIT with quote number saved in \"([^\"]*)\"$")
	public void DeclineQuoteInSIT(String testdata) throws Throwable {
		workManagerSteps.declineQuoteinSIT(testdata);
	}
	
	@Given("^verify refral rules has been re run for quote with quote number saved in \"([^\"]*)\"$")
	public void verifyRerunRefrals(String testdata) throws Throwable {
		workManagerSteps.verifyRerunRefrals(testdata);
	}
	@Given("^Search quote in work manager with quote number saved in \"([^\"]*)\"$")
	public void searchQuote(String testdata) throws Throwable {
		workManagerSteps.searchQuote(testdata);
	}
	@Given("^Edit Policy in work manager with policy number saved in \"([^\"]*)\"$")
	public void editInPolicy(String testdata) throws Throwable {
		workManagerSteps.editInPolicy(testdata);
		
	}
	
	@Given("^Verify the transaction channel field and text role \"([^\"]*)\" of the quote saved in \"([^\"]*)\"$")
	public void VerifyTRansactionChannelandRole(String Role , String testdata) throws Throwable
	{
		workManagerSteps.VerifyTransactionChannelandRole(Role,testdata);
	}
	
//	@Given("^Workmanager is launched$")
//	public void launchWorkManager() throws Throwable
//	{
//		common_functions.launch_WorkManager_app();
//	}
	
	@And("^Verify transaction channel under filter items$")	
	public void verifychannelinfilteritem() throws Throwable
	{
		workManagerSteps.Verifytransactionchannelinfilteritem();
	}
	
	@Given("^approve policy in SIT with quote number saved in \"([^\"]*)\"$")
	public void approvePolicyinSIT(String testdata) throws Throwable {
		workManagerSteps.approvePolicyinSIT(testdata);
	}

}