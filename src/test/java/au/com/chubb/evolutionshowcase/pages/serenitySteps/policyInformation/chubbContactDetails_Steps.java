package au.com.chubb.evolutionshowcase.pages.serenitySteps.policyInformation;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.runner.RunWith;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.pages.objectModel.policyAdmin.policyAdminWelcomePage;
import au.com.chubb.evolutionshowcase.utilities.CommonFunctions;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class chubbContactDetails_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public chubbContactDetails_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);

	}

	@Step
	public void select_chubb_contact_name(String chubb_contact_name) throws InterruptedException {
		new Select(getDriver().findElement(By.id("chubbContact"))).selectByVisibleText(chubb_contact_name);
		Thread.sleep(1000);

	}

	@Step
	public void do_not_renew(String justificationReason) throws InterruptedException {

		getDriver().findElement(By.xpath(".//*[@id='isDNR']")).click();
		Thread.sleep(1000);
		getDriver().findElement(By.id("dnrJustification")).sendKeys(justificationReason);

	}

	@Step
	public void manual_renewal(String justificationReason) throws InterruptedException {

		getDriver().findElement(By.xpath(".//*[@id='referUponRenewal']")).click();
		Thread.sleep(1000);
		getDriver().findElement(By.id("referUponRenewalJsutificationText")).sendKeys(justificationReason);

	}

	@Step
	public void click_next_button() throws InterruptedException {

		getDriver().findElement(By.id("btnProceedNext")).click();
		Thread.sleep(15000);
		try
		   {
			   Alert alert = getDriver().switchTo().alert();
			   alert.accept();
			   Thread.sleep(20000);
		   }catch (Exception e)
		   {
			   
		   }

	}

	@Step
	public void click_risk_and_coverages() throws InterruptedException {

		getDriver().findElement(By.xpath(".//*[@id='sideNavigation']/ul/li[2]/a")).click();
		Thread.sleep(15000);
	}

}