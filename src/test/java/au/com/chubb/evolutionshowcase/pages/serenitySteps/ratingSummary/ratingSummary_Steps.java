package au.com.chubb.evolutionshowcase.pages.serenitySteps.ratingSummary;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.tools.ant.types.CommandlineJava.SysProperties;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

import java.util.regex.*;

@RunWith(SerenityRunner.class)
public class ratingSummary_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public ratingSummary_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);

	}

	public void enter_lossHistory_details(String lossHistory_testData) throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		Thread.sleep(1000);

		String sheetName = "LossHistory";
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: for (int row = 2; row <= Row_Count; row++) {

			System.out.println("Liability test data --" + lossHistory_testData);
			if (lossHistory_testData.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				break innerloop;
			}
		}
		System.out.println("Value of row number " + rowNum);

		// Claim History
		List<WebElement> claimsHistoryFlag = getDriver().findElements(By.name("claimsHistoryFlag"));
		if (XLS.getCellData(sheetName, "ClaimHistory", rowNum).equalsIgnoreCase("Yes")) {
			claimsHistoryFlag.get(0).click();
			Thread.sleep(1000);
			String Claim_Count = XLS.getCellData(sheetName, "ClaimCount", rowNum);
			int claim_count = Integer.parseInt(Claim_Count);
			int locator = 9;
			for (int i = 0; i < claim_count; i++) {

				getDriver().findElement(By.id("addClaim")).click();
				Thread.sleep(1000);

				try {

					getDriver().findElement(By.id("claim_" + i + "_lossDate"))
							.sendKeys(XLS.getCellData(sheetName, "claim_" + i + "_lossDate", rowNum));
					Thread.sleep(500);
					getDriver().findElement(By.id("claim_" + i + "_lossDate")).sendKeys(Keys.ENTER);
					Thread.sleep(500);

				} catch (Exception e) {

				}

				try {

					getDriver().findElement(By.id("claim_" + i + "_lossDescription"))
							.sendKeys(XLS.getCellData(sheetName, "claim_" + i + "_lossDescription", rowNum));
					Thread.sleep(500);
				} catch (Exception e) {

				}

				try {

					new Select(getDriver().findElement(By.id("claim_" + i + "_LossType")))
							.selectByVisibleText(XLS.getCellData(sheetName, "claim_" + i + "_LossType", rowNum));
					Thread.sleep(1000);

				} catch (Exception e) {
					System.out.println(e);

				}

				try {
					getDriver().findElement(By.id("claim_" + i + "_paidAmount"))
							.sendKeys(XLS.getCellData(sheetName, "claim_" + i + "_paidAmount", rowNum));
					Thread.sleep(1000);
				} catch (Exception e) {

				}

				try {
					getDriver().findElement(By.id("claim_" + i + "_riskAddress")).click();
					Thread.sleep(1000);
					getDriver().findElement(By.id("claim_" + i + "_riskAddress"))
							.sendKeys(XLS.getCellData(sheetName, "claim_" + i + "_riskAddress", rowNum));
					Thread.sleep(1000);
				} catch (Exception e) {

				}

				if (XLS.getCellData(sheetName, "claim_" + i + "_exemptFromRating", rowNum).equalsIgnoreCase("Yes")) {

					try {

						getDriver().findElement(By.id("claim_" + i + "_exemptFromRating")).click();
						Thread.sleep(1000);
						Thread.sleep(1000);
						// XLS.getCellData(sheetName,
						// "JustificationForExclusion_" + i, rowNum)
						new Select(getDriver().findElement(By.id("ko_unique_ " + locator)))
								.selectByVisibleText("Accommodation");
						Thread.sleep(2000);
						getDriver()
								.findElement(By
										.xpath(".//*[@id='lossHistoryForm']/div/div[1]/div/div[3]/table[1]/tbody/tr[2]/td/div/div/div/button[1]"))
								.click();
						Thread.sleep(1000);

					} catch (Exception e) {

					}

				}
				locator = locator + 2;

			}

		} else if (XLS.getCellData(sheetName, "ClaimHistory", rowNum).equalsIgnoreCase("No")) {
			claimsHistoryFlag.get(1).click();
			Thread.sleep(100);

		}

		// InsuranceRefusal
		List<WebElement> InsuranceRefusalFlag = getDriver().findElements(By.name("insuranceRefusalCancelRejectedFlag"));
		if (XLS.getCellData(sheetName, "InsuranceRefusal", rowNum).equalsIgnoreCase("Yes")) {
			InsuranceRefusalFlag.get(0).click();
			Thread.sleep(500);
			getDriver()
					.findElement(
							By.xpath(".//*[@id='lossHistoryForm']/div/div[2]/div/div/div/div[2]/div/span[2]/textarea"))
					.sendKeys(XLS.getCellData(sheetName, "InsuranceRefusalReason", rowNum));
			Thread.sleep(500);

		} else if (XLS.getCellData(sheetName, "InsuranceRefusal", rowNum).equalsIgnoreCase("No")) {
			InsuranceRefusalFlag.get(1).click();
			Thread.sleep(100);

		}

		JavascriptExecutor jse = (JavascriptExecutor) getDriver();
		jse.executeScript("window.scrollBy(0,450)", "");
		Thread.sleep(3000);

		// Charge Convicted
		List<WebElement> ChargeConvictedFlag = getDriver().findElements(By.name("chargeConvictedFlag"));
		if (XLS.getCellData(sheetName, "ChargeConvicted", rowNum).equalsIgnoreCase("Yes")) {
			ChargeConvictedFlag.get(0).click();
			Thread.sleep(500);
			getDriver()
					.findElement(
							By.xpath(".//*[@id='lossHistoryForm']/div/div[3]/div/div/div/div[2]/div/span[2]/textarea"))
					.sendKeys(XLS.getCellData(sheetName, "ChargeConvictedReason", rowNum));
			Thread.sleep(500);

		} else if (XLS.getCellData(sheetName, "ChargeConvicted", rowNum).equalsIgnoreCase("No")) {
			ChargeConvictedFlag.get(1).click();
			Thread.sleep(500);

		}

		jse.executeScript("window.scrollBy(0,450)", "");
		Thread.sleep(3000);

		// Bankruptcy
		List<WebElement> BankruptcyFlag = getDriver().findElements(By.name("bankruptcyFiledFlag"));
		if (XLS.getCellData(sheetName, "Bankruptcy", rowNum).equalsIgnoreCase("Yes")) {
			BankruptcyFlag.get(0).click();
			Thread.sleep(500);

			getDriver()
					.findElement(
							By.xpath(".//*[@id='lossHistoryForm']/div/div[4]/div/div/div/div[2]/div/span[2]/textarea"))
					.sendKeys(XLS.getCellData(sheetName, "BankruptcyReason", rowNum));
			Thread.sleep(500);

		} else if (XLS.getCellData(sheetName, "Bankruptcy", rowNum).equalsIgnoreCase("No")) {
			BankruptcyFlag.get(1).click();
			Thread.sleep(500);

		}

		try {
			getDriver().findElement(By.id("actionFooter")).click();
			Thread.sleep(3000);
			getDriver().findElement(By.id("btnSaveProgress")).click();
			Thread.sleep(7000);
		} catch (Exception e) {

		}

	}

	@Step
	public void click_proceed_with_Quote() throws InterruptedException {

		getDriver().findElement(By.xpath("//button[contains(.,'Proceed With Quote')]")).click();
		Thread.sleep(7000);

	}

	@Step
	public void retrieve_values_rating_summary() {

		System.out.println("TOTAL ANNUAL BASE PREMIUM -- " + getDriver()
				.findElement(By.xpath(".//*[@id='subSideNavigationHeader']/div[1]/div/div/div[1]/div/p")).getText());
		System.out.println("TOTAL POLICY BASE PREMIUM -- "
				+ getDriver().findElement(By.id("ModifiedTransactionPremiumAmount_policy")).getAttribute("value"));
		System.out.println("TOTAL CHARGE POLICY PREMIUM -- "
				+ getDriver().findElement(By.id("ChargedPremiumAmount_policy")).getAttribute("value"));
		System.out.println("POLICY COMISSION RATE -- "
				+ getDriver().findElement(By.id("PolicyCommissionRate_policy")).getAttribute("value"));

	}

	@Step
	public void modify_rating_summary_details(String location, String testData) throws InterruptedException {

		int location_identifier = Integer.parseInt(location);
		getDriver().findElement(By.xpath(".//*[@id='subSideNavigation']/ul/li[" + location_identifier + "]/a")).click();
		Thread.sleep(3000);

		int sub_identifier = location_identifier - 1;

		String sheetName = "RatingSummary";

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		loop: for (int row = 2; row <= Row_Count; row++) {
			if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(testData)) {
				rowNum = row;
				break loop;
			}

		}

		JavascriptExecutor jse = (JavascriptExecutor) getDriver();
		jse.executeScript("window.scrollBy(0,450)", "");
		Thread.sleep(3000);

		jse.executeScript("window.scrollBy(0,450)", "");
		Thread.sleep(3000);

		getDriver().findElement(By.id("ApprovedLineLimitAmount_" + sub_identifier))
				.sendKeys(XLS.getCellData(sheetName, "LineLimit", rowNum));
		Thread.sleep(500);
		if (XLS.getCellData(sheetName, "AuthorityReceived", rowNum).equalsIgnoreCase("Yes")) {
			getDriver().findElement(By.id("OutsideAuthorityReceivedFlag_" + sub_identifier)).click();
		}

		jse.executeScript("window.scrollBy(0,450)", "");
		Thread.sleep(3000);

		jse.executeScript("window.scrollBy(0,450)", "");
		Thread.sleep(3000);

		try {
			getDriver().findElement(By.id("AuthorisedByName_" + sub_identifier))
					.sendKeys(XLS.getCellData(sheetName, "AuthorisedBy", rowNum));
			Thread.sleep(500);
		} catch (Exception e) {

		}

		try {
			getDriver().findElement(By.id("AuthorisedDate_" + sub_identifier))
					.sendKeys(XLS.getCellData(sheetName, "DateAuthorised", rowNum));
			Thread.sleep(500);
		} catch (Exception e) {

		}

		try {
			getDriver().findElement(By.id("AuthorisedDate_" + sub_identifier)).sendKeys(Keys.ENTER);
			Thread.sleep(500);
		} catch (Exception e) {

		}
		try {
			if (XLS.getCellData(sheetName, "ApprovalField_EUF", rowNum).equalsIgnoreCase("Yes")) {
				getDriver().findElement(By.id("ApprovalFiledInEUFFlag_" + sub_identifier)).click();
				Thread.sleep(500);

			}

		} catch (Exception e) {

		}
		Thread.sleep(1000);

		getDriver().findElement(By.id("actionFooter")).click();

		Thread.sleep(2000);
		getDriver().findElement(By.xpath(".//*[@id='actionFooter']/div[1]/span[1]/button")).click();

		Thread.sleep(7000);

	}

}
