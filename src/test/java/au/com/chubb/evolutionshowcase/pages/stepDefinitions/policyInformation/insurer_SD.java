package au.com.chubb.evolutionshowcase.pages.stepDefinitions.policyInformation;
import au.com.chubb.evolutionshowcase.pages.serenitySteps.policyInformation.insurer_Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class insurer_SD {

	@Steps
	insurer_Steps insurer;
	
	@And("^select current insurer \"([^\"]*)\"$")
	public void select_current_insurer(String insurer_name) throws Throwable {
		insurer.select_current_insurer(insurer_name);
	}
	
	@And("^edit named insured with type \"([^\"]*)\" as below$")
	public void edit_named_insured(String insurer_name, DataTable arg1) throws Throwable {
		insurer.edit_named_insured(insurer_name,arg1);
	}
	
	@And("^delete existing named insured$")
	public void delete_existing_named_insured() throws InterruptedException  {
		insurer.delete_existing_named_insured();
	}
	
	@Then("^verify fields in \"([^\"]*)\" page$")
	public void verify_fields_in_page(String namedInsured)  {
		insurer.verifyFieldsInPage(namedInsured);
	}

	@Then("^\"([^\"]*)\" Named Insured as per \"([^\"]*)\"$")
	public void named_Insured_as_per(String action, String testflow) throws Throwable {
		insurer.addNamedInsured(action,testflow);
	}
	
	@And("^edit \"([^\"]*)\" Named Insured as per \"([^\"]*)\"$")
	public void named_Insured_Edit(String position, String testflow) throws Throwable {
		insurer.edit_named_insured(position, testflow);
	}

	@Then("^verify Named Insured added succesfully \"([^\"]*)\"$")
	public void verify_Named_Insured_added_succesfully(String testflow) throws InterruptedException  {
		
		insurer.verifyNamedInsuredAddedSuccesfully(testflow);
	}
	
	@Then("^verify Named Insured added succesfully in UW view$")
	public void verify_Named_Insured_added_succesfully_InUWView(){
		
	}
	
	
	@And("^add name to be displayed in document \"([^\"]*)\"$")
	public void add_name_to_document(String document_name) {
		insurer.add_name_to_document(document_name);
	}
	
	@And("^search and add named insured \"([^\"]*)\"$")
	public void search_and_add_named_insured(String namedInsured_name) throws Throwable {
		insurer.search_and_add_named_insured(namedInsured_name);
	}
	
	@And("^search address \"([^\"]*)\" in policy information$")
	public void search_address_policy_information(String address) throws Throwable {
		insurer.search_address_policy_information(address);
	}
	
	@Then("^verify default values in named insured section$")
	public void verify_named_insured_default_values(){
	   insurer.verify_named_insured_default_values();
	}
	 
	@Given("^change insurer$")
	public void Current_Insurer(DataTable insurerName) throws Throwable {
		insurer.changeInsurer(insurerName);
	}
	
	@Given("^verify insurer$")
	public void Verify_Insurer(DataTable insurerName) throws Throwable {
		insurer.verifyInsurer(insurerName);
	}
	
	@Given("^verify insurer section is not editable$")
	public void verifyInsurerSectionNotEditable() throws Throwable {
		insurer.verifyInsurerSectionNotEditable();
	}
	
	@Given("^Remove existing named insured$")
	public void Remove_named_Insured() throws Throwable {
		insurer.Remove_named_Insured();
	}

}
