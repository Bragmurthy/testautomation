package au.com.chubb.evolutionshowcase.pages.serenitySteps.policyInformation;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class policyInformation_misc_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;
	DynamicSeleniumFunctions functions;

	public policyInformation_misc_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);

	}

	@Step
	public void verify_quote_status(String quote_status) throws InterruptedException {
		// TODO Auto-generated method stub
		
		Thread.sleep(4000);
		//String quote_status_APP=getDriver().findElement(By.id("transactionStatusCode")).getAttribute("value");
		String quote_status_APP=getDriver().findElement(By.xpath("//span[contains(text(),'Status')]")).getText();
		quote_status_APP=quote_status_APP.split(":")[1].trim();
		
		//if (quote_status_APP.equalsIgnoreCase(quote_status))
		if (quote_status_APP.contains(quote_status))
		{
			Assert.assertTrue("Quote status in evolution is " +quote_status_APP, true);
		}else
		{
			Assert.assertTrue("Quote status in evolution is " +quote_status_APP+" and Expected is "+quote_status+"", false);

		}
	}

	@Step
	public void verify_ClientName(String clientName) throws InterruptedException {
		Thread.sleep(4000);
		String clientName_App=getDriver().findElement(By.xpath("//a[contains(text(),'Client:')]")).getText();
		
		if (clientName_App.contains(clientName))
		{
			Assert.assertTrue("Client Name in evolution is " +clientName_App, true);
		}else
		{
			Assert.assertTrue("Client Name in evolution is " +clientName_App+" and Expected is "+clientName+"", false);

		}
	}
	
	@Step
	public void verify_NamedInsuredSearch(String namedInsuredName) throws InterruptedException {
		Thread.sleep(4000);
		String namedInsured_App=getDriver().findElement(By.xpath("//a[contains(text(),'Client:')]")).getText();
		
		if (namedInsured_App.contains(namedInsuredName))
		{
			Assert.assertTrue("Named Insured Name in evolution is " +namedInsured_App, true);
		}else
		{
			Assert.assertTrue("Named Insured name in evolution is " +namedInsured_App+" and Expected is "+namedInsuredName+"", false);

		}
	}
	
	@Step
	public void verify_side_navigation_bar_absence(String link_name) {

		boolean navigation_link_display=true;
		
		try
		{
		getDriver().findElement(By.xpath("//*[contains(text(),('" + link_name + "'))]")).click();
		}catch (Exception e)
		{
			navigation_link_display=false;
		}
		
		
		if (navigation_link_display==false)
		{
			Assert.assertTrue(link_name +" is not displayed in side navigation bar ", true);
		}else
		{
			Assert.assertTrue(link_name +" is not displayed in side navigation bar ", false);

		}

	}

	@Step
	public void verify_side_navigation_bar_presence(String link_name) {
		// TODO Auto-generated method stub
       boolean navigation_link_display=false;
		
		try
		{
		getDriver().findElement(By.xpath("//*[contains(text(),('" + link_name + "'))]")).click();
		}catch (Exception e)
		{
			navigation_link_display=true;
		}
		
		
		if (navigation_link_display==true)
		{
			Assert.assertTrue(link_name +" is not displayed in side navigation bar ", true);
		}else
		{
			Assert.assertTrue(link_name +" is not displayed in side navigation bar ", false);

		}

	}
	
	
	@Step
	public void verifyUserIsOnPolicyInfromationPage() {
	    boolean fieldDisplayed=true;
		
		try
		{
		getDriver().findElement(By.xpath("//h4[contains(text(),'Effective Term')]")).isDisplayed();
		}catch (Exception e)
		{
			fieldDisplayed=false;
		}
		
		
		if (fieldDisplayed)
		{
			Assert.assertTrue("User is on Policy Information page", true);
		}else
		{
			Assert.assertTrue( "User is not on Policy Information page", false);

		}
	}
@Step
	public void clicOnOK() throws InterruptedException {
	getDriver().findElement(By.xpath("//span[button[contains(text(),'OK')]]")).click();
	Thread.sleep(5000);
	}

	@Step
	public void verifyclientdetailsarenoneditable(String ClientName, String ClientPhoneumber) throws InterruptedException, IOException{
		boolean name = functions.verifyFieldNonEditable(ClientName);
		boolean phone = functions.verifyFieldNonEditable(ClientPhoneumber);	
		if (name && phone)
			Assert.assertTrue("Name, phone are not editable", true);
			else
			Assert.assertTrue("Name, phone are  editable", false);
	}
	
	@Step
	public void verifypolicydetailsarenoneditable(String policyaddress) throws InterruptedException, IOException{
		boolean address = functions.verifyFieldNonEditable(policyaddress);
		
		if (address)
			Assert.assertTrue("Adress is not editable", true);
			else
			Assert.assertTrue("Adressnot is editable", false);

	}

	@Step
	public void verifyPolicyAddressNonEditable() {
	
	boolean  isdisplayedEditor=getDriver().findElement(By.xpath("//input[@id='addressEditor_0_Search']")).isDisplayed();
	boolean  isdisplayedEditorUnitno=getDriver().findElement(By.xpath("//input[@id='addressEditor_0_UnitNo']")).isDisplayed();
	
	if (!(isdisplayedEditor&&isdisplayedEditorUnitno))
			{
				Assert.assertTrue("Policy Address is not editable", true);
			}else
			{
				Assert.assertTrue( "Policy Address is editable", false);

			}
	}

	@Step
	public void verifyAllDefaultDetailsofPolicyInformation(String testflow ) throws InterruptedException{
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "QuoteData";
		Thread.sleep(1000);
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {
		
			if (testflow.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				System.out.println("test data --" + testflow);
				System.out.println("try to come out of loop "+rowNum);
				break innerloop;
			}
		}
		
		String clientName=XLS.getCellData(sheetName, "clientName", rowNum);	
		String clientDOB=XLS.getCellData(sheetName, "clientDOB", rowNum);
		String clientPhone=XLS.getCellData(sheetName, "clientPhone", rowNum);
		String clientEmail=XLS.getCellData(sheetName, "clientEmail", rowNum);
		String clientRetired=XLS.getCellData(sheetName, "clientRetired", rowNum);
		String clientIndustry=XLS.getCellData(sheetName, "clientIndustry", rowNum);
		String clientOccupation=XLS.getCellData(sheetName, "clientOccupation", rowNum);
		String clientEmployer=XLS.getCellData(sheetName, "clientEmployer", rowNum);
		
		String clientNameApp=getDriver().findElement(By.xpath("//span[contains(@data-bind, 'Client.Item.FullName')]")).getText();
		
		String clientDOBApp=getDriver().findElement(By.xpath("//span[contains(@data-bind, 'Client.Item.DateOfBirth')]")).getText();
		String clientPhoneApp=getDriver().findElement(By.xpath("//span[contains(@data-bind, 'Client.Item.Contact.PhoneNumber')]")).getText();
		String clientEmailApp=getDriver().findElement(By.xpath("//span[contains(@data-bind, 'Client.Item.Contact.Email')]")).getText();
		String clientRetiredApp=getDriver().findElement(By.xpath("//span[contains(@data-bind, 'Client.Item.Retired')]")).getText();
		String clientIndustryApp=getDriver().findElement(By.xpath("//span[contains(@data-bind, 'Client.IndustryText')]")).getText();
		String clientOccupationApp=getDriver().findElement(By.xpath("//span[contains(@data-bind, 'Client.Item.OccupationText')]")).getText();
		String clientEmployerApp=getDriver().findElement(By.xpath("//span[contains(@data-bind, 'Client.Item.EmployerName')]")).getText();
		
		if(clientNameApp.equals(clientName) &&  clientDOBApp.equals(clientDOB) && clientPhoneApp.equals(clientPhone) && clientEmailApp.equals(clientEmail)
				&& clientRetiredApp.equals(clientRetired) && clientIndustryApp.equals(clientIndustry) && clientOccupationApp.equals(clientOccupation)
				&& clientEmployerApp.equals(clientEmployer))
			Assert.assertTrue("Client Details reflected", true);
		else
			Assert.assertTrue("Client Details not reflected", false);
	
		String namedInsuredName=XLS.getCellData(sheetName, "namedInsuredName", rowNum);
		String namedInsuredDOB=XLS.getCellData(sheetName, "namedInsuredDOB", rowNum);
		String namedInsuredRetired=XLS.getCellData(sheetName, "namedInsuredRetired", rowNum);
		String namedInsuredIndustry=XLS.getCellData(sheetName, "namedInsuredIndustry",rowNum);
		String namedInsuredOccupation=XLS.getCellData(sheetName, "namedInsuredOccupation",rowNum);
		String namedInsuredEmployer=XLS.getCellData(sheetName, "namedInsuredEmployer", rowNum);
		String namedInsuredPhone=XLS.getCellData(sheetName, "namedInsuredPhone", rowNum);
		String namedInsuredEmail=XLS.getCellData(sheetName, "namedInsuredEmail", rowNum);
		String namedInsuredDate=XLS.getCellData(sheetName, "namedInsuredDate", rowNum);
		
		String namedInsuredNameApp=getDriver().findElement(By.xpath("//span[contains(@data-bind,'item().fullName()')]")).getText();
		String namedInsuredDOBApp=getDriver().findElement(By.xpath("//span[contains(@data-bind,'item().dateOfBirth')]")).getText();
		String namedInsuredRetiredApp=getDriver().findElement(By.xpath("//span[contains(@data-bind,'item().retired()')]")).getText();
		String namedInsuredIndustryApp=getDriver().findElement(By.xpath("//span[contains(@data-bind,'industryText')]")).getText();	
		String namedInsuredOccupationApp=getDriver().findElement(By.xpath("//span[contains(@data-bind,'item().occupationText')]")).getText();
		String namedInsuredEmployerApp=getDriver().findElement(By.xpath("//span[contains(@data-bind,'item().employerName')]")).getText();
		String namedInsuredPhoneApp=getDriver().findElement(By.xpath("//span[contains(@data-bind,'item().contact.phoneNumber(), ')]")).getText();
		String namedInsuredEmailApp=getDriver().findElement(By.xpath("//span[contains(@data-bind,'item().contact.email()')]")).getText();
		String namedInsuredDateApp=getDriver().findElement(By.xpath("//span[contains(@data-bind,'createDate')]")).getText();	
		
		if(namedInsuredNameApp.equals(namedInsuredName)
				&& namedInsuredDOBApp.equals(namedInsuredDOB) 
				&& namedInsuredRetiredApp.equals(namedInsuredRetired)
				&& namedInsuredIndustryApp.equals(namedInsuredIndustry)
				&& namedInsuredOccupationApp.equals(namedInsuredOccupation) 
				&& namedInsuredEmployerApp.equals(namedInsuredEmployer) 
				&&namedInsuredPhoneApp.equals(namedInsuredPhone)
				&& namedInsuredEmailApp.equals(namedInsuredEmail))
				//&& namedInsuredDateApp.equals(namedInsuredDate))
			Assert.assertTrue("Named Insured Details reflected", true);
		else
			Assert.assertTrue("Named Insured Details not reflected", false);
		
		
		String brokerOrg=XLS.getCellData(sheetName, "brokerOrg", rowNum);
		String brokerAddress=XLS.getCellData(sheetName, "brokerAddress", rowNum);
		String brokerCountry=XLS.getCellData(sheetName, "brokerCountry", rowNum);
		String brokerContact=XLS.getCellData(sheetName, "brokerContact", rowNum);
		String brokerEmail=XLS.getCellData(sheetName, "brokerEmail", rowNum);
		String brokerPhone=XLS.getCellData(sheetName, "brokerPhone", rowNum);
		String brokerMobile=XLS.getCellData(sheetName, "brokerMobile", rowNum);

		String brokerOrgApp=getDriver().findElement(By.xpath("//span[contains(@data-bind,'organizationName')]")).getText();
		String brokerAddressApp=getDriver().findElement(By.xpath("//span[contains(@data-bind,'streetCityAddress')]")).getText();
		String brokerCountryApp=getDriver().findElement(By.xpath("//span[contains(@data-bind,'countryName')]")).getText();
		String brokerContactApp=getDriver().findElement(By.xpath("//span[contains(@data-bind,'surname()')]")).getText();	
		String brokerEmailApp=getDriver().findElement(By.xpath("//span[contains(@data-bind,'email')]")).getText();
		String brokerPhoneApp=getDriver().findElement(By.xpath("//span[contains(@data-bind,'formatInput: phoneNumber')]")).getText();
		String brokerMobileApp=getDriver().findElement(By.xpath("//span[contains(@data-bind,'formatInput: mobileNumber')]")).getText();
		
		if(brokerOrgApp.equals(brokerOrg)
				&& brokerAddressApp.equals(brokerAddress) 
				&& brokerCountryApp.equals(brokerCountry)
				&& brokerContactApp.equals(brokerContact)
				&& brokerEmailApp.equals(brokerEmail) 
				&& brokerPhoneApp.equals(brokerPhone) 
				&& brokerMobileApp.equals(brokerMobile))
			Assert.assertTrue("Broker Details reflected", true);
		else
			Assert.assertTrue("Broker Details not reflected", false);
		
//		Select select = new Select(getDriver().findElement(By.xpath("//select[@name,'chubbContact']")));
//		WebElement option = select.getFirstSelectedOption();
//		String defaultItem = option.getText();
//		
//		if(getDriver().findElement(By.xpath("//span[contains(@data-bind,'optionsText: 'email'')]")).getText().equals("aus.pld@chubb.com")
//			&& getDriver().findElement(By.xpath("//span[contains(@data-bind,'optionsText: 'phoneNumber'')]")).getText().equals("0392425111")
//			&& defaultItem.equals("Personal Lines Team"))
//		
//			Assert.assertTrue("Chubb Contact Details reflected", true);
//		else
//			Assert.assertTrue("Chubb Contact Details not reflected", false);			
		
	}	
}
