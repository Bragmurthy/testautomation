package au.com.chubb.evolutionshowcase.pages.stepDefinitions.policyAdmin;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote.riskLocation_calculatePremium_Steps;
import au.com.chubb.evolutionshowcase.pages.serenitySteps.policyAdmin.viewDuplicateClientsSteps;
import au.com.chubb.evolutionshowcase.pages.serenitySteps.policyAdmin.standardEndorsement_Steps;
import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import cucumber.api.java.en.And;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Steps;

public class viewDuplicateClient_SD extends PageObject {

	@Steps
	viewDuplicateClientsSteps viewDuplicateClientsSteps;
	DynamicSeleniumFunctions dynamicseleniumfunctions;
	riskLocation_calculatePremium_Steps  calculatePremium;

    @And("^select possible duplicate client$")
	public void selectPossibleDuplicateClient() throws Throwable {
    	viewDuplicateClientsSteps.selectPossibleDuplicateClient();
	}
    
    @And("^select existing client$")
	public void selectExistingClient() throws Throwable {
    	viewDuplicateClientsSteps.selectExistingClient();
	}
}
