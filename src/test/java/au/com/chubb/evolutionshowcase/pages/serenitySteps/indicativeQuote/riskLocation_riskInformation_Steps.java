package au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class riskLocation_riskInformation_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public riskLocation_riskInformation_Steps() throws IOException {
		/*
		 * OR = new Properties(); FileInputStream fo = new FileInputStream(root +
		 * "/ObjectRepository/landingPage.properties"); OR.load(fo);
		 */
	}

	@Step
	public void verify_default_value(String field_name, String location_identifier) throws InterruptedException {

		if (location_identifier.equalsIgnoreCase("1"))
		{
			Thread.sleep(2000);	
			WebElement AddNextRiskLoaction=getDriver().findElement(By.xpath("//*[contains(text(),('Add Next Risk Location'))]"));
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", AddNextRiskLoaction);
            Thread.sleep(2000);
			getDriver().findElement(By.xpath("//*[contains(text(),('Add Next Risk Location'))]")).click();
            Thread.sleep(2000);

		}
		
		
		
		int locationIdentifier = Integer.parseInt(location_identifier);
		if (field_name.equalsIgnoreCase("Property Ownership")) {

			WebElement PropertyOwnership = getDriver()
					.findElement(By.id("property_" + locationIdentifier + "_OwnershipType"));
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", PropertyOwnership);
			Thread.sleep(2000);

			String Property_Ownership_APP = getDriver()
					.findElement(By.id("property_" + locationIdentifier + "_OwnershipType")).getAttribute("value");
			System.out.println("Property_Ownership_APP-- "+Property_Ownership_APP);
			if (Property_Ownership_APP.isEmpty()) {
				Assert.assertTrue("Property Onweship default value for " + locationIdentifier + " is Owner ", true);

			} else {
				Assert.assertTrue("Property Onweship default value for " + locationIdentifier + " is Owner ", false);

			}

		} else if (field_name.equalsIgnoreCase("Type of Residence"))

		{
			WebElement TypeOfResidence = getDriver()
					.findElement(By.id("property_" + locationIdentifier + "_ResidenceType"));
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", TypeOfResidence);
			Thread.sleep(2000);

			String Type_Of_Residence_APP = getDriver()
					.findElement(By.id("property_" + locationIdentifier + "_ResidenceType")).getAttribute("value");
		System.out.println("Type_Of_Residence_APP-- "+Type_Of_Residence_APP);
		
			if (Type_Of_Residence_APP.isEmpty()) {
				Assert.assertTrue("Type of Residence default value for " + locationIdentifier + " is Primary Residence ", true);

			} else {
				Assert.assertTrue("Type of Residence default value for " + locationIdentifier + " is Primary Residence ", false);

			} 
		} else if (field_name.equalsIgnoreCase("Property Type"))

		{
			WebElement PropertyType = getDriver()
					.findElement(By.id("property_" + locationIdentifier + "_PropertyType"));
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", PropertyType);
			Thread.sleep(2000);

			String Property_Type_APP = getDriver()
					.findElement(By.id("property_" + locationIdentifier + "_PropertyType")).getAttribute("value");
			System.out.println("Property_Type_APP-- "+Property_Type_APP);

			if (Property_Type_APP.isEmpty()) {
				Assert.assertTrue("Property Type default value for " + locationIdentifier + " is House ", true);

			} else {
				Assert.assertTrue("Property Type default value for " + locationIdentifier + " is House ", false);

			} 
		} else if (field_name.equalsIgnoreCase("Burglar Alarm"))

		{
			WebElement BurglarAlarm = getDriver()
					.findElement(By.id("property_" + locationIdentifier + "_BurglarAlarmDetection"));
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", BurglarAlarm);
			Thread.sleep(2000);

			String Burglar_Alarm_APP = getDriver()
					.findElement(By.id("property_" + locationIdentifier + "_BurglarAlarmDetection")).getAttribute("value");
			System.out.println("Burglar_Alarm_APP-- "+Burglar_Alarm_APP);

			if (Burglar_Alarm_APP.isEmpty()) {
				Assert.assertTrue("Burglar Alarm default value for " + locationIdentifier + " is None ", true);

			} else {
				Assert.assertTrue("Burglar Alarm default value for " + locationIdentifier + " is None ", false);

			} 
		} else if (field_name.equalsIgnoreCase("External Construction"))

		{

			WebElement ExternalConstruction = getDriver()
					.findElement(By.id("property_" + locationIdentifier + "_ExternalConstruction"));
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);",
					ExternalConstruction);
			Thread.sleep(2000);

			String External_Construction_APP = getDriver()
					.findElement(By.id("property_" + locationIdentifier + "_ExternalConstruction")).getAttribute("value");
			System.out.println("External_Construction_APP-- "+External_Construction_APP);

			if (External_Construction_APP.isEmpty()) {
				Assert.assertTrue("External Construction default value for " + locationIdentifier + " is empty ", true);

			} else {
				Assert.assertTrue("Burglar Alarm default value for " + locationIdentifier + " is empty ", false);

			} 

		} else {

		}
	}

	@Step
	public void verify_other_description(String location_identifier) throws InterruptedException {

		if (location_identifier.equalsIgnoreCase("1"))
		{
			Thread.sleep(2000);	
			WebElement AddNextRiskLoaction=getDriver().findElement(By.xpath("//*[contains(text(),('Add Next Risk Location'))]"));
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", AddNextRiskLoaction);
            Thread.sleep(2000);
			getDriver().findElement(By.xpath("//*[contains(text(),('Add Next Risk Location'))]")).click();
            Thread.sleep(4000);

		}
		
		int locationIdentifier = Integer.parseInt(location_identifier);
		
		WebElement TypeOfResidence = getDriver()
				.findElement(By.id("property_" + locationIdentifier + "_ResidenceType"));
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", TypeOfResidence);
		Thread.sleep(2000);
		Select s = new Select(TypeOfResidence);
		s.selectByVisibleText("Other");
		
		Thread.sleep(2000);
		
		boolean element_present =true;
		
		try
		{
				getDriver().findElement(By.id("property_"+locationIdentifier+"_OtherResidenceTypeText")).sendKeys("sample text");
				Thread.sleep(100);
			
		} catch (Exception e)
		{
			element_present=false;
		}
		
		if (element_present== true)
		{
			Assert.assertTrue("Other Description text box is displayed when Type of Residence Other is selected", true);
		}
		else
		{
			Assert.assertTrue("Other Description text box is displayed when Type of Residence Other is selected", false);

		}
		
		
		
		
		
		
	}

	@Step
	public void verify_risk_information_section(String riskLocation_identifier, DataTable arg1) {

		int locationIdentifier=Integer.parseInt(riskLocation_identifier);
		List<List<String>> data = arg1.raw();
		
		String PropertyOwnership_APP=getDriver().findElement(By.id("property_"+locationIdentifier+"_OwnershipType")).getAttribute("value");
		String TypeOfResidence_APP=getDriver().findElement(By.id("property_"+locationIdentifier+"_ResidenceType")).getAttribute("value");
		String YearOfConstruction_APP=getDriver().findElement(By.id("YearOfConstruction")).getAttribute("value");
		String PropertyType_APP=getDriver().findElement(By.id("property_"+locationIdentifier+"_PropertyType")).getAttribute("value");
		String BurglarAlarm_APP=getDriver().findElement(By.id("property_"+locationIdentifier+"_BurglarAlarmDetection")).getAttribute("value");
		String ExternalConstruction_APP=getDriver().findElement(By.id("property_"+locationIdentifier+"_ExternalConstruction")).getAttribute("value");
	
		
		System.out.println("PropertyOwnership_APP -- " +PropertyOwnership_APP);
		System.out.println("PropertyOwnership_APP -- " +data.get(1).get(1));

		
		System.out.println("TypeOfResidence_APP-- " +TypeOfResidence_APP);
		System.out.println("TypeOfResidence_APP -- " +data.get(2).get(1));

		System.out.println("YearOfConstruction_APP-- " +YearOfConstruction_APP);
		System.out.println("YearOfConstruction_APP -- " +data.get(3).get(1));

		System.out.println("PropertyType_APP -- " +PropertyType_APP);
		System.out.println("PropertyType_APP -- " +data.get(4).get(1));

		System.out.println("BurglarAlarm_APP -- " +BurglarAlarm_APP);
		System.out.println("BurglarAlarm_APP -- " +data.get(5).get(1));

		System.out.println("ExternalConstruction_APP -- " +ExternalConstruction_APP);
		System.out.println("ExternalConstruction_APP -- " +data.get(6).get(1));
	
		if (PropertyOwnership_APP.equalsIgnoreCase(data.get(1).get(1)) &&
				TypeOfResidence_APP.equalsIgnoreCase(data.get(2).get(1)) &&
				YearOfConstruction_APP.equalsIgnoreCase(data.get(3).get(1)) &&
				PropertyType_APP.equalsIgnoreCase(data.get(4).get(1)) &&
				BurglarAlarm_APP.equalsIgnoreCase(data.get(5).get(1)) &&
				ExternalConstruction_APP.equalsIgnoreCase(data.get(6).get(1)))
																{
			                                                            Assert.assertTrue("Risk Location " +riskLocation_identifier+"details are auto populated after Edit Quote", true);
																} else {
																	Assert.assertTrue("Risk Location " +riskLocation_identifier+"details are not auto populated after Edit Quote", false);

																}
											
				
}

	
	@Step
	public void verify_risk_location_error_message(String errorMessage) {
		String errorMessage_APP=getDriver().findElement(By.xpath("//div[contains(@data-bind,'fadeVisible: !__isValidPropertyCombo()')]")).getText();
		
		System.out.println("Error Message App " +errorMessage_APP);
		if (errorMessage_APP.equalsIgnoreCase(errorMessage))
			
		{
			Assert.assertTrue(errorMessage +"is doisplayed for invalid combination of Risk location ", true);
		}
		else
		{
			Assert.assertTrue(errorMessage +"is doisplayed for invalid combination of Risk location ", false);

		}
		
	}

}