package au.com.chubb.evolutionshowcase.pages.serenitySteps.policyAdmin;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class cancelEndorsementSteps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;
	DynamicSeleniumFunctions functions;

	public cancelEndorsementSteps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);

	}
	@Step
	public void verifyOptionsInCancellationReason() {
		
		String[] expectedOptions={"","Cover no longer required","Client self-insured","Broker placed elsewhere - Premium","Broker placed elsewhere - Terms","Other"};
		List<WebElement> options =getDriver().findElements(By.xpath("//select[@id='endorse-cancelreason']/option"));
		int count=0;
		
		for (WebElement we : options)
		{
			for (int index=0; index<expectedOptions.length; index++)
			{
				if(we.getText().equals(expectedOptions[index]))
				{
					count++;
				}
			}
		}
		
		if (count==6)
		{
			Assert.assertTrue("All Fields in Enodorsement light box are displayed", true);
		}else
		{
			Assert.assertTrue("All Fields in Enodorsement light box are not displayed", false);

		}
	}
	
@Step
	public void verifyfieldsInCancellationEndorsementLightbox() {
		new Select(getDriver().findElement(By.xpath("//select[@id='broker-std-endorse-type']"))).selectByVisibleText("Cancellation");
		if (getDriver().findElement(By.xpath("//h3[contains(text(),'Endorse Policy:')]")).isDisplayed()
				&&getDriver().findElement(By.xpath("//label[contains(text(),'Effective Date:')]")).isDisplayed()
				&&getDriver().findElement(By.xpath("//span[contains(@data-bind,'support.ModalResult')]//button[contains(text(),'Cancel')]")).isDisplayed()){
			Assert.assertTrue("All Fields in Enodorsement light box are displayed", true);
		}else
		Assert.assertTrue("All Fields in Enodorsement light box are not displayed", false);
	}

	@Step 
	public void verifyManualReferral() throws InterruptedException{
		String manualReferral = getDriver().findElement(By.tagName("h3")).getText();
		if(manualReferral.contains("Please submit Manual Referral for Underwriter review."))
		{
			Assert.assertTrue("Manual Refferal text displayed", true);
		}
		else
		{
			Assert.assertTrue("Manual Refferal text displayed", true);
		}
		
		if(!getDriver().findElement(By.id("btnIssueQuote")).isEnabled())
		{
			Assert.assertTrue("Button is disabled", true);
		}
		
		getDriver().findElement(By.xpath("//button[contains(@data-bind,'saveManualReferral')]")).click();
		Thread.sleep(4000);
		getDriver().findElement(By.xpath("//textarea[contains(@data-bind,'referralNotes')]")).sendKeys("Testing");
		getDriver().findElement(By.xpath("//button[contains(text(),'Confirm')]")).click();
		Thread.sleep(10000);
	}


	@Step
	public void createCancellationEndorsement() throws InterruptedException {
		new Select(getDriver().findElement(By.xpath("//select[@id='endorse-cancelreason']"))).selectByVisibleText("Client self-insured");
		getDriver().findElement(By.xpath("//button[contains(text(),'Endorse')]")).click();
		Thread.sleep(30000);
	}
}

		

	




		
