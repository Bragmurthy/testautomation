package au.com.chubb.evolutionshowcase.pages.stepDefinitions.closing;


import au.com.chubb.evolutionshowcase.pages.serenitySteps.closing.closingSteps;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

public class closingStepDefinition {

	@Steps
	closingSteps closingSteps;
	
	@And("^close the quote as UW \"([^\"]*)\"$")
	public void close_quote_as_uw(String refNumber) throws Throwable{
		closingSteps.close_quote_as_uw(refNumber);
	}
}