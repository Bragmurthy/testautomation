package au.com.chubb.evolutionshowcase.pages.serenitySteps.policyAdmin;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;



@RunWith(SerenityRunner.class)
public class standardEndorsement_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;
	DynamicSeleniumFunctions functions;

	public standardEndorsement_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);

	}
	@Step
	public void verifyOptionsInEndorsementType() {
			String expectedOption="Standard Endorsement";
			String actualOptions="";
		
		List<WebElement> options =getDriver().findElements(By.xpath("//select[@id='broker-std-endorse-type']/option"));
		
		for(int i=0;i<options.size();i++)
		{
			actualOptions=actualOptions+options.get(i).getText();
		}
		if (expectedOption.equals(actualOptions))
		{
			Assert.assertTrue("All Fields in Enodorsement light box are displayed", true);
		}else
		{
			Assert.assertTrue("All Fields in Enodorsement light box are not displayed", false);

		}
	}
	
@Step
	public void verifyfieldsInEndorsementLightbox() {
		 boolean fieldDisplayed=true;
		 boolean fieldDisplayedRennovations=true;
		try
		{
		getDriver().findElement(By.xpath("//label[contains(text(),'Appraisal Endorsement')]")).isDisplayed();
		}catch (Exception e)
		{
			fieldDisplayed=false;
		}
		
		if (!fieldDisplayed)
		{
			Assert.assertTrue("Field Appraisal Endorsement not displayed", true);
		}else
		{
			Assert.assertTrue("Field Appraisal Endorsement not displayed", false);

		}
		try
		{
		getDriver().findElement(By.xpath("//label[contains(text(),'Rennovations')]")).isDisplayed();
		}catch (Exception e)
		{
			fieldDisplayedRennovations=false;
		}
		
		
		if (!fieldDisplayedRennovations)
		{
			Assert.assertTrue("Field Rennovations not displayed", true);
		}else
		{
			Assert.assertTrue("Field Rennovations not displayed", false);

		}
		
		new Select(getDriver().findElement(By.xpath("//select[@id='broker-std-endorse-type']"))).selectByVisibleText("Standard Endorsement");
		if (getDriver().findElement(By.xpath("//h3[contains(text(),'Endorse Policy:')]")).isDisplayed()
				&&getDriver().findElement(By.xpath("//input[@id='endorse-description']")).isDisplayed()
				&&getDriver().findElement(By.xpath("//input[@id='endorse-effectivedate']")).isDisplayed()
				&&getDriver().findElement(By.xpath("//span[contains(@data-bind,'support.ModalResult')]//button[contains(text(),'Cancel')]")).isDisplayed()){
			Assert.assertTrue("All Fields in Enodorsement light box are displayed", true);
		}else
		Assert.assertTrue("All Fields in Enodorsement light box are not displayed", false);
	}

	@Step
	public void createEndorsement() throws InterruptedException {
		new Select(getDriver().findElement(By.xpath("//select[@id='broker-std-endorse-type']"))).selectByVisibleText("Standard Endorsement");
		getDriver().findElement(By.xpath("//input[@id='endorse-description']")).click();
		getDriver().findElement(By.xpath("//input[@id='endorse-description']")).sendKeys("Endorsement Description");
		getDriver().findElement(By.xpath("//button[contains(text(),'Endorse')]")).click();
		Thread.sleep(30000);
	}
	public void selectEndorsementType(String endType) {
		// TODO Auto-generated method stub
		new Select(getDriver().findElement(By.xpath("//select[@id='broker-std-endorse-type']"))).selectByVisibleText("Standard Endorsement");
	}
	public void VerifyCheckBoxesInEndorsementPopUp(String appraisalCB, String renovationCB) {
		// TODO Auto-generated method stub
		try
		{
			if(getDriver().findElement(By.xpath("//input[contains(text(),('" + appraisalCB + "'))]")).isDisplayed())
			{
				Assert.assertTrue("Appraisal Endorsement check box is dipslayed", true);
			}
			else
			{
				Assert.assertTrue("Appraisal Endorsement check box is not dipslayed", false);
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		try
		{
			if(getDriver().findElement(By.xpath("//input[contains(text(),('" + renovationCB + "'))]")).isDisplayed())
			{
				Assert.assertTrue("Renovations check box is dipslayed", true);
			
			}
			else
			{
				Assert.assertTrue("Renovations check box is not dipslayed",false);
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	public void SelectCheckBoxInEndorsementPopUp(String endChkBox) {
		// TODO Auto-generated method stub
		try{
		WebElement checkbox=getDriver().findElement(By.xpath("//input[contains(text(),'"+endChkBox+"')]"));
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", checkbox);
		checkbox.click();		
		}
		catch(Exception e)
		{
			System.out.println("Check box in not clickable : "+endChkBox);
		}
	}
	
	@Step
	public void createTermchangeEndorsement() throws InterruptedException{
		new Select(getDriver().findElement(By.xpath("//select[@id='broker-tc-endorse-type']"))).selectByVisibleText("Term Change");
		
		getDriver().findElement(By.xpath("//button[contains(text(),'Endorse')]")).click();
		Thread.sleep(30000);
	}
	
	@Step
	public void verifyCurrentDateFields() throws InterruptedException, IOException{

		new Select(getDriver().findElement(By.xpath("//select[@id='broker-tc-endorse-type']"))).selectByVisibleText("Term Change");		
//		functions.verifyFieldNonEditableForDates("CurrentStartDate");
//		functions.verifyFieldNonEditableForDates("CurrentExpiryDate");
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		cal.add(Calendar.DATE, 200);
		System.out.println("date was "+dateFormat.format(cal.getTime()));  
		String NewExpiry_App = getDriver().findElement(By.id("endorse-newexpirydate")).getAttribute("value");
		if(NewExpiry_App.equals(dateFormat.format(cal.getTime())))
		{
			Assert.assertTrue("Start Defaulted from Sunrise", true);
		}
		else
		{
			Assert.assertTrue("Start Defaulted from Sunrise", false);
		}
		getDriver().findElement(By.xpath("//button[contains(text(),'Endorse')]")).click();
		Thread.sleep(50000);
	}
	
	@Step
	public void CancellationEndorsement() throws InterruptedException, IOException{

		new Select(getDriver().findElement(By.xpath("//select[@id='broker-cancel-endorse-type']"))).selectByVisibleText("Cancellation");
		Thread.sleep(4000);
		new Select(getDriver().findElement(By.xpath("//select[@id='endorse-cancelreason']"))).selectByVisibleText("Cover no longer required");
		getDriver().findElement(By.xpath("//button[contains(text(),'Endorse')]")).click();
		Thread.sleep(50000);
	}
	
	
	@Step
	public void validatePremiumSummaryPage() throws InterruptedException{
		boolean finish = getDriver().findElement(By.id("btnFinish")).isEnabled();
		boolean saveExit = getDriver().findElement(By.id("btnSaveExit")).isDisplayed();
		if(!finish && saveExit)
		{
			Assert.assertTrue("Controls are in Expected State", true);
		}
		else
		{
			Assert.assertTrue("Controls are in Expected State", false);
		}
		
		getDriver().findElement(By.xpath("//button[contains(text(),'Submit Referral')]")).click();
		getDriver().findElement(By.xpath("//textarea[contains(@data-bind,'referralNotes')]")).sendKeys("Testing");
		getDriver().findElement(By.xpath("//button[contains(text(),'Confirm')]")).click();
		Thread.sleep(50000);
	}
	
	@Step
	public void validatePremiumSummaryPageForCancellation() throws InterruptedException{
		WebElement elmnt =getDriver().findElement(By.xpath("//a[contains(text(),('Premium Summary'))]"));  
		Actions builder = new Actions(getDriver());   
		builder.moveToElement(elmnt, +0, +0).click().build().perform();
		WebDriverWait wait=new WebDriverWait(getDriver(), 300);
		Thread.sleep(10000);
		boolean finish = getDriver().findElement(By.id("btnIssueQuote")).isEnabled();
		boolean saveExit = getDriver().findElement(By.id("btnSaveExit")).isDisplayed();
		if(!finish && saveExit)
		{
			Assert.assertTrue("Controls are in Expected State", true);
		}
		else
		{
			Assert.assertTrue("Controls are in Expected State", false);
		}
		
		getDriver().findElement(By.xpath("//button[contains(text(),'Submit Referral')]")).click();
		getDriver().findElement(By.xpath("//textarea[contains(@data-bind,'referralNotes')]")).sendKeys("Testing");
		getDriver().findElement(By.xpath("//button[contains(text(),'Confirm')]")).click();
		Thread.sleep(50000);
	}
	
	@Step
	public void bookingCancelled() throws InterruptedException{
		//<span class="evo-statusbar">Status: Newline In Force</span>
		
		String policyStatus_App= getDriver().findElement(By.xpath("//span[contains(text(), 'Status')]")).getText();
		
		if(policyStatus_App.contains("Cancelled"))
		{
			Assert.assertTrue("Cancelled Successfull", true);
		}
		else
		{
			Assert.assertTrue("Cancellation not	 Successfull", false);
		}		
	}
	
	@Step
	public void bookingSuccess() throws InterruptedException{
		//<span class="evo-statusbar">Status: Newline In Force</span>
		
		String policyStatus_App= getDriver().findElement(By.xpath("//span[contains(text(), 'Status')]")).getText();
		
		if(policyStatus_App.contains("Newline In Force"))
		{
			Assert.assertTrue("Booking Successfull", true);
		}
		else
		{
			Assert.assertTrue("Booking not Successfull", false);
		}		
	}
	
	@Step
	public void get_Alerttext_AttachmentDate() 
	{
	
//		Screen expectedpopup= new Screen();
//		if (expectedpopup.exists(root +"/src/test/java/au/com/chubb/evolutionshowcase/Images/AttachmentPopup.PNG")!=null)
//		{
//			Assert.assertTrue("Expected message", true);
//		}
//		else
//		{
//			Assert.assertTrue("Wrong Message", false);
//		}		
//		
	}
	public void addVACinMTA() {
		// TODO Auto-generated method stub
		try
		{
		getDriver().findElement(By.xpath("//span[contains(text(),'Valuable Articles')]")).click();
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//button[contains(text(),'Add Coverage')]")).click();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		try
		{
		new Select(getDriver().findElement(By.id("vacEdit_Category"))).selectByVisibleText("Furs");
		Thread.sleep(5000);
		getDriver().findElement(By.id("vacEdit_BlanketSum")).sendKeys("15000");
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		
	}
}

		

	




		
