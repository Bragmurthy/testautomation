package au.com.chubb.evolutionshowcase.pages.objectModel.eBusinessPortal;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;

public class eBusinessPortalAddNewQuotePage extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;
	String sheetName = "QuoteData";
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");


	public eBusinessPortalAddNewQuotePage() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/eBusinessPortal.properties");
		OR.load(fo);

	}

	public void click_add_new_quote() throws InterruptedException {

		loop:
			for (int i=0;i<=100;i++)
			{
				
					getDriver().findElement(By.xpath("//a[contains(text(),'Add new quote')]")).click();
					Thread.sleep(10000);
					try
					{
					if(getDriver().findElement(By.name("txtClientId")).isDisplayed()){
						break loop;	
					};
					
				}catch (Exception e)
				{
					getDriver().findElement(By.xpath("//a[contains(text(),'Add new quote')]")).click();
					Thread.sleep(10000);
				}

			}
	}

	public void click_find_customer() throws InterruptedException {
		loop:
				for (int i=0;i<=100;i++)
				{
					try
					{
						getDriver().findElement(By.xpath("//a[contains(text(),'Find quote')]")).click();
						break loop;
					}catch (Exception e)
					{
						Thread.sleep(1000);
					}
				}
		}
	public void click_find_quote() throws InterruptedException {

		loop:
			for (int i=0;i<=100;i++)
			{
				try
				{
					getDriver().findElement(By.xpath(OR.getProperty("Find_Quote_Xpath"))).click();
					break loop;
				}catch (Exception e)
				{
					Thread.sleep(1000);
				}

			}
	Thread.sleep(5000);


	}
	
	public void enter_new_quote_details(DataTable arg1) throws InterruptedException {

		List<List<String>> data = arg1.raw();
		getDriver().findElement(By.name("txtClientId")).sendKeys(data.get(1).get(1));
		new Select(getDriver().findElement(By.name("selProdId"))).selectByVisibleText(data.get(2).get(1));
		getDriver().findElement(By.name("txtInsuredId")).clear();
		Thread.sleep(1000);
		getDriver().findElement(By.name("txtInsuredId")).sendKeys(data.get(3).get(1));
		XLS.setCellData(sheetName, "InsuredId", 2,data.get(3).get(1));

		getDriver().findElement(By.name("txtStartDate")).clear();
		getDriver().findElement(By.name("txtStartDate")).sendKeys(data.get(4).get(1));
		XLS.setCellData(sheetName, "EffectiveDate", 2,data.get(4).get(1));

		getDriver().findElement(By.name("txtStartDate")).sendKeys(Keys.TAB);
		getDriver().findElement(By.name("txtEndDate")).clear();
		getDriver().findElement(By.name("txtEndDate")).sendKeys(data.get(5).get(1));
		XLS.setCellData(sheetName, "ExpiryDate", 2,data.get(5).get(1));

		getDriver().findElement(By.name("txtEndDate")).sendKeys(Keys.TAB);
		/*
		new Select(getDriver().findElement(By.name("selBranchId"))).selectByVisibleText(data.get(6).get(1));
		 */
	}
	
	public void enter_policy_start_date_details(DataTable arg1) throws InterruptedException {
		List<List<String>> data = arg1.raw();
		String date = data.get(1).get(1);
	getDriver().findElement(By.name("txtAttachDate")).sendKeys(date);
	}

	public void click_add_risk_details() throws InterruptedException {

		//getDriver().findElement(By.xpath(OR.getProperty("Add_Risk_Details_Button_Xpath"))).click();
		WebElement elmnt=getDriver().findElement(By.xpath("//input[@value='Add Risk Details']"));
		
		JavascriptExecutor js = (JavascriptExecutor) getDriver();  
		js.executeScript("arguments[0].click();", elmnt);
		WebDriverWait wait=new WebDriverWait(getDriver(),80);
//		wait.until(ExpectedConditions.presenceOfElementLocated((By.id("btnFull"))));
		Thread.sleep(10000);
	}
	
	public void click_edit_risk_details() throws InterruptedException {

		//getDriver().findElement(By.xpath(OR.getProperty("Add_Risk_Details_Button_Xpath"))).click();
		WebElement elmnt=getDriver().findElement(By.xpath("//input[@value='Edit Risk Details']"));
		JavascriptExecutor js = (JavascriptExecutor) getDriver();  
		js.executeScript("arguments[0].click();", elmnt);
		Thread.sleep(40000);

	}
	
	public void click_view_risk_details() throws InterruptedException {

		//getDriver().findElement(By.xpath(OR.getProperty("Add_Risk_Details_Button_Xpath"))).click();
		getDriver().findElement(By.xpath("//input[@value='View Risk Details']")).click();
		Thread.sleep(40000);

	}
	

	public void click_new_business() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(By.xpath(OR.getProperty("New_Business_Xpath"))).click();

	}
@Step
	public void verifyQuoteStatus(String quoteStatus) {
		 if(getDriver().findElement(By.xpath("//td[input[@name='selProcStatus']]")).getText().equals(quoteStatus))
		   {
			 Assert.assertTrue("Quote Status is displayed as expected"+quoteStatus, true);  
		   }
		 else {
				Assert.assertTrue("Quote Status is not displayed as expected "+quoteStatus, false);

			}
	}
	@Step
	public String verify_transaction_channel() {
	WebElement element = getDriver().findElement(By.xpath("//*[@id='policy-search-result_wrapper']/div[2]/div/table/tbody/tr/td[2]"));
	String test = element.getText();
	return test;
}
}