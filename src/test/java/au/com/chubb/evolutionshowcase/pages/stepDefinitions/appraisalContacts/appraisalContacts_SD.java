package au.com.chubb.evolutionshowcase.pages.stepDefinitions.appraisalContacts;


import au.com.chubb.evolutionshowcase.pages.serenitySteps.appraisalContacts.appraisalContacts_Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;


public class appraisalContacts_SD {

	appraisalContacts_Steps appraisalContactss;
	@And("^select checkbox for Adding Appraisal Contact$")
	public void select_checkbox() throws Throwable {
		appraisalContactss.select_checkbox();
	}
	
	@And("^verify fields in Add Apprisal Contact page$")
	public void verify_Fields() throws Throwable {
		appraisalContactss.verify_Fields();
	}
	
	@And("^Add \"([^\"]*)\" Appraisal contact for location \"([^\"]*)\" as per below data$")
	public void add_appraisal_contact(String appraisals_count, String Location_Identifier, DataTable arg1) throws Throwable {
		appraisalContactss.add_AppraisalContact(appraisals_count, Location_Identifier, arg1);
	}
	@And("^verify \"([^\"]*)\" Appraisal Contact is saved for testflow \"([^\"]*)\" as per below data$")
	public void verify_AppraisalContact(String interested_party_count, String Location_Identifier, DataTable arg1) throws Throwable {
		appraisalContactss.verify_AppraisalContact(interested_party_count,Location_Identifier,arg1);
	}
	
	@And("^verify required field error meesages in Appraisal Contacts page$")
	public void verify_RequiredFiledErrorMessage_AppraisalContactPage(){
		appraisalContactss.verify_RequiredFiledErrorMessage_AppraisalContactPage();
	}

}
