package au.com.chubb.evolutionshowcase.pages.serenitySteps.policyAdmin;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.fluentlenium.core.search.Search;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import au.com.chubb.evolutionshowcase.pages.objectModel.policyAdmin.policyAdminWelcomePage;
import au.com.chubb.evolutionshowcase.utilities.CommonFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class workManagerSteps extends PageObject {

	policyAdminWelcomePage policyadminwelcomepage;
	CommonFunctions common_functions;
	String root = System.getProperty("user.dir");
	public static Properties OR = null;
	
	@Step
	public void approveQuote(String test_data) throws InterruptedException, IOException {
		getDriver().get("http://apevodweb121:1111/WorkManager");
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//a[contains(text(),'Global Search')]")).click();	
		Thread.sleep(10000);
		
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		System.out.println("control reached enter risk location details" );

		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

				if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
					rowNum = row;
					System.out.println("test data --" + test_data);
					System.out.println("try to come out of loop "+rowNum);
					break innerloop;
				}
			}

		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyAdmin.properties");
		OR.load(fo);

		getDriver().findElement(By.xpath("//input[@id='SearchPolicyNum']")).sendKeys(XLS.getCellData(sheetName, "Quote_Reference_Number", rowNum));
		
    	getDriver().findElement(By.xpath("//button[@id='Search']")).click();
    	
    	Thread.sleep(10000);
    	getDriver().findElement(By.xpath("//td[contains(text(),'Referral')]")).click();
    	Thread.sleep(5000);
		List<WebElement> items=getDriver().findElements(By.xpath("//select[contains(@name,'WorkItem')]"));
		XLS.setCellData(sheetName, "ReferalCount", rowNum,Integer.toString(items.size()));
		for(int i=0;i<=items.size()-2;i++)
		{
			new Select(getDriver().findElement(By.id("ddl"+i+""))).selectByVisibleText("Approve");
		}
		Thread.sleep(5000);

		WebElement saveProgress=getDriver().findElement(By.xpath("//input[@id='btnSave']"));
		
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", saveProgress);
		saveProgress.click();
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//input[@id='btnApply']")).click();
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//input[@id='btnComplete']")).click();
		Thread.sleep(20000);
	}
	
	@Step
	public void approveQuoteinSIT(String test_data) throws InterruptedException, IOException {
		getDriver().get("http://apregevwebqa121:1111/WorkManager");
		Thread.sleep(15000);
		WebDriverWait waits = new WebDriverWait(getDriver(), 30);		
		waits.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Global Search')]")));
		WebElement search=getDriver().findElement(By.xpath("//a[contains(text(),'Global Search')]"));
		JavascriptExecutor js = (JavascriptExecutor) getDriver();  
		js.executeScript("arguments[0].click();", search);
		Thread.sleep(15000);
		
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		System.out.println("control reached enter risk location details" );

		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

				if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
					rowNum = row;
					System.out.println("test data --" + test_data);
					System.out.println("try to come out of loop "+rowNum);
					break innerloop;
				}
			}

		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyAdmin.properties");
		OR.load(fo);
		WebDriverWait wait = new WebDriverWait(getDriver(), 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='SearchPolicyNum']")));

		getDriver().findElement(By.xpath("//input[@id='SearchPolicyNum']")).sendKeys(XLS.getCellData(sheetName, "Quote_Reference_Number", rowNum));
		
    	getDriver().findElement(By.xpath("//button[@id='Search']")).click();
    	
    	Thread.sleep(10000);
    	getDriver().findElement(By.xpath("//td[contains(text(),'Referral')]")).click();
    	Thread.sleep(5000);
		List<WebElement> items=getDriver().findElements(By.xpath("//select[contains(@name,'WorkItem')]"));
		XLS.setCellData(sheetName, "ReferalCount", rowNum,Integer.toString(items.size()));
		for(int i=0;i<=items.size()-2;i++)
		{
			new Select(getDriver().findElement(By.id("ddl"+i+""))).selectByVisibleText("Approve");
		}
		Thread.sleep(5000);

		WebElement saveProgress=getDriver().findElement(By.xpath("//input[@id='btnSave']"));
		
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", saveProgress);
		saveProgress.click();
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//input[@id='btnApply']")).click();
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//input[@id='btnComplete']")).click();
		Thread.sleep(20000);
	}
	
	@Step
	public void declineQuote(String test_data) throws InterruptedException, IOException {
		getDriver().get("http://apevodweb121:1111/WorkManager");
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//a[contains(text(),'Global Search')]")).click();	
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		System.out.println("control reached enter risk location details" );

		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

				if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
					rowNum = row;
					System.out.println("test data --" + test_data);
					System.out.println("try to come out of loop "+rowNum);
					break innerloop;
				}
			}

		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyAdmin.properties");
		OR.load(fo);

		getDriver().findElement(By.xpath("//input[@id='SearchPolicyNum']")).sendKeys(XLS.getCellData(sheetName, "Quote_Reference_Number", rowNum));
		getDriver().findElement(By.xpath("//button[@id='Search']")).click();
		
		Thread.sleep(5000);
    	getDriver().findElement(By.xpath("//td[contains(text(),'Referral')]")).click();
    	Thread.sleep(5000);
		
		List<WebElement> items=getDriver().findElements(By.xpath("//select[contains(@name,'WorkItem')]"));
		for(int i=0;i<=items.size()-3;i++)
		{
			new Select(getDriver().findElement(By.id("ddl"+i+""))).selectByVisibleText("Approve");
		}
		int declineItem=items.size()-2;
		new Select(getDriver().findElement(By.id("ddl"+declineItem+""))).selectByVisibleText("Decline");
		
		Thread.sleep(5000);
		WebElement saveProgress=getDriver().findElement(By.xpath("//input[@id='btnSave']"));
		
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", saveProgress);
		saveProgress.click();
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//input[@id='btnApply']")).click();
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//input[@id='btnComplete']")).click();
		Thread.sleep(20000);
	}
	
	@Step
	public void declineQuoteinSIT(String test_data) throws InterruptedException, IOException {
		getDriver().get("http://apregevwebqa121:1111/WorkManager");
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//a[contains(text(),'Global Search')]")).click();	
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		System.out.println("control reached enter risk location details" );

		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

				if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
					rowNum = row;
					System.out.println("test data --" + test_data);
					System.out.println("try to come out of loop "+rowNum);
					break innerloop;
				}
			}

		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyAdmin.properties");
		OR.load(fo);

		getDriver().findElement(By.xpath("//input[@id='SearchPolicyNum']")).sendKeys(XLS.getCellData(sheetName, "Quote_Reference_Number", rowNum));
		getDriver().findElement(By.xpath("//button[@id='Search']")).click();
		
		Thread.sleep(5000);
    	getDriver().findElement(By.xpath("//td[contains(text(),'Referral')]")).click();
    	Thread.sleep(5000);
		
		List<WebElement> items=getDriver().findElements(By.xpath("//select[contains(@name,'WorkItem')]"));
		for(int i=0;i<=items.size()-3;i++)
		{
			new Select(getDriver().findElement(By.id("ddl"+i+""))).selectByVisibleText("Approve");
		}
		int declineItem=items.size()-2;
		new Select(getDriver().findElement(By.id("ddl"+declineItem+""))).selectByVisibleText("Decline");
		
		Thread.sleep(5000);
		WebElement saveProgress=getDriver().findElement(By.xpath("//input[@id='btnSave']"));
		
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", saveProgress);
		saveProgress.click();
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//input[@id='btnApply']")).click();
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//input[@id='btnComplete']")).click();
		Thread.sleep(20000);
	}
@Step
	public void verifyRerunRefrals(String test_data) throws InterruptedException, IOException {
    getDriver().get("http://apregevwebqa121:1111/WorkManager");	
	getDriver().findElement(By.xpath("//a[contains(text(),'Global Search')]")).click();	
	Thread.sleep(10000);
	
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
	String sheetName = "IndicativeQuoteRiskLocation";
	Thread.sleep(1000);
	System.out.println("control reached enter risk location details" );

	int Row_Count = XLS.getRowCount(sheetName);
	int rowNum = 0;

	innerloop: 
		for (int row = 2; row <= Row_Count; row++) {

			if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				System.out.println("test data --" + test_data);
				System.out.println("try to come out of loop "+rowNum);
				break innerloop;
			}
		}
	getDriver().findElement(By.xpath("//input[@id='SearchPolicyNum']")).sendKeys(XLS.getCellData(sheetName, "Quote_Reference_Number", rowNum));
	getDriver().findElement(By.xpath("//button[@id='Search']")).click();
	Thread.sleep(5000);
	getDriver().findElement(By.xpath("//td[contains(text(),'Referral')]")).click();
	Thread.sleep(5000);
	List<WebElement> items=getDriver().findElements(By.xpath("//select[contains(@name,'WorkItem')]"));
	int referralCount=items.size();
	String referals=XLS.getCellData(sheetName, "ReferalCount", rowNum);
	int refralsBefore=Integer.parseInt(referals);
	if(referralCount>refralsBefore)
		Assert.assertTrue("Referal Rules has been Re run successfully", true);
	else
		Assert.assertTrue("Referal Rules has not been Re Run", false);
	}

	@Step
	public void searchQuote(String testdata)throws InterruptedException,IOException {
		getDriver().get("http://apregevwebqa121:1111/WorkManager");	
		getDriver().findElement(By.xpath("//a[contains(text(),'Global Search')]")).click();	
		Thread.sleep(10000);
		
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		System.out.println("control reached enter risk location details" );

		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

				if (testdata.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
					rowNum = row;
					System.out.println("test data --" + testdata);
					System.out.println("try to come out of loop "+rowNum);
					break innerloop;
				}
			}

		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyAdmin.properties");
		OR.load(fo);

		getDriver().findElement(By.xpath("//input[@id='SearchPolicyNum']")).sendKeys(XLS.getCellData(sheetName, "Quote_Reference_Number", rowNum));
		getDriver().findElement(By.xpath("//button[@id='Search']")).click();
    	
    	Thread.sleep(10000);
    	WebElement Element = getDriver().findElement(By.xpath("//tr[@class='work-item-row']"));
    	String text= Element.getText();
    	if(text.contains(XLS.getCellData(sheetName, "Quote_Reference_Number", rowNum)) & text.contains("Broker"))
    	{
    		Assert.assertTrue("Transaction Done by Broker", true);    		
    	}
    	else
    	{
    		System.out.println("test Failed ");
			Assert.assertTrue("Transaction not Done by Broker", false);
    	}    	    	
	}
	
	@Step
	public void editInPolicy(String test_data) throws InterruptedException, IOException {
		getDriver().get("http://apregevwebqa121:1111/WorkManager");
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//a[contains(text(),'Global Search')]")).click();	
		Thread.sleep(10000);
		
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		System.out.println("control reached enter risk location details" );

		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

				if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
					rowNum = row;
					System.out.println("test data --" + test_data);
					System.out.println("try to come out of loop "+rowNum);
					break innerloop;
				}
			}

		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyAdmin.properties");
		OR.load(fo);
//		getDriver().findElement(By.xpath("//input[@id='SearchPolicyNum']")).sendKeys(XLS.getCellData(sheetName, "Policy_Number", rowNum));
		


		getDriver().findElement(By.xpath("//input[@id='SearchPolicyNum']")).sendKeys(XLS.getCellData(sheetName, "Quote_Reference_Number", rowNum));
		
    	getDriver().findElement(By.xpath("//button[@id='Search']")).click();
    	
    	Thread.sleep(10000);
    	getDriver().findElement(By.xpath("//td[contains(text(),'Referral')]")).click();

    	Thread.sleep(5000);
    	
    	String WorkManagerwindow= getDriver().getWindowHandle();
    	System.out.println("present window is" +WorkManagerwindow);

    	WebElement editInPolicy=getDriver().findElement(By.xpath("//input[@id='btnEditPolicy']"));
		
    	((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", editInPolicy);
		editInPolicy.click();
		Thread.sleep(5000);
		
		Set handles=getDriver().getWindowHandles();
		System.out.println("present window is" +handles);
		
		for( String policyAdminwindow:getDriver().getWindowHandles())
		{
			getDriver().switchTo().window(policyAdminwindow);
		}
			
	}
	
	@Step
	public void VerifyTransactionChannelandRole(String role , String testdata) throws InterruptedException, IOException {
		getDriver().findElement(By.xpath("//a[contains(text(),'Global Search')]")).click();	
		Thread.sleep(10000);
		
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		System.out.println("control reached enter risk location details" );

		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

				if (testdata.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
					rowNum = row;
					System.out.println("test data --" + testdata);
					System.out.println("try to come out of loop "+rowNum);
					break innerloop;
				}
			}

		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyAdmin.properties");
		OR.load(fo);

		getDriver().findElement(By.xpath("//input[@id='SearchPolicyNum']")).sendKeys(XLS.getCellData(sheetName, "Quote_Reference_Number", rowNum));
		getDriver().findElement(By.xpath("//button[@id='Search']")).click();
    	
    	Thread.sleep(10000);
    	WebElement Element = getDriver().findElement(By.xpath("//tr[@class='work-item-row']/td[3]"));
    	WebElement elmnt = getDriver().findElement(By.xpath("//tr[@class='work-item-row']"));
    	String role_App= Element.getText();
    	String workItem_App=elmnt.getText();
    	if(workItem_App.contains(XLS.getCellData(sheetName, "Quote_Reference_Number", rowNum)) && role_App.equals(role))
    	{
    		Assert.assertTrue("Transaction Created by+"+role+"", true);    		
    	}
    	else
    	{
    		System.out.println("test Failed ");
			Assert.assertTrue("Transaction Created by+"+role+"", false);
    	}    	    	
		
	}
	
	@Step
	public void Verifytransactionchannelinfilteritem() throws InterruptedException, IOException {
		getDriver().findElement(By.xpath("//a[contains(text(),'My Inbox')]")).click();	
		Thread.sleep(10000);
		getDriver().findElement(By.id("FilterMenu")).click();	
		int i=0;
		Thread.sleep(10000);
		WebElement filterheader = getDriver().findElement(By.className("modal-title"));
		if(filterheader.equals("Filter Items"))
		{
			WebElement transactionlabel= getDriver().findElement(By.xpath("//div[@class='modal-body']/div[2]/div[3]/div/label"));
			if(transactionlabel.equals("Transaction Channel:"))
			{
				Assert.assertTrue("Transaction Channel text is found", true);  
				//List<WebElement> transactionlst = new Select((WebElement) getDriver().findElements(By.xpath("//button[@id='TransactionChannelTypeID']/option)"))).getOptions();
				List<WebElement> tnelementsoptions= getDriver().findElements(By.xpath("///*[@name='TransactionChannelTypeID'])/../..//li"));
				for(WebElement element :tnelementsoptions)				
				{
					if (element.getText().contains("Select all") || element.getText().contains("Broker") || element.getText().contains("Internal"))
						Assert.assertTrue("All option are as expected", true);
					else
						Assert.assertTrue("All option are as expected", false);
					
				}
			}
	    
	    	else
	    	{
	    		System.out.println("test Failed ");
				Assert.assertTrue("Transaction Channel text is not found", false);
	    	}    	    	
		}
	}
	
	@Step
	public void approvePolicyinSIT(String test_data) throws InterruptedException, IOException {
		getDriver().get("http://apregevwebqa121:1111/WorkManager");
		Thread.sleep(15000);
		WebDriverWait waits = new WebDriverWait(getDriver(), 30);		
		waits.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Global Search')]")));
		WebElement search=getDriver().findElement(By.xpath("//a[contains(text(),'Global Search')]"));
		JavascriptExecutor js = (JavascriptExecutor) getDriver();  
		js.executeScript("arguments[0].click();", search);
		Thread.sleep(15000);
		
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		System.out.println("control reached enter risk location details" );

		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

				if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
					rowNum = row;
					System.out.println("test data --" + test_data);
					System.out.println("try to come out of loop "+rowNum);
					break innerloop;
				}
			}

		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyAdmin.properties");
		OR.load(fo);
		WebDriverWait wait = new WebDriverWait(getDriver(), 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='SearchPolicyNum']")));

		getDriver().findElement(By.xpath("//input[@id='SearchPolicyNum']")).sendKeys(XLS.getCellData(sheetName, "PolicyNumber", rowNum).replace("'",""));
		
    	getDriver().findElement(By.xpath("//button[@id='Search']")).click();
    	
    	Thread.sleep(10000);
    	getDriver().findElement(By.xpath("//td[contains(text(),'Referral')]")).click();
    	Thread.sleep(5000);
		List<WebElement> items=getDriver().findElements(By.xpath("//select[contains(@name,'WorkItem')]"));
		XLS.setCellData(sheetName, "ReferalCount", rowNum,Integer.toString(items.size()));
		for(int i=0;i<=items.size()-2;i++)
		{
			new Select(getDriver().findElement(By.id("ddl"+i+""))).selectByVisibleText("Approve");
		}
		Thread.sleep(5000);

		WebElement saveProgress=getDriver().findElement(By.xpath("//input[@id='btnSave']"));
		
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", saveProgress);
		saveProgress.click();
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//input[@id='btnApply']")).click();
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//input[@id='btnComplete']")).click();
		Thread.sleep(20000);
	}
}