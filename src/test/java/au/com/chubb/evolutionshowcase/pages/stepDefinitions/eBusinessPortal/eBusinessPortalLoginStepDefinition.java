package au.com.chubb.evolutionshowcase.pages.stepDefinitions.eBusinessPortal;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.eBusinessPortal.eBusinessPortalLoginSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class eBusinessPortalLoginStepDefinition {

	@Steps
	eBusinessPortalLoginSteps eBusiness_potal_login_steps;
	
	
	@Given("^eBusines portal is launched$")
	public void launch_evolution_app() throws Throwable {
		eBusinessPortalLoginSteps.launch_eBusiness_portal_app();
		Thread.sleep(3000);
	}

	
	@And("^login eBusiness portal with \"([^\"]*)\"$")
	public void enter_login_details(String testData) throws Throwable {
		eBusiness_potal_login_steps.enter_login_details(testData);
	}
		
	
	@And("^enter company id as \"([^\"]*)\"$")
	public void enter_company_id(String company_id) throws Throwable {
		eBusiness_potal_login_steps.enter_company_id(company_id);
	}

	@And("^enter user id as \"([^\"]*)\"$")
	public void enter_user_id(String user_id) throws Throwable {
		eBusiness_potal_login_steps.enter_user_id(user_id);
	}

	@And("^enter password \"([^\"]*)\"$")
	public void enter_password(String password) throws Throwable {
		eBusiness_potal_login_steps.enter_password(password);
	}

	@And("^click login$")
	public void click_search() throws Throwable {
		eBusiness_potal_login_steps.click_login();
	}
	
	@Given("^SIT Workmanager is launched$")
	public void launch_SITWorkManager() throws Throwable {
		eBusiness_potal_login_steps.launch_SITWorkManager();
	}
	
	
	

}