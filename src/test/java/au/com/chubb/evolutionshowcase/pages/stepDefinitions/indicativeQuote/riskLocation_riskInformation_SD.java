package au.com.chubb.evolutionshowcase.pages.stepDefinitions.indicativeQuote;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote.riskLocation_riskInformation_Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

public class riskLocation_riskInformation_SD {

	
	@Steps
	riskLocation_riskInformation_Steps riskInformation;

	@And("^verify default value in \"([^\"]*)\" dropdown for Risk Location \"([^\"]*)\"$")
	public void verify_default_value(String field_name, String location_identifier) throws Throwable {
		riskInformation.verify_default_value(field_name, location_identifier);
	}

	@And("^verify Other Description box is displayed when Type of Residence is other for Risk Location \"([^\"]*)\"$")
	public void verify_other_description(String location_identifier) throws Throwable {
		riskInformation.verify_other_description( location_identifier);
		
	}	
		
	@And("^verify below details are populated in Risk information for Risk Location \"([^\"]*)\"$")
		public void verify_risk_information_section(String riskLocation, DataTable arg1) throws Throwable {
		riskInformation.verify_risk_information_section( riskLocation, arg1);
		}	
		
   		
	@And("^verify error message \"([^\"]*)\" is displayed for invalid combination of risk location$")
	public void verify_risk_location_error_message(String errorMessage) throws Throwable {
	riskInformation.verify_risk_location_error_message(errorMessage);
	}	
	

}
