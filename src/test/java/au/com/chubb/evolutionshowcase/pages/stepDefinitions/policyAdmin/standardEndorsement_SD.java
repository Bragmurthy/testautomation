package au.com.chubb.evolutionshowcase.pages.stepDefinitions.policyAdmin;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote.riskLocation_calculatePremium_Steps;
import au.com.chubb.evolutionshowcase.pages.serenitySteps.policyAdmin.standardEndorsement_Steps;
import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import cucumber.api.java.en.And;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Steps;

public class standardEndorsement_SD extends PageObject {

	@Steps
	standardEndorsement_Steps standardEndorsement;
	DynamicSeleniumFunctions dynamicseleniumfunctions;
	riskLocation_calculatePremium_Steps  calculatePremium;

	@And("^verify fields in standard endorsement lightbox$")
	public void verify_quote_status() throws Throwable {
		standardEndorsement.verifyfieldsInEndorsementLightbox();
	}
	
	@And("^Create Standard Endorsement$")
	public void createEndorsement() throws Throwable {
		standardEndorsement.createEndorsement();
	}
	@And("^verify options in Endorsement type$")
	public void verifyEndorsementOptions() throws Throwable {
		standardEndorsement.verifyOptionsInEndorsementType();
	}
	@And("^select Endorsement type as \"([^\"]*)\"$")
	public void selectEndorsementType(String EndType) throws Throwable {
		standardEndorsement.selectEndorsementType(EndType);
	}
	@And("^verify \"([^\"]*)\" and \"([^\"]*)\" check boxes are displayed$")
	public void verifyCheckBoxesInEndorsementPopUp(String AppraisalCB, String RenovationCB)
	{
		standardEndorsement.VerifyCheckBoxesInEndorsementPopUp(AppraisalCB,RenovationCB);
	}
	@And("^select \"([^\"]*)\" check box$")
	public void SelectCheckBoxInEndorsementPopUp(String EndChkBox)
	{
		standardEndorsement.SelectCheckBoxInEndorsementPopUp(EndChkBox);
	}
	
	@And("^create Term change Endorsement$")
	public void createTermchangeEndorsement() throws Throwable {
		standardEndorsement.createTermchangeEndorsement();
	}
	
	@And("^Verify Date Fields and create Termchange Endorsement$")
	public void verifyCurrentDateFields() throws Throwable {
		standardEndorsement.verifyCurrentDateFields();
	}
	
	@And("^Intiate Cancellation Endorsement$")
	public void CancellationEndorsement() throws Throwable {
		standardEndorsement.CancellationEndorsement();
	}
	
	@And("^validate in Premium summary page$")
	public void validatePremiumSummaryPage() throws Throwable {
		standardEndorsement.validatePremiumSummaryPage();
	}
	
	@And("^validate in Premium summary page For Cancellation$")
	public void validatePremiumSummaryPageForCancellation() throws Throwable {
		standardEndorsement.validatePremiumSummaryPageForCancellation();
	}
	
	@And("^policy should be in New line Inforce$")
	public void bookingSuccess() throws Throwable {
		standardEndorsement.bookingSuccess();	
	}
	
	@And("^policy should be Cancelled$")
	public void bookingCancelled() throws Throwable {
		standardEndorsement.bookingCancelled();	
	}
	
	@And("^get alert text for attachment date prior to start date$")
	public void get_Alerttext_AttachmentDate() throws Throwable {
		standardEndorsement.get_Alerttext_AttachmentDate();	
	}
	
	@And ("^Add VAC in MTA$")
	public void add_VAC_in_MTA()
	{
		standardEndorsement.addVACinMTA();
	}
}
