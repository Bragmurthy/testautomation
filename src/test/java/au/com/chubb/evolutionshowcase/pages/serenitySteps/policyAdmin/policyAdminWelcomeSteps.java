package au.com.chubb.evolutionshowcase.pages.serenitySteps.policyAdmin;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;

import javax.validation.constraints.AssertTrue;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.FindElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import au.com.chubb.evolutionshowcase.pages.objectModel.policyAdmin.policyAdminWelcomePage;
import au.com.chubb.evolutionshowcase.utilities.CommonFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class policyAdminWelcomeSteps extends PageObject {

	policyAdminWelcomePage policyadminwelcomepage;
	CommonFunctions common_functions;
	String root = System.getProperty("user.dir");
	public static Properties OR = null;
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
	String sheetName="WinBeatQuoteData";


	@Step
	public void launch_evolution_app() throws InterruptedException {

		common_functions.launch_evolution_app();

	}
	@Step
	public void launch_evolution_app_in_SIT() throws InterruptedException {

		common_functions.launch_evolution_app_in_SIT();

	}
	@Step
	public void select_search_type(String searchType) throws InterruptedException {

		policyadminwelcomepage.select_search_type(searchType);

	}

	@Step
	public void enter_search_value(String searchValue) throws InterruptedException {

		policyadminwelcomepage.enter_search_value(searchValue);

	}

	@Step
	public void click_search() throws InterruptedException {

		policyadminwelcomepage.click_search();
	}

	@Step
	public void select_search_result(String searchResult) {

		loop:

			for (int i = 1;i<=100; i++) {

				try {
					String named_insured_APP = getDriver()
							.findElement(By.xpath(".//*[@id='globalSearch']/div/div[2]/div[2]/div/div[" + i + "]"))
							.getText();
					System.out.println(named_insured_APP);
					if (named_insured_APP.equalsIgnoreCase(searchResult)) {
						getDriver().findElement(By.xpath(".//*[@id='globalSearch']/div/div[2]/div[2]/div/div[" + i + "]"))
						.click();
						break loop;
					}

				} catch (Exception e) {
					break loop;
				}

			}

	}

	@Step	
	public void click_get_a_quote() throws InterruptedException {

		Thread.sleep(1000);			
		getDriver().findElement(By.xpath(("//*[contains(text(), 'Get a Quote')]"))).click();
		Thread.sleep(1000);			


	}

	@Step
	public void search_saved_indicative_quote(String test_data) throws InterruptedException, IOException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		System.out.println("control reached enter risk location details" );

		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

				if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
					rowNum = row;
					System.out.println("test data --" + test_data);
					System.out.println("try to come out of loop "+rowNum);
					break innerloop;
				}
			}

		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyAdmin.properties");
		OR.load(fo);

		getDriver().findElement(By.xpath(OR.getProperty("searchInput_Xpath"))).sendKeys(XLS.getCellData(sheetName, "Quote_Reference_Number", rowNum));
		Thread.sleep(3000);
		getDriver().findElement(By.xpath(OR.getProperty("searchInput_Xpath"))).sendKeys(Keys.DOWN);
		Thread.sleep(3000);
		getDriver().findElement(By.xpath(OR.getProperty("searchInput_Xpath"))).sendKeys(Keys.ENTER);
		//getDriver().findElement(By.id("globalSearchButton")).click();
		Thread.sleep(10000);
	}
	
	@Step
	public void search_quote_OurRef(String test_data) throws InterruptedException, IOException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		System.out.println("control reached enter risk location details" );

		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

				if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
					rowNum = row;
					System.out.println("test data --" + test_data);
					System.out.println("try to come out of loop "+rowNum);
					break innerloop;
				}
			}

		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyAdmin.properties");
		OR.load(fo);
		getDriver().findElement(By.xpath("//a[contains(text(),'Find quote')]")).click();
		Thread.sleep(8000);
		
		getDriver().findElement(By.xpath("//input[@name='txtOurRef']")).sendKeys(XLS.getCellData(sheetName, "OurRef", rowNum));
		Thread.sleep(3000);
		getDriver().findElement(By.xpath("//input[@value='Search']")).click();
		Thread.sleep(8000);
		
		getDriver().findElement(By.xpath("//input[@value='Search']")).click();
		Thread.sleep(8000);
	}
	
	@Step
	public void search_saved_quote(String test_data) throws InterruptedException, IOException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";

		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

				if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
					rowNum = row;
					System.out.println("test data --" + test_data);
					System.out.println("try to come out of loop "+rowNum);
					break innerloop;
				}
			}

		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyAdmin.properties");
		OR.load(fo);

		getDriver().findElement(By.xpath(OR.getProperty("searchInput_Xpath"))).sendKeys(XLS.getCellData(sheetName, "Quote_Reference_Number", rowNum));
		Thread.sleep(3000);
		getDriver().findElement(By.id("globalSearchButton")).click();
		Thread.sleep(8000);
	}
	
	@Step
	public void search_saved_policy(String test_data) throws InterruptedException, IOException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		System.out.println("control reached enter risk location details" );

		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

				if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
					rowNum = row;
					System.out.println("test data --" + test_data);
					System.out.println("try to come out of loop "+rowNum);
					break innerloop;
				}
			}

		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyAdmin.properties");
		OR.load(fo);
		String policynumber=XLS.getCellData(sheetName, "PolicyNumber", rowNum).replace("'","");
		getDriver().findElement(By.xpath(OR.getProperty("searchInput_Xpath"))).sendKeys(policynumber);
		Thread.sleep(3000);
		getDriver().findElement(By.xpath(OR.getProperty("searchInput_Xpath"))).sendKeys(Keys.DOWN);
		getDriver().findElement(By.xpath(OR.getProperty("searchInput_Xpath"))).sendKeys(Keys.ENTER);
		Thread.sleep(35000);
	}
	
	@Step
	public void close_quote_as_uw(String test_data) throws InterruptedException,IOException{
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

				if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
					rowNum = row;
					System.out.println("test data --" + test_data);
					System.out.println("try to come out of loop "+rowNum);
					break innerloop;
				}
			}

		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyAdmin.properties");
		OR.load(fo);
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'closingReferenceId')]")).sendKeys(XLS.getCellData(sheetName, "Quote_Reference_Number", rowNum));
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = sdf.format(cal.getTime());
		System.out.println("Current date in String Format: "+strDate);
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'closingReferenceId')]")).sendKeys(strDate);
		String premium_App = getDriver().findElement(By.xpath("//input[contains(@data-bind,'premium.premiumPayableToChubbAmount')]")).getText();
		premium_App=premium_App .replace("$", "");
		premium_App=premium_App .replace(",", "");
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'closingReferenceId')]")).sendKeys(premium_App);
	}
	
	@Step
	public void crate_new_client(String testdata) throws InterruptedException, IOException
	{
		XLSReader  XLS = new XLSReader(root +"/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "AddClient";
		int row_count = XLS.getRowCount(sheetName);
		int row =0;
		
		loop:
			for (int currentrow=2; currentrow<=row_count; currentrow++){
				if (testdata.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", currentrow))){
					row=currentrow;
					System.out.println("test data fornd" + testdata);
					System.out.println("come out from loop "+row);
					break loop;					
				}
			}
		
		
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyAdmin.properties");
		OR.load(fo);
		getDriver().findElement(By.xpath(OR.getProperty("searchInput_Xpath"))).sendKeys("newClient");
		Thread.sleep(3000);
		getDriver().findElement(By.id("globalSearchButton")).click();
		Thread.sleep(2000);
		getDriver().findElement(By.xpath("//a[contains(text(),'Create a New Client')]")).click();
		Thread.sleep(2000);
		
		String ALPHA_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		Random random = new Random();	     
	    String randResult = "";
	    String randFirstName="";
	    for (int i = 0; i < 6; i++) {
	        int index = random.nextInt(ALPHA_STRING.length());
	        randResult += ALPHA_STRING.charAt(index);
	    }
	    
	     
	    getDriver().findElement(By.id("individualFirstName")).click();
		getDriver().findElement(By.id("individualFirstName")).sendKeys(randResult);
		getDriver().findElement(By.id("individualLastName")).sendKeys(randResult+"Evol");
		getDriver().findElement(By.id("individualDateOfBirth")).sendKeys("25/08/1986");
		new Select(getDriver().findElement(By.id("individualIndustryId"))).selectByVisibleText(XLS.getCellData(sheetName, "Industry", row));
		new Select(getDriver().findElement(By.id("individualOccupationId"))).selectByVisibleText(XLS.getCellData(sheetName, "Occupation", row));
		getDriver().findElement(By.id("addressEditor_Ind_StreetNo")).sendKeys(XLS.getCellData(sheetName, "Street No", row));
		getDriver().findElement(By.id("addressEditor_Ind_StreetName")).sendKeys(XLS.getCellData(sheetName, "Street Name", row));
		getDriver().findElement(By.id("addressEditor_Ind_City")).sendKeys(XLS.getCellData(sheetName, "City", row));
		getDriver().findElement(By.id("addressEditor_Ind_PostCode")).sendKeys(XLS.getCellData(sheetName, "Post Code", row));
		new Select(getDriver().findElement(By.id("addressEditor_Ind_Country"))).selectByVisibleText(XLS.getCellData(sheetName, "Country", row));
		new Select(getDriver().findElement(By.id("addressEditor_Ind_State"))).selectByVisibleText(XLS.getCellData(sheetName, "State", row));
		getDriver().findElement(By.id("individualAddressEmail")).sendKeys(XLS.getCellData(sheetName, "email", row));
		getDriver().findElement(By.xpath("//button[contains(@data-bind, 'save')]")).click();
		Thread.sleep(15000);
	}
	
	@Step
	public void get_Quote(String testdata) throws InterruptedException
	{
		XLSReader  XLS = new XLSReader(root +"/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "Create Quote";
		int row_count = XLS.getRowCount(sheetName);
		int row =0;
		loop:
			for (int currentrow=2; currentrow<=row_count; currentrow++){
				if (testdata.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", currentrow))){
					row=currentrow;
					System.out.println("test data fornd" + testdata);
					System.out.println("come out from loop "+row);
					break loop;					
				}
			}
		getDriver().findElement(By.xpath("//button[contains(text(),'Get a Quote')]")).click();
		Thread.sleep(10000);
		new Select(getDriver().findElement(By.id("insurer"))).selectByVisibleText(XLS.getCellData(sheetName, "Insurer", row));
		Thread.sleep(5000);
		getDriver().findElement(By.id("btnAddProducer")).click();
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("arguments[0].scrollIntoView();", getDriver().findElement(By.id("btnAddProducer")));
		getDriver().findElement(By.id("btnAddProducer")).click();
		new Select(getDriver().findElement(By.id("addProducer_Branch"))).selectByVisibleText(XLS.getCellData(sheetName, "Broker Branch", row));
		getDriver().findElement(By.xpath("//label[contains(text(),'Broker Name or Code:')]/following::input[2]")).sendKeys(XLS.getCellData(sheetName, "Broker Name", row));
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//label[contains(text(),'Broker Name or Code:')]/following::input[2]")).sendKeys(Keys.DOWN);
		getDriver().findElement(By.xpath("//label[contains(text(),'Broker Name or Code:')]/following::input[2]")).sendKeys(Keys.ENTER);
		getDriver().findElement(By.xpath("//label[contains(text(),'Broker Contact:')]/following::input[2]")).click();
		getDriver().findElement(By.xpath("//label[contains(text(),'Broker Contact:')]/following::input[2]")).sendKeys(XLS.getCellData(sheetName, "Broker Contact", row));
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//label[contains(text(),'Broker Contact:')]/following::input[2]")).sendKeys(Keys.DOWN);
		getDriver().findElement(By.xpath("//label[contains(text(),'Broker Contact:')]/following::input[2]")).sendKeys(Keys.ENTER);
		getDriver().findElement(By.xpath("//button[contains(text(),'OK')]")).click();
		getDriver().findElement(By.id("btnProceed")).click();
		//Risk Location	
		WebDriverWait wait=new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(), 'Add Location')]")));
		getDriver().findElement(By.xpath("//button[contains(text(), 'Add Location')]")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("PropertyTypeId")));
		new Select(getDriver().findElement(By.id("PropertyTypeId"))).selectByVisibleText(XLS.getCellData(sheetName, "Property Type", row));
		 
		List<WebElement> rdBtn_Owner = getDriver().findElements(By.id("OwnershipTypeId"));
		String Ownership=XLS.getCellData(sheetName, "Ownership", row);
		if(Ownership.equalsIgnoreCase("Owner"))
			rdBtn_Owner.get(0).click();
		else
			rdBtn_Owner.get(1).click();
		new Select(getDriver().findElement(By.id("ResidenceTypeId"))).selectByVisibleText(XLS.getCellData(sheetName, "Type Of Residence", row));
		 
		List<WebElement> rdBtn_Vac = getDriver().findElements(By.id("VacFlag"));
		String VACFlag=XLS.getCellData(sheetName, "VAC", row);
		if(VACFlag.equalsIgnoreCase("Yes"))
			rdBtn_Vac.get(0).click();
		else
			rdBtn_Vac.get(1).click();
			
		getDriver().findElement(By.id("YearOfConstruction")).sendKeys(XLS.getCellData(sheetName, "YOC", row));
		getDriver().findElement(By.id("YearOfConstruction")).sendKeys(Keys.TAB);
		if (getDriver().findElement(By.id("NewlyBuiltHomeComplete")).isDisplayed())
		{
			String NewlyBuiltHomeComplete=XLS.getCellData(sheetName, "isNewlyBuiltHomeComplete", row);
			List<WebElement> rdn_NewlyBuiltHomeComplete=getDriver().findElements(By.id("NewlyBuiltHomeComplete"));
			if(NewlyBuiltHomeComplete.equalsIgnoreCase("Yes"))
				rdn_NewlyBuiltHomeComplete.get(0).click();
			else
				rdn_NewlyBuiltHomeComplete.get(1).click();
			
			getDriver().findElement(By.id("OutstandingWorkText")).sendKeys("Test");
		}

		List<WebElement> rdBtn_Fire = getDriver().findElements(By.id("FireResistantFlag"));
		String isFireresistant=XLS.getCellData(sheetName, "isFireresistant", row);
		if(isFireresistant.equalsIgnoreCase("Yes"))
			rdBtn_Fire.get(0).click();
		else
			rdBtn_Fire.get(1).click();
		
		new Select(getDriver().findElement(By.id("ExternalConstructionId"))).selectByVisibleText(XLS.getCellData(sheetName, "ExternalConstruction", row));
		new Select(getDriver().findElement(By.id("RoofConstructionId"))).selectByVisibleText(XLS.getCellData(sheetName, "Roof Construction", row));
		
		List<WebElement> rdBtn_Basement = getDriver().findElements(By.id("BasementFlag"));
		String isBasement=XLS.getCellData(sheetName, "isBasement", row);
		if(isBasement.equalsIgnoreCase("Yes"))
			rdBtn_Basement.get(0).click();
		else
			rdBtn_Basement.get(1).click();
		
		List<WebElement> rdBtn_Gutters = getDriver().findElements(By.id("BoxGutterFlag"));
		String isGutters=XLS.getCellData(sheetName, "isGutters", row);
		if(isGutters.equalsIgnoreCase("Yes"))
			rdBtn_Gutters.get(0).click();
		else
			rdBtn_Gutters.get(1).click();
		
		List<WebElement> rdBtn_Occupied = getDriver().findElements(By.id("OccupiedFlag"));
		String isOccupied=XLS.getCellData(sheetName, "isOccupied", row);
		if(isOccupied.equalsIgnoreCase("Yes"))
			rdBtn_Occupied.get(0).click();
		else
			rdBtn_Occupied.get(1).click();
		
		if(isOccupied.equalsIgnoreCase("Yes"))
		{
			List<WebElement> rdBtn_Rented = getDriver().findElements(By.id("RentedFlag"));
			String isRented=XLS.getCellData(sheetName, "isRented", row);
			if(isRented.equalsIgnoreCase("Yes"))
				rdBtn_Rented.get(0).click();
			else
				rdBtn_Rented.get(1).click();
		}		
		
		List<WebElement> rdBtn_Vacant = getDriver().findElements(By.id("VacantFlag"));
		String isVacant=XLS.getCellData(sheetName, "isVacant", row);
		if(isVacant.equalsIgnoreCase("Yes"))
			rdBtn_Vacant.get(0).click();
		else
			rdBtn_Vacant.get(1).click();
		
		new Select(getDriver().findElement(By.id("HeritageListedId"))).selectByVisibleText(XLS.getCellData(sheetName, "Heritage", row));
		
		List<WebElement> rdBtn_CourseOfConstruction = getDriver().findElements(By.id("CourseOfConstructionFlag"));
		String isCourseOfConstruction=XLS.getCellData(sheetName, "isCourseOfConstruction", row);
		if(isCourseOfConstruction.equalsIgnoreCase("Yes"))
			rdBtn_CourseOfConstruction.get(0).click();
		else
			rdBtn_CourseOfConstruction.get(1).click();
		
		new Select(getDriver().findElement(By.id("BurglarAlarmId"))).selectByVisibleText(XLS.getCellData(sheetName, "Burglar Alarm", row));
		new Select(getDriver().findElement(By.id("SmokeDetectorId"))).selectByVisibleText(XLS.getCellData(sheetName, "Smoke Detector", row));
		
		List<WebElement> rdBtn_DeadlockedDoors = getDriver().findElements(By.id("DeadlockedDoorsFlag"));
		String isDeadlockedDoors=XLS.getCellData(sheetName, "isOccupied", row);
		if(isDeadlockedDoors.equalsIgnoreCase("Yes"))
			rdBtn_DeadlockedDoors.get(0).click();
		else
			rdBtn_DeadlockedDoors.get(1).click();
		
		List<WebElement> rdBtn_KeyedWindows = getDriver().findElements(By.id("KeyedWindowsFlag"));
		String isKeyedWindows=XLS.getCellData(sheetName, "isKeyedWindows", row);
		if(isKeyedWindows.equalsIgnoreCase("Yes"))
			rdBtn_KeyedWindows.get(0).click();
		else
			rdBtn_KeyedWindows.get(1).click();
		
		new Select(getDriver().findElement(By.id("SafeTypeId"))).selectByVisibleText(XLS.getCellData(sheetName, "Safe Type", row));
		getDriver().findElement(By.xpath("//button[contains(text(),'Save This')]")).click();
		Thread.sleep(5000);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'OK')]")));
		if(getDriver().findElement(By.xpath("//button[contains(text(),'OK')]")).isDisplayed())
			getDriver().findElement(By.xpath("//button[contains(text(),'OK')]")).click();
		getDriver().findElement(By.id("selectProperty_0_Home_Coverage")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("RiskLocation_0_BuildingSumInsured")));
	    getDriver().findElement(By.id("RiskLocation_0_BuildingSumInsured")).sendKeys(XLS.getCellData(sheetName, "BSI", row).replace("'",""));
		List<WebElement> rdBtn_FloodCoverage = getDriver().findElements(By.name("FloodCoverageFlag"));
		String isFlood=XLS.getCellData(sheetName, "isFlood", row);
		JavascriptExecutor jse = (JavascriptExecutor) getDriver();
		jse.executeScript("window.scrollBy(0,450)", "");
		
		if(isFlood.equalsIgnoreCase("Yes"))
		
			rdBtn_FloodCoverage.get(0).click();
		else
			rdBtn_FloodCoverage.get(1).click();
		
		getDriver().findElement(By.xpath("//button[contains(text(),'Proceed With Quote')]")).click();
		
		if(XLS.getCellData(sheetName, "VAC", row).equalsIgnoreCase("Yes"))
		{
			getDriver().findElement(By.xpath("//button[contains(@data-bind,'openAddVacCategoryDlg')]")).click();
			new Select(getDriver().findElement(By.id("vacEdit_Category"))).selectByVisibleText(XLS.getCellData(sheetName, "VACCAtegory", row));
			new Select(getDriver().findElement(By.id("vacEdit_CategoryDeductible"))).selectByVisibleText(XLS.getCellData(sheetName, "Category Deductable", row).replace("'",""));
			getDriver().findElement(By.xpath("//button[contains(@data-bind,'itemizedCover')]")).click();
			getDriver().findElement(By.id("vacEdit_TSI")).sendKeys(XLS.getCellData(sheetName, "VAC Total Sum",row).replace("'",""));
			getDriver().findElement(By.id("vacEdit_MostValuable")).sendKeys(XLS.getCellData(sheetName, "VAC MostValuable",row).replace("'",""));
			getDriver().findElement(By.xpath("//button[contains(text(),'OK')]")).click();
		}
		getDriver().findElement(By.id("btnSaveProgress")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("RiskLocation_0_BuildingSumInsured")));
		getDriver().findElement(By.xpath("//button[contains(text(),'Proceed With Quote')]")).click();
		//Interested Parties
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("btnAddInterestepdParty")));
		getDriver().findElement(By.xpath("//button[contains(text(),'Proceed With Quote')]")).click();
		//Liability
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("personalLiabilitySumInsured")));
		getDriver().findElement(By.xpath("//button[contains(text(),'Proceed With Quote')]")).click();
		//Loss History
		
		List<WebElement> rdBtn_insuranceRefusalCancelRejectedFlag = getDriver().findElements(By.name("insuranceRefusalCancelRejectedFlag"));
		String isinsuranceRefusalCancelRejected=XLS.getCellData(sheetName, "isinsuranceRefusalCancelRejected", row);
		if(isinsuranceRefusalCancelRejected.equalsIgnoreCase("Yes"))
			rdBtn_insuranceRefusalCancelRejectedFlag.get(0).click();
		else
			rdBtn_insuranceRefusalCancelRejectedFlag.get(1).click();
		
		List<WebElement> rdBtn_chargeConvicted = getDriver().findElements(By.name("chargeConvictedFlag"));
		String ischargeConvictedFlag=XLS.getCellData(sheetName, "ischargeConvictedFlag", row);
		if(ischargeConvictedFlag.equalsIgnoreCase("Yes"))
			rdBtn_chargeConvicted.get(0).click();
		else
			rdBtn_chargeConvicted	.get(1).click();
		
		List<WebElement> rdBtn_bankruptcyFiledFlag = getDriver().findElements(By.name("bankruptcyFiledFlag"));
		String isbankruptcyFiledFlag=XLS.getCellData(sheetName, "isbankruptcyFiledFlag", row);
		if(isbankruptcyFiledFlag.equalsIgnoreCase("Yes"))
			rdBtn_bankruptcyFiledFlag.get(0).click();
		else
			rdBtn_bankruptcyFiledFlag.get(1).click();
		getDriver().findElement(By.xpath("//button[contains(text(),'Proceed With Quote')]")).click();
	}
	
	@Step
	public void crate_new_company_client() throws InterruptedException, IOException
	{
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyAdmin.properties");
		OR.load(fo);
		getDriver().findElement(By.xpath(OR.getProperty("searchInput_Xpath"))).sendKeys("newClient");
		Thread.sleep(3000);
		getDriver().findElement(By.id("globalSearchButton")).click();
		Thread.sleep(2000);
		getDriver().findElement(By.xpath("//a[contains(text(),'Create a New Client')]")).click();
		Thread.sleep(2000);
		
		String ALPHA_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		Random random = new Random();	     
	    String randResult = "";
	    String randFirstName="";
	    for (int i = 0; i < 6; i++) {
	        int index = random.nextInt(ALPHA_STRING.length());
	        randResult += ALPHA_STRING.charAt(index);
	    }
	    
	    getDriver().findElement(By.xpath("//span[text()='Company']/preceding-sibling::input")).click();
	    //getDriver().findElement(By.xpath("//input[@value, '3']")).click();
	    getDriver().findElement(By.id("companyName")).click();
		getDriver().findElement(By.id("companyName")).sendKeys(randResult);
		new Select(getDriver().findElement(By.id("companyIndustryId"))).selectByVisibleText("Entertainment");
		getDriver().findElement(By.id("addressEditor_Co_StreetNo")).sendKeys("9");
		getDriver().findElement(By.id("addressEditor_Co_StreetName")).sendKeys("ankuri road");
		getDriver().findElement(By.id("addressEditor_Co_City")).sendKeys("Tarneit");
		getDriver().findElement(By.id("addressEditor_Co_PostCode")).sendKeys("3142");
		new Select(getDriver().findElement(By.id("addressEditor_Co_Country"))).selectByVisibleText("Australia");
		new Select(getDriver().findElement(By.id("addressEditor_Co_State"))).selectByVisibleText("Victoria");
		getDriver().findElement(By.id("companyContact_0_FirstName")).sendKeys(randResult+"ContactFirst");
		getDriver().findElement(By.id("companyContact_0_Surname")).sendKeys(randResult+"ContactSurname");
		getDriver().findElement(By.id("companyContact_0_Email")).sendKeys(randResult+"email@test.com");
		new Select(getDriver().findElement(By.id("companyContact_0_OccupationId"))).selectByVisibleText("Actor");
		
		getDriver().findElement(By.xpath("//button[contains(@data-bind, 'save')]")).click();
		Thread.sleep(15000);
	}
	
	@Step
	public void verify_ChubbDirectandNAB() throws InterruptedException
	{
		getDriver().findElement(By.xpath("//button[contains(text(),'Get a Quote')]")).click();
		Thread.sleep(2000);
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("arguments[0].scrollIntoView();", getDriver().findElement(By.id("btnAddProducer")));
		getDriver().findElement(By.id("btnAddProducer")).click();
		new Select(getDriver().findElement(By.id("addProducer_Branch"))).selectByVisibleText("Perth");
		getDriver().findElement(By.xpath("//label[contains(text(),'Broker Name or Code:')]/following::input[2]")).sendKeys("7501400");
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//label[contains(text(),'Broker Name or Code:')]/following::input[2]")).sendKeys(Keys.DOWN);
		getDriver().findElement(By.xpath("//label[contains(text(),'Broker Name or Code:')]/following::input[2]")).sendKeys(Keys.ENTER);
		
		if (getDriver().findElement(By.id("addProducer_clientEmail")).isDisplayed() && getDriver().findElement(By.id("addProducer_clientName")).isDisplayed())
		{
			System.out.println("***********Passed");
		}
		getDriver().findElement(By.id("addProducer_clientEmail")).click();
		getDriver().findElement(By.id("addProducer_clientEmail")).sendKeys(Keys.TAB);
		getDriver().findElement(By.id("addProducer_clientEmail")).sendKeys(Keys.ENTER);
		
		Thread.sleep(5000);
		getDriver().findElement(By.id("btnAddProducer")).click();
		new Select(getDriver().findElement(By.id("addProducer_Branch"))).selectByVisibleText("Sydney");
		getDriver().findElement(By.xpath("//label[contains(text(),'Broker Name or Code:')]/following::input[2]")).sendKeys("7501424");
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//label[contains(text(),'Broker Name or Code:')]/following::input[2]")).sendKeys(Keys.DOWN);
		getDriver().findElement(By.xpath("//label[contains(text(),'Broker Name or Code:')]/following::input[2]")).sendKeys(Keys.ENTER);
		
		if (getDriver().findElement(By.id("addProducer_clientEmail")).isDisplayed() && getDriver().findElement(By.id("addProducer_clientName")).isDisplayed())
		{
			System.out.println("Passed");
		}
		
	}
	
	@Step
	public void waitForDocumentGeneration() throws InterruptedException {
		
		try{
			WebDriverWait wait=new WebDriverWait(getDriver(), 240);
			//wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath("//div[contains(@data-bind,'visible: areAttachmentsReady')]"))));
			wait.until(ExpectedConditions.elementToBeClickable(By.id("btnProceed")));
			}
		catch(Exception e)
		{
			Assert.assertTrue("Document Generation Failed", false);
		}
	}
@Step
	public void search_QuoteNumber(String test_flow) throws InterruptedException, IOException {
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";

		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

				if (test_flow.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
					rowNum = row;
					System.out.println("test data --" + test_flow);
					System.out.println("try to come out of loop "+rowNum);
					break innerloop;
				}
			}

		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyAdmin.properties");
		OR.load(fo);
		String quoteNumber=XLS.getCellData(sheetName, "QuoteNumber",rowNum);
		quoteNumber=quoteNumber.replace("'", "");
        System.out.println("Quote Number " +quoteNumber);
		getDriver().findElement(By.xpath(OR.getProperty("searchInput_Xpath"))).sendKeys(quoteNumber);
		Thread.sleep(3000);
		getDriver().findElement(By.xpath(OR.getProperty("searchInput_Xpath"))).sendKeys(Keys.DOWN);
		getDriver().findElement(By.xpath(OR.getProperty("searchInput_Xpath"))).sendKeys(Keys.ENTER);
		getDriver().findElement(By.id("globalSearchButton")).click();
		Thread.sleep(8000);
		
	}

@Step

public void releaseEditLock() throws InterruptedException
{
	Thread.sleep(2000);
	getDriver().findElement(By.xpath("//button[@title='This policy is locked for editing by you']")).click();
	Thread.sleep(2000);
	WebElement element=getDriver().findElement(By.xpath("//img[contains(@src,'Unlock_Black')]"));
	((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
}

	public void click_policy_number(String test_flow) throws Exception {
		Thread.sleep(10000);
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";

		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

				if (test_flow.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
					rowNum = row;
					System.out.println("test data --" + test_flow);
					System.out.println("try to come out of loop "+rowNum);
					break innerloop;
				}
			}
		String quoteNumber=XLS.getCellData(sheetName, "Quote_Reference_Number",rowNum);
		quoteNumber=quoteNumber.replace("'", "");
		Thread.sleep(2000);
		getDriver().findElement(By.xpath("//td[contains(text(),'"+quoteNumber+"')]")).click();
		Thread.sleep(2000);
		
	}
	
	public void verify_policy_effective_date() throws InterruptedException, Exception
	{
		
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		cal.add(Calendar.DATE,0);
		System.out.println("Yesterday's date was "+dateFormat.format(cal.getTime()));  
		//getDriver().findElement(By.name("txtStartDate")).clear();
		//getDriver().findElement(By.name("txtStartDate")).sendKeys(dateFormat.format(cal.getTime()));
		
		
//		Thread.sleep(10000);		
//		List<List<String>> data = arg1.raw();
//		String exp_date = data.get(1).get(1);
//		System.out.println(exp_date);
//		
//		DateFormat originalFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
//		DateFormat targetFormat = new SimpleDateFormat("dd/MM/yyyy");
////		Date date = originalFormat.parse("07 Apr 2018");
//		Date date = originalFormat.parse(exp_date);
		
		//String effective_date_expected = targetFormat.format(date);
		
		String actual_effective_date = (getDriver().findElement(By.xpath("//p[contains(@data-bind,'EffectiveDate')]")).getText());
		if(actual_effective_date.contains(dateFormat.format(cal.getTime())))
			Assert.assertTrue("Effective date is as expected", true);
		
		else
			Assert.assertTrue("Effective date is not as expected", false);
		
	}
	@Step
	public void searchClientNameFromWinBeat() throws InterruptedException, AWTException {
		String name=XLS.getCellData(sheetName, "ClientName",3);
		enter_search_value(name);
		Robot robot=new Robot();
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyRelease(KeyEvent.VK_DOWN);
		Thread.sleep(1000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	    Thread.sleep(7000);
	}
	@Step
	public void saveQuoteNumber() throws InterruptedException
	{
		Thread.sleep(8000);
		String quoteNumber=getDriver().findElement(By.xpath("//td[contains(@class,'sorting')]/a")).getText();
		System.out.println("quote number"+quoteNumber);
		XLS.setCellData(sheetName, "QuoteNumber",3,quoteNumber);
		getDriver().quit();
	}
	public void searchAndCloseTheQuote() throws InterruptedException, AWTException {
		String name=XLS.getCellData(sheetName, "PolicyNumber",3);
		enter_search_value(name);
		Robot robot=new Robot();
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyRelease(KeyEvent.VK_DOWN);
		Thread.sleep(1000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	    Thread.sleep(7000);
	    getDriver().findElement(By.xpath("//td[contains(@class,'sorting')]/a")).click();
		Thread.sleep(15000);
		
		getDriver().findElement(By.xpath("//a[contains(text(),'Closing')]")).click();
		Thread.sleep(7000);
			
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'closingReferenceId')]")).click();
    	getDriver().findElement(By.xpath("//input[contains(@data-bind,'closingReferenceId')]")).sendKeys("4567");
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = sdf.format(cal.getTime());
		System.out.println("Current date in String Format: "+strDate);
		
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'closingIssueDate')]")).sendKeys(strDate);
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'closingIssueDate')]")).sendKeys(Keys.TAB);
		
		String premium_App = getDriver().findElement(By.xpath("//td[contains(@data-bind,'premium.premiumPayableToChubbAmount')]")).getText();
		premium_App=premium_App .replace("$", "");
		premium_App=premium_App .replace(",", "");
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'closingPremiumPayableToChubbAmount')]")).sendKeys(premium_App);

		getDriver().findElement(By.id("btnBookAndIssue")).click();;

		
		try{
			WebDriverWait wait=new WebDriverWait(getDriver(), 240);
			//wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath("//div[contains(@data-bind,'visible: areAttachmentsReady')]"))));
			wait.until(ExpectedConditions.elementToBeClickable(By.id("btnProceed")));
			}
		catch(Exception e)
		{
			Assert.assertTrue("Document Generation Failed", false);
		}
		getDriver().findElement(By.id("btnProceed")).click();
		Thread.sleep(65000);
		getDriver().quit();
	}
	
	

}