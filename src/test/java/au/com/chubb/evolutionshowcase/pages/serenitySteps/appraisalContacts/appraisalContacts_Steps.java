package au.com.chubb.evolutionshowcase.pages.serenitySteps.appraisalContacts;



import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;



@RunWith(SerenityRunner.class)
public class appraisalContacts_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public appraisalContacts_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);

	}
	@Step
	public void select_checkbox() throws InterruptedException {
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'parent.selectProperty.bind')]")).click();
		Thread.sleep(5000);

	}
	@Step
	public void verify_Fields() throws InterruptedException {
		if(getDriver().findElement(By.xpath("//span[contains(text(),'*')][parent::label[contains(text(),'Contact Name:')]]")).isDisplayed()
				&&getDriver().findElement(By.xpath("//span[contains(text(),'*')][parent::label[contains(text(),'Phone (Mobile):')]]")).isDisplayed()
				&&getDriver().findElement(By.xpath("//textarea[@id='appraisalContact_Comments'][preceding::label[contains(text(),'Preferred Day/s in the week:')]]")).isDisplayed())
		{
			Assert.assertTrue("Apprisal Contacts Expected fileds displayed", true);
		}
		else
			Assert.assertTrue("Apprisal Contacts Expected fileds not displayed", false);
	}

	@Step
	public void add_AppraisalContact(String interested_party_count, String Location_Identifier, DataTable arg1)
			throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		int Location_Number = Integer.parseInt(Location_Identifier);
		int appraisalContacts_count = Integer.parseInt(interested_party_count);
		Thread.sleep(1000);
		if (appraisalContacts_count > 0)
		{
			getDriver().findElement(By.id("p"+Location_Number+"")).click();
			Thread.sleep(3000);

			for (int i = 1; i <= appraisalContacts_count; i++) {

				String sheetName = "AppraisalContacts";
				int Row_Count = XLS.getRowCount(sheetName);
				int rowNum = 0;

				List<List<String>> data = arg1.raw();

				innerloop: for (int row = 2; row <= Row_Count; row++) {
					String vac_test_data = data.get(i-1).toString().replace("[", "").replace("]", "").trim();

					System.out.println("Interested Parties test data --" + vac_test_data);
					if (vac_test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
						rowNum = row;
						break innerloop;
					}
				}
				System.out.println("Value of row number " + rowNum);

				try {
					getDriver().findElement(By.id("btnAddAppraisalContact")).click();;
					Thread.sleep(500);
					// Enter Contact name
					getDriver().findElement(By.name("appraisalContact_Name")).clear();
					getDriver().findElement(By.name("appraisalContact_Name")).sendKeys(XLS.getCellData(sheetName, "ContactName", rowNum));

					Thread.sleep(500);

					// Enter Mobile Number
					// ************************//
					getDriver().findElement(By.name("appraisalContact_Mobile")).clear();
					getDriver().findElement(By.id("appraisalContact_Mobile")).sendKeys(XLS.getCellData(sheetName, "MobileNumber", rowNum));
					getDriver().findElement(By.id("btnAppraisalContact_Add")).click();
					Thread.sleep(7000);
					} 

				catch (Exception e) {

				}

			}
		}
	}
	@Step
	public void verify_AppraisalContact(String interested_party_count, String Location_Identifier, DataTable arg1)
			throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "AppraisalContacts";
		int Location_Number = Integer.parseInt(Location_Identifier);
		int Row_Count = XLS.getRowCount(sheetName);
		List<List<String>> data = arg1.raw();
		for (int row = 2; row <= Row_Count; row++) {
			String vac_test_data = data.get(0).get(0).toString().replace("[", "").replace("]", "").trim();

			System.out.println("Interested Parties test data --" + vac_test_data);
			if (vac_test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				Location_Number = row;
				break ;		
		}
		}
	
  	    String contactName = XLS.getCellData(sheetName, "ContactName", Location_Number);
  		String MobileNumber=XLS.getCellData(sheetName, "MobileNumber", Location_Number);
  		MobileNumber="+"+MobileNumber;

		System.out.println("Excel Values are"+contactName +MobileNumber);
	
		String contactName_APP=getDriver().findElement(By.xpath("//span[@data-bind='text: ContactName'][preceding::label[contains(text(),'Contact Name:')]]")).getText();
		System.out.println("App Values are"+contactName_APP);
		String MobileNumber_APP=getDriver().findElement(By.xpath("//span[contains(@data-bind,'MobilePhone')][preceding::label[contains(text(),'Phone (Mobile):')]]")).getText();
		
		System.out.println("App Values are" +MobileNumber_APP);
		
		if(contactName.equals(contactName_APP) && MobileNumber.equals(MobileNumber_APP)){
			Assert.assertTrue("Apprasial Contact is Added Succesfully", true);
		}
		else
			Assert.assertTrue("Apprasial Contact is not Added name Actual name "+contactName_APP+ "and number "+MobileNumber+"", false);
	}
	@Step
	public void verify_RequiredFiledErrorMessage_AppraisalContactPage() {
		
		List<WebElement> errorMessageList=getDriver().findElements(By.xpath("//span[contains(text(),'This field is required.')]"));
		String errorMessage="This field is required.";
		
		int noOfErros=errorMessageList.size();
		boolean flag=true;
		String contactNameError=getDriver().findElement(By.xpath("//span[@id='appraisalContact_Name-error']")).getText();
		String mobilePhoneError=getDriver().findElement(By.xpath("//span[@id='appraisalContact_Mobile-error']")).getText();
			
		if(!mobilePhoneError.equalsIgnoreCase(errorMessage) )
		{
			flag=false;
		}
		if(noOfErros==2 && flag)
		{
			Assert.assertTrue("Appraisal Contact page field required error messages are displayed", true);
		}
		else
		 Assert.assertTrue("Appraisal Contact page field required error messages are displayed", false);
		
	}
		
	} 
		





