package au.com.chubb.evolutionshowcase.pages.objectModel.eBusinessPortal;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import au.com.chubb.evolutionshowcase.utilities.XLSReader;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;

public class eBusinessPortalAddNewCustomerPage extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;
	String sheetName = "QuoteData";
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");


	public eBusinessPortalAddNewCustomerPage() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/eBusinessPortal.properties");
		OR.load(fo);

	}

	@Step
	public void click_add_new_customer() throws InterruptedException {
		loop:
			for (int i=0;i<=100;i++)
			{
				try
				{
					WebElement elmnt=getDriver().findElement(By.xpath(OR.getProperty("Add_a_New_Customer_Xpath")));
					JavascriptExecutor js = (JavascriptExecutor) getDriver();  
					js.executeScript("arguments[0].click();", elmnt);
					
					//WebElement newCustomer = getDriver().findElement(By.xpath(OR.getProperty("Add_a_New_Customer_Xpath"));
					//getDriver().findElement(By.xpath(OR.getProperty("Add_a_New_Customer_Xpath"))).click();
					Thread.sleep(10000);
					break loop;
				}catch (Exception e)
				{
					Thread.sleep(10000);
				}
			}
	Thread.sleep(5000);

		WebDriverWait wait = new WebDriverWait(getDriver(), 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='txtEntityId']")));
	Thread.sleep(1000);
	}
	
	@Step
	public void enter_new_customer_details() throws InterruptedException {
		
		
		String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		String ALPHA_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		Random random = new Random();	     
	    String randResult = "";
	    String randFirstName="";
	    for (int i = 0; i < 6; i++) {
	        int index = random.nextInt(ALPHA_STRING.length());
	        randResult += ALPHA_STRING.charAt(index);
	    }
	    for (int i = 0; i < 6; i++) {
	        int index = random.nextInt(ALPHA_STRING.length());
	        randFirstName += ALPHA_STRING.charAt(index);
	    }
        String email=randResult+"@chubb.com";
		getDriver().findElement(By.name("txtEntityId")).sendKeys(randResult);
		new Select(getDriver().findElement(By.name("selType"))).selectByVisibleText("Person");
		getDriver().findElement(By.name("txtFirstName")).sendKeys(randFirstName);
		getDriver().findElement(By.name("txtMiddleName")).sendKeys("MiddleEbix");
		getDriver().findElement(By.name("txtLastName")).sendKeys(randResult);
		Thread.sleep(1000);
		getDriver().findElement(By.name("txtEmail")).sendKeys(email);
		Thread.sleep(5000);
		
		WebElement elmnt=getDriver().findElement(By.name("cmdSave"));
		JavascriptExecutor js = (JavascriptExecutor) getDriver();  
		js.executeScript("arguments[0].click();", elmnt);

		Thread.sleep(10000);
		
		
		XLS.setCellData(sheetName, "InsuredId", 3,randResult);
		XLS.setCellData(sheetName, "Firstname", 3,randFirstName);
		XLS.setCellData(sheetName, "Middlename", 3,"MiddleEbix");
		XLS.setCellData(sheetName, "Lastname", 3,randResult);
		XLS.setCellData(sheetName, "Email", 3,email);
		}
	@Step
	public void saveCustomerID() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 100);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[contains(text(),'Customer ')]")));
		
		String customerId=getDriver().findElement(By.xpath("//td[contains(text(),'Customer ')]")).getText();
		customerId=customerId.split(" ")[1];
		customerId=customerId.replaceAll("\"", "");
		XLS.setCellData(sheetName, "InsuredId", 3,customerId);
	}
	
	@Step
	public void saveCustomerID(String test_data) {
		String customerId=getDriver().findElement(By.xpath("//td[contains(text(),'Customer ')]")).getText();
		customerId=customerId.split(" ")[1];
		customerId=customerId.replaceAll("\"", "");	
	
	int Row_Count = XLS.getRowCount(sheetName);
	int rowNum = 0;
	
	innerloop: for (int row = 2; row <= Row_Count; row++) {

		System.out.println("test data --" + test_data);
		if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
			rowNum = row;
			break innerloop;
		}
	}
	System.out.println(customerId);
	XLS.setCellData("sheetName", "InsuredId", rowNum,customerId);
	XLS.setCellData("sheetName", "InsuredId", rowNum,customerId);
	
	}
	@Step
	public void click_add_a_new_quote() throws InterruptedException
	{
		
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", getDriver().findElement(By.name("btnAddQuote")));
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", getDriver().findElement(By.name("btnAddQuote")));
		//getDriver().findElement(By.name("btnAddQuote")).click();
		Thread.sleep(10000);
	}
	
	@Step
	public void click_add_new_business() throws InterruptedException
	{
		
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", getDriver().findElement(By.name("btnAddQuote")));
		getDriver().findElement(By.name("btnAddPolicy")).click();
		Thread.sleep(10000);
	}
	
	@Step
	public void fill_identification_details_for_new_quote(String test_data) throws InterruptedException
	{
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "QuoteData";
		Thread.sleep(1000);
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop: for (int row = 2; row <= Row_Count; row++) {

			System.out.println("test data --" + test_data);
			if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				break innerloop;
			}
		}
		String InsuredId=XLS.getCellData(sheetName, "InsuredId", rowNum);
		System.out.println("InsuredId -- " +InsuredId );
		
		String Product=XLS.getCellData(sheetName, "Product", rowNum);
		System.out.println("Product -- " +Product );
		
		String StartDate=XLS.getCellData(sheetName, "StartDate", rowNum);
		System.out.println("StartDate -- " +StartDate );
		
		String EndDate=XLS.getCellData(sheetName, "EndDate", rowNum);
		System.out.println("EndDate -- " +Product );
		
		getDriver().findElement(By.name("txtClientId")).sendKeys(InsuredId);
		new Select(getDriver().findElement(By.name("selProdId"))).selectByVisibleText(Product);				
	}
	
	@Step
	public void enter_previousDate()
	{
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
		cal.add(Calendar.DATE, -1);
		System.out.println("Yesterday's date was "+dateFormat.format(cal.getTime()));  
		getDriver().findElement(By.name("txtStartDate")).clear();
		getDriver().findElement(By.name("txtStartDate")).sendKeys(dateFormat.format(cal.getTime()));
		getDriver().findElement(By.name("txtStartDate")).sendKeys(Keys.TAB);
	}
	
	@Step
	public void enter_NextStartDate()
	{
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
		cal.add(Calendar.DATE, 42);
		System.out.println("Yesterday's date was "+dateFormat.format(cal.getTime()));  
		getDriver().findElement(By.name("txtStartDate")).clear();
		getDriver().findElement(By.name("txtStartDate")).sendKeys(dateFormat.format(cal.getTime()));
		getDriver().findElement(By.name("txtStartDate")).sendKeys(Keys.TAB);
	}
	
	@Step
	public void enter_SystemDate_as_StartDate()
	{
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
		cal.add(Calendar.DATE, 0);
		System.out.println("Yesterday's date was "+dateFormat.format(cal.getTime()));  
		getDriver().findElement(By.name("txtStartDate")).clear();
		getDriver().findElement(By.name("txtStartDate")).sendKeys(dateFormat.format(cal.getTime()));
		getDriver().findElement(By.name("txtStartDate")).sendKeys(Keys.TAB);
	}
	@Step
	public void enter_LaterEndDate()
	{
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
		cal.add(Calendar.DATE, 200);
		System.out.println("Yesterday's date was "+dateFormat.format(cal.getTime()));  
		getDriver().findElement(By.name("txtEndDate")).clear();
		getDriver().findElement(By.name("txtEndDate")).sendKeys(dateFormat.format(cal.getTime()));
		getDriver().findElement(By.name("txtEndDate")).sendKeys(Keys.TAB);
	}
	
	@Step
	public void enter_AttachmentDate_PreviousDate()
	{
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
		cal.add(Calendar.DATE, -10);
		System.out.println("Yesterday's date was "+dateFormat.format(cal.getTime()));  
		getDriver().findElement(By.name("txtAttachDate")).clear();
		getDriver().findElement(By.name("txtAttachDate")).sendKeys(dateFormat.format(cal.getTime()));
		getDriver().findElement(By.name("txtAttachDate")).sendKeys(Keys.TAB);
	}
	
	@Step
	public void enter_AttachmentDate_LaterDate(int noOfDays)
	{
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
		cal.add(Calendar.DATE, noOfDays);
		System.out.println("Yesterday's date was "+dateFormat.format(cal.getTime()));  
		getDriver().findElement(By.name("txtAttachDate")).clear();
		getDriver().findElement(By.name("txtAttachDate")).sendKeys(dateFormat.format(cal.getTime()));
		getDriver().findElement(By.name("txtAttachDate")).sendKeys(Keys.TAB);
	}
}