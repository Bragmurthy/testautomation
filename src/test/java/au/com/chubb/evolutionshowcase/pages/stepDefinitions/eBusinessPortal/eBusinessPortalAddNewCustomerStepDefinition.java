package au.com.chubb.evolutionshowcase.pages.stepDefinitions.eBusinessPortal;


import au.com.chubb.evolutionshowcase.pages.serenitySteps.eBusinessPortal.eBusinessPortalAddNewCustomerSteps;
import au.com.chubb.evolutionshowcase.pages.serenitySteps.eBusinessPortal.eBusinessPortalAddNewQuoteSteps;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class eBusinessPortalAddNewCustomerStepDefinition {

	@Steps
	eBusinessPortalAddNewCustomerSteps add_new_customer;
	
	
	@And("^click on Add new Customer$")
	public void click_add_new_customer() throws Throwable{
		add_new_customer.click_add_new_customer();
	}
	
	@And("^enter new customer details$")
	public void enter_new_customer_details() throws Throwable{
		add_new_customer.enter_new_customer_details();
	}

	@And("^save Customer Id$")
	public void save_customer_Id() throws Throwable{
		add_new_customer.save_customer_details();
	}
	
	@And("^save Customer Id in \"([^\"]*)\"$")
	public void save_customer_details_toSheet(String testdata) throws Throwable{
		add_new_customer.save_customer_details_toSheet(testdata);
	}
	
	@And("^click on Add a New Quote in Add new Customer page$")
	public void click_add_new_quote() throws Throwable{
		add_new_customer.click_add_new_quote();
	}
	
	@And("^click on Add New Business in Add new Customer page$")
	public void click_add_new_business() throws Throwable{
		add_new_customer.click_add_new_business();
	}
	@And("^enter identification details in \"([^\"]*)\"$")
	public void enter_identification_details(String testdata) throws Throwable{
		add_new_customer.enter_identification_details(testdata);
	}
	
	@And("^Save Policy Number in \"([^\"]*)\"$")
	public void SavePolicyNumber(String test_data) throws Throwable{
		add_new_customer.savePolicyNumber(test_data);
	}
	@And("^Update Start Date in ebusiness portal to previous date$")
	public void enterPreviousDayDate() throws Throwable{
		add_new_customer.enterYesterdayDate();
	}
	
	@And("^Update Start Date in ebusiness portal to later date$")
	public void enter_NextStartDate() throws Throwable{
		add_new_customer.enter_NextStartDate();
	}
	
	@And("^Update Start Date in ebusiness portal to current system date$")
	public void enter_TodayStartDate() throws Throwable{
		add_new_customer.enter_TodayStartDate();
	}
	
	@And("^Update End Date in ebusiness portal to later date$")
	public void enter_LaterEndDate() throws Throwable{
		add_new_customer.enter_LaterEndDate();
	}
	
	@And("^Update Attachment Date in ebusiness portal to Previous date$")
	public void enter_AttachmentDate_PreviousDate() throws Throwable{
		add_new_customer.enter_AttachmentDate_PreviousDate();
	}
	
	@And("^Update Attachment Date in ebusiness portal with \"([^\"]*)\" days Later date$")
	public void enter_AttachmentDate_LaterDate(String noOfDays) throws Throwable{
		add_new_customer.enter_AttachmentDate_LaterDate(Integer.parseInt(noOfDays));
	}
}
