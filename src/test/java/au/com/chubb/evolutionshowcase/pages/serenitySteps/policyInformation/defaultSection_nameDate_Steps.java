package au.com.chubb.evolutionshowcase.pages.serenitySteps.policyInformation;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class defaultSection_nameDate_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;
	DynamicSeleniumFunctions functions;
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
	String sheetName="QuoteData";

	public defaultSection_nameDate_Steps() throws IOException {
	/*	OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/landingPage.properties");
		OR.load(fo);
*/
	}

	@Step
	public void default_dates() {
		
		
		//String effectiveDate_APP=getDriver().findElement(By.xpath("//p[@id='effectiveDate']")).getText();
		String effectiveDate_APP=getDriver().findElement(By.xpath("//input[@id='effectiveDate']")).getAttribute("value");
		
		//String expiryDate_APP=getDriver().findElement(By.xpath("//p[@id='expiryDate']")).getText();
		String expiryDate_APP=getDriver().findElement(By.xpath("//input[@id='expiryDate']")).getAttribute("value");
		System.out.println("From App Effective Date"+effectiveDate_APP);
		System.out.println("From App Expiry Date"+expiryDate_APP);
		//String submissionDate_APP=getDriver().findElement(By.xpath("//p[@id='submissionDate']")).getText();
		String submissionDate_APP=getDriver().findElement(By.xpath("//p[@id='submissionDate']")).getText();
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = sdf.format(cal.getTime());
		System.out.println("Current date in String Format: "+strDate);
	
		cal.add(Calendar.YEAR, 1); // to get previous year add -1
		String endDate = sdf.format(cal.getTime());
		if (effectiveDate_APP.equalsIgnoreCase(strDate) && expiryDate_APP.equalsIgnoreCase(endDate) && submissionDate_APP.equalsIgnoreCase(strDate))
		{
			Assert.assertTrue("effectiveDate " +effectiveDate_APP+" effectiveDate " +expiryDate_APP+" and Submission Date "+submissionDate_APP+" successfully displayed in indicative quote page", true);
		} else {
			Assert.assertTrue("Dates doesn't match ", false);
		}
	}

	@Step
	public void fieldsNotEditable(String effectiveDate, String expiryDate, String submissionRecievedDate) throws IOException {
		functions.verifyFieldNonEditableForDates(effectiveDate);
		functions.verifyFieldNonEditableForDates(expiryDate);
		boolean flag=functions.verifyFieldNonEditable(submissionRecievedDate);
		
		if (flag)
			Assert.assertTrue("submissionRecievedDate is not ediatble", true);
			else
				Assert.assertTrue("submissionRecievedDate is ediatble", false);
		}
	
	@Step
	public void fieldsNotEditableInInforce(String effectiveDate, String expiryDate, String submissionRecievedDate) throws IOException {
		boolean flagEffectiveDate=functions.verifyFieldNonEditable(effectiveDate);
		boolean flagExpiryDate=functions.verifyFieldNonEditable(expiryDate);
		boolean flag=functions.verifyFieldNonEditable(submissionRecievedDate);
		
		if (flag &&flagEffectiveDate &&flagExpiryDate)
			Assert.assertTrue("date fields are not ediatble", true);
			else
				Assert.assertTrue("date fields are ediatble", false);
		}
@Step
	public void verifyQuoteStatus(String labelQuoteStatus) {
		String quoteStatus_App=getDriver().findElement(By.xpath("//span[contains(text(),'Status: Quote In Progress')]")).getText();
		if (quoteStatus_App.equalsIgnoreCase(labelQuoteStatus))
		Assert.assertTrue("Quote Status text is displayed as expected", true);
		else
		Assert.assertTrue("Quote Status text is not displayed as expected", false);
		
	}

@Step
public void verifyRequiredFieldLabel(String label) {
	String requiredFiledLabel_App=getDriver().findElement(By.xpath("//p[span[contains(text(),'*')]]")).getText();
	if (requiredFiledLabel_App.equalsIgnoreCase(label))
	Assert.assertTrue("Required field label is displayed as expected", true);
	else
	Assert.assertTrue("Required field label not displayed as expected actual is"+requiredFiledLabel_App, false);

}
@Step
public void verifyQuoteNumber() {

	Pattern p = Pattern.compile("Quote No. \\d{7}"); 
	String quoteNumber=getDriver().findElement(By.xpath("//h2[contains(text(),'Quote')]")).getText();
	Matcher m = p.matcher(quoteNumber);  
	boolean matcher = m.matches();  
	if (matcher)
	Assert.assertTrue("Quote Number is displayed as expected", true);
	else
	Assert.assertTrue("Quote Number is not displayed as expected", false);
	
}

@Step
public void verifyAllMandatoryFiledsErrorMessage(String riskLocationNumber) {
	int location=Integer.parseInt(riskLocationNumber);
	String errorMessage="This field is required.";
	String streetNo=getDriver().findElement(By.xpath("//span[@id='addressEditor_"+location+"_StreetNo-error']")).getText();
	String streetName=getDriver().findElement(By.xpath("//span[@id='addressEditor_"+location+"_StreetName-error']")).getText();
	String city=getDriver().findElement(By.xpath("//span[@id='addressEditor_"+location+"_City-error']")).getText();
	String postcode=getDriver().findElement(By.xpath("//span[@id='addressEditor_"+location+"_PostCode-error']")).getText();
	String country=getDriver().findElement(By.xpath("//span[@id='addressEditor_"+location+"_Country-error']")).getText();
	String Insurer=getDriver().findElement(By.xpath("//span[@id='insurer-error']")).getText();
	
	if(streetNo.equalsIgnoreCase(errorMessage)&&
			streetName.equalsIgnoreCase(errorMessage) &&
			city.equalsIgnoreCase(errorMessage) &&
			postcode.equalsIgnoreCase(errorMessage) &&
			country.equalsIgnoreCase(errorMessage) &&
			Insurer.equalsIgnoreCase(errorMessage) ){
		Assert.assertTrue("Filed Required Error Messages are displayed for all fieldsas expected", true);	}
	else
		Assert.assertTrue("Filed Required Error Messages are not displayed for all fields as expected", false);
}

}