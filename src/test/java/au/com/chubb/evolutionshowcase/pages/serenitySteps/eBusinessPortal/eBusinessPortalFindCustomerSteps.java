package au.com.chubb.evolutionshowcase.pages.serenitySteps.eBusinessPortal;

import org.junit.runner.RunWith;
import au.com.chubb.evolutionshowcase.pages.objectModel.eBusinessPortal.eBusinessPortalFindCustomerPage;
import au.com.chubb.evolutionshowcase.utilities.CommonFunctions;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class eBusinessPortalFindCustomerSteps extends PageObject {
	String root = System.getProperty("user.dir");
	static eBusinessPortalFindCustomerPage ebusinessportalFindCustomerpage;
	static CommonFunctions common_functions;

	@Step	
	public void verify_searchResult(String customerName, String expectedCustomer) throws InterruptedException {
		ebusinessportalFindCustomerpage.enterSearchCriteria(customerName);
		ebusinessportalFindCustomerpage.verifyClientSearchResult(expectedCustomer);
	}
}