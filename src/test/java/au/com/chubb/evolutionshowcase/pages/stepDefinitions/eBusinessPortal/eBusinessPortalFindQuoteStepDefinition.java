package au.com.chubb.evolutionshowcase.pages.stepDefinitions.eBusinessPortal;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.eBusinessPortal.eBusinessPortalFindQuoteSteps;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

public class eBusinessPortalFindQuoteStepDefinition {

	@Steps
	eBusinessPortalFindQuoteSteps find_quote;

	
	@And("^click on find Quote$")
	public void click_find_quote() throws Throwable {
		find_quote.click_find_quote();
		}
	
	
	@And("^search for quote reference number from \"([^\"]*)\" and verify status of quote is \"([^\"]*)\"$")
	public void verify_quote_status(String testData, String quoteStatus) throws Throwable {
		find_quote.verify_quote_status(testData,quoteStatus );
	}
	
	@And("^search for quote reference number from \"([^\"]*)\"$")
	public void searchQuoteIneBix(String testData) throws Throwable {
		find_quote.searchQuoteIneBix(testData);
	}
	
	@And("^search for quote reference number from \"([^\"]*)\" and verify status of quote is \"([^\"]*)\" by passing Client ID in ebusinenss porlrta$")
	public void verify_quote_status_by_passing_clientId(String testData, String quoteStatus) throws Throwable {
		find_quote.verify_quote_status_by_passing_clientId (testData,quoteStatus );
	}
	
	@And("^search for policy reference number from Ebix \"([^\"]*)\"$")
	public void searchPolicyIneBix(String testData) throws Throwable {
		find_quote.searchPolicyIneBix(testData);
	}
	
	@And("^search for quote our reference number from \"([^\"]*)\"$")
	public void searchQuoteIneBixOurRef(String testData) throws Throwable {
		find_quote.searchQuoteIneBixOurRef(testData);
	}
}