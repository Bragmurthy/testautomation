package au.com.chubb.evolutionshowcase.pages.serenitySteps.riskAndCoverages;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.tools.ant.types.CommandlineJava.SysProperties;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class VerifyLocationDetailsFromIndicativeQuote_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public VerifyLocationDetailsFromIndicativeQuote_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);

	}

	@Step
	public void verify_risk_location_details(String location, String test_data) throws InterruptedException {
		// TODO Auto-generated method stub
		
		
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "RiskandCoveragesDataTransfer";
		Thread.sleep(1000);
		System.out.println("control reached enter risk location details" );
	
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		int match_counter=0;
		
		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

			if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				System.out.println("test data --" + test_data);
				System.out.println("try to come out of loop "+rowNum);
				break innerloop;
			}
		}
		
			int k=Integer.parseInt(location);
			Thread.sleep(1000);
			getDriver().findElement(By.id("selectProperty_"+k)).click();
		    Thread.sleep(1000);
		    
		    WebElement LocationDetailsTable=getDriver().findElement(By.xpath(".//*[@id='subSideNavigationMainBody']/div/div/div[1]/div[1]/div/div/div[2]/div"));
			
			try
			{			System.out.println("O--" +   LocationDetailsTable.findElement(By.xpath("//div[1]/table/tbody/tr[1]/td[2]/span")).getText());
			            System.out.println("I--"+XLS.getCellData(sheetName, "Address", rowNum));
			            if ((LocationDetailsTable.findElement(By.xpath("//div[1]/table/tbody/tr[1]/td[2]/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "Address", rowNum)))
                          {
			            	match_counter++;        
                          }
			            else
			            {
			            	System.out.println("Unmatched -- " +XLS.getCellData(sheetName, "Address", rowNum));
			            }
			}catch (Exception e)
			{
				
			}
			try
			{			System.out.println("O--" +   LocationDetailsTable.findElement(By.xpath("//div[1]/table/tbody/tr[3]/td[2]/span")).getText());
                        System.out.println("I--"+XLS.getCellData(sheetName, "Suburb/City", rowNum));
                        if ((LocationDetailsTable.findElement(By.xpath("//div[1]/table/tbody/tr[3]/td[2]/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "Suburb/City", rowNum)))
                        {
			            	match_counter++;        
                        }
                        else
			            {
			            	System.out.println("Unmatched -- " +XLS.getCellData(sheetName, "Suburb/City", rowNum));
			            }

			}catch (Exception e)
			{
				
			}
			try
			{			System.out.println("O--" + LocationDetailsTable.findElement(By.xpath("//div[2]/table/tbody/tr[1]/td[2]/span")).getText());
                        System.out.println("I--"+XLS.getCellData(sheetName, "State/Province", rowNum));
                        if ((LocationDetailsTable.findElement(By.xpath("//div[2]/table/tbody/tr[1]/td[2]/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "State/Province", rowNum)))
                        {
			            	match_counter++;        
                        }
                        else
			            {
			            	System.out.println("Unmatched -- " +XLS.getCellData(sheetName, "State/Province", rowNum));
			            }
                        
			}catch (Exception e)
			{
				
			}
			try
			{			System.out.println("O--" + LocationDetailsTable.findElement(By.xpath("//div[2]/table/tbody/tr[2]/td[2]/span")).getText());
                        System.out.println("I--"+XLS.getCellData(sheetName, "Country", rowNum));
                        if ((LocationDetailsTable.findElement(By.xpath("//div[2]/table/tbody/tr[2]/td[2]/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "Country", rowNum)))
                        {
			            	match_counter++;        
                        }
                        else
			            {
			            	System.out.println("Unmatched -- " +XLS.getCellData(sheetName, "Country", rowNum));
			            }

			}catch (Exception e)
			{
				
			}
			try
			{			System.out.println("O--" + LocationDetailsTable.findElement(By.xpath("//div[2]/table/tbody/tr[3]/td[2]/span")).getText());
                        System.out.println("I--"+XLS.getCellData(sheetName, "PostCode", rowNum));
                        if ((LocationDetailsTable.findElement(By.xpath("//div[2]/table/tbody/tr[3]/td[2]/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "PostCode", rowNum)))
                        {
			            	match_counter++;        
                        }
                        else
			            {
			            	System.out.println("Unmatched -- " +XLS.getCellData(sheetName, "PostCode", rowNum));
			            }


			}catch (Exception e)
			{
				
			}
		    WebElement RiskSecurityInformationTable=getDriver().findElement(By.xpath(".//*[@id='subSideNavigationMainBody']/div/div/div[1]"));

			try
			{			System.out.println("O--" +   RiskSecurityInformationTable.findElement(By.xpath("//div[5]/div[1]/div/span")).getText());
                        System.out.println("I--"+XLS.getCellData(sheetName, "PropertyType", rowNum));
                        if ((LocationDetailsTable.findElement(By.xpath("//div[5]/div[1]/div/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "PropertyType", rowNum)))
                        {
			            	match_counter++;        
                        }
                        else
			            {
			            	System.out.println("Unmatched -- " +XLS.getCellData(sheetName, "PropertyType", rowNum));
			            }



			}catch (Exception e)
			{
				
			}
			try
			{			System.out.println("O--" +  RiskSecurityInformationTable.findElement(By.xpath("//div[5]/div[2]/div/span")).getText());
                        System.out.println("I--"+XLS.getCellData(sheetName, "Ownership", rowNum));
                        if ((LocationDetailsTable.findElement(By.xpath("//div[5]/div[2]/div/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "Ownership", rowNum)))
                        {
			            	match_counter++;        
                        }
                        else
			            {
			            	System.out.println("Unmatched -- " +XLS.getCellData(sheetName, "Ownership", rowNum));
			            }



			}catch (Exception e)
			{
				
			}
			try
			{			System.out.println("O--" +   RiskSecurityInformationTable.findElement(By.xpath("//div[5]/div[3]/div/span")).getText());
                        System.out.println("I--"+XLS.getCellData(sheetName, "TypeOfResidence", rowNum));
                        if ((LocationDetailsTable.findElement(By.xpath("//div[5]/div[3]/div/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "TypeOfResidence", rowNum)))
                        {
			            	match_counter++;        
                        }
                        else
			            {
			            	System.out.println("Unmatched -- " +XLS.getCellData(sheetName, "TypeOfResidence", rowNum));
			            }

			}catch (Exception e)
			{
				
			}
			try
			{			System.out.println("O--" +   RiskSecurityInformationTable.findElement(By.xpath("//div[6]/div[1]/div/span")).getText());
                        System.out.println("I--"+XLS.getCellData(sheetName, "YearOfConstruction", rowNum));
                        if ((LocationDetailsTable.findElement(By.xpath("//div[6]/div[1]/div/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "YearOfConstruction", rowNum)))
                        {
			            	match_counter++;        
                        }
                        else
			            {
			            	System.out.println("Unmatched -- " +XLS.getCellData(sheetName, "YearOfConstruction", rowNum));
			            }

			}catch (Exception e)
			{
				
			}
			try
			{			System.out.println("O--" +   RiskSecurityInformationTable.findElement(By.xpath("//div[6]/div[2]/div/span")).getText());
                        System.out.println("I--"+XLS.getCellData(sheetName, "ExternalConstruction", rowNum));
                        if ((LocationDetailsTable.findElement(By.xpath("//div[6]/div[2]/div/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "ExternalConstruction", rowNum)))
                        {
			            	match_counter++;        
                        }
                        else
			            {
			            	System.out.println("Unmatched -- " +XLS.getCellData(sheetName, "ExternalConstruction", rowNum));
			            }


			}catch (Exception e)
			{
				
			}
			try
			{
				System.out.println("O--" +   RiskSecurityInformationTable.findElement(By.xpath("//div[6]/div[3]/div/span")).getText());
                System.out.println("I--"+XLS.getCellData(sheetName, "RoofConstruction", rowNum));
                if ((LocationDetailsTable.findElement(By.xpath("//div[6]/div[3]/div/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "RoofConstruction", rowNum)))
                {
	            	match_counter++;        
                }
                else
	            {
	            	System.out.println("Unmatched -- " +XLS.getCellData(sheetName, "RoofConstruction", rowNum));
	            }



			}catch (Exception e)
			{
				
			}
			
			try
			{
		    System.out.println("Risk information------------------------");
		   

			System.out.println("Protected--" +   RiskSecurityInformationTable.findElement(By.xpath("//div[9]/div[2]/div/span")).getText());
            System.out.println("O--"+XLS.getCellData(sheetName, "Protected", rowNum));

			
			}catch (Exception e)
			{
				
			}
			JavascriptExecutor jse = (JavascriptExecutor)getDriver();
	
					
			try
			{
				System.out.println("O--" +   RiskSecurityInformationTable.findElement(By.xpath("//div[11]/div/div/span")).getText());
                System.out.println("I--"+XLS.getCellData(sheetName, "NoTradesPeople?", rowNum));
                if ((RiskSecurityInformationTable.findElement(By.xpath("//div[11]/div/div/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "NoTradesPeople?", rowNum)))
                {
	            	match_counter++;        
                }
                else
	            {
	            	System.out.println("NoTradesPeople? -- ");
	            }
                jse.executeScript("window.scrollBy(0,50)", "");
				Thread.sleep(200);

			}catch (Exception e)
			{
				
			}
			
			try
			{
				System.out.println("O--" +   RiskSecurityInformationTable.findElement(By.xpath("//div[12]/div/div/span")).getText());
                System.out.println("I--"+XLS.getCellData(sheetName, "Basement?", rowNum));
                if ((RiskSecurityInformationTable.findElement(By.xpath("//div[12]/div/div/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "Basement?", rowNum)))
                {
	            	match_counter++;        
                }
                else
	            {
	            	System.out.println("Basement? -- ");
	            }
            
                jse.executeScript("window.scrollBy(0,50)", "");
				Thread.sleep(200);

			}catch (Exception e)
			{
				
			}
			
			try
			{
				System.out.println("O--" +   RiskSecurityInformationTable.findElement(By.xpath("//div[13]/div/div/span")).getText());
                System.out.println("I--"+XLS.getCellData(sheetName, "BoxGutters?", rowNum));
                if ((RiskSecurityInformationTable.findElement(By.xpath("//div[13]/div/div/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "BoxGutters?", rowNum)))
                {
	            	match_counter++;        
                }
                else
	            {
	            	System.out.println("BoxGutters?");
	            }
                jse.executeScript("window.scrollBy(0,50)", "");
				Thread.sleep(200);

			}catch (Exception e)
			{
				
			}
			
			try
			{
				System.out.println("O--" +   RiskSecurityInformationTable.findElement(By.xpath("//div[14]/div/div/span")).getText());
                System.out.println("I--"+XLS.getCellData(sheetName, "CurrentlyOccupied?", rowNum));
                if ((RiskSecurityInformationTable.findElement(By.xpath("//div[14]/div/div/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "CurrentlyOccupied?", rowNum)))
                {
	            	match_counter++;        
                }
                else
	            {
	            	System.out.println("CurrentlyOccupied?");
	            }
                jse.executeScript("window.scrollBy(0,50)", "");
				Thread.sleep(200);

			}catch (Exception e)
			{
				
			}
			
			try
			{
				System.out.println("O--" +   RiskSecurityInformationTable.findElement(By.xpath("//div[16]/div/div/span")).getText());
                System.out.println("I--"+XLS.getCellData(sheetName, "CurrentlyVacant?", rowNum));
                if ((RiskSecurityInformationTable.findElement(By.xpath("//div[16]/div/div/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "CurrentlyVacant?", rowNum)))
                {
	            	match_counter++;        
                }
                else
	            {
	            	System.out.println("CurrentlyVacant?");
	            }
                jse.executeScript("window.scrollBy(0,50)", "");
				Thread.sleep(200);

			}catch (Exception e)
			{
				
			}
			
			try
			{
				System.out.println("O--" +   RiskSecurityInformationTable.findElement(By.xpath("//div[17]/div/div/span")).getText());
                System.out.println("I--"+XLS.getCellData(sheetName, "HeritageListed?", rowNum));
                if ((RiskSecurityInformationTable.findElement(By.xpath("//div[17]/div/div/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "HeritageListed?", rowNum)))
                {
	            	match_counter++;        
                }
                else
	            {
	            	System.out.println("HeritageListed?");
	            }
                jse.executeScript("window.scrollBy(0,50)", "");
				Thread.sleep(200);

			}catch (Exception e)
			{
				
			}
			
			try
			{
				System.out.println("O--" +   RiskSecurityInformationTable.findElement(By.xpath("//div[18]/div/div/span")).getText());
                System.out.println("I--"+XLS.getCellData(sheetName, "RennovationNext12Months?", rowNum));
                if ((RiskSecurityInformationTable.findElement(By.xpath("//div[18]/div/div/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "RennovationNext12Months?", rowNum)))
                {
	            	match_counter++;        
                }
                else
	            {
	            	System.out.println("RennovationNext12Months?");
	            }
                jse.executeScript("window.scrollBy(0,50)", "");
				Thread.sleep(200);

			}catch (Exception e)
			{
				
			}
			
			try
			{
				System.out.println("O--" +   RiskSecurityInformationTable.findElement(By.xpath("//div[20]/div/div/span")).getText());
                System.out.println("I--"+XLS.getCellData(sheetName, "BurglarAlarm?", rowNum));
                if ((RiskSecurityInformationTable.findElement(By.xpath("//div[20]/div/div/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "BurglarAlarm?", rowNum)))
                {
	            	match_counter++;        
                }
                else
	            {
	            	System.out.println("BurglarAlarm?");
	            }
                jse.executeScript("window.scrollBy(0,50)", "");
				Thread.sleep(200);

			}catch (Exception e)
			{
				
			}
			
			try
			{
				System.out.println("O--" +   RiskSecurityInformationTable.findElement(By.xpath("//div[21]/div/div/span")).getText());
                System.out.println("I--"+XLS.getCellData(sheetName, "Smoke Detectors?", rowNum));
                if ((RiskSecurityInformationTable.findElement(By.xpath("//div[21]/div/div/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "Smoke Detectors?", rowNum)))
                {
	            	match_counter++;        
                }
                else
	            {
	            	System.out.println("NoTradesPeople?");
	            }
                jse.executeScript("window.scrollBy(0,50)", "");
				Thread.sleep(200);

			}catch (Exception e)
			{
				
			}
			
			try
			{
				System.out.println("O--" +   RiskSecurityInformationTable.findElement(By.xpath("//div[22]/div/div/span")).getText());
                System.out.println("I--"+XLS.getCellData(sheetName, "Deadlocks?", rowNum));
                if ((RiskSecurityInformationTable.findElement(By.xpath("//div[22]/div/div/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "Deadlocks?", rowNum)))
                {
	            	match_counter++;        
                }
                else
	            {
	            	System.out.println("SmokeDetectors?");
	            }
                jse.executeScript("window.scrollBy(0,50)", "");
				Thread.sleep(200);

			}catch (Exception e)
			{
				
			}
			
			try
			{
				System.out.println("O--" +   RiskSecurityInformationTable.findElement(By.xpath("//div[23]/div/div/span")).getText());
                System.out.println("I--"+XLS.getCellData(sheetName, "WindowLocks?", rowNum));
                if ((RiskSecurityInformationTable.findElement(By.xpath("//div[23]/div/div/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "WindowLocks?", rowNum)))
                {
	            	match_counter++;        
                }
                jse.executeScript("window.scrollBy(0,50)", "");
				Thread.sleep(200);

			}catch (Exception e)
			{
				
			}
			
			try
			{
				System.out.println("O--" +   RiskSecurityInformationTable.findElement(By.xpath("//div[24]/div/div/span")).getText());
                System.out.println("I--"+XLS.getCellData(sheetName, "Safe?", rowNum));
                if ((RiskSecurityInformationTable.findElement(By.xpath("//div[24]/div/div/span")).getText()).equalsIgnoreCase(XLS.getCellData(sheetName, "Safe?", rowNum)))
                {
	            	match_counter++;        
                }
                jse.executeScript("window.scrollBy(0,50)", "");
				Thread.sleep(200);

			}catch (Exception e)
			{
				
			}
			
			String matchcounter=Integer.toString(match_counter);
			
			XLS.setCellData(sheetName, "MatchCounter", rowNum, matchcounter);
			
			
			
		System.out.println("final matches --" +match_counter);
			
			
		
	}

	@Step
	public void click_edit_quote(String location) throws InterruptedException {
		
		int k=Integer.parseInt(location);
		Thread.sleep(1000);
		getDriver().findElement(By.id("selectProperty_"+k)).click();
	    Thread.sleep(2000);
		  
	    // Edit Quote Button
	   // getDriver().findElement(By.xpath(".//*[@id='subSideNavigationMainBody']/div/div/div[1]/div[1]/div/div/div[1]/div/span[3]/button")).click();
	    getDriver().findElement(By.xpath("//button[contains(text(),'Edit')]")).click();
	    Thread.sleep(4000);
		
	}

	@Step
	public void verifyProtectedFiledValue(String expectedText) {
		if (getDriver().findElement(By.xpath("//span[contains(text(),'Yes')][preceding-sibling::strong[contains(text(),'Protected')]]")).getText().equalsIgnoreCase(expectedText))
		{
			Assert.assertTrue("Protected field value is displayed as Expected The Text is:" +expectedText, true);
		} else {
			Assert.assertTrue("Protected field value is not displayed as Expected The Text is:" +expectedText, true);
		} 
	}

	@Step
	public void verifyFloodZoneValue(String EliasValue, String OverrideReason) {
		if (getDriver().findElement(By.xpath("//span[@data-bind='text: __getOverrideEliasRatingText']")).getText().equalsIgnoreCase(EliasValue))
		{
			Assert.assertTrue("Flood Zone field value is displayed as Expected The value is:" +EliasValue, true);
		} else {
			Assert.assertTrue("Flood Zone field value is not displayed as Expected The Text is:" +EliasValue, true);
		} 
		if (getDriver().findElement(By.xpath("//span[contains(text(),'ELIAS Not Mapped')]")).getText().equalsIgnoreCase(OverrideReason))
		{
			Assert.assertTrue("Override Reason is displayed as Expected The Text is:" +OverrideReason, true);
		} else {
			Assert.assertTrue("Override Reason is not displayed as Expected The Text is:" +OverrideReason, true);
		} 
		
	}
@Step
	public void verifyFieldSelectedByDefault(String fieldName) throws InterruptedException {
		getDriver().findElement(By.xpath("//a[span[contains(text(),'House and Contents')]]")).click();
		Thread.sleep(2000);
		WebElement element=getDriver().findElement(By.xpath("//div[@id='RiskLocation_0_FloodCoverage']//input[parent::label[span[contains(text(),'No')]]]"));
		boolean flag=(boolean) ((JavascriptExecutor) getDriver()).executeScript("arguments[0].prop('checked')", element);
		if (flag)
		{
			Assert.assertTrue("Flood coverage default selection is No ", true);
		} else {
			Assert.assertTrue("Flood coverage default selection is No ", false);
		} 

	}


}