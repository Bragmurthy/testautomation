package au.com.chubb.evolutionshowcase.pages.stepDefinitions.policyInformation;


import java.io.IOException;

import org.openqa.selenium.By;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote.riskLocation_calculatePremium_Steps;
import au.com.chubb.evolutionshowcase.pages.serenitySteps.policyInformation.policyInformation_misc_Steps;
import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Steps;

public class policyinformation_misc_SD extends PageObject {

	@Steps
	policyInformation_misc_Steps policyInformation;
	DynamicSeleniumFunctions dynamicseleniumfunctions;
	riskLocation_calculatePremium_Steps  calculatePremium;

	@And("^verify quote status is \"([^\"]*)\"$")
	public void verify_quote_status(String quote_status) throws Throwable {
		policyInformation.verify_quote_status(quote_status);
	}
	
	@And("^verify clientName is \"([^\"]*)\"$")
	public void verify_ClientName(String clientName) throws Throwable {
		policyInformation.verify_ClientName(clientName);
	}
	
	@And("^Verify \"([^\"]*)\" is not displayed in side navigation$")
	public void verify_side_navigation_bar_absence(String link_name) throws InterruptedException {
	
		policyInformation.verify_side_navigation_bar_absence(link_name);
	}
	
	@And("^Verify \"([^\"]*)\" is displayed in side navigation$")
	public void verify_side_navigation_bar_presence(String link_name) throws InterruptedException {
		policyInformation.verify_side_navigation_bar_presence(link_name);
	}
	
	@Then("^verify user is on Policy Information page$")
	public void verifyUserIsOnPolicyInfromationPage() throws InterruptedException {
		policyInformation.verifyUserIsOnPolicyInfromationPage();
	}
	
	@Then("^verify quote is editable$")
	public void verifyQuoteisEditable() throws InterruptedException, IOException{
		dynamicseleniumfunctions.verifyFieldEditable("UnitNo","Unti No");	
		getDriver().findElement(By.xpath("//button[@id='btnProceedNext']")).click();
		Thread.sleep(15000);		
		calculatePremium.click_button("Appraisals");
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//a[@id='property_0_AppraisalTypeSpecialInstructions']")).click();
		dynamicseleniumfunctions.verifyFieldEditable("AppraisalInstructions","Contact");
	}	
	
	@Then("^verify quote is editable in ebusiness potal$")
	public void verifyQuoteisEditableinebusinessportal() throws InterruptedException, IOException{
		getDriver().findElement(By.xpath("//input[@id='addressEditor_0_BuildingName']")).click();
		dynamicseleniumfunctions.verifyFieldEditable("BuildingName","Name");
	}	
	
	@Then("^click on OK button of client Already exists popup$")
	public void clicOnOK() throws InterruptedException {
		policyInformation.clicOnOK();
	}
	
	@Then("^verify \"([^\"]*)\" , \"([^\"]*)\" in client details should be non editable$")
	public void verifyclientdetailsarenoneditable(String ClientName, String ClientPhoneumber) throws InterruptedException, IOException{		
		policyInformation.verifyclientdetailsarenoneditable(ClientName,ClientPhoneumber);
	}	
	
	@Then("^verify policy address should be non editable")
	public void verifyPolicyAddressNonEditable() throws InterruptedException, IOException{		
		policyInformation.verifyPolicyAddressNonEditable();
		
	}	
	
	@And("^verify policy default values as UW in \"([^\"]*)\"")
	public void verifyAllDefaultDetailsofPolicyInformation(String testdata) throws InterruptedException, IOException{		
		policyInformation.verifyAllDefaultDetailsofPolicyInformation(testdata);
		
	}
	
}
