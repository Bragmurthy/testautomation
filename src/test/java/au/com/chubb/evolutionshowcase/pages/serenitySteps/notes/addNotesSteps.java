package au.com.chubb.evolutionshowcase.pages.serenitySteps.notes;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.server.handler.FindElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import au.com.chubb.evolutionshowcase.pages.objectModel.policyAdmin.policyAdminWelcomePage;
import au.com.chubb.evolutionshowcase.utilities.CommonFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class addNotesSteps extends PageObject {

	policyAdminWelcomePage policyadminwelcomepage;
	CommonFunctions common_functions;
	String root = System.getProperty("user.dir");
	public static Properties OR = null;
	
	public void addNotesandverifyNotes() throws InterruptedException {
		
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'model.title')]")).sendKeys("Automation Notes Title");
		getDriver().findElement(By.xpath("//textarea[contains(@data-bind,'model.text')]")).sendKeys("Automation Notes Text");
		getDriver().findElement(By.xpath("//button[contains(text(),'Action')]")).click();
		getDriver().findElement(By.xpath("//a[contains(text(),'Finish Note')]")).click();
		Thread.sleep(10000);
		
		String Notes_App = getDriver().findElement(By.className("odd")).getText();
		
		if(Notes_App.contains("Automation Notes Title"))
			
				Assert.assertTrue("Notes displayed", true);
		else
				Assert.assertTrue("Notes not displayed", false);		
		
	}
}