package au.com.chubb.evolutionshowcase.pages.stepDefinitions.indicativeQuote;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote.riskLocation_locationInfomation_Steps;
import au.com.chubb.evolutionshowcase.pages.serenitySteps.policyInformation.brokerDetails_Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

public class riskLocation_locationInfomation_SD {

	@Steps
	riskLocation_locationInfomation_Steps locationInformation;
	
	@And("^verify text \"([^\"]*)\" for location \"([^\"]*)\"$")
	public void verifyText(String expectedText, String riskLocation) throws Throwable {
		locationInformation.verifyText(expectedText, riskLocation);
	}

	@And("^search address \"([^\"]*)\" for Risk Location \"([^\"]*)\"$")
	public void address_search(String address, String riskLocation) throws Throwable {
		locationInformation.address_search(address, riskLocation);
	}
	
	@And("^search address with no google address picker \"([^\"]*)\" for Risk Location \"([^\"]*)\"$")
	public void address_searchNoGooglePicker(String address, String riskLocation) throws Throwable {
		locationInformation.address_searchNoGooglePicker(address, riskLocation);
	}
	
	@And("^verify below details are populated after address search for Risk Location \"([^\"]*)\"$")
	public void address_data_auto_populate(String riskLocation, DataTable arg1) throws Throwable {
		locationInformation.address_data_auto_populate( riskLocation, arg1);
	}

}
