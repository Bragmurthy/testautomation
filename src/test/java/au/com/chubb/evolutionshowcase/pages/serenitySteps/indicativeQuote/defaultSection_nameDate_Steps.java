package au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class defaultSection_nameDate_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public defaultSection_nameDate_Steps() throws IOException {
		/*
		 * OR = new Properties(); FileInputStream fo = new FileInputStream(root +
		 * "/ObjectRepository/landingPage.properties"); OR.load(fo);
		 */
	}

	@Step
	public void verify_navigate_URL(String URL) {

		String Current_URL = getDriver().getCurrentUrl();
		System.out.println("Hellkjksdsj");
		if (Current_URL.equalsIgnoreCase(URL)) {
			Assert.assertTrue("Page Navigation successful ", true);
		} else {
			Assert.assertTrue("Page Navigation successful ", false);

		}
	}

	@Step
	public void default_firstname(String firstname) {
		String firstname_APP = getDriver().findElement(By.id("ClientFirstName"))
				.getAttribute("value");
		System.out.println("First Name --  " +firstname_APP);
		if (firstname_APP.equalsIgnoreCase(firstname)) {
			Assert.assertTrue("First name successful displayed in indicative quote page " + firstname + " ", true);
		} else {
			Assert.assertTrue("First name doesnt match ", false);

		}

	}

	@Step
	public void default_surname(String surname) {
		String surname_APP = getDriver().findElement(By.id("ClientLastName"))
				.getAttribute("value");
		System.out.println("SurName -- "+surname_APP);

		if (surname_APP.equalsIgnoreCase(surname)) {
			Assert.assertTrue("Surname successful displayed in indicative quote page  " + surname + " ", true);
		} else {
			Assert.assertTrue("Surname doesnt match ", false);

		}

	}

	@Step
	public void default_dates(String effectiveDate, String expiryDate) {

		String effectiveDate_APP = getDriver()
				.findElement(By.id("EffectiveDate")).getAttribute("value");

		System.out.println("Effective Date App -- " +effectiveDate_APP);

		String expiryDate_APP = getDriver().findElement(By.id("ExpiryDate"))
				.getAttribute("value");
		System.out.println("Expiry Date --" +expiryDate_APP);

		if (effectiveDate_APP.equalsIgnoreCase(effectiveDate) && expiryDate_APP.equalsIgnoreCase(expiryDate)) {
			Assert.assertTrue("effectiveDate " + effectiveDate + " effectiveDate " + expiryDate
					+ " successfully displayed in indicative quote page", true);
		} else {
			Assert.assertTrue("Dates doesnt match ", false);

		}

	}

	@Step
	public void verifyUserIsOnNewQuotePage() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("txtClientId")));
	    Thread.sleep(3000);

		if (getDriver().findElement(By.name("txtClientId")).isDisplayed()) {
			Assert.assertTrue("User is On New Quote page", true);
		} else {
			Assert.assertTrue("User is not On New Quote page", false);
		}
	}

	@Step
	public void edit_name_date(DataTable arg1) {

		boolean date_section=false;
		List<List<String>> data = arg1.raw();
		getDriver().findElement(By.id("ClientFirstName")).clear();
		getDriver().findElement(By.id("ClientFirstName")).sendKeys(data.get(1).get(1));

		
		getDriver().findElement(By.id("ClientLastName")).clear();
		getDriver().findElement(By.id("ClientLastName")).sendKeys(data.get(2).get(1));

		
		try
		{
		getDriver().findElement(By.id("EffectiveDate")).clear();
		getDriver().findElement(By.id("EffectiveDate")).sendKeys(data.get(3).get(1));
		getDriver().findElement(By.id("EffectiveDate")).sendKeys(Keys.ENTER);
		
		
		getDriver().findElement(By.id("ExpiryDate")).clear();
		getDriver().findElement(By.id("ExpiryDate")).sendKeys(data.get(4).get(1));
		getDriver().findElement(By.id("ExpiryDate")).sendKeys(Keys.ENTER);
		
		}catch (Exception e)
		{
			date_section=true;
		}
		
		String FirstName_APP=getDriver().findElement(By.id("ClientFirstName")).getAttribute("value");
		String LastName_APP=getDriver().findElement(By.id("ClientLastName")).getAttribute("value");
		String EffectiveDate_APP=getDriver().findElement(By.id("EffectiveDate")).getAttribute("value");
		String ExpiryDate_APP=getDriver().findElement(By.id("ExpiryDate")).getAttribute("value");

		if (FirstName_APP.equalsIgnoreCase(data.get(1).get(1)) &&
				LastName_APP.equalsIgnoreCase(data.get(2).get(1)) &&
				date_section==true)
		{
			Assert.assertTrue("Broker is able to edit Name and not Date section in indicative quote page ", true);
		} else {
			Assert.assertTrue("Broker is able to edit Name and not Date section in indicative quote page ", false);
		}
		}
		
}