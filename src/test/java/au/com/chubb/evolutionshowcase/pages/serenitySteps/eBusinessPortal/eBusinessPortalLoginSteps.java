package au.com.chubb.evolutionshowcase.pages.serenitySteps.eBusinessPortal;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import au.com.chubb.evolutionshowcase.pages.objectModel.eBusinessPortal.eBusinessPortalLoginPage;
import au.com.chubb.evolutionshowcase.utilities.CommonFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class eBusinessPortalLoginSteps extends PageObject {

	static eBusinessPortalLoginPage ebusinessportalloginpage;
	static CommonFunctions common_functions;
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	
	public eBusinessPortalLoginSteps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/eBusinessPortal.properties");
		OR.load(fo);

	}



	@Step
	public static void launch_eBusiness_portal_app() throws InterruptedException {

		common_functions.launch_eBusiness_portal_app();
	}

	@Step
	public static void launch_SITWorkManager() throws InterruptedException {

		common_functions.launch_SITWorkManager();
	}
	
	@Step
	public void enter_company_id(String company_id) throws InterruptedException {

		ebusinessportalloginpage.enter_company_id(company_id);

	}

	@Step
	public void enter_user_id(String user_id) throws InterruptedException {

		ebusinessportalloginpage.enter_user_id(user_id);

	}
	
	
	@Step
	public void enter_password(String password) throws InterruptedException {

		ebusinessportalloginpage.enter_password(password);

	}

	@Step
	public void click_login() throws InterruptedException {

		ebusinessportalloginpage.click_login();
	}

	@Step
	public void enter_login_details(String test_data) throws InterruptedException {

	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "eBusinessLogin";
		Thread.sleep(1000);
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop: for (int row = 2; row <= Row_Count; row++) {

			System.out.println("test data --" + test_data);
			if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				break innerloop;
			}
		}
		
		getDriver().findElement(By.name(OR.getProperty("CompanyId_Name"))).sendKeys(XLS.getCellData(sheetName, "CompanyID", rowNum));
		
		getDriver().findElement(By.name(OR.getProperty("UserId_Name"))).sendKeys(XLS.getCellData(sheetName, "UserID", rowNum));
		getDriver().findElement(By.name(OR.getProperty("Password_Name"))).sendKeys(XLS.getCellData(sheetName, "Password", rowNum));
		
		WebElement elmnt =getDriver().findElement(By.name(OR.getProperty("Login_Name")));   
		JavascriptExecutor js = (JavascriptExecutor) getDriver();  
		js.executeScript("arguments[0].click();", elmnt);
		Thread.sleep(10000);
		//getDriver().findElement(By.name(OR.getProperty("Login_Name"))).click();

		
		
	}

}