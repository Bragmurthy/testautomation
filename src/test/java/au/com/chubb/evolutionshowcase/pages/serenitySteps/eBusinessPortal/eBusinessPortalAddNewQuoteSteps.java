package au.com.chubb.evolutionshowcase.pages.serenitySteps.eBusinessPortal;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import au.com.chubb.evolutionshowcase.pages.objectModel.eBusinessPortal.eBusinessPortalAddNewQuotePage;
import au.com.chubb.evolutionshowcase.utilities.CommonFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;


@RunWith(SerenityRunner.class)
public class eBusinessPortalAddNewQuoteSteps extends PageObject {
	String root = System.getProperty("user.dir");
	String sheetName = "QuoteData";
	String sheetNameQuote="IndicativeQuoteRiskLocation";
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
	static eBusinessPortalAddNewQuotePage ebusinessportaladdNewQuotepage;
	static CommonFunctions common_functions;

	@Step
	public void click_add_new_quote() throws InterruptedException {

		ebusinessportaladdNewQuotepage.click_add_new_quote();

	}
	
	@Step
	public void click_find_quote() throws InterruptedException {

		ebusinessportaladdNewQuotepage.click_find_quote();

	}
	
	@Step
	public void click_new_business() throws InterruptedException {
		ebusinessportaladdNewQuotepage.click_new_business();
		
	}


	@Step
	public void enter_new_quote_details(DataTable arg1) throws InterruptedException {

		ebusinessportaladdNewQuotepage.enter_new_quote_details(arg1);

	}
	
	@Step
	public void enter_policy_start_date_details(DataTable arg1) throws InterruptedException {

		ebusinessportaladdNewQuotepage.enter_policy_start_date_details(arg1);
	}	

	@Step
	public void click_add_risk_details() throws InterruptedException {

		ebusinessportaladdNewQuotepage.click_add_risk_details();

	}
	
	@Step
	public void click_edit_risk_details() throws InterruptedException {

		ebusinessportaladdNewQuotepage.click_edit_risk_details();

	}
	
	@Step
	public void click_view_risk_details() throws InterruptedException{
		ebusinessportaladdNewQuotepage.click_view_risk_details();
	}

	@Step
	public void verify_landing_URL(String URL) throws InterruptedException {
		// TODO Auto-generated method stub
		Thread.sleep(10000);
		System.out.println("I -- " +URL);
		String CurrentURL = getDriver().getCurrentUrl();
		System.out.println("O-- " +CurrentURL);

		if (CurrentURL.contains(URL)) {
			System.out.println("test Passed ");
			Assert.assertTrue("Landing Pgae is displayed successfully", true);
		} else {
			System.out.println("test Failed ");
			Assert.assertTrue("Landing Pgae is not displayed", false);
		}
	}
	@Step
	
	public void verify_Quote_status_is(String quoteStatus) throws Throwable {
		ebusinessportaladdNewQuotepage.verifyQuoteStatus(quoteStatus);
}
	
	@Step
	public void click_button(String button_name) throws InterruptedException {
		
		WebElement element=getDriver().findElement(By.name(button_name));
		if (button_name!="Convert Risk Details" || button_name!="Modify Risk Details" || button_name!="Terminate Risk Details" )
		{
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
		}
		
		element.click();
		Thread.sleep(50000);
	}
	
	@Step
	public void click_SaveAndAccept() throws InterruptedException {
		//getDriver().findElement(By.name()).click();
		WebElement element=getDriver().findElement(By.xpath("//input[@name='btnSaveAndAccept']"));
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='btnSaveAndAccept']")));
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", element);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[contains(text(),'successfully saved and accepted')]")));
		Thread.sleep(50000);
	}
	@Step
	public void click_onbutton(String button_name) throws InterruptedException {
		WebElement element=getDriver().findElement(By.xpath("//input[@value='"+button_name+"']"));
		if (button_name!="Convert Risk Details" || button_name!="Modify Risk Details" || button_name!="Terminate Risk Details" )
		{
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
		}
		
		element.click();
		Thread.sleep(50000);
	}
	
	@Step
	public void click_Delete_then_ok_button() throws InterruptedException, Exception {
		Thread.sleep(5000);	
		String directory=System.getProperty("user.dir");
		getDriver().findElement(By.xpath("//input[@value='Delete']")).sendKeys(Keys.ENTER);	
		Runtime.getRuntime().exec(directory+"\\drivers\\SubmitReferral.exe");
		
		Robot robot=new Robot();
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(5000);
	}
@Step
	public void click_ButtonByID(String button_id) throws InterruptedException {
		getDriver().findElement(By.id(button_id)).click();
		Thread.sleep(30000);
	}

@Step
public void validate_no_justification_dropdown() throws InterruptedException {
	try{if ((getDriver().findElement(By.id("vacEdit_Justification")).isDisplayed())) {
		System.out.println("element is displayed so validation got failed");
		Assert.assertTrue(false);
		}
		else{
			System.out.println("element is not displayed so validation passed");
			Assert.assertTrue(true);
		}
	}
	catch(Exception e)
	{
		System.out.println("element is not displayed so validation pass");
		Assert.assertTrue(true);
	}
	Thread.sleep(20000);
}
@Step
public void validate_no_location_id() throws InterruptedException {
	try{if ((getDriver().findElement(By.xpath("//*[contains(text(),'Loacation id:')]")).isDisplayed())) {
		System.out.println("Locatin id element is displayed so validation got failed");
		Assert.assertTrue(false);
		}
		else{
			System.out.println("Locatin id element is not displayed so validation passed");
			Assert.assertTrue(true);
		}
	}
	catch(Exception e)
	{
		System.out.println("Locatin id element is not displayed so validation pass");
		Assert.assertTrue(true);
	}
	Thread.sleep(20000);
}

@Step
public void click_by_text(String button_text) throws InterruptedException {
	Thread.sleep(10000);
	getDriver().findElement(By.xpath("//*[text()='"+button_text+"']")).click();
	System.out.println("clicked on it");
	Thread.sleep(10000);
}

@Step
public void acceptAlert() throws InterruptedException, AWTException, IOException {
	Thread.sleep(10000);	
	String directory=System.getProperty("user.dir");
	getDriver().findElement(By.xpath("//input[@name='btnManualRate']")).sendKeys(Keys.ENTER);	
	Runtime.getRuntime().exec(directory+"\\drivers\\SubmitReferral.exe");
	Thread.sleep(25000);
	
}

@Step
public void acceptFrompopup() throws InterruptedException, AWTException, IOException {
	Thread.sleep(10000);
	getDriver().findElement(By.xpath("//input[text()='OK']")).click();
	Thread.sleep(25000);
	
}


@Step
public void acceptAcceptanceAlert() throws InterruptedException, AWTException, IOException {
	Thread.sleep(10000);	
	String directory=System.getProperty("user.dir");
	getDriver().findElement(By.xpath("//input[@name='btnAccept']")).sendKeys(Keys.ENTER);	
	Runtime.getRuntime().exec(directory+"\\drivers\\SubmitReferral.exe");
	Thread.sleep(25000);
	
}

@Step
public void verify_transaction_channelshouldbroker(String text)throws InterruptedException{
	
	String testisitBroker = ebusinessportaladdNewQuotepage.verify_transaction_channel();
	if (testisitBroker.equals(text))
	{
		Assert.assertTrue("Transaction Done by Broker", true);
	}
	else
	{
		System.out.println("test Failed ");
		Assert.assertTrue("Transaction not Done by Broker", false);
	}
}

@Step
public void verify_Quote_Search(String quotestatus, String testdata) throws InterruptedException {
	
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
	String sheetName = "IndicativeQuoteRiskLocation";
	Thread.sleep(1000);
	System.out.println("control reached enter risk location details" );

	int Row_Count = XLS.getRowCount(sheetName);
	int rowNum = 0;

	innerloop: 
		for (int row = 2; row <= Row_Count; row++) {

			if (testdata.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				System.out.println("test data --" + testdata);
				System.out.println("try to come out of loop "+rowNum);
				break innerloop;
			}
		}

	WebElement element = getDriver().findElement(By.xpath("//*[@id='policy-search-result_wrapper']/div[2]/div/table/tbody/tr/td[4]"));
	//WebElement element = getDriver().findElement(By.xpath("//span[contains(text(), 'Status:')]"));
	String quoteStatus_APP = element.getText();
	
	if (quoteStatus_APP.contains(quotestatus))
	{
		Assert.assertTrue("Quote status is displayed as expected in UW view  i.e "+quotestatus, true);
	}
	else
	{
		System.out.println("test Failed ");
		Assert.assertTrue("Quote status is not displayed as expected in UW view actual is"+quoteStatus_APP, false);
	}
}

public void waitforQuoteToUnlock() throws InterruptedException {
	Thread.sleep(500000);
	
}

	@Step
	public void save_policy_quote_details(String testflow) throws InterruptedException {
		String root = System.getProperty("user.dir");
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "QuoteData";
		Thread.sleep(1000);
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {
		
			if (testflow.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				System.out.println("test data --" + testflow);
				System.out.println("try to come out of loop "+rowNum);
				break innerloop;
			}
		}
		
		String clientName=getDriver().findElement(By.xpath("//span[contains(@data-bind, 'Client.Item.FullName')]")).getText();
												  
		String clientDOB=getDriver().findElement(By.xpath("//span[contains(@data-bind, 'Client.Item.DateOfBirth')]")).getText();
		String clientPhone=getDriver().findElement(By.xpath("//span[contains(@data-bind, 'Client.Item.Contact.PhoneNumber')]")).getText();
		String clientEmail=getDriver().findElement(By.xpath("//span[contains(@data-bind, 'Client.Item.Contact.Email')]")).getText();
		String clientRetired=getDriver().findElement(By.xpath("//span[contains(@data-bind, 'Client.Item.Retired')]")).getText();
		String clientIndustry=getDriver().findElement(By.xpath("//span[contains(@data-bind, 'Client.IndustryText')]")).getText();
		String clientOccupation=getDriver().findElement(By.xpath("//span[contains(@data-bind, 'Client.Item.OccupationText')]")).getText();
		String clientEmployer=getDriver().findElement(By.xpath("//span[contains(@data-bind, 'Client.Item.EmployerName')]")).getText();
	
		XLS.setCellData(sheetName, "clientName", rowNum,clientName);
		XLS.setCellData(sheetName, "clientDOB", rowNum,clientDOB);
		XLS.setCellData(sheetName, "clientPhone", rowNum,clientPhone);
		XLS.setCellData(sheetName, "clientEmail", rowNum,clientEmail);
		XLS.setCellData(sheetName, "clientRetired", rowNum,clientRetired);
		XLS.setCellData(sheetName, "clientIndustry", rowNum,clientIndustry);
		XLS.setCellData(sheetName, "clientOccupation", rowNum,clientOccupation);
		XLS.setCellData(sheetName, "clientEmployer", rowNum,clientEmployer);
			
		String namedInsuredName=getDriver().findElement(By.xpath("//span[contains(@data-bind,'item().fullName()')]")).getText();
		String namedInsuredDOB=getDriver().findElement(By.xpath("//span[contains(@data-bind,'item().dateOfBirth')]")).getText();
		String namedInsuredRetired=getDriver().findElement(By.xpath("//span[contains(@data-bind,'item().retired()')]")).getText();
		String namedInsuredIndustry=getDriver().findElement(By.xpath("//span[contains(@data-bind,'industryText')]")).getText();	
		String namedInsuredOccupation=getDriver().findElement(By.xpath("//span[contains(@data-bind,'item().occupationText')]")).getText();
		String namedInsuredEmployer=getDriver().findElement(By.xpath("//span[contains(@data-bind,'item().employerName')]")).getText();
		String namedInsuredPhone=getDriver().findElement(By.xpath("//span[contains(@data-bind,'item().contact.phoneNumber(), ')]")).getText();
		String namedInsuredEmail=getDriver().findElement(By.xpath("//span[contains(@data-bind,'item().contact.email()')]")).getText();
		//String namedInsuredDate=getDriver().findElement(By.xpath("//span[contains(@data-bind,'createDate')]")).getText();	
		
		XLS.setCellData(sheetName, "namedInsuredName", rowNum,namedInsuredName);
		XLS.setCellData(sheetName, "namedInsuredDOB", rowNum,namedInsuredDOB);
		XLS.setCellData(sheetName, "namedInsuredRetired", rowNum,namedInsuredRetired);
		XLS.setCellData(sheetName, "namedInsuredIndustry", rowNum,namedInsuredIndustry);
		XLS.setCellData(sheetName, "namedInsuredOccupation", rowNum,namedInsuredOccupation);
		XLS.setCellData(sheetName, "namedInsuredEmployer", rowNum,namedInsuredEmployer);
		XLS.setCellData(sheetName, "namedInsuredPhone", rowNum,namedInsuredPhone);
		XLS.setCellData(sheetName, "namedInsuredEmail", rowNum,namedInsuredEmail);
		//XLS.setCellData(sheetName, "namedInsuredDate", rowNum,namedInsuredDate);

		String brokerOrg=getDriver().findElement(By.xpath("//span[contains(@data-bind,'organizationName')]")).getText();
		String brokerAddress=getDriver().findElement(By.xpath("//span[contains(@data-bind,'streetCityAddress')]")).getText();
		String brokerCountry=getDriver().findElement(By.xpath("//span[contains(@data-bind,'countryName')]")).getText();
		String brokerContact=getDriver().findElement(By.xpath("//span[contains(@data-bind,'surname()')]")).getText();	
		String brokerEmail=getDriver().findElement(By.xpath("//span[contains(@data-bind,'email')]")).getText();
		String brokerPhone=getDriver().findElement(By.xpath("//span[contains(@data-bind,'formatInput: phoneNumber')]")).getText();
		String brokerMobile=getDriver().findElement(By.xpath("//span[contains(@data-bind,'formatInput: mobileNumber')]")).getText();
		
		XLS.setCellData(sheetName, "brokerOrg", rowNum,brokerOrg);
		XLS.setCellData(sheetName, "brokerAddress", rowNum,brokerAddress);
		XLS.setCellData(sheetName, "brokerCountry", rowNum,brokerCountry);
		XLS.setCellData(sheetName, "brokerContact", rowNum,brokerContact);
		XLS.setCellData(sheetName, "brokerEmail", rowNum,brokerEmail);
		XLS.setCellData(sheetName, "brokerPhone", rowNum,brokerPhone);
		XLS.setCellData(sheetName, "brokerMobile", rowNum,brokerMobile);
		
		

//		System.out.println("date values are"+dateElement.getAttribute("value").equals(date));
//		System.out.println("loss type values are"+LossTypeElement.getFirstSelectedOption().getText());
//		System.out.println("amount values are"+paidAmountElement.getAttribute("value").equals(amount));
//		System.out.println("loss desc values are"+lossDescElement.getAttribute("value"));
	}
	@Step
	public void saveQuoteNumber(String test_data) throws InterruptedException {
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop:
			for (int row = 2; row <= Row_Count; row++) {

			System.out.println("test data --" + test_data);
			if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				break innerloop;
			}
		}
		String quoteNumber=getDriver().findElement(By.xpath("//td[input[@name='txtInsurerQuoteNo']]")).getText();
		XLS.setCellData(sheetNameQuote, "QuoteNumber", rowNum,"'"+quoteNumber+"'");
	}
@Step
	public void SavetoReferenceNumber(String test_data) throws InterruptedException {
		
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop:
			for (int row = 2; row <= Row_Count; row++) {

			System.out.println("test data --" + test_data);
			if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				break innerloop;
			}
		}
		String quoteNumber=getDriver().findElement(By.xpath("//td[input[@name='txtInsurerQuoteNo']]")).getText();
		XLS.setCellData(sheetNameQuote, "Quote_Reference_Number", rowNum,""+quoteNumber+"");
		
	}
@Step
public void verifyPolicyStatus(String policystatus) {
	// TODO Auto-generated method stub
	WebElement element = getDriver().findElement(By.xpath("//td[contains(text(), 'Endorsement')]"));
	String PolicyStatus_APP= element.getText();
	System.out.println(PolicyStatus_APP);
	
	if(policystatus.equalsIgnoreCase(PolicyStatus_APP))
	{
		Assert.assertEquals(policystatus, true);
		System.out.println("Policy status displayed is correct "+policystatus);
		
	}else
	{
		Assert.assertEquals(policystatus, false);
		System.out.println("Policy status displayed is not correct "+PolicyStatus_APP);
		
	}
	
	}

@Step
public void verify_policy_period_error_message(String errorType) throws Exception {
	Thread.sleep(5000);
	String errorMessage = getDriver().findElement(By.xpath("//*[@class='ColErrData']/p")).getText();
	System.out.println(errorMessage);
	String expected_error= null;
	switch(errorType){
		case "policy date":    
			expected_error ="Policy period must be between nine and twelve months";
			break; 
		case "quote validity expire":    
			expected_error ="Quote valid days expired, please re-edit insurer product. Last Rated Date";
			break;
		case "policy start date":    
			expected_error ="Please update the Policy Effective Date to today's date or later before proceeding to Bind";
			 break;   
		    
		default:     
		 System.out.println("not matching with any of the error message");
	}
	
//	String policy_date_message = "Policy period must be between nine and twelve months";
//	String policy_valid_expiredate_message ="Quote valid days expired, please re-edit insurer product. Last Rated Date";
//	String policy_Start_date_message ="Please update the Policy Effective Date to today's date or later before proceeding to Bind";
	
//	String policy_date_message ="Please update the Policy Effective Date to today's date or later before proceeding to Bind";
	if(errorMessage.contains(expected_error))
	{
		Assert.assertTrue(true);
		System.out.println(expected_error+ "  error message displayed");
		
	}else
	{
		Assert.assertTrue(false);
		System.out.println(expected_error+ " error message not displayed");
		
	}
	
	}
@Step
public void waitForTimeInMinutes(String text) throws InterruptedException {
	int timeToWait=Integer.parseInt(text);
	Thread.sleep(TimeUnit.MINUTES.toMillis(timeToWait));
}

@Step
public void click_Cancel_then_ok_button() throws InterruptedException, Exception {
	Thread.sleep(5000);	
	String directory=System.getProperty("user.dir");
	getDriver().findElement(By.xpath("//input[@value='Cancel']")).sendKeys(Keys.ENTER);	
	Runtime.getRuntime().exec(directory+"\\drivers\\SubmitReferral.exe");
	
	Robot robot=new Robot();
	robot.keyPress(KeyEvent.VK_TAB);
	robot.keyPress(KeyEvent.VK_TAB);
	robot.keyPress(KeyEvent.VK_ENTER);
	Thread.sleep(5000);
}

public void clear_mandatorydetails_in_Appraisalcontactspage() {
	// TODO Auto-generated method stub
	try{
	getDriver().findElements(By.xpath("//input[@id='appraisalContact_Name']")).clear();		
	Thread.sleep(1000);
	getDriver().findElements(By.xpath("//input[@id='appraisalContact_Mobile']")).clear();
	Thread.sleep(1000);
	}
	catch(Exception e)
	{
		System.out.println("Unable find fields"+e.getMessage());
	}
}

}

