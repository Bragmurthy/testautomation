package au.com.chubb.evolutionshowcase.pages.stepDefinitions.appraisals;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.appraisals.appraisals_Steps;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

public class appraisals_SD {

	@Steps
	appraisals_Steps appraisals;

	@And("^enter appraisal details for location \"([^\"]*)\" based on \"([^\"]*)\"$")
	public void enter_appraisal_details(String location, String testData) throws Throwable {
		appraisals.enter_appraisal_details(location, testData);
	}

	@And("^click proceed with Quote in appraisals page$")
	public void click_proceed_with_Quote() throws Throwable {
		appraisals.click_proceed_with_Quote();
	}
	
	@And("^verify default value for Appraisal type for Risk Location \"([^\"]*)\" is \"([^\"]*)\"$")
	public void verify_appraisal_type(String Location, String AppraisalType) throws Throwable {
		appraisals.verify_appraisal_type(Location, AppraisalType);
	}
	

}
