package au.com.chubb.evolutionshowcase.pages.objectModel.eBusinessPortal;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import net.serenitybdd.core.pages.PageObject;

public class eBusinessPortalFindCustomerPage extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;
	String sheetName = "QuoteData";
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");

	public eBusinessPortalFindCustomerPage() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/eBusinessPortal.properties");
		OR.load(fo);
}
		public void enterSearchCriteria(String searchCriteria) {
		getDriver().findElement(By.xpath("//input[@name='txtEntityId']")).sendKeys(searchCriteria);
}
		public void verifyClientSearchResult(String searchCriteria) throws InterruptedException {
			
			getDriver().findElement(By.xpath("//input[@name='btnSearch']")).click();;
			Thread.sleep(5000);
			if (getDriver().findElement(By.xpath("//td[contains(text(),'"+searchCriteria+"')]")).isDisplayed())
			{
					Assert.assertTrue("Customer Search is Successfull for customer ", true);
			} else {
				Assert.assertTrue("Customer Search is not Succesfull for customer ", false);
			}
		}

}