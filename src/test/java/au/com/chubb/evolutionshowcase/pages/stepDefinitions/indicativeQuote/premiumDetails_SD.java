package au.com.chubb.evolutionshowcase.pages.stepDefinitions.indicativeQuote;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote.premiumDetails_Steps;
import au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote.riskLocation_calculatePremium_Steps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class premiumDetails_SD {
	
	@Steps
	premiumDetails_Steps  premiumetailsSteps;

	@Then("^save Premium Details for \"([^\"]*)\"$")
	public void save_Premium_Details_for(String scenario) throws Throwable {
		premiumetailsSteps.savePremiumetails(scenario);
	}
	@And("^verify text \"([^\"]*)\" is displayed$")
	public void verifyWorkersCompensationText(String expectedText) throws Throwable {
	premiumetailsSteps.verifyWorkersCompensationText(expectedText);
	}

	
}