package au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class landingPage_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public landingPage_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/landingPage.properties");
		OR.load(fo);

	}

	@Step
	public void click_indicative_quote() {

		getDriver().findElement(By.id("")).click();

	}

	@Step
	public void click_full_quote() {

		getDriver().findElement(By.id("")).click();

	}

	@Step
	public void verify_navigate_URL(String URL) {

		String Current_URL = getDriver().getCurrentUrl();

		if (Current_URL.equalsIgnoreCase(URL)) {
			Assert.assertTrue("Page Navigation successful ", true);
		} else {
			Assert.assertTrue("Page Navigation successful ", false);

		}
	}

}