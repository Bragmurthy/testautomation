package au.com.chubb.evolutionshowcase.pages.stepDefinitions.binding;


import au.com.chubb.evolutionshowcase.pages.serenitySteps.binding.binding_Steps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class binding_SD {

	@Steps
	binding_Steps binding_Steps;


	@Given("^verify user is able to select deductible option for \"([^\"]*)\"$")
	public void verify_user_is_able_to_select_deductible_option_for(String value) throws Throwable {
		binding_Steps.verifyUserAbleToSelect_Deductible(value);
	}

	@Given("^select confirmation checkbox$")
	public void selectConfirmation_Checkbox() throws Throwable {
		binding_Steps.selectConfirmation_Checkbox();
	}
	@Given("^verify details are saved in Binding page$")
	public void verifyDetails_Saved() throws Throwable {
		binding_Steps.verifyDetails_Saved();
	}


	@Given("^\"([^\"]*)\" button should not be available in Risk/Coverage, Loss History, Premium Summary pages$")
	public void button_should_not_be_available_in_Risk_Coverage_Loss_History_Premium_Summary_pages(String arg1) throws Throwable {
		binding_Steps.verifySaveAndExitNotDisplayed();
	}
	
	@Given("^Save & Exit button will be available in Policy Information, Interested Parties, Appraisal Contact and Binding pages\\.$")
	public void save_Exit_button_will_be_available_in_Policy_Information_Interested_Parties_Appraisal_Contact_and_Binding_pages() throws Throwable {
		binding_Steps.verifySaveAndExitDisplayed();
	}
	
	@Then("^static text \"([^\"]*)\" should be displayed$")
	public void static_text_should_be_displayed(String expectedText) throws Throwable {
		binding_Steps.verifyTextPleaseNote(expectedText);
		}
}
