package au.com.chubb.evolutionshowcase.pages.stepDefinitions.ratingSummary;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.ratingSummary.ratingSummary_Steps;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

public class ratingSummary_SD {

	@Steps
	ratingSummary_Steps ratingSummary;

	@And("^retrive values from rating summary page$")
	public void retrieve_values_rating_summary() throws Throwable {
		ratingSummary.retrieve_values_rating_summary();
	}

	
	@And("^modify rating summary details for location \"([^\"]*)\" based on \"([^\"]*)\"$")
	public void modify_rating_summary_details(String location, String testData) throws Throwable {
		ratingSummary.modify_rating_summary_details(location, testData);
	}

	@And("^click proceed with Quote in rating summary page$")
	public void click_proceed_with_Quote() throws Throwable {
		ratingSummary.click_proceed_with_Quote();
	}

	
	
	
}
