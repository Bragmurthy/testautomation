package au.com.chubb.evolutionshowcase.pages.serenitySteps.policyInformation;


import java.util.List;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class policyAddress_Steps extends PageObject {

	@Step
	public void modify_policy_address(String policy_Address) throws InterruptedException {
		
		getDriver().findElement(By.id("addressEditor_0_Search")).sendKeys(policy_Address);
		Thread.sleep(1000);
		getDriver().findElement(By.id("addressEditor_0_Search")).sendKeys(Keys.DOWN);
		Thread.sleep(1000);
		getDriver().findElement(By.id("addressEditor_0_Search")).sendKeys(Keys.TAB);
		Thread.sleep(3000);

	}
@Step
	public void verifyAddressEdited(String policy_Address) {
		
	String streetName_App=getDriver().findElement(By.xpath("//input[@id='addressEditor_0_StreetNo']")).getText();
	String streetNameEntgered=policy_Address.split("")[0];

	if(streetName_App.equalsIgnoreCase(streetNameEntgered)){
		Assert.assertTrue("Address google search function enabled", true);}
	else
	{	Assert.assertTrue("Address google search function not enabled", false);}
}
@Step
public void enterDataInMandatoryFields(String yearOfConstruction, String externalConstruction,String buildingSumInsured, String locationNumber) throws InterruptedException {
	
	int riskLocationNumber=Integer.parseInt(locationNumber);
	getDriver().findElement(By.xpath("//input[@id='YearOfConstruction']")).sendKeys(yearOfConstruction);
	getDriver().findElement(By.xpath("//input[@id='YearOfConstruction']")).sendKeys(Keys.DOWN);
	Thread.sleep(1000);
	getDriver().findElement(By.xpath("//input[@id='YearOfConstruction']")).sendKeys(Keys.TAB);
	
	new Select(getDriver().findElement(By.xpath("//select[@id='property_"+riskLocationNumber+"_ExternalConstruction']"))).selectByVisibleText(externalConstruction);
	
	getDriver().findElement(By.xpath("//input[@id='RiskLocation_"+riskLocationNumber+"_BuildingSumInsured']")).sendKeys(buildingSumInsured);
}
@Step
public void verifyDataInMandatoryFields(String yearOfConstruction, String externalConstruction,String buildingSumInsured, String locationNumber) throws InterruptedException {
	
	int riskLocationNumber=Integer.parseInt(locationNumber);
	String yearOfConstruction_APP=getDriver().findElement(By.xpath("//input[@id='YearOfConstruction']")).getAttribute("value");
	String construction_APP=new Select(getDriver().findElement(By.xpath("//select[@id='property_"+riskLocationNumber+"_ExternalConstruction']"))).getFirstSelectedOption().getText();
	String buildingSI_APP=getDriver().findElement(By.xpath("//input[@id='RiskLocation_"+riskLocationNumber+"_BuildingSumInsured']")).getAttribute("value");;
	
	if(yearOfConstruction.equalsIgnoreCase(yearOfConstruction_APP)&& externalConstruction.equalsIgnoreCase(construction_APP) &&buildingSumInsured.equalsIgnoreCase(buildingSI_APP)){
		Assert.assertTrue("Fileds are Saved Successfully after clicking on Save and Exit", true);}
	else
	{	Assert.assertTrue("Fileds are not Saved Successfully after clicking on Save and Exit", false);}
}
@Step
public void verifyAllDefaultValues(String riskLocation_identifier) {
	int locationIdentifier=Integer.parseInt(riskLocation_identifier);
	String UnitNo_APP=getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_UnitNo")).getAttribute("value");
	String StreetNo_APP=getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_StreetNo")).getAttribute("value");
	String StreetName_APP=getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_StreetName")).getAttribute("value");
	String BuildingName_APP=getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_BuildingName")).getAttribute("value");
	String City_APP=getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_City")).getAttribute("value");
	String PostCode_APP=getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_PostCode")).getAttribute("value");
	Select countryDropdown=new Select(getDriver().findElement(By.xpath("//select[@id='addressEditor_"+locationIdentifier+"_Country']")));
	String Country_APP=countryDropdown.getFirstSelectedOption().getText();
	String State_APP=getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_State")).getAttribute("value");
			
	if (StreetNo_APP.equalsIgnoreCase("") &&
					StreetName_APP.equalsIgnoreCase("") &&
									City_APP.equalsIgnoreCase("") &&
											PostCode_APP.equalsIgnoreCase("") &&
											UnitNo_APP.equalsIgnoreCase("") &&
											BuildingName_APP.equalsIgnoreCase("") &&
													Country_APP.equalsIgnoreCase("") &&
															State_APP.equalsIgnoreCase(""))
															{
		                                                            Assert.assertTrue("Risk Location " +riskLocation_identifier+"details are blank by default", true);
															} else {
																Assert.assertTrue("Risk Location " +riskLocation_identifier+"details are not blank by default", false);

															}


}
@Step
public void clickOnContinueFullQuote() throws InterruptedException {
	
	getDriver().findElement(By.xpath("//button[contains(text(),'Continue Full Quote')]")).click();
	Thread.sleep(50000);
	
//	WebDriverWait wait = new WebDriverWait(getDriver(), 300);
//	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Update Client')]")));
//	Thread.sleep(1000);
//	getDriver().findElement(By.xpath("//button[contains(text(),'OK')]")).click();
//	Thread.sleep(1000);
}

	
}

