package au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class riskLocation_calculatePremium_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public riskLocation_calculatePremium_Steps() throws IOException {
		/*
		 * OR = new Properties(); FileInputStream fo = new FileInputStream(root +
		 * "/ObjectRepository/landingPage.properties"); OR.load(fo);
		 */
	}
	
	@Step
	public void verify_quote_offered_page() throws InterruptedException {
		if(getDriver().findElement(By.xpath("//*[contains(text(),'Quote No. ')]")).isDisplayed()){
			Assert.assertTrue("Navigated to quote offered page", true); 
		}
		else
			Assert.assertTrue("Navigated to quote offered page", false);

	}
	
	
	@Step
	public void click_button(String button_name) throws InterruptedException {
		Thread.sleep(1000);
		
		WebElement button = getDriver().findElement(By.xpath("//*[contains(text(),('" + button_name + "'))]"));
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", button);
		Thread.sleep(20000);
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", button);		
		//getDriver().findElement(By.xpath("//*[contains(text(),('" + button_name + "'))]")).click();
		Thread.sleep(50000);

		// disabled alert in security settings by adding to trusted sited in IE
		// http://apevodweb121.aceins.com
		// http://horizonapi-dev-evolution.azurewebsites.net
	}
	
	@Step
	public void click_SaveAndExit_Button() throws InterruptedException {
		Thread.sleep(1000);
		WebElement button = getDriver().findElement(By.xpath("//*[contains(@data-bind,'saveExit')]"));
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", button);
		Thread.sleep(2000);
				
		getDriver().findElement(By.xpath("//*[contains(@data-bind,'saveExit')]")).click();
		Thread.sleep(35000);
		try {
			Alert alert = getDriver().switchTo().alert();
			alert.accept();
			Thread.sleep(10000);
		} catch (Exception e) {

		}
		try {
			Alert alert = getDriver().switchTo().alert();
			alert.accept();
			Thread.sleep(10000);
		} catch (Exception e) {

		}
		// disabled alert in security settings by adding to trusted sited in IE
		// http://apevodweb121.aceins.com
		// http://horizonapi-dev-evolution.azurewebsites.net

	}

	@Step
	public void verftyNoQuoteValidationMessage() {
		try{
		String actual=getDriver().findElement(By.xpath("//p[contains(text(),'Our system indicates that we have an existing record for this client/property')]")).getText(); 
		if(actual.equals("Our system indicates that we have an existing record for this client/property"))
			Assert.assertFalse("Quote Validation Message popup is not displayed", false); 
		
		else
			Assert.assertFalse("Quote Validation Message popup is displayed", true);
		}catch(Exception e)
		{
			Assert.assertFalse("Quote Validation Message popup is not displayed", false);
		}
		
	}
	
	@Step
	public void click_button_from_popup(String button_name) throws InterruptedException {
		Thread.sleep(1000);
		getDriver().findElement(By.xpath("//button[text()='" + button_name + "']")).click();
		Thread.sleep(10000);
//		WebElement button = getDriver().findElement(By.xpath("//button[text()='" + button_name + "']"));
//		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", button);
//		Thread.sleep(2000);
//	
//
//		getDriver().findElement(By.xpath("//*[contains(text(),('" + button_name + "'))]")).click();
//		Thread.sleep(25000);
//		if (button_name.equalsIgnoreCase("Continue Full Quote") || button_name.equalsIgnoreCase("Calculate Premium")
//				|| button_name.equalsIgnoreCase("Save and Exit")) {
//			Thread.sleep(25000);
//		}
		// disabled alert in security settings by adding to trusted sited in IE
		// http://apevodweb121.aceins.com
		// http://horizonapi-dev-evolution.azurewebsites.net

	}
	
	
	@Step
	public void verify_premium_ebusiness_portal(String button_name) throws InterruptedException {

		String Base_Premium_APP = getDriver()
				.findElement(By.xpath(".//*[@id='indicativeQuote']/div/div[8]/div[1]/table/tbody/tr[1]/td[2]/a"))
				.getText();
		String Total_Premium_APP = getDriver()
				.findElement(By.xpath(".//*[@id='indicativeQuote']/div/div[8]/div[1]/table/tbody/tr[3]/td[2]/a"))
				.getText();

		System.out.println("Premium APP " + Base_Premium_APP);
		String Evolution_Base_Premium = Base_Premium_APP.substring(1);
		String Evolution_Total_Premium = Total_Premium_APP.substring(1);
		System.out.println("Evolution Base Premium " + Evolution_Base_Premium);
		System.out.println("Evolution Total Premium " + Evolution_Total_Premium);
		getDriver().findElement(By.xpath("//*[contains(text(),('" + button_name + "'))]")).click();
		Thread.sleep(12000);
		try {
			Alert alert = getDriver().switchTo().alert();
			alert.accept();
			Thread.sleep(10000);
		} catch (Exception e) {

		}

		String eBusiness_Base_Premium = getDriver().findElement(By.name("txtPremium")).getAttribute("value");
		System.out.println("eBusiness Base " + eBusiness_Base_Premium);
		WebElement eBusinessTotalPremium = getDriver().findElement(By.xpath("//*[contains(text(),('Grand Total'))]"));
		WebElement Parent_tr = eBusinessTotalPremium.findElement(By.xpath("./.."));
		String eBusiness_Total_Premium = getDriver().findElement(By.className("ColFixedTotal")).getText();
		System.out.println("eBusiness Total " + eBusiness_Total_Premium);

		if (eBusiness_Base_Premium.equalsIgnoreCase(Evolution_Base_Premium)
				&& (eBusiness_Total_Premium.equalsIgnoreCase(Evolution_Total_Premium))) {

			Assert.assertTrue("Base " + eBusiness_Base_Premium + " and Total Premium " + eBusiness_Total_Premium
					+ " are same in eBusiness and Evolution", true);
		} else {
			Assert.assertTrue("Base " + eBusiness_Base_Premium + " and Total Premium " + eBusiness_Total_Premium
					+ " are same in eBusiness and Evolution", false);

		}

	}

	@Step
	public void verify_quote_status_ebusiness_portal(String Quote_Status) throws InterruptedException {

		//String eBusiness_Quote_Status = getDriver().findElement(By.className("selProcStatus")).getText();
		String eBusiness_Quote_Status = getDriver().findElement(By.xpath("//td[input[@name='selProcStatus']]")).getText().trim();
		System.out.println("eBusiness Quote Status " + eBusiness_Quote_Status);

		if (eBusiness_Quote_Status.equals(Quote_Status)) {

			Assert.assertTrue("Quote Status is  " + eBusiness_Quote_Status + " in eBusiness Portal", true);
		} else {
			Assert.assertTrue("Quote Status is  " + eBusiness_Quote_Status + " in eBusiness Portal", false);
		}

	}

	@Step
	public void save_verify_calculated_Premium(String Action) throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativePremium";
		Thread.sleep(1000);
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		List<WebElement> Base_Pemium =getDriver().findElements(By.xpath("//td[a[contains(@data-bind,'policyTermPremium.chargedPremiumAmount')]]"));
		List<WebElement> Total_Charges =getDriver().findElements(By.xpath("//td[a[contains(@data-bind,'__totalCharges()')]]"));
        List<WebElement> Total_Premium =getDriver().findElements(By.xpath("//td[a[contains(@data-bind,'policyTermPremium.chargedPremiumWithTaxesAmount')]]"));

		
		
		if (Action.equalsIgnoreCase("Save"))
		{
		
		try
		{
		
		Thread.sleep(500);
		System.out.println(Base_Pemium.get(0).getText());  
        System.out.println(Base_Pemium.get(1).getText());
		
		XLS.setCellData(sheetName, "500 Deductible", 2, Base_Pemium.get(0).getText());
		XLS.setCellData(sheetName, "1000 Deductible", 2, Base_Pemium.get(1).getText());
        
		}catch(Exception e)
		{
			
		}

        try
        {
		Thread.sleep(500);
		XLS.setCellData(sheetName, "500 Deductible", 3, Total_Charges.get(0).getText());
		XLS.setCellData(sheetName, "1000 Deductible", 3, Total_Charges.get(1).getText());
        
        System.out.println(Total_Charges.get(0).getText());  
        System.out.println(Total_Charges.get(1).getText());
        }catch (Exception e)
        {
        	
        }
        
        try
        {     
		Thread.sleep(500);
		
		XLS.setCellData(sheetName, "500 Deductible", 4, Total_Premium.get(0).getText());
		XLS.setCellData(sheetName, "1000 Deductible", 4, Total_Premium.get(1).getText());
        System.out.println(Total_Premium.get(0).getText());  
        System.out.println(Total_Premium.get(1).getText());  
        }catch (Exception e)
        {
        	
        }
		}else if (Action.equalsIgnoreCase("verify"))
		{
			
	      String Base_Premium_500=XLS.getCellData(sheetName, "500 Deductible", 2);
	      String Base_Premium_1000=XLS.getCellData(sheetName, "1000 Deductible", 2);

	      String Total_Charges_500=XLS.getCellData(sheetName, "500 Deductible", 3);
	      String Total_Charges_1000=XLS.getCellData(sheetName, "1000 Deductible", 3);
	      
	      String Total_Premium_500=XLS.getCellData(sheetName, "500 Deductible", 3);
	      String Total_Premium_1000=XLS.getCellData(sheetName, "1000 Deductible", 4);
	    
	      System.out.println("------APP----------");
	      
	  	System.out.println(Base_Pemium.get(0).getText());  
        System.out.println(Base_Pemium.get(1).getText());
        System.out.println(Total_Charges.get(0).getText());  
        System.out.println(Total_Charges.get(1).getText());
        System.out.println(Total_Premium.get(0).getText());  
        System.out.println(Total_Premium.get(1).getText());  
        
	      System.out.println("------INP----------");
            System.out.println(Base_Premium_500); 
            System.out.println(Base_Premium_1000); 
            System.out.println(Total_Charges_500); 
            System.out.println(Total_Charges_1000); 
            System.out.println(Total_Premium_500); 
            System.out.println(Total_Premium_1000); 
      
	      
	      if (Base_Premium_500.equalsIgnoreCase(Base_Pemium.get(0).getText()) &&
	    		  Base_Premium_1000.equalsIgnoreCase(Base_Pemium.get(1).getText()) &&
	    		  Total_Charges_500.equalsIgnoreCase(Total_Charges.get(0).getText()) &&
	    		  Total_Charges_1000.equalsIgnoreCase(Total_Charges.get(1).getText()) &&
	    		  Total_Premium_500.equalsIgnoreCase(Total_Premium.get(0).getText()) &&
	    		  Total_Premium_1000.equalsIgnoreCase(Total_Premium.get(1).getText()) )
	    	  
	      {
	    	  
	    	  Assert.assertTrue("Premium calculation matches with the saved data before Edit inidicative quote", true);

	    	  
	    	  
	      }else
	      {
	    	  Assert.assertTrue("Premium calculation matches with the saved data before Edit inidicative quote", true);

	      }
	      
	      
	      
			
			
		}
		
		else
		{
			
		}
			
			


	}

	public void verify_Quote_Validation_popup() {
		// TODO Auto-generated method stub
		String Validation_APP= getDriver().findElement(By.xpath("//h3[@class='modal-title']")).getText().trim();
		
		if(Validation_APP.equalsIgnoreCase("Quote Validation"))
		{
			Assert.assertTrue("Quote Validation", true);
		}
		else
		{
			Assert.assertTrue("Quote Validation", false);	
		}
		
	}

	public void verify_claims_warning() {
		// TODO Auto-generated method stub
		String ClaimsWarning_APP= getDriver().findElement(By.xpath("//div[@class='modal-body']")).getText().trim();
		
		if(ClaimsWarning_APP.equalsIgnoreCase("We are unable to provide an indicative quote due to claims history"))
		{
			Assert.assertTrue("We are unable to provide an indicative quote due to claims history", true);
		}
		else
		{
			Assert.assertTrue("We are unable to provide an indicative quote due to claims history", false);
		}
		
	}

	public void verify_DeclinePostcode_warning() {
		// TODO Auto-generated method stub
		String DeclinePostcode_APP= getDriver().findElement(By.xpath("//div[@class='modal-body']")).getText().trim();
		
		if(DeclinePostcode_APP.equalsIgnoreCase("We are unable to provide an indicative quote due to the location of the property"))
		{
			Assert.assertTrue("We are unable to provide an indicative quote due to the location of the property", true);
		}
		else
		{
			Assert.assertTrue("We are unable to provide an indicative quote due to the location of the property", false);
		}
		
	}

	public void edit_policycommission_rate(String commission_rate) throws InterruptedException {
		// TODO Auto-generated method stub
		
		getDriver().findElement(By.id("policy_commission_rate")).clear();
		Thread.sleep(1000);
		
		getDriver().findElement(By.id("policy_commission_rate")).sendKeys(commission_rate);
		Thread.sleep(1000);
	}

	public void verify_policycommission_rate(String commission_rate) throws InterruptedException {
		// TODO Auto-generated method stub
		
		String Policy_Commissionrate_APP= getDriver().findElement(By.id("policy_commission_rate")).getAttribute("value");
		System.out.println("Policy comission rate "+Policy_Commissionrate_APP);
		Thread.sleep(1000);
		if(commission_rate.equalsIgnoreCase(Policy_Commissionrate_APP))
			Assert.assertTrue("Policy commision rate is saved as" + Policy_Commissionrate_APP, true);
		else
			Assert.assertTrue("Policy commision rate is saved as" + Policy_Commissionrate_APP, false);
		
	}

	public void verify_NewDeductibleOption(String new_Deductible) {
		// TODO Auto-generated method stub
		
		Select Deductible =new Select(getDriver().findElement(By.id("btnOptionDeductible_2")));
		String newDeductible_APP= Deductible.getFirstSelectedOption().getText();
		System.out.println("The deductible value is" +newDeductible_APP);
		
		if(newDeductible_APP.equalsIgnoreCase(new_Deductible))
			Assert.assertTrue("new deductible option is displayed as " + newDeductible_APP, true);
		else
			Assert.assertTrue("new deductible option is displayed as " + newDeductible_APP, false);
	}

	public void verify_PremiumTable_IsDisplayed() {
		// TODO Auto-generated method stub
		try{
			String PremiumDetails_APP= getDriver().findElement(By.xpath("+PremiumDetails+")).getText();
			String TotalBasePremium_App= getDriver().findElement(By.xpath("+TotalBasePremium+")).getText();
			if(PremiumDetails_APP.equalsIgnoreCase("Premium Details:"))
			getDriver().findElement(By.xpath("+PremiumDetails+")).isDisplayed();
			getDriver().findElement(By.xpath("+TotalBasePremium+")).isDisplayed();
		}
		catch(Exception e)
		{
			System.out.println("Premium table is not displayed");
		}
		
		
	}
	@Step
	public void clickOnPremiumSummary() throws InterruptedException, AWTException {
	WebElement elmnt =getDriver().findElement(By.xpath("//a[contains(text(),('Premium Summary'))]"));  
	Actions builder = new Actions(getDriver());   
	builder.moveToElement(elmnt, +0, +0).click().build().perform();
	WebDriverWait wait=new WebDriverWait(getDriver(), 300);
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(text(),('Finish'))]")));
	Thread.sleep(1000);
	
	getDriver().findElement(By.id("chkMultiquote")).click();
	}
	
	@Step
	public void verify_Flood_cover_not_applicablePopUp()
	{
		String ExpectedPopUpTitle = "Flood cover not applicable";
		String ExpectedPopUpText = "Please select without Flood Coverage option";
		String popUp_App = getDriver().findElement(By.xpath("//h3[contains(@class, 'modal-title')]")).getText();
		String popUpBodyText_App = getDriver().findElement(By.xpath("//div[contains(@class,'modal-body')]")).getText();
		
		if (popUp_App.contains(ExpectedPopUpTitle) && popUpBodyText_App.contains(ExpectedPopUpText))
		{
			Assert.assertTrue("Popup Triggered", true);
			getDriver().findElement(By.xpath("//h3[contains(@class, 'modal-title')]")).sendKeys(Keys.TAB);
			getDriver().findElement(By.xpath("//h3[contains(@class, 'modal-title')]")).sendKeys(Keys.TAB);
			getDriver().findElement(By.xpath("//h3[contains(@class, 'modal-title')]")).sendKeys(Keys.ENTER);
		}
		else
			Assert.assertTrue("Popup not Triggered", false);	
	}
}