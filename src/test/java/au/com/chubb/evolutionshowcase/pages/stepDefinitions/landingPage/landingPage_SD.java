package au.com.chubb.evolutionshowcase.pages.stepDefinitions.landingPage;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.landingPage.landingPage_Steps;
import au.com.chubb.evolutionshowcase.pages.serenitySteps.policyInformation.brokerDetails_Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

public class landingPage_SD {

	@Steps
	landingPage_Steps landingPage;

	@And("^click I would like an indicative quotation$")
	public void click_indicative_quote() throws Throwable {
		landingPage.click_indicative_quote();
	}
	
	@And("^click I would like an full quotation$")
	public void click_full_quote() throws Throwable {
		landingPage.click_full_quote();
	}
	
	@And("^verify URL is \"([^\"]*)\" and update status in \"([^\"]*)\"$")
	public void verify_navigate_URL(String URL, String testdata) throws Throwable {
		landingPage.verify_navigate_URL(URL,testdata);
	}

	@And("^verify footer link \"([^\"]*)\" re-directs to \"([^\"]*)\"$")
	public void click_footer_link(String footer_link, String URL) throws Throwable {
		landingPage.verify_footer_URL(footer_link, URL);
	}
	
	@And("^click on quick indication button and continue$")
	public void click_On_Quick_Indication_Button() throws Throwable {
		landingPage.click_On_Quick_Indication_Button();
	}
	
//	@And("^click on privacy statement link in the Indicative quote page and open PDF$")
//	public void click_privacy_link_openPdf() throws Throwable {
//		landingPage.click_privacy_link_openPdf();
//	}
//	
//	@And("^click Duty of Disclousure link on UI$")
//	public void click_DutyofDisclousure() throws Throwable {
//		landingPage.click_duty_of_disclousure();
//	}
	

	
}
