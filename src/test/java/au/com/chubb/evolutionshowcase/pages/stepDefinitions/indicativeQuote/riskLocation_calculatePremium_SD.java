package au.com.chubb.evolutionshowcase.pages.stepDefinitions.indicativeQuote;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote.riskLocation_calculatePremium_Steps;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

public class riskLocation_calculatePremium_SD {
	
	@Steps
	riskLocation_calculatePremium_Steps  calculatePremium;

	@And("^validate quote offered page$")
	public void verify_quote_offered_page() throws Throwable {
		calculatePremium.verify_quote_offered_page();
	
	}
	
	@And("^click on \"([^\"]*)\"$")
	public void click_button(String button_name) throws Throwable {
		calculatePremium.click_button(button_name);
	
	}
	
	@And("^click SaveAndExit$")
	public void click_SaveAndExit() throws Throwable {
		calculatePremium.click_SaveAndExit_Button();
	
	}
	
	@And("^click on Premium Summary$")
	public void clickOnPremiumSummary() throws Throwable {
		calculatePremium.clickOnPremiumSummary();
	
	}
	
	@And("^validate Quote validation popup not displayed$")
	public void verftyNoQuoteValidationMessage() throws Throwable {
		calculatePremium.verftyNoQuoteValidationMessage();
	
	}
	
	@And("^click on \"([^\"]*)\" button from popup$")
	public void click_button_from_popup(String button_name) throws Throwable {
		calculatePremium.click_button_from_popup(button_name);
	
	}	
	
	@And("^click on \"([^\"]*)\" and verify premium details in ebusiness portal$")
		public void verify_premium_ebusiness_portal(String button_name) throws Throwable {
			calculatePremium.verify_premium_ebusiness_portal(button_name);	
	
	}		
	@And("^verify quote status in ebusiness portal is \"([^\"]*)\"$")
			public void verify_quote_status_ebusiness_portal(String Quote_Status) throws Throwable {
				calculatePremium.verify_quote_status_ebusiness_portal(Quote_Status);	
	
	}
	@And("^\"([^\"]*)\" calculated premium$")
				public void save_verify_calculated_Premium(String Action) throws Throwable {
				calculatePremium.save_verify_calculated_Premium(Action);	
}
	@And("^verify Quote Validation pop up is dipslayed$")
	public void verify_Quote_Validation_popup() throws Throwable {
	calculatePremium.verify_Quote_Validation_popup();	
}
	@And("^verify claims warning is displayed$")
	public void verify_claims_warning() throws Throwable {
	calculatePremium.verify_claims_warning();	
}
	@And("^Decline Postcode warning is displayed$")
	public void verify_DeclinePostcode_warning() throws Throwable {
	calculatePremium.verify_DeclinePostcode_warning();	
}
	@And("^edit policy commission rate \"([^\"]*)\"$")
	public void edit_Policycommission_Rate(String commission_rate) throws Throwable {
		calculatePremium.edit_policycommission_rate(commission_rate);
}
	@And("^verify policy commission rate \"([^\"]*)\"$")
	public void verify_Policycommission_Rate(String commission_rate) throws Throwable {
		calculatePremium.verify_policycommission_rate(commission_rate);
}
	@And("^verify new deductible \"([^\"]*)\" is displayed$")
	public void verify_NewDeductibleOption(String new_Deductible) throws Throwable {
		calculatePremium.verify_NewDeductibleOption(new_Deductible);
}
	@And("^verify Premium Table is displayed$")
	public void verify_PremiumTable_IsDisplayed() throws Throwable {
		calculatePremium.verify_PremiumTable_IsDisplayed();
}
	
	@And("^verify popup for Flood cover not applicable$")
	public void verify_Flood_cover_not_applicablePopUp() throws Throwable {
		calculatePremium.verify_Flood_cover_not_applicablePopUp();
	}
	
}