package au.com.chubb.evolutionshowcase.pages.stepDefinitions.riskAndCoverages;

import java.io.IOException;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.riskAndCoverages.houseAndContents_VAC_Steps;
import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class houseAndContents_VAC_SD {

	@Steps
	houseAndContents_VAC_Steps houseAndContents;
	DynamicSeleniumFunctions dynamicSeleniumFunctions;

	@And("^enter house and contents details as per \"([^\"]*)\" and \"([^\"]*)\" VAC for that location \"([^\"]*)\" as per below data for \"([^\"]*)\"$")
	public void enter_house_and_contents_details(String testData, String VAC_Count, String Location_Identifier, String user_profile, DataTable arg1) throws Throwable {
		houseAndContents.enter_house_and_contents_details(testData,VAC_Count, Location_Identifier, user_profile, arg1 );
	}
	
	@And("^select category from \"([^\"]*)\" and \"([^\"]*)\" VAC for that location \"([^\"]*)\"$")
			public void select_category_from_VAC_popup(String testData, String VAC_Count, String Location_Identifier) throws Throwable {
				houseAndContents.select_category_from_VAC_popup(testData,VAC_Count, Location_Identifier);
			}
	
	@And("^click proceed with Quote in risk and coverages page$")
	public void click_proceed_with_Quote() throws Throwable {
		houseAndContents.click_proceed_with_Quote();
	}
	
	@Then("^the deductible value selected should default to the highest Building Deductible value across for risk location \"([^\"]*)\" and \"([^\"]*)\"$")
	public void the_deductible_value_selected_should_default_to_the_highest_Building_Deductible_value_across_for_risk_location_and(String locationExpected, String LocationActual) throws Throwable {
		houseAndContents.verifyDefaultBuildingDectibleValueForAllLocations(locationExpected,LocationActual);
	}

	@And("^verify Building Replacement Cost Type as \"([^\"]*)\" for location \"([^\"]*)\"$")
	public void should_default_value_for_Building_Replacement_Cost_Type_based_on_existing_Evolution_functionality_And_field_should_be_read_only(String extendedReplacementCost,String location) throws Throwable {
		houseAndContents.verifyDefaultBuildingReplcaementCostType(extendedReplacementCost,location);
	}
	
	@And("^select Home Business Property Coverage as \"([^\"]*)\" and Home Business Property Sum Insured as \"([^\"]*)\" for risk location \"([^\"]*)\"$")
	public void verify_home_business_property_coverage(String selection,String value,String location) throws Throwable {
		houseAndContents.verify_home_business_property_coverage(selection,value,location);
	}
	
	@And("^'Valuable Articles Coverage - Add Valuables Coverage' page is displayed$")
	public void ClickAddVAC() throws Throwable {
		houseAndContents.clickAddCoverage();
	}

	@And("^validate for missing mandatory fields for VAC$")
	public void validateMandatoryFiledsForVAC() throws Throwable {
		houseAndContents.validateMandatoryFiledsForVAC();
	}
	
	/*@Author: Premchand
	 * Description: checking upload and download schedule buttons are available in the VAC page.
	 * arguments - NA
	 * return type: void
	 */
	
	
	@Then("^verify upload and download VAC schedule buttons are available$")
	public void verify_upload_and_download_VAC_schedule_buttons_are_available() throws Throwable {
		houseAndContents.verify_upload_and_download_VAC_schedule_buttons_are_available();
	}
	
	
	@And("^verify upload schedule button functionality$")
	public void verify_upload_schedule_button_functionality() throws Throwable {
		houseAndContents.verify_upload_schedule_button_functionality();
	}
	
	@And("^verify download schedule button functionality$")
	public void verify_download_schedule_button_functionality() throws Throwable {
		houseAndContents.verify_download_schedule_button_functionality();
	}
	
	
	@And("^validate for missing mandatory fields for Houses and Contents$")
	public void validateMandatoryFiledsForHouseAndContents() throws Throwable {
		houseAndContents.validateMandatoryFiledsForHousesAndContents();
	}
	
	@When("^click on 'OK' button$")
	public void clickonOKButton() throws Throwable {
		houseAndContents.clickonOKButton();
	}
		
	@And("^'Details for House and Contents Coverage' page is displayed$")
	public void clickHousesAndContents() throws Throwable {
		houseAndContents.clickHousesAndContents();
	}
	
	@And("^validate flood coverage disable for location \"([^\"]*)\"$")
	public void virifyFloodCoverageDisabled(String location) throws Throwable {
		houseAndContents.virifyFloodCoverageDisabled(location);
	}
	
	@And("^validate flood and coverage is selected as \"([^\"]*)\"$")
	public void virifyFloodCoverageSelecteded(String option) throws Throwable {
		houseAndContents.virifyFloodCoverageSelecteded(option);
	}
	 
	@And("^click on \"([^\"]*)\" button$")
	public void click_button(String button_name) throws Throwable {
		houseAndContents.click_button(button_name);
	}	
	
	
	@And("^click on House and Contents for Risk Location \"([^\"]*)\"$")
	public void click_house_and_contents(String location) throws Throwable {
		houseAndContents.click_house_and_contents(location);
	}	
	
	@And("^click on VAC for Risk Location \"([^\"]*)\"$")
	public void click_VAC(String location) throws Throwable {
		houseAndContents.click_VAC(location);
	}	
	
	@And("^verify VAC display in Risk and Coverages$")
	public void verify_VAC_from_indicative_quote(DataTable arg1) throws Throwable {
		houseAndContents.verify_VAC_from_indicative_quote(arg1);
	}
	
	
	@And("^\"([^\"]*)\" Building coverage for Risk Location \"([^\"]*)\"$")
	public void edit_verify_building_coverage(String Action, String Location, DataTable arg1) throws Throwable {
		houseAndContents.edit_verify_building_coverage(Action, Location, arg1);
	}	
	
	@And("^verify Building Deductible and Conntents Deductible for location \"([^\"]*)\" has same value$")
	public void verify_building_contents_deductible(String Location) throws Throwable {
		houseAndContents.verify_building_contents_deductible(Location);
	}	
	
	
	@And("^\"([^\"]*)\" Contents coverage for Risk Location \"([^\"]*)\"$")
	public void edit_verify_contents_coverage(String Action, String Location, DataTable arg1) throws Throwable {
		houseAndContents.edit_verify_contents_coverage(Action, Location, arg1);
	}	
	
	@And("^verify \"([^\"]*)\" is editable by passing value \"([^\"]*)\"$")
	public void verify_field_is_editable(String property, String value) throws Throwable{
		dynamicSeleniumFunctions.verifyFieldEditable(property, value);
	}
	
	@And("^verify help text for House and contents fields for location \"([^\"]*)\"$")
	public void verify_helptext_houseandcontents(String location) throws Throwable {
		houseAndContents.verify_helptext_houseandcontents(location);
	}
	
	@And("^verify help text for VAC fields for location \"([^\"]*)\"$")
	public void verify_helptext_VAC(String location) throws Throwable {
		houseAndContents.verify_helptext_vac(location);
	}	
	
	@And("^override Elias Rating with value \"([^\"]*)\" and reason \"([^\"]*)\"$")
	public void overrideEliasRating(String EliasValue, String OverrideReason ) throws Throwable {
		houseAndContents.overrideEliasRating(EliasValue, OverrideReason);
	}
	
	@And("^select Flood coverage as \"([^\"]*)\"$")
	public void select_Flood_coverage_as(String value ) throws Throwable {
		houseAndContents.selectFloodCoverageAs(value);
	}
	
	@And("^verify Flood coverage static text is displayed$")
	public void verifyFloodcoverageTextDispalyed( ) throws Throwable {
		houseAndContents.verifyFloodCoverageText();
	}
	
	@And("^verify Flood coverage static text NOT is displayed$")
	public void verifyFloodcoverageTextNotDispalyed( ) throws Throwable {
		houseAndContents.verifyFloodCoverageTextNotDisplayed();
	}
	
	@And("^enter building sum insured from \"([^\"]*)\" for full quote$")
	public void enter_building_sum_insured_full_quote(String testData ) throws Throwable {
		houseAndContents.enter_building_sum_insured_full_quote(testData);
	}
	
	@And("^enter building sum insured when 2 locations as \"([^\"]*)\" and \"([^\"]*)\" for full quote$")
	public void enter_building_sum_insured_full_quote_two_locations(String testData,String testData1 ) throws Throwable {
		houseAndContents.enter_building_sum_insured_full_quote_two_locations(testData,testData1);
	}
	
	@And("^verify content sum insured based on building sum insured from \"([^\"]*)\" for full quote$")
	public void verify_building_sum_insured_default_full_quote(String testData) throws Throwable {
		houseAndContents.verify_building_sum_insured_default_full_quote(testData);
	}
	
	@And("^verify content sum insured based on building sum insured when 2 locations as \"([^\"]*)\" and \"([^\"]*)\" for full quote$")
	public void verify_building_sum_insured_default_full_quote_two_locations(String testData,String testData1 ) throws Throwable {
		houseAndContents.verify_building_sum_insured_default_full_quote_two_locations(testData, testData1);
	}
	
}
