package au.com.chubb.evolutionshowcase.pages.stepDefinitions.interestedParties;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.interestedParties.interestedParties_Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class interestedParties_SD {

	@Steps
	interestedParties_Steps interestedParties;

	@And("^Add \"([^\"]*)\" Interested Party for location \"([^\"]*)\" as per below data$")
	public void add_interested_party(String interested_party_count, String Location_Identifier, DataTable arg1) throws Throwable {
		interestedParties.add_interested_party(interested_party_count,Location_Identifier,arg1);
	}
	
	
	@And("^click proceed with Quote in interested parties page$")
	public void click_proceed_with_Quote() throws Throwable {
		interestedParties.click_proceed_with_Quote();
	}
	
	@And("^verify \"([^\"]*)\" Interested Party added for testflow \"([^\"]*)\" as per below data$")
	public void verify_interested_party(String interested_party_count, String Location_Identifier, DataTable arg1) throws Throwable {
		interestedParties.verify_interested_party(interested_party_count,Location_Identifier,arg1);
	}
	
	@And("^option \"([^\"]*)\" should only be listed for selection$")
	public void verify_Role_dropdown(String optionName) throws InterruptedException{
		interestedParties.verify_Role_DropdownOptions(optionName);
	}

	@Given("^click on Cancel button$")
	public void click_Cancel() throws Throwable {
		interestedParties.click_Cancel();
	}
	@Given("^click checkbox for Add Interested Party$")
	public void click_checkbox_InterestedParty() throws Throwable {
		interestedParties.click_checkbox_InterestedParty();
	}
	
	@And("^verify required field error Messages for Add Interested Party$")
	public void verifyAllMandatoryFiledsErrorMessages() throws Throwable {
		interestedParties.verifyAllMandatoryFiledsErrorMessages();
	}
	
}
