package au.com.chubb.evolutionshowcase.pages.serenitySteps.binding;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;

import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class binding_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;
	DynamicSeleniumFunctions functions;

	public binding_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);

	}

	public void verifyUserAbleToSelect_Deductible(String value) throws InterruptedException {
		if(value.contains("$500"))
		{	
			getDriver().findElement(By.xpath("//input[@id='rbQuoteOption_0']")).click();
		Thread.sleep(3000);}
		else
		{getDriver().findElement(By.xpath("//input[@id='rbQuoteOption_1']")).click();}

		if (getDriver().findElement(By.xpath("//input[@id='chkConfirmNonChubb']")).isDisplayed())
		{
			Assert.assertTrue("user is able to select Deductible value", true);
		}else
		{
			Assert.assertTrue("user is able to select Deductible value", false);
		}

	}

	public void selectConfirmation_Checkbox() throws InterruptedException {
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", getDriver().findElement(By.xpath("//input[@id='chkConfirmNonChubb']")));
		getDriver().findElement(By.xpath("//input[@id='chkConfirmNonChubb']")).click();
		Thread.sleep(3000);
	}

	@Step
	public void verifyDetails_Saved() {
		if (getDriver().findElement(By.xpath("//i[@id='rbQuoteOption_0']")).isDisplayed())
		{
			Assert.assertTrue("Options are Saved after clicking on Save and Exit in Binding Page", true);
		}else
		{
			Assert.assertTrue("Options are not Saved after clicking on Save and Exit in Binding Page", false);
		}
	}
	@Step
	public void verifySaveAndExitNotDisplayed() throws InterruptedException {
		boolean flag=true;
		getDriver().findElement(By.xpath("//a[contains(text(),'Loss History')]")).click();
		Thread.sleep(20000);

		try{
		if(getDriver().findElement(By.xpath("//button[contains(text(),'Save and Exit')]")).isDisplayed()){}
		}

		catch(Exception e)
		{
			if(!e.getClass().getName().contains("NoSuchElement")){
				flag=false;
			}
		}
			getDriver().findElement(By.xpath("//a[contains(text(),'Risks / Coverages')]")).click();
			Thread.sleep(20000);

			try{
				if(getDriver().findElement(By.xpath("//button[contains(text(),'Save and Exit')]")).isDisplayed()){}
			}

			catch(Exception e1)
			{
				if(!e1.getClass().getName().contains("NoSuchElement")){
					flag=false;
				}	
			}
				getDriver().findElement(By.xpath("//a[contains(text(),'Premium Summary')]")).click();
				Thread.sleep(20000);

				try{
					if(getDriver().findElement(By.xpath("//button[contains(text(),'Save and Exit')]")).isDisplayed()){}
				}

				catch(Exception e2)
				{
					if(!e2.getClass().getName().contains("NoSuchElement")){
						flag=false;
					}
				}

					if (flag)
					{
						Assert.assertTrue("Save and Exit button not displayed in Loss History,Risks and Coverages,Premium Summary pages", true);
					}else
					{
						Assert.assertTrue("Save and Exit button displayed in Loss History,Risks and Coverages,Premium Summary pages", false);
					}		

				}

	@Step
	public void verifySaveAndExitDisplayed() throws InterruptedException {
		boolean flag=true;

		
		try{
			if(getDriver().findElement(By.xpath("//button[contains(text(),'Save and Exit')]")).isDisplayed()){}
			getDriver().findElement(By.xpath("//a[contains(text(),'Policy Information')]")).click();
			Thread.sleep(20000);
			if(getDriver().findElement(By.xpath("//button[contains(text(),'Save and Exit')]")).isDisplayed()){}
				getDriver().findElement(By.xpath("//a[contains(text(),'Interested Parties')]")).click();
				Thread.sleep(20000);
				if(getDriver().findElement(By.xpath("//button[contains(text(),'Save and Exit')]")).isDisplayed()){}
				getDriver().findElement(By.xpath("//a[contains(text(),'Appraisal Contacts')]")).click();
				Thread.sleep(20000);
				if(getDriver().findElement(By.xpath("//button[contains(text(),'Save and Exit')]")).isDisplayed()){}
		}
				catch(Exception e)
				{
					if(e.getClass().getName().contains("NoSuchElement")){
						flag=false;
				}	

					if (flag)
					{
						Assert.assertTrue("Save and Exit button  displayed in Policy Information, Interested Parties, Appraisal Contact and Binding  pages", true);
					}else
					{
						Assert.assertTrue("Save and Exit button not displayed in Policy Information, Interested Parties, Appraisal Contact and Binding ", false);
					}		
			}
			}
	@Step
	public void verifyTextPleaseNote(String expectedText) {
		String actualText=getDriver().findElement(By.xpath("//div[@class='alert alert-warning alert-inline']")).getText();
		if (expectedText.equals(actualText))
		{
			Assert.assertTrue("Static text 'Please Note' is displayed as expected", true);
		}else
		{
			Assert.assertTrue("Static text 'Please Note' is not displayed as expected and actual text is"+actualText, false);
		}		
	}
}