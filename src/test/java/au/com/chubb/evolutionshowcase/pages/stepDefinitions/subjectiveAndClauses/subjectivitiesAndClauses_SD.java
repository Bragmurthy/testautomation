package au.com.chubb.evolutionshowcase.pages.stepDefinitions.subjectiveAndClauses;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.subjectivesAndClauses.subjectivitiesAndClauses_Steps;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class subjectivitiesAndClauses_SD {

	@Steps
	subjectivitiesAndClauses_Steps subjectivitiesClauses;

		
	@And("^\"([^\"]*)\" enter subjectivities details for location \"([^\"]*)\" based on \"([^\"]*)\"$")
	public void enter_appraisal_details(String action,String location, String testData) throws Throwable {
		subjectivitiesClauses.enter_subjectivitiesl_details(action, location, testData);
	}

	@And("^\"([^\"]*)\" enter clause details for location \"([^\"]*)\" based on \"([^\"]*)\"$")
	public void enter_clause_details(String action,String location, String testData) throws Throwable {
		subjectivitiesClauses.enter_clause_details(action, location, testData);
	}
	
	@And("^click proceed with Quote in subjectivities and clauses page$")
	public void click_proceed_with_Quote() throws Throwable {
		subjectivitiesClauses.click_proceed_with_Quote();
	}
	
	@And("^delete subjectivities for location \"([^\"]*)\"$")
	public void delete_subjectivities_details(String location) throws Throwable{
		subjectivitiesClauses.delete_subjectivities_details(location);
	}
	
	@And("^delete clause for location \"([^\"]*)\"$")
	public void delete_clause_details(String location) throws Throwable{
		subjectivitiesClauses.delete_clause_details(location);
	}
}
