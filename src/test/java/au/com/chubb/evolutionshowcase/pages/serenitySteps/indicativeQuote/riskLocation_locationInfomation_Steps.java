package au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class riskLocation_locationInfomation_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public riskLocation_locationInfomation_Steps() throws IOException {
	/*	OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/landingPage.properties");
		OR.load(fo);
*/
	}


	
	@Step
	public void address_search(String address, String riskLocation_identifier) throws InterruptedException {
		
		int locationIdentifier=Integer.parseInt(riskLocation_identifier);
		
		if (riskLocation_identifier.equalsIgnoreCase("1"))
		{
			
			
			WebElement AddNextRiskLoaction=getDriver().findElement(By.xpath("//*[contains(text(),('Add Next Risk Location'))]"));
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", AddNextRiskLoaction);
            Thread.sleep(2000);
			getDriver().findElement(By.xpath("//*[contains(text(),('Add Next Risk Location'))]")).click();
			
		}
		
		loop:
		for (int i=1;i<100;i++)
		{
		try
		{
			getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_Search")).sendKeys(address);
			Thread.sleep(5000);

			break loop;
		}catch (Exception e)
		{
			Thread.sleep(1000);
		}
		}

		getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_Search")).sendKeys(Keys.DOWN);
		Thread.sleep(4000);
		getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_Search")).sendKeys(Keys.TAB);
		Thread.sleep(2000);
		
	}

	@Step
	public void address_data_auto_populate(String riskLocation_identifier, DataTable arg1) throws InterruptedException {

		int locationIdentifier=Integer.parseInt(riskLocation_identifier);
		List<List<String>> data = arg1.raw();
		
		String UnitNo_APP=getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_UnitNo")).getAttribute("value");
		String StreetNo_APP=getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_StreetNo")).getAttribute("value");
		String StreetName_APP=getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_StreetName")).getAttribute("value");
		String BuildingName_APP=getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_BuildingName")).getAttribute("value");
		String City_APP=getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_City")).getAttribute("value");
		String PostCode_APP=getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_PostCode")).getAttribute("value");
		String Country_APP=getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_Country")).getAttribute("value");
		String State_APP=getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_State")).getAttribute("value");

		
		System.out.println("UnitNo_APP -- " +UnitNo_APP);
		System.out.println("UnitNo_INP -- " +data.get(1).get(1));

		
		System.out.println("StreetNo_APP-- " +StreetNo_APP);
		System.out.println("StreetNo_INP -- " +data.get(2).get(1));

		System.out.println("StreetName_APP-- " +StreetName_APP);
		System.out.println("StreetName_INP -- " +data.get(3).get(1));

		System.out.println("BuildingName_APP -- " +BuildingName_APP);
		System.out.println("BuildingName_INP -- " +data.get(4).get(1));

		System.out.println("City_APP -- " +City_APP);
		System.out.println("City_INP -- " +data.get(5).get(1));

		System.out.println("PostCode_APP -- " +PostCode_APP);
		System.out.println("PostCode_INP -- " +data.get(6).get(1));

		System.out.println("Country_APP -- " +Country_APP);
		System.out.println("Country_INP -- " +data.get(7).get(1));

		System.out.println("State_APP -- " +State_APP);
		System.out.println("State_INP -- " +data.get(8).get(1));

				
		if (StreetNo_APP.equalsIgnoreCase(data.get(2).get(1)) &&
						StreetName_APP.equalsIgnoreCase(data.get(3).get(1)) &&
										City_APP.equalsIgnoreCase(data.get(5).get(1)) &&
												PostCode_APP.equalsIgnoreCase(data.get(6).get(1)) &&
														Country_APP.equalsIgnoreCase(data.get(7).get(1)) &&
																State_APP.equalsIgnoreCase(data.get(8).get(1)))
																{
			                                                            Assert.assertTrue("Risk Location " +riskLocation_identifier+"details are auto populated after search", true);
																} else {
																	Assert.assertTrue("Risk Location " +riskLocation_identifier+"details are not auto populated after search", false);

																}
																}


@Step
public void verifyText(String expectedText, String riskLocation) {

List<WebElement> list=getDriver().findElements(By.xpath("//h4[@class='panel-inline-title']"));
int location =Integer.parseInt(riskLocation);
String locationInfo_APP=list.get(location).getText();
if(location>0){
	
if(expectedText.equalsIgnoreCase(locationInfo_APP))
{
       Assert.assertTrue("Risk Location label is displayed", true);
} else {
	Assert.assertTrue("Risk Location label is not displayed", false);
}
}
else
{
	if(expectedText.equalsIgnoreCase(locationInfo_APP))
	{
	       Assert.assertTrue("Risk Location label is displayed", true);
	} else {
		Assert.assertTrue("Risk Location label is not displayed", false);
	}
}
}


@Step
public void address_searchNoGooglePicker(String address, String riskLocation) throws InterruptedException {
	int locationIdentifier=Integer.parseInt(riskLocation);
	
	if (riskLocation.equalsIgnoreCase("1"))
	{
		
		
		WebElement AddNextRiskLoaction=getDriver().findElement(By.xpath("//*[contains(text(),('Add Next Risk Location'))]"));
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", AddNextRiskLoaction);
        Thread.sleep(2000);
		getDriver().findElement(By.xpath("//*[contains(text(),('Add Next Risk Location'))]")).click();
		
	}
	
	loop:
	for (int i=1;i<100;i++)
	{
	try
	{
		//getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_Search")).sendKeys(address);
		//Thread.sleep(5000);
		getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_StreetNo")).clear();
	 	getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_StreetNo")).sendKeys("48");
	 	getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_StreetName")).clear();
		getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_StreetName")).sendKeys("Kambara Drive");
		getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_City")).clear();
		getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_City")).sendKeys("Mulgrave");
		getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_PostCode")).clear();
		getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_PostCode")).sendKeys("3170");
		new Select(getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_Country"))).selectByVisibleText("Australia");
		new Select(getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_State"))).selectByVisibleText("Victoria");
		break loop;
	}catch (Exception e)
	{
		Thread.sleep(1000);
	}
	}

	//getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_Search")).sendKeys(Keys.DOWN);
	//Thread.sleep(4000);
	//getDriver().findElement(By.id("addressEditor_"+locationIdentifier+"_Search")).sendKeys(Keys.TAB);
	
}
	}
