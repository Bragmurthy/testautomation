package au.com.chubb.evolutionshowcase.pages.stepDefinitions.indicativeQuote;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote.riskLocation_coverageInformation_Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

public class riskLocation_coverageInformation_SD {

	@Steps
	riskLocation_coverageInformation_Steps coverageInformation;

	@And("^enter \"([^\"]*)\" in building sum insured field for location \"([^\"]*)\" and verify value accepted by application is \"([^\"]*)\"$")
	public void verify_building_sum_insured_value(String float_value, String location, String input_value)
			throws Throwable {
		coverageInformation.verify_building_sum_insured_value(float_value, location, input_value);
	}

	@And("^enter \"([^\"]*)\" for Building Sum Insured and verify \"([^\"]*)\" is displayed for Contents Sum Insured for location \"([^\"]*)\"$")
	public void verify_content_sum_insured_default(String building_sum_insured, String contents_sum_insured,
			String location) throws Throwable {
		coverageInformation.verify_content_sum_insured_default(building_sum_insured, contents_sum_insured, location);
	}

	@And("^verify default Building and Contents Deductible is \"([^\"]*)\" when Building Sum Insured is \"([^\"]*)\" and residence type is \"([^\"]*)\" for location \"([^\"]*)\"$")
	public void verify_building_contents_deductible_default(String building_contents_default,
			String building_sum_insured, String residence_type, String location) throws Throwable {
		coverageInformation.verify_building_contents_deductible_default(building_contents_default, building_sum_insured,
				residence_type, location);
	}

	@And("^select property type as \"([^\"]*)\" for location \"([^\"]*)\" and verify label \"([^\"]*)\" is displayed$")
	public void verify_label_change(String property_type, String location, String label_name) throws Throwable {
		coverageInformation.verify_label_change(property_type, location, label_name);
	}

	@And("^select property type as \"([^\"]*)\" for location \"([^\"]*)\" and verify Contents Sum Insured is \"([^\"]*)\"$")
	public void verify_contents_sum_insured(String property_type, String location, String contents_sum_insured)
			throws Throwable {
		coverageInformation.verify_contents_sum_insured(property_type, location, contents_sum_insured);
	}

	@And("^verify pop up error message \"([^\"]*)\" when Building Sum Insured is \"([^\"]*)\" Contents Sum Insured is \"([^\"]*)\" for location \"([^\"]*)\"$")
	public void verify_contents_sum_insured_pop_up_error_message(String pop_up_error, String building_sum_insured_value,
			String content_sum_insured_value, String location) throws Throwable {
		coverageInformation.verify_contents_sum_insured_pop_up_error_message(pop_up_error, building_sum_insured_value,
				content_sum_insured_value, location);
	}

	@And("^enter building sum insured as \"([^\"]*)\" and verify user is able to select below valuable category from drop down for location \"([^\"]*)\"$")
	public void verify_valuable_category_values(String building_sum_insured, String location, DataTable arg1)
			throws Throwable {
		coverageInformation.verify_valuable_category_values(building_sum_insured, location, arg1);
	}

	@And("^delete added \"([^\"]*)\" valuable categories from location \"([^\"]*)\"$")
	public void delete_valuable_category_values(String valuable_category_count, String location) throws Throwable {
		coverageInformation.delete_valuable_category_values(valuable_category_count, location);
	}

	@And("^verify below details are populated in Coverage information for Risk Location \"([^\"]*)\" for property type \"([^\"]*)\"$")
	public void verify_coverage_information_section(String riskLocation,  String propertyType, DataTable arg1) throws Throwable {
		coverageInformation.verify_coverage_information_section(riskLocation, propertyType, arg1);
	}
	
	@And("^enter building sum insured from \"([^\"]*)\"$")
	public void enter_building_sum_insured_default(String testData) throws Throwable {
		coverageInformation.enter_building_sum_insured(testData);
	}
	
	@And("^verify content sum insured based on building sum insured from \"([^\"]*)\"$")
	public void verify_content_sum_insured_default(String testData) throws Throwable {
		coverageInformation.verify_building_sum_insured_default(testData);
	}
	
}


