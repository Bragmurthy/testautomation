package au.com.chubb.evolutionshowcase.pages.stepDefinitions.policyInformation;

import java.io.IOException;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.policyInformation.brokerDetails_Steps;
import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class brokerDetails_SD {

	@Steps
	brokerDetails_Steps brokerDetails;
	

	@And("^add broker as per \"([^\"]*)\"$")
	public void add_broker(String broker_details) throws Throwable {
		brokerDetails.add_broker(broker_details);
	}
		
	@And("^change broker$")
	public void chnage_broker(DataTable arg1) throws Throwable {
		brokerDetails.chnage_broker(arg1);
	}

	@And("^search broker contact \"([^\"]*)\"$")
	public void search_broker_contact(String broker_name) throws Throwable {
		brokerDetails.search_broker_contact(broker_name);
	}
	
	@And("^\"([^\"]*)\" broker contact as per \"([^\"]*)\"$")
	public void broker_contact(String action, String testData) throws Throwable {
		brokerDetails.broker_contact(action, testData);
	}
	
	@And("^verify broker details as per \"([^\"]*)\"$")
	public void verifyBrokerDetailsNameEmailOfficeAndMobileNumbers(String testData) throws Throwable {
		brokerDetails.verifyBrokerDetailsNameEmailOfficeAndMobileNumbers(testData);
	}
	
	@Then("Fields 'Broker', 'Address', 'Country' should default as below$")
	public void broker_defaultDetails(DataTable arg1)  {
		brokerDetails.verifyDefaultBrokerDetails(arg1);
	}

	@Then("^fields should be non editable$")
	public void fields_should_be_non_editable() throws IOException  {
		brokerDetails.verifyBrokerDetailsNotEditable();
		
	}
	
	@Given("^change broker contact$")
	public void change_broker_contact(DataTable contactName) throws Throwable {
		brokerDetails.changeBrokerContact(contactName);
	}
	
	@Then("^verify broker details are saved$")
	public void verify_broker_deatails(DataTable contactName) throws Throwable {
		brokerDetails.verifybrokerdeatails(contactName);
	}
	
	@Then("^verify broker details are saved in UW view$")
	public void verify_broker_deatailsInUWView(DataTable contactName) throws Throwable {
		brokerDetails.verifybrokerdeatailsInUWView(contactName);
	}
	
	@And("^\"([^\"]*)\" broker contact in popup as per \"([^\"]*)\"$")
	public void broker_contact_In_Popup(String action, String testData) throws Throwable {
		brokerDetails.broker_contact_In_Popup(action, testData);
	}
	
	@Then("verify broker contact in Add broker contact popup as per \"([^\"]*)\"$")
	public void broker_contact_in_popup(String testData)  {
		brokerDetails.broker_contact_in_popup(testData);
	}
	
	@Then("^validate \"([^\"]*)\" button must be displayed$")
	public void verify_element_display_xpath(String text) throws Throwable {
		brokerDetails.verify_element_display_xpath(text);
	
	}
	
}
