package au.com.chubb.evolutionshowcase.pages.serenitySteps.premiumSummary;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebElement;

import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class premiumSummary_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;
	DynamicSeleniumFunctions functions;
	String sheetName = "IndicativeQuoteRiskLocation";
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");

	public premiumSummary_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);

	}
	@Step
	public void userOnPremiumPage() {
		if (getDriver().findElement(By.xpath("//button[contains(text(),'Finish Quote')]")).isDisplayed())
		{
			Assert.assertTrue("Premium Summary Page is Displayed", true);
		}else
		{
			Assert.assertTrue("Premium Summary Page is not Displayed", false);
		}
	}
	
	@Step
	public void saveQuoteNumber(String test_data) throws InterruptedException {
		
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop:
			for (int row = 2; row <= Row_Count; row++) {

			System.out.println("test data --" + test_data);
			if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				break innerloop;
			}
		}
		String quoteNumber=getDriver().findElement(By.xpath("//h2[contains(text(),'Quote No.')]")).getText();
		quoteNumber=quoteNumber.split(". ")[2];
		XLS.setCellData(sheetName, "Quote_Reference_Number", rowNum,quoteNumber);
		XLS.setCellData(sheetName, "QuoteNumber", rowNum,quoteNumber);
		
		
	}
	
	@Step
	public void saveOurRefNumber(String test_data) throws InterruptedException {
		
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop: for (int row = 2; row <= Row_Count; row++) {

			System.out.println("test data --" + test_data);
			if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				break innerloop;
			}
		}
		//String quoteNumber=getDriver().findElement(By.xpath("//td[contains(text(),'Quote ')]")).getText();
		String quoteNumber=getDriver().findElement(By.xpath("//td[input[@name='txtQuoteNo']]")).getText();
		XLS.setCellData(sheetName, "OurRef", rowNum,quoteNumber);
	}
	
//EP3-191 chnages
	
	public void SelectDeductibleOption() throws InterruptedException {
		// TODO Auto-generated method stub
		
		WebElement button = getDriver().findElement(By.xpath("//input[@id='rbQuoteOption_0']"));
		
		if(button.isEnabled())
		{	
			getDriver().findElement(By.xpath("//input[@id='rbQuoteOption_0']")).click();
		Thread.sleep(3000);
		}
		else
		{
			getDriver().findElement(By.xpath("//input[@id='rbQuoteOption_1']")).click();
			
		}

	}
}
	
	

	