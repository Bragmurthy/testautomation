package au.com.chubb.evolutionshowcase.pages.stepDefinitions.indicativeQuote;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote.riskLocation_enterDetails_Steps;
import au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote.riskLocation_riskInformation_Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

public class riskLocation_enterDetails_SD {

	
	@Steps
	riskLocation_enterDetails_Steps risklocation;

	@And("^enter details for Risk Location as per test data \"([^\"]*)\" and save quote reference number$")
	public void enter_risk_location_details(String test_data) throws Throwable {
		risklocation.enter_risk_location_details( test_data);
	}
	
	@And("^enter details for Risk Location as per test data with no google address picker \"([^\"]*)\" and save quote reference number$")
	public void enter_risklocation_details(String test_data) throws Throwable {
		risklocation.enter_risklocation_details(test_data);
	}
	
	@And("^Change pincode of the risk location as \"([^\"]*)\"$")
	public void change_pincode_risklocation(String pincode) throws Throwable {
		risklocation.change_pincode_risklocation(pincode);
	}
	
	
	@And("^verify default value for field \"([^\"]*)\" is empty$")
	public void verify_default_value(String field_locator) throws Throwable {
		risklocation.verify_default_value(field_locator);
	}

	@And("^enter client surname$")
	public void surName() throws Throwable {
		risklocation.enterClientSurName();
	}
	
	
	@And("^select residence type only as per test data \"([^\"]*)\"$")
	public void select_type_of_residence_only(String test_data) throws Throwable {
		risklocation.select_type_of_residence_only( test_data);
	}
	

}
