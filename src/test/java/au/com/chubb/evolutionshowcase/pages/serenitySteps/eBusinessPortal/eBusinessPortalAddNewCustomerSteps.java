package au.com.chubb.evolutionshowcase.pages.serenitySteps.eBusinessPortal;


import org.junit.runner.RunWith;
import org.openqa.selenium.By;

import au.com.chubb.evolutionshowcase.pages.objectModel.eBusinessPortal.eBusinessPortalAddNewCustomerPage;
import au.com.chubb.evolutionshowcase.utilities.CommonFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.java.en.And;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;


@RunWith(SerenityRunner.class)
public class eBusinessPortalAddNewCustomerSteps extends PageObject {
	String root = System.getProperty("user.dir");
	String sheetName = "IndicativeQuoteRiskLocation";
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
	static eBusinessPortalAddNewCustomerPage ebusinessportaladdnewcustomerpage;
	static CommonFunctions common_functions;
	
	@Step
	public void click_add_new_customer() throws InterruptedException {
		ebusinessportaladdnewcustomerpage.click_add_new_customer();
	}
	
	@Step
	public void enter_new_customer_details() throws InterruptedException {
		ebusinessportaladdnewcustomerpage.enter_new_customer_details();
	}
	
	@Step
	public void save_customer_details() throws InterruptedException {
		ebusinessportaladdnewcustomerpage.saveCustomerID();
	}
	
	@Step
	public void save_customer_details_toSheet(String test_data) throws InterruptedException {
		ebusinessportaladdnewcustomerpage.saveCustomerID(test_data);
	}
	
	
	@Step
	public void click_add_new_quote() throws InterruptedException {
		ebusinessportaladdnewcustomerpage.click_add_a_new_quote();
	}
	
	@Step
	public void click_add_new_business() throws InterruptedException {
		ebusinessportaladdnewcustomerpage.click_add_new_business();
	}
	
	@Step
	public void enter_identification_details(String test_data) throws InterruptedException {
		ebusinessportaladdnewcustomerpage.fill_identification_details_for_new_quote(test_data);
	}

	@Step
	public void savePolicyNumber(String test_data) throws InterruptedException {
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop: for (int row = 2; row <= Row_Count; row++) {

			System.out.println("test data --" + test_data);
			if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				break innerloop;
			}
		}
		String quoteNumber=getDriver().findElement(By.xpath("//td[input[@name='txtInsurerPolicyNo']]")).getText();
		XLS.setCellData(sheetName, "PolicyNumber",rowNum,"'"+quoteNumber+"'");
	}

	public void searchPolicyNumber(String test_flow) {
		// TODO Auto-generated method stub
		
	}
	
	@Step
	public void enterYesterdayDate() throws InterruptedException {
		ebusinessportaladdnewcustomerpage.enter_previousDate();
	}
	
	@Step
	public void enter_NextStartDate() throws InterruptedException {
		ebusinessportaladdnewcustomerpage.enter_NextStartDate();
	}
	
	@Step
	public void enter_TodayStartDate() throws InterruptedException {
		ebusinessportaladdnewcustomerpage.enter_SystemDate_as_StartDate();
	}
	
	
	@Step
	public void enter_LaterEndDate() throws InterruptedException {
		ebusinessportaladdnewcustomerpage.enter_LaterEndDate();
	}
	

	@Step
	public void enter_AttachmentDate_PreviousDate() throws InterruptedException {
		ebusinessportaladdnewcustomerpage.enter_AttachmentDate_PreviousDate();
	}
	
	@Step
	public void enter_AttachmentDate_LaterDate(int noOfDays) throws InterruptedException {
		ebusinessportaladdnewcustomerpage.enter_AttachmentDate_LaterDate(noOfDays);
	}
}

