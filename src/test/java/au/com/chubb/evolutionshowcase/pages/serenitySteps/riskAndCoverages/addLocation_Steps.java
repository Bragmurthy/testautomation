package au.com.chubb.evolutionshowcase.pages.serenitySteps.riskAndCoverages;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class addLocation_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;
	
	DynamicSeleniumFunctions dynamicSeleniumFunctions;
	public addLocation_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root +"/ObjectRepository/riskLocation.properties");
		OR.load(fo);
	}

	@Step
	public void add_location() throws InterruptedException {

	}
@Step
	public void add_edit_location_and_enter_risk_information(String Action, String location, String testData) throws InterruptedException {

		String sheetName = "AddRisklocation";
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		loop: for (int row = 2; row <= Row_Count; row++) {
			if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(testData)) {
				rowNum = row;
				break loop;
			}

		}
//		JavascriptExecutor jsx = (JavascriptExecutor)getDriver();
//		getDriver().switchTo().activeElement().sendKeys(Keys.TAB);

		// Change Address
		int k=Integer.parseInt(location);
		if (Action.equalsIgnoreCase("add"))
		{
			getDriver().findElement(By.xpath(".//*[@id='btnAddProperty']/button")).click();
			Thread.sleep(4000);
		}
		
		if (Action.equalsIgnoreCase("edit"))
		{
			getDriver().findElement(By.id("selectProperty_"+location+"")).click();
			getDriver().findElement(By.xpath("//button[contains(text(),'Edit')]")).click();
			Thread.sleep(4000);
			getDriver().findElement(By.xpath("//button[@title='Toggle search bar']")).click();
			Thread.sleep(2000);
		}
		
		if (XLS.getCellData(sheetName, "ChangeAddress", rowNum).equalsIgnoreCase("Yes")) {
//			getDriver().findElement(By.id("addressEditor_"+k+"_Search"))
//					.sendKeys(XLS.getCellData(sheetName, "Address", rowNum));
//			Thread.sleep(2000);
//			getDriver().findElement(By.id("addressEditor_"+k+"_Search")).sendKeys(Keys.DOWN);
//			Thread.sleep(100);
//			getDriver().findElement(By.id("addressEditor_"+k+"_Search")).sendKeys(Keys.TAB);
			getDriver().findElement(By.id("addressEditor_"+k+"_StreetNo")).clear();
			getDriver().findElement(By.id("addressEditor_"+k+"_StreetNo")).sendKeys("36");
			getDriver().findElement(By.id("addressEditor_"+k+"_StreetName")).clear();
			getDriver().findElement(By.id("addressEditor_"+k+"_StreetName")).sendKeys(" briggs street");
			getDriver().findElement(By.id("addressEditor_"+k+"_City")).clear();
			getDriver().findElement(By.id("addressEditor_"+k+"_City")).sendKeys("Mount waverly");
			getDriver().findElement(By.id("addressEditor_"+k+"_PostCode")).clear();
			getDriver().findElement(By.id("addressEditor_"+k+"_PostCode")).sendKeys("3149");
			new Select(getDriver().findElement(By.id("addressEditor_"+k+"_Country"))).selectByVisibleText("Australia");
			new Select(getDriver().findElement(By.id("addressEditor_"+k+"_State"))).selectByVisibleText("Victoria");
		}
		
		// Property Type
		try
		{
		new Select(getDriver().findElement(By.id("PropertyTypeId"))).selectByVisibleText(XLS.getCellData(sheetName, "PropertyType", rowNum));
		Thread.sleep(100);
		}catch (Exception e)
		{
			
		}

		if (XLS.getCellData(sheetName, "PropertyType", rowNum).equalsIgnoreCase("Other")) {
			try
			{
			getDriver().findElement(By.id("OtherPropertyTypeText"))
					.sendKeys(XLS.getCellData(sheetName, "PropertyTypeOther", rowNum));
			Thread.sleep(100);
			}catch (Exception e)
			{
				
			}

		}
		// Ownership
		try {
			List<WebElement> Ownership = getDriver().findElements(By.id("OwnershipTypeId"));
			if (XLS.getCellData(sheetName, "Ownership", rowNum).equalsIgnoreCase("Owner")) {
				Ownership.get(0).click();
				Thread.sleep(100);
				

			} else {
				Ownership.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// Type of Residence
		try {
			new Select(getDriver().findElement(By.id("ResidenceTypeId")))
					.selectByVisibleText(XLS.getCellData(sheetName, "TypeOfResidence", rowNum));
			Thread.sleep(100);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String TypeOfResidence = XLS.getCellData(sheetName, "TypeOfResidence", rowNum);
		Thread.sleep(100);

		if (TypeOfResidence.equalsIgnoreCase("Holiday Home")) {
			try {
				new Select(getDriver().findElement(By.id("PropertyCheckedFrequencyId")))
						.selectByVisibleText(XLS.getCellData(sheetName, "FrequentPropertyVisits", rowNum));
				Thread.sleep(100);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		if (TypeOfResidence.equalsIgnoreCase("Other")) {
			try {
				getDriver().findElement(By.id("OtherResidenceTypeText"))
						.sendKeys(XLS.getCellData(sheetName, "TypeOfResidenceOther", rowNum));
				Thread.sleep(100);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Valuable Articles coverage

		try {
			List<WebElement> VAC = getDriver().findElements(By.id("VacFlag"));
			if (XLS.getCellData(sheetName, "VAC", rowNum).equalsIgnoreCase("Yes")) {
				VAC.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "VAC", rowNum).equalsIgnoreCase("No")) {
				VAC.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Year of Construction
		String YearOfConstruction = XLS.getCellData(sheetName, "YearOfConstruction", rowNum);
		try {
			getDriver().findElement(By.id("YearOfConstruction"))
			.clear();
			Thread.sleep(1000);

			getDriver().findElement(By.id("YearOfConstruction"))
					.sendKeys(XLS.getCellData(sheetName, "YearOfConstruction", rowNum));
			Thread.sleep(100);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			getDriver().findElement(By.id("YearOfConstruction")).sendKeys(Keys.TAB);
			Thread.sleep(1000);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		int Year = Integer.parseInt(YearOfConstruction);

		if (Year >= 2016) {
			try {
				List<WebElement> NewlyBuiltHome = getDriver().findElements(By.id("NewlyBuiltHomeComplete"));
				if (XLS.getCellData(sheetName, "NewlyBuiltHomes", rowNum).equalsIgnoreCase("Yes")) {
					NewlyBuiltHome.get(0).click();
					Thread.sleep(100);

				} else if (XLS.getCellData(sheetName, "NewlyBuiltHomes", rowNum).equalsIgnoreCase("No")) {
					NewlyBuiltHome.get(1).click();
					Thread.sleep(1500);
					getDriver().findElement(By.id("OutstandingWorkText"))
							.sendKeys(XLS.getCellData(sheetName, "RemainsOutstanding", rowNum));
					Thread.sleep(100);

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (Year >= 2008 && Year < 2016) {
			// Nothing
		} else if (Year <= 2007) {
			try {
				List<WebElement> Rewired = getDriver().findElements(By.id("RewiredFlag"));
				if (XLS.getCellData(sheetName, "Re-Wired", rowNum).equalsIgnoreCase("Yes")) {
					Rewired.get(0).click();
					Thread.sleep(1500);

					getDriver().findElement(By.id("RewiredYearNumber"))
							.sendKeys(XLS.getCellData(sheetName, "YearRe-Wired", rowNum));
					
					Thread.sleep(1500);
					getDriver().findElement(By.id("RewiredYearNumber"))
					.sendKeys(Keys.TAB);
					Thread.sleep(1000);


				} else if (XLS.getCellData(sheetName, "Re-Wired", rowNum).equalsIgnoreCase("No")) {
					Rewired.get(1).click();
					Thread.sleep(100);

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				List<WebElement> RePlumbed = getDriver().findElements(By.id("ReplumbedFlag"));
				if (XLS.getCellData(sheetName, "Re-Plumbed", rowNum).equalsIgnoreCase("Yes")) {
					RePlumbed.get(0).click();
					Thread.sleep(1500);

					getDriver().findElement(By.id("ReplumbedYearNumber"))
							.sendKeys(XLS.getCellData(sheetName, "YearRe-Pumbed", rowNum));
					Thread.sleep(1500);
					getDriver().findElement(By.id("ReplumbedYearNumber"))
					.sendKeys(Keys.TAB);
					Thread.sleep(1000);

				} else if (XLS.getCellData(sheetName, "Re-Plumbed", rowNum).equalsIgnoreCase("No")) {
					RePlumbed.get(1).click();
					Thread.sleep(100);

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				List<WebElement> ReRoofed = getDriver().findElements(By.id("ReroofedFlag"));
				if (XLS.getCellData(sheetName, "Re-Roofed", rowNum).equalsIgnoreCase("Yes")) {
					ReRoofed.get(0).click();
					Thread.sleep(1500);

					getDriver().findElement(By.id("ReroofedYearNumber"))
							.sendKeys(XLS.getCellData(sheetName, "YearRe-Roofed", rowNum));
					
					Thread.sleep(1500);
					getDriver().findElement(By.id("ReroofedYearNumber"))
					.sendKeys(Keys.TAB);
					Thread.sleep(1000);

				} else if (XLS.getCellData(sheetName, "Re-Plumbed", rowNum).equalsIgnoreCase("No")) {
					ReRoofed.get(1).click();
					Thread.sleep(100);

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Fire Reistant
		try {
			List<WebElement> FireResistant = getDriver().findElements(By.id("FireResistantFlag"));
			if (XLS.getCellData(sheetName, "FireResistant", rowNum).equalsIgnoreCase("Yes")) {
				FireResistant.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "FireResistant", rowNum).equalsIgnoreCase("No")) {
				FireResistant.get(1).click();
				Thread.sleep(1000);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// for Rural Postal Codes - if fields not available will be catched in
		// exception


			try {
				List<WebElement> DistanceToFireStation = getDriver().findElements(By.id("FireHallDistanceId"));
				if (XLS.getCellData(sheetName, "DistanceFireStation", rowNum).equalsIgnoreCase("< 8 kilometres")) {
					DistanceToFireStation.get(0).click();
					Thread.sleep(100);

				} else if (XLS.getCellData(sheetName, "DistanceFireStation", rowNum).equalsIgnoreCase("> 8 kilometres")) {
					DistanceToFireStation.get(1).click();
					Thread.sleep(100);

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				new Select(getDriver().findElement(By.id("FireHallTypeId")))
						.selectByVisibleText(XLS.getCellData(sheetName, "TypeofFireService", rowNum));
				Thread.sleep(100);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			try {
				List<WebElement> DistanceFromFireHydrant = getDriver().findElements(By.id("FireHydrantDistanceId"));
				if (XLS.getCellData(sheetName, "DistanceFireHydrant", rowNum).equalsIgnoreCase("< 200 metres")) {
					DistanceFromFireHydrant.get(0).click();
					Thread.sleep(100);

				} else if (XLS.getCellData(sheetName, "DistanceFireHydrant", rowNum).equalsIgnoreCase("> 200 metres")) {
					DistanceFromFireHydrant.get(1).click();
					Thread.sleep(100);

				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		// External Construction

		/*
		 * WebElement External_Construction=getDriver().findElement(By.id(
		 * "ExternalConstructionId"));
		 * ((JavascriptExecutor)getDriver()).executeScript(
		 * "arguments[0].scrollIntoView();" ,External_Construction);
		 */
		String ExternalConstruction = XLS.getCellData(sheetName, "ExternalConstruction", rowNum);
		try {
			new Select(getDriver().findElement(By.id("ExternalConstructionId"))).selectByVisibleText(ExternalConstruction);
			Thread.sleep(100);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (ExternalConstruction.equalsIgnoreCase("Other")) {
			try {
				getDriver().findElement(By.id("OtherExternalConstructionMaterialText"))
						.sendKeys(XLS.getCellData(sheetName, "EnterConstructionOther", rowNum));
				Thread.sleep(100);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Roof Construction

		String RoofConstruction = XLS.getCellData(sheetName, "RoofConstruction", rowNum);
		try {
			new Select(getDriver().findElement(By.id("RoofConstructionId"))).selectByVisibleText(RoofConstruction);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Thread.sleep(100);

		if (RoofConstruction.equalsIgnoreCase("Other")) {
			try {
				getDriver().findElement(By.id("OtherRoofingMaterialText"))
						.sendKeys(XLS.getCellData(sheetName, "RoofConstructionOther", rowNum));
				Thread.sleep(100);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		try {
			List<WebElement> DistanceToFireStation = getDriver().findElements(By.id("FireHallDistanceId"));
			if (XLS.getCellData(sheetName, "DistanceFireStation", rowNum).equalsIgnoreCase("< 8 kilometres")) {
				DistanceToFireStation.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "DistanceFireStation", rowNum).equalsIgnoreCase("> 8 kilometres")) {
				DistanceToFireStation.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Basement

		try {
			List<WebElement> Basement = getDriver().findElements(By.id("BasementFlag"));
			if (XLS.getCellData(sheetName, "PropertyBasement", rowNum).equalsIgnoreCase("Yes")) {
				Basement.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "PropertyBasement", rowNum).equalsIgnoreCase("No")) {
				Basement.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Box Gutter

		try {
			List<WebElement> BoxGutter = getDriver().findElements(By.id("BoxGutterFlag"));
			if (XLS.getCellData(sheetName, "PropertyGutters", rowNum).equalsIgnoreCase("Yes")) {
				BoxGutter.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "PropertyGutters", rowNum).equalsIgnoreCase("No")) {
				BoxGutter.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Currently Occupied

		try {
			List<WebElement> CurrentlyOccupied = getDriver().findElements(By.id("OccupiedFlag"));
			if (XLS.getCellData(sheetName, "CurrentlyOccupied", rowNum).equalsIgnoreCase("Yes")) {
				CurrentlyOccupied.get(0).click();
				Thread.sleep(100);

				// RentedOthers
				List<WebElement> RentedOthers = getDriver().findElements(By.id("RentedFlag"));
				if (XLS.getCellData(sheetName, "RentedOthers", rowNum).equalsIgnoreCase("Yes")) {
					RentedOthers.get(0).click();
					Thread.sleep(100);

				} else if (XLS.getCellData(sheetName, "RentedOthers", rowNum).equalsIgnoreCase("No")) {
					RentedOthers.get(1).click();
					Thread.sleep(100);

				}

			} else if (XLS.getCellData(sheetName, "CurrentlyOccupied", rowNum).equalsIgnoreCase("No")) {
				CurrentlyOccupied.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Currently Vacant

		try {
			List<WebElement> CurrentlyVacant = getDriver().findElements(By.id("VacantFlag"));
			if (XLS.getCellData(sheetName, "CurentlyVacant", rowNum).equalsIgnoreCase("Yes")) {
				CurrentlyVacant.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "CurentlyVacant", rowNum).equalsIgnoreCase("No")) {
				CurrentlyVacant.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Hertiage listing

		try {
			new Select(getDriver().findElement(By.id("HeritageListedId")))
					.selectByVisibleText(XLS.getCellData(sheetName, "HertiageListing", rowNum));
			Thread.sleep(100);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Rennovation next 12 months

		try {
			List<WebElement> Rennovation = getDriver().findElements(By.id("CourseOfConstructionFlag"));
			if (XLS.getCellData(sheetName, "RennovationNext12Months", rowNum).equalsIgnoreCase("Yes")) {
				Rennovation.get(0).click();

			} else if (XLS.getCellData(sheetName, "RennovationNext12Months", rowNum).equalsIgnoreCase("No")) {
				Rennovation.get(1).click();
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// ---------------------------Security
		// ---------------------------------------------------------//

		// Burglar Alarm

		try {
			new Select(getDriver().findElement(By.id("BurglarAlarmId")))
					.selectByVisibleText(XLS.getCellData(sheetName, "BurglarAlarm", rowNum));
			Thread.sleep(100);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Smoke Detectors

		try {
			new Select(getDriver().findElement(By.id("SmokeDetectorId")))
					.selectByVisibleText(XLS.getCellData(sheetName, "SmokeDetectors", rowNum));
			Thread.sleep(100);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Dead locks
		try {
			List<WebElement> DeadLocks = getDriver().findElements(By.id("DeadlockedDoorsFlag"));
			if (XLS.getCellData(sheetName, "DeadLocks", rowNum).equalsIgnoreCase("Yes")) {
				DeadLocks.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "DeadLocks", rowNum).equalsIgnoreCase("No")) {
				DeadLocks.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Window locks
		try {
			List<WebElement> WindowLocks = getDriver().findElements(By.id("KeyedWindowsFlag"));
			if (XLS.getCellData(sheetName, "WindowLocks", rowNum).equalsIgnoreCase("Yes")) {
				WindowLocks.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "WindowLocks", rowNum).equalsIgnoreCase("No")) {
				WindowLocks.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Safe

		try {
			String Safe = XLS.getCellData(sheetName, "Safe", rowNum);
			new Select(getDriver().findElement(By.id("SafeTypeId"))).selectByVisibleText(Safe);
			Thread.sleep(100);

			if (Safe.equalsIgnoreCase("Freestanding") || Safe.equalsIgnoreCase("Fixed")) {
				getDriver().findElement(By.id("SafeCashRatingAmount"))
						.sendKeys(XLS.getCellData(sheetName, "CashRating", rowNum));
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String PropertyType = XLS.getCellData(sheetName, "PropertyType", rowNum);

		// Gated Community
		try {
			List<WebElement> GatedCommunity = getDriver().findElements(By.id("GatedCommunityFlag"));
			if (XLS.getCellData(sheetName, "GatedCommunity", rowNum).equalsIgnoreCase("Yes")) {
				GatedCommunity.get(0).click();
				Thread.sleep(100);
				

			} else if (XLS.getCellData(sheetName, "GatedCommunity", rowNum).equalsIgnoreCase("No")) {
				GatedCommunity.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Security Protection

		try {
			if (PropertyType.equalsIgnoreCase("House") || PropertyType.equalsIgnoreCase("Other")) {
				List<WebElement> SecurityProtection = getDriver().findElements(By.id("SecurityPersonnelFlag"));
				if (XLS.getCellData(sheetName, "SecurityProtection", rowNum).equalsIgnoreCase("Yes")) {
					SecurityProtection.get(0).click();
					Thread.sleep(100);

				} else if (XLS.getCellData(sheetName, "SecurityProtection", rowNum).equalsIgnoreCase("No")) {
					SecurityProtection.get(1).click();
					Thread.sleep(100);

				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Care taker on premise
		try {
			if (PropertyType.equalsIgnoreCase("House") || PropertyType.equalsIgnoreCase("Other")) {
				List<WebElement> CareTakerPremise = getDriver().findElements(By.id("CaretakerOnPremisesFlag"));
				if (XLS.getCellData(sheetName, "CareTakerOnPremise", rowNum).equalsIgnoreCase("Yes")) {
					CareTakerPremise.get(0).click();
					Thread.sleep(100);

				} else if (XLS.getCellData(sheetName, "CareTakerOnPremise", rowNum).equalsIgnoreCase("No")) {
					CareTakerPremise.get(1).click();
					Thread.sleep(100);

				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Building Access Security Protection

		if (PropertyType.equalsIgnoreCase("Unit")) {
			List<WebElement> BuildingAccess = getDriver().findElements(By.id("BuildingAccessSecurityFlag"));
			if (XLS.getCellData(sheetName, "BuildingAccess", rowNum).equalsIgnoreCase("Yes")) {
				BuildingAccess.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "BuildingAccess", rowNum).equalsIgnoreCase("No")) {
				BuildingAccess.get(1).click();
				Thread.sleep(100);

			}
		}

		// Signal Continuity
		try {
			List<WebElement> SignalContinuity = getDriver().findElements(By.id("SignalContinuityFlag"));
			if (XLS.getCellData(sheetName, "SignalContinuityProtection", rowNum).equalsIgnoreCase("Yes")) {
				SignalContinuity.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "SignalContinuityProtection", rowNum).equalsIgnoreCase("No")) {
				SignalContinuity.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Backup Generator
		try {
			if (PropertyType.equalsIgnoreCase("House") || PropertyType.equalsIgnoreCase("Other")) {
				List<WebElement> BackupGenerator = getDriver().findElements(By.id("BackupGeneratorFlag"));
				if (XLS.getCellData(sheetName, "BackUpGenerator", rowNum).equalsIgnoreCase("Yes")) {
					BackupGenerator.get(0).click();
					Thread.sleep(100);

				} else if (XLS.getCellData(sheetName, "BackUpGenerator", rowNum).equalsIgnoreCase("No")) {
					BackupGenerator.get(1).click();
					Thread.sleep(100);

				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Other Security Information
		try
		{
		getDriver().findElement(By.id("OtherSecurityText"))
				.sendKeys(XLS.getCellData(sheetName, "OtherSecurityInformation", rowNum));
		Thread.sleep(100);
		} catch (Exception e)
		{
			
		}
		// Save this New Location
		try
		{
		getDriver().findElement(By.xpath(".//*[@id='addRiskLocation']/div[3]/button[2]")).click();
		}catch (Exception e)
		{
		}
			
		
		Thread.sleep(3000);
		System.out.println("Save this new location clicked ----");
		Thread.sleep(28000);
		
		if (getDriver().findElement(By.xpath(".//*[@id='btnAddProperty']/button")).isDisplayed())
		{
			Assert.assertTrue(Action+"functionality for Risk location in Risk and Coverages", true);
		}else
		{
			Assert.assertTrue(Action+"functionality for Risk location in Risk and Coverages", false);

		}

		// if matching Risk location exists
		
		
		if (XLS.getCellData(sheetName, "ExistingRiskLocation", rowNum).equalsIgnoreCase("Yes")) {
			try {
				getDriver().switchTo().activeElement().sendKeys(Keys.ESCAPE);
			} catch (Exception e) {

			}
		}
		Thread.sleep(2000);
    	/*getDriver().findElement(By.id("actionFooter")).click();
		Thread.sleep(1000);
		System.out.println("Clicked Base footer");
		System.out.println("Before clicking save progress --- ");
		System.out.println("Button Display " + getDriver().findElement(By.id("btnSaveProgress")).isDisplayed());
		getDriver().findElement(By.id("btnSaveProgress")).click();
		System.out.println("After clicking save progress --- ");
		Thread.sleep(2000);
        */
	}

@Step
public void verifyFieldnotDisplayed(String fieldName) throws IOException {
	try{
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root +"\\ObjectRepository\\riskLocation.properties");
		OR.load(fo);
		if(OR.getProperty(fieldName).startsWith("\\"))
		getDriver().findElement(By.xpath(OR.getProperty(fieldName))).isDisplayed();
		else
	    getDriver().findElement(By.id(OR.getProperty(fieldName))).isDisplayed();
	}

	catch(Exception e)
	{
		if(e.getClass().getName().contains("NoSuchElement")){
		Assert.assertTrue("Field "+fieldName+" is not displayed", true);
	  }
	else {
		System.out.println("test Failed ");
		Assert.assertTrue("Field "+fieldName+" is displayed", false);
	}
	}
	
}

public void verifyFiledsDisplayed() {
if(getDriver().findElement(By.xpath("//button[@id='btnAddCoverage']")).isDisplayed() && getDriver().findElement(By.xpath("//button[contains(text(),'Edit')]")).isDisplayed() &&getDriver().findElement(By.xpath("//button[contains(@data-bind,'deleteProperty')]")).isDisplayed())
{
	Assert.assertTrue("Fields Edit,Add Coverage,Delete are displayed", true);
}
else
{
	Assert.assertTrue("Fields Edit,Add Coverage,Delete are not displayed", false);
}
}

public void verifyFieldEnabled(String fieldName) {
	if(getDriver().findElement(By.xpath("//button[contains(text(),'Next')]")).getAttribute("data-bind").contains("disabled"))
	{
		Assert.assertTrue("Filed "+fieldName+" is not Enabled", false);
		
	}
	else
		Assert.assertTrue("Filed "+fieldName+" is Enabled", true);
}

public void verifyFooter() {
	boolean privacyStatement=getDriver().findElement(By.xpath("//a[contains(text(),'Privacy Statement')]")).isDisplayed();
	boolean policyWording=getDriver().findElement(By.xpath("//a[contains(text(),'PDS and Policy Wording')]")).isDisplayed();
	boolean masterBrochure=getDriver().findElement(By.xpath("//a[contains(text(),'Masterpiece Brochure')]")).isDisplayed();
	boolean chubbInsured=getDriver().findElement(By.xpath("//li[contains(text(),'Chubb. Insured.')]")).isDisplayed();
	boolean chubbLimited=getDriver().findElement(By.xpath("//span[contains(text(),'©2017 Chubb Insurance Australia Limited. Chubb®, its logo and Chubb. Insured')]")).isDisplayed();
	if(privacyStatement &&policyWording &&masterBrochure&&chubbInsured&&chubbLimited)
	{
		Assert.assertTrue("Footer fileds \"Privacy Statement,PDS and Policy Wording,Masterpiece Brochure,Chubb. Insured,©2017 Chubb Insurance Australia Limited.\" are displayed", true);
	}
	else
		Assert.assertTrue("Footer fileds \"Privacy Statement,PDS and Policy Wording,Masterpiece Brochure,Chubb. Insured,©2017 Chubb Insurance Australia Limited.\" are not displayed", false);
	
}
@Step
public void verifyUserOnRisksAndCoveragesPage() {
	if(getDriver().findElement(By.xpath("//button[contains(text(),'Add Location')]")).isDisplayed())
	{
		Assert.assertTrue("User is On Risks and Coverages Page", true);
		
	}
	else
		Assert.assertTrue("User is not On Risks and Coverages Page", false);
}

	@Step
	public void click_on_VAC_type() {
		getDriver().findElement(By.xpath("//span[contains(@data-bind,'getCategoryText()')]")).click();		
	}
	
	@Step
	public void editrisk_information_MTA(String location, String testData) throws InterruptedException, IOException {
		boolean flag = true;
		String sheetName = "AddRisklocation";
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		loop: for (int row = 2; row <= Row_Count; row++) {
			if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(testData)) {
				rowNum = row;
				break loop;
			}

		}
		JavascriptExecutor jsx = (JavascriptExecutor)getDriver();
		getDriver().switchTo().activeElement().sendKeys(Keys.TAB);

		getDriver().findElement(By.id("selectProperty_"+location+"")).click();
		getDriver().findElement(By.xpath("//button[contains(text(),'Edit')]")).click();
		Thread.sleep(4000);
		
		dynamicSeleniumFunctions.verifyFieldNonEditable("ClientAddress");
		try{
			new Select(getDriver().findElement(By.id("PropertyTypeId"))).selectByVisibleText("Unit");
		}
		catch (Exception e)
		{
			if(e.getMessage().contains("Element is not enabled"))			
				flag=false;
		}
		if (flag)
		{
			Assert.assertTrue("Property Element is readonly", true);
		}else
		{
			Assert.assertTrue("Property Element is readonly", false);
		}		
		
		try{
			new Select(getDriver().findElement(By.id("ResidenceTypeId"))).selectByVisibleText("Investment Property");
		}
		catch (Exception e)
		{
			if(e.getMessage().contains("Element is not enabled"))			
				flag=false;
		}
		if (flag)
		{
			Assert.assertTrue("Residence Element is readonly", true);
		}else
		{
			Assert.assertTrue("Residence Element is readonly", false);
		}		
		
				// Valuable Articles coverage

		try {
			List<WebElement> VAC = getDriver().findElements(By.id("VacFlag"));
			if (XLS.getCellData(sheetName, "VAC", rowNum).equalsIgnoreCase("Yes")) {
				VAC.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "VAC", rowNum).equalsIgnoreCase("No")) {
				VAC.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Year of Construction
		String YearOfConstruction = XLS.getCellData(sheetName, "YearOfConstruction", rowNum);
		try {
			getDriver().findElement(By.id("YearOfConstruction"))
			.clear();
			Thread.sleep(1000);

			getDriver().findElement(By.id("YearOfConstruction"))
					.sendKeys(XLS.getCellData(sheetName, "YearOfConstruction", rowNum));
			Thread.sleep(100);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			getDriver().findElement(By.id("YearOfConstruction")).sendKeys(Keys.TAB);
			Thread.sleep(1000);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		int Year = Integer.parseInt(YearOfConstruction);

		if (Year >= 2016) {
			try {
				List<WebElement> NewlyBuiltHome = getDriver().findElements(By.id("NewlyBuiltHomeComplete"));
				if (XLS.getCellData(sheetName, "NewlyBuiltHomes", rowNum).equalsIgnoreCase("Yes")) {
					NewlyBuiltHome.get(0).click();
					Thread.sleep(100);

				} else if (XLS.getCellData(sheetName, "NewlyBuiltHomes", rowNum).equalsIgnoreCase("No")) {
					NewlyBuiltHome.get(1).click();
					Thread.sleep(1500);
					getDriver().findElement(By.id("OutstandingWorkText"))
							.sendKeys(XLS.getCellData(sheetName, "RemainsOutstanding", rowNum));
					Thread.sleep(100);

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (Year >= 2008 && Year < 2016) {
			// Nothing
		} else if (Year <= 2007) {
			try {
				List<WebElement> Rewired = getDriver().findElements(By.id("RewiredFlag"));
				if (XLS.getCellData(sheetName, "Re-Wired", rowNum).equalsIgnoreCase("Yes")) {
					Rewired.get(0).click();
					Thread.sleep(1500);

					getDriver().findElement(By.id("RewiredYearNumber"))
							.sendKeys(XLS.getCellData(sheetName, "YearRe-Wired", rowNum));
					
					Thread.sleep(1500);
					getDriver().findElement(By.id("RewiredYearNumber"))
					.sendKeys(Keys.TAB);
					Thread.sleep(1000);


				} else if (XLS.getCellData(sheetName, "Re-Wired", rowNum).equalsIgnoreCase("No")) {
					Rewired.get(1).click();
					Thread.sleep(100);

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				List<WebElement> RePlumbed = getDriver().findElements(By.id("ReplumbedFlag"));
				if (XLS.getCellData(sheetName, "Re-Plumbed", rowNum).equalsIgnoreCase("Yes")) {
					RePlumbed.get(0).click();
					Thread.sleep(1500);

					getDriver().findElement(By.id("ReplumbedYearNumber"))
							.sendKeys(XLS.getCellData(sheetName, "YearRe-Pumbed", rowNum));
					Thread.sleep(1500);
					getDriver().findElement(By.id("ReplumbedYearNumber"))
					.sendKeys(Keys.TAB);
					Thread.sleep(1000);

				} else if (XLS.getCellData(sheetName, "Re-Plumbed", rowNum).equalsIgnoreCase("No")) {
					RePlumbed.get(1).click();
					Thread.sleep(100);

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				List<WebElement> ReRoofed = getDriver().findElements(By.id("ReroofedFlag"));
				if (XLS.getCellData(sheetName, "Re-Roofed", rowNum).equalsIgnoreCase("Yes")) {
					ReRoofed.get(0).click();
					Thread.sleep(1500);

					getDriver().findElement(By.id("ReroofedYearNumber"))
							.sendKeys(XLS.getCellData(sheetName, "YearRe-Roofed", rowNum));
					
					Thread.sleep(1500);
					getDriver().findElement(By.id("ReroofedYearNumber"))
					.sendKeys(Keys.TAB);
					Thread.sleep(1000);

				} else if (XLS.getCellData(sheetName, "Re-Plumbed", rowNum).equalsIgnoreCase("No")) {
					ReRoofed.get(1).click();
					Thread.sleep(100);

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Fire Reistant
		try {
			List<WebElement> FireResistant = getDriver().findElements(By.id("FireResistantFlag"));
			if (XLS.getCellData(sheetName, "FireResistant", rowNum).equalsIgnoreCase("Yes")) {
				FireResistant.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "FireResistant", rowNum).equalsIgnoreCase("No")) {
				FireResistant.get(1).click();
				Thread.sleep(1000);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// for Rural Postal Codes - if fields not available will be catched in
		// exception


			try {
				List<WebElement> DistanceToFireStation = getDriver().findElements(By.id("FireHallDistanceId"));
				if (XLS.getCellData(sheetName, "DistanceFireStation", rowNum).equalsIgnoreCase("< 8 kilometres")) {
					DistanceToFireStation.get(0).click();
					Thread.sleep(100);

				} else if (XLS.getCellData(sheetName, "DistanceFireStation", rowNum).equalsIgnoreCase("> 8 kilometres")) {
					DistanceToFireStation.get(1).click();
					Thread.sleep(100);

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				new Select(getDriver().findElement(By.id("FireHallTypeId")))
						.selectByVisibleText(XLS.getCellData(sheetName, "TypeofFireService", rowNum));
				Thread.sleep(100);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			try {
				List<WebElement> DistanceFromFireHydrant = getDriver().findElements(By.id("FireHydrantDistanceId"));
				if (XLS.getCellData(sheetName, "DistanceFireHydrant", rowNum).equalsIgnoreCase("< 200 metres")) {
					DistanceFromFireHydrant.get(0).click();
					Thread.sleep(100);

				} else if (XLS.getCellData(sheetName, "DistanceFireHydrant", rowNum).equalsIgnoreCase("> 200 metres")) {
					DistanceFromFireHydrant.get(1).click();
					Thread.sleep(100);

				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		// External Construction

		/*
		 * WebElement External_Construction=getDriver().findElement(By.id(
		 * "ExternalConstructionId"));
		 * ((JavascriptExecutor)getDriver()).executeScript(
		 * "arguments[0].scrollIntoView();" ,External_Construction);
		 */
		String ExternalConstruction = XLS.getCellData(sheetName, "ExternalConstruction", rowNum);
		try {
			new Select(getDriver().findElement(By.id("ExternalConstructionId"))).selectByVisibleText(ExternalConstruction);
			Thread.sleep(100);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (ExternalConstruction.equalsIgnoreCase("Other")) {
			try {
				getDriver().findElement(By.id("OtherExternalConstructionMaterialText"))
						.sendKeys(XLS.getCellData(sheetName, "EnterConstructionOther", rowNum));
				Thread.sleep(100);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Roof Construction

		String RoofConstruction = XLS.getCellData(sheetName, "RoofConstruction", rowNum);
		try {
			new Select(getDriver().findElement(By.id("RoofConstructionId"))).selectByVisibleText(RoofConstruction);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Thread.sleep(100);

		if (RoofConstruction.equalsIgnoreCase("Other")) {
			try {
				getDriver().findElement(By.id("OtherRoofingMaterialText"))
						.sendKeys(XLS.getCellData(sheetName, "RoofConstructionOther", rowNum));
				Thread.sleep(100);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		try {
			List<WebElement> DistanceToFireStation = getDriver().findElements(By.id("FireHallDistanceId"));
			if (XLS.getCellData(sheetName, "DistanceFireStation", rowNum).equalsIgnoreCase("< 8 kilometres")) {
				DistanceToFireStation.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "DistanceFireStation", rowNum).equalsIgnoreCase("> 8 kilometres")) {
				DistanceToFireStation.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Basement

		try {
			List<WebElement> Basement = getDriver().findElements(By.id("BasementFlag"));
			if (XLS.getCellData(sheetName, "PropertyBasement", rowNum).equalsIgnoreCase("Yes")) {
				Basement.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "PropertyBasement", rowNum).equalsIgnoreCase("No")) {
				Basement.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Box Gutter

		try {
			List<WebElement> BoxGutter = getDriver().findElements(By.id("BoxGutterFlag"));
			if (XLS.getCellData(sheetName, "PropertyGutters", rowNum).equalsIgnoreCase("Yes")) {
				BoxGutter.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "PropertyGutters", rowNum).equalsIgnoreCase("No")) {
				BoxGutter.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Currently Occupied

		try {
			List<WebElement> CurrentlyOccupied = getDriver().findElements(By.id("OccupiedFlag"));
			if (XLS.getCellData(sheetName, "CurrentlyOccupied", rowNum).equalsIgnoreCase("Yes")) {
				CurrentlyOccupied.get(0).click();
				Thread.sleep(100);

				// RentedOthers
				List<WebElement> RentedOthers = getDriver().findElements(By.id("RentedFlag"));
				if (XLS.getCellData(sheetName, "RentedOthers", rowNum).equalsIgnoreCase("Yes")) {
					RentedOthers.get(0).click();
					Thread.sleep(100);

				} else if (XLS.getCellData(sheetName, "RentedOthers", rowNum).equalsIgnoreCase("No")) {
					RentedOthers.get(1).click();
					Thread.sleep(100);

				}

			} else if (XLS.getCellData(sheetName, "CurrentlyOccupied", rowNum).equalsIgnoreCase("No")) {
				CurrentlyOccupied.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Currently Vacant

		try {
			List<WebElement> CurrentlyVacant = getDriver().findElements(By.id("VacantFlag"));
			if (XLS.getCellData(sheetName, "CurentlyVacant", rowNum).equalsIgnoreCase("Yes")) {
				CurrentlyVacant.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "CurentlyVacant", rowNum).equalsIgnoreCase("No")) {
				CurrentlyVacant.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Hertiage listing

		try {
			new Select(getDriver().findElement(By.id("HeritageListedId")))
					.selectByVisibleText(XLS.getCellData(sheetName, "HertiageListing", rowNum));
			Thread.sleep(100);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Rennovation next 12 months

		try {
			List<WebElement> Rennovation = getDriver().findElements(By.id("CourseOfConstructionFlag"));
			if (XLS.getCellData(sheetName, "RennovationNext12Months", rowNum).equalsIgnoreCase("Yes")) {
				Rennovation.get(0).click();

			} else if (XLS.getCellData(sheetName, "RennovationNext12Months", rowNum).equalsIgnoreCase("No")) {
				Rennovation.get(1).click();
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// ---------------------------Security
		// ---------------------------------------------------------//

		// Burglar Alarm

		try {
			new Select(getDriver().findElement(By.id("BurglarAlarmId")))
					.selectByVisibleText(XLS.getCellData(sheetName, "BurglarAlarm", rowNum));
			Thread.sleep(100);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Smoke Detectors

		try {
			new Select(getDriver().findElement(By.id("SmokeDetectorId")))
					.selectByVisibleText(XLS.getCellData(sheetName, "SmokeDetectors", rowNum));
			Thread.sleep(100);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Dead locks
		try {
			List<WebElement> DeadLocks = getDriver().findElements(By.id("DeadlockedDoorsFlag"));
			if (XLS.getCellData(sheetName, "DeadLocks", rowNum).equalsIgnoreCase("Yes")) {
				DeadLocks.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "DeadLocks", rowNum).equalsIgnoreCase("No")) {
				DeadLocks.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Window locks
		try {
			List<WebElement> WindowLocks = getDriver().findElements(By.id("KeyedWindowsFlag"));
			if (XLS.getCellData(sheetName, "WindowLocks", rowNum).equalsIgnoreCase("Yes")) {
				WindowLocks.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "WindowLocks", rowNum).equalsIgnoreCase("No")) {
				WindowLocks.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Safe

		try {
			String Safe = XLS.getCellData(sheetName, "Safe", rowNum);
			new Select(getDriver().findElement(By.id("SafeTypeId"))).selectByVisibleText(Safe);
			Thread.sleep(100);

			if (Safe.equalsIgnoreCase("Freestanding") || Safe.equalsIgnoreCase("Fixed")) {
				getDriver().findElement(By.id("SafeCashRatingAmount"))
						.sendKeys(XLS.getCellData(sheetName, "CashRating", rowNum));
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String PropertyType = XLS.getCellData(sheetName, "PropertyType", rowNum);

		// Gated Community
		try {
			List<WebElement> GatedCommunity = getDriver().findElements(By.id("GatedCommunityFlag"));
			if (XLS.getCellData(sheetName, "GatedCommunity", rowNum).equalsIgnoreCase("Yes")) {
				GatedCommunity.get(0).click();
				Thread.sleep(100);
				

			} else if (XLS.getCellData(sheetName, "GatedCommunity", rowNum).equalsIgnoreCase("No")) {
				GatedCommunity.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Security Protection

		try {
			if (PropertyType.equalsIgnoreCase("House") || PropertyType.equalsIgnoreCase("Other")) {
				List<WebElement> SecurityProtection = getDriver().findElements(By.id("SecurityPersonnelFlag"));
				if (XLS.getCellData(sheetName, "SecurityProtection", rowNum).equalsIgnoreCase("Yes")) {
					SecurityProtection.get(0).click();
					Thread.sleep(100);

				} else if (XLS.getCellData(sheetName, "SecurityProtection", rowNum).equalsIgnoreCase("No")) {
					SecurityProtection.get(1).click();
					Thread.sleep(100);

				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Care taker on premise
		try {
			if (PropertyType.equalsIgnoreCase("House") || PropertyType.equalsIgnoreCase("Other")) {
				List<WebElement> CareTakerPremise = getDriver().findElements(By.id("CaretakerOnPremisesFlag"));
				if (XLS.getCellData(sheetName, "CareTakerOnPremise", rowNum).equalsIgnoreCase("Yes")) {
					CareTakerPremise.get(0).click();
					Thread.sleep(100);

				} else if (XLS.getCellData(sheetName, "CareTakerOnPremise", rowNum).equalsIgnoreCase("No")) {
					CareTakerPremise.get(1).click();
					Thread.sleep(100);

				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Building Access Security Protection

		if (PropertyType.equalsIgnoreCase("Unit")) {
			List<WebElement> BuildingAccess = getDriver().findElements(By.id("BuildingAccessSecurityFlag"));
			if (XLS.getCellData(sheetName, "BuildingAccess", rowNum).equalsIgnoreCase("Yes")) {
				BuildingAccess.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "BuildingAccess", rowNum).equalsIgnoreCase("No")) {
				BuildingAccess.get(1).click();
				Thread.sleep(100);

			}
		}

		// Signal Continuity
		try {
			List<WebElement> SignalContinuity = getDriver().findElements(By.id("SignalContinuityFlag"));
			if (XLS.getCellData(sheetName, "SignalContinuityProtection", rowNum).equalsIgnoreCase("Yes")) {
				SignalContinuity.get(0).click();
				Thread.sleep(100);

			} else if (XLS.getCellData(sheetName, "SignalContinuityProtection", rowNum).equalsIgnoreCase("No")) {
				SignalContinuity.get(1).click();
				Thread.sleep(100);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Backup Generator
		try {
			if (PropertyType.equalsIgnoreCase("House") || PropertyType.equalsIgnoreCase("Other")) {
				List<WebElement> BackupGenerator = getDriver().findElements(By.id("BackupGeneratorFlag"));
				if (XLS.getCellData(sheetName, "BackUpGenerator", rowNum).equalsIgnoreCase("Yes")) {
					BackupGenerator.get(0).click();
					Thread.sleep(100);

				} else if (XLS.getCellData(sheetName, "BackUpGenerator", rowNum).equalsIgnoreCase("No")) {
					BackupGenerator.get(1).click();
					Thread.sleep(100);

				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Other Security Information
		try
		{
		getDriver().findElement(By.id("OtherSecurityText"))
				.sendKeys(XLS.getCellData(sheetName, "OtherSecurityInformation", rowNum));
		Thread.sleep(100);
		} catch (Exception e)
		{
			
		}
		// Save this New Location
		try
		{
		getDriver().findElement(By.xpath(".//*[@id='addRiskLocation']/div[3]/button[2]")).click();
		}catch (Exception e)
		{
		}
			
		
		Thread.sleep(3000);
		System.out.println("Save this new location clicked ----");
		Thread.sleep(20000);
		
		if (getDriver().findElement(By.xpath(".//*[@id='btnAddProperty']/button")).isDisplayed())
		{
			Assert.assertTrue("Edit functionality for Risk location in Risk and Coverages", true);
		}else
		{
			Assert.assertTrue("Edit functionality for Risk location in Risk and Coverages", false);

		}

		// if matching Risk location exists
		
		
		if (XLS.getCellData(sheetName, "ExistingRiskLocation", rowNum).equalsIgnoreCase("Yes")) {
			try {
				getDriver().switchTo().activeElement().sendKeys(Keys.ESCAPE);
			} catch (Exception e) {

			}
		}
		Thread.sleep(2000);
    	/*getDriver().findElement(By.id("actionFooter")).click();
		Thread.sleep(1000);
		System.out.println("Clicked Base footer");
		System.out.println("Before clicking save progress --- ");
		System.out.println("Button Display " + getDriver().findElement(By.id("btnSaveProgress")).isDisplayed());
		getDriver().findElement(By.id("btnSaveProgress")).click();
		System.out.println("After clicking save progress --- ");
		Thread.sleep(2000);
        */
		
	}
	
	@Step
	public void Flood_coverage_not_available()
	{
		String expectedText = "Flood coverage may not be available for this location – please contact Chubb";
		String floodText_App = getDriver().findElement(By.xpath("//p[contains(@data-bind,'showFloodCoverNotAvailableMsg()')]")).getText();
		if(floodText_App.contains(expectedText))
		{
			Assert.assertTrue("Able to see Flood coverage may not be available for this location – please contact Chubb", true);
			
		}
		else
			Assert.assertTrue("Flood coverage may not be available for this location – please contact Chubb not dislayed", false);
	}
	
	@Step
	public void flood_coverage_RadioButton()
	{
		boolean expectedState = getDriver().findElement(By.name("FloodCoverageFlag")).isEnabled();
		if(!expectedState)
			Assert.assertTrue("Flood coverage Radio Button Disabled as expected", true);
		else
			Assert.assertTrue("Flood coverage Radio Button not Disabled as expected", false);
	}
}

