package au.com.chubb.evolutionshowcase.pages.stepDefinitions.eBusinessPortal;

import au.com.chubb.evolutionshowcase.pages.objectModel.eBusinessPortal.eBusinessPortalAddNewQuotePage;
import au.com.chubb.evolutionshowcase.pages.serenitySteps.eBusinessPortal.eBusinessPortalFindCustomerSteps;
import au.com.chubb.evolutionshowcase.pages.serenitySteps.eBusinessPortal.eBusinessPortalFindQuoteSteps;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

public class eBusinessPortalFindCustomerStepDefinition {

	@Steps
	eBusinessPortalFindCustomerSteps find_customer;
	eBusinessPortalAddNewQuotePage ebusinessportaladdNewQuotepage;
	
	@And("^click on find customer$")
	public void click_find_customer() throws Throwable {
		ebusinessportaladdNewQuotepage.click_find_customer();
		}

	@And("^customer is searched using Find Customer \"([^\"]*)\" menu option Then display matching search \"([^\"]*)\" results list$")
	public void verify_searchResult(String customerName, String expectedCustomer) throws Throwable {
		find_customer.verify_searchResult(customerName, expectedCustomer);
	}
}