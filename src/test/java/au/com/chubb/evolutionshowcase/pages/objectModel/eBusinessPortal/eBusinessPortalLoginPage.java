package au.com.chubb.evolutionshowcase.pages.objectModel.eBusinessPortal;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;

public class eBusinessPortalLoginPage extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public eBusinessPortalLoginPage() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/eBusinessPortal.properties");
		OR.load(fo);

	}

	public void enter_company_id(String company_id) throws InterruptedException {

		getDriver().findElement(By.name(OR.getProperty("CompanyId_Name"))).sendKeys(company_id);

	}

	public void enter_user_id(String user_id) {

		getDriver().findElement(By.name(OR.getProperty("UserId_Name"))).sendKeys(user_id);

	}

	public void enter_password(String password) {

		getDriver().findElement(By.name(OR.getProperty("Password_Name"))).sendKeys(password);

	}

	public void click_login() throws InterruptedException {

		getDriver().findElement(By.name(OR.getProperty("Login_Name"))).click();
		Thread.sleep(8000);
		

	}

}