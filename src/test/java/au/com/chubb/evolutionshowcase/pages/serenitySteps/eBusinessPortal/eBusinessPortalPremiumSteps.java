package au.com.chubb.evolutionshowcase.pages.serenitySteps.eBusinessPortal;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import au.com.chubb.evolutionshowcase.pages.objectModel.eBusinessPortal.eBusinessPortalLoginPage;
import au.com.chubb.evolutionshowcase.utilities.CommonFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class eBusinessPortalPremiumSteps extends PageObject {

	static eBusinessPortalLoginPage ebusinessportalloginpage;
	static CommonFunctions common_functions;
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	
	public eBusinessPortalPremiumSteps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/eBusinessPortal.properties");
		OR.load(fo);

	}

	
	@Step
	public void validate_premium_details() throws InterruptedException {

	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativePremium";
		Thread.sleep(1000);
		String premium = getDriver().findElement(By.xpath(OR.getProperty("premium"))).getText();
		String GST = getDriver().findElement(By.xpath(OR.getProperty("GST"))).getText();
		String stamp_Duty = getDriver().findElement(By.xpath(OR.getProperty("Stamp_Duty"))).getText();
		
		double value = Double.parseDouble(GST) + Double.parseDouble(stamp_Duty);		
		String subtotal = String.valueOf(value);
		
//		String.valueOf()
//		String subtotal = Integer.toString(intsubtotal);
		String totalpremium = getDriver().findElement(By.xpath(OR.getProperty("totalpremium"))).getText();
		
		System.out.println("premium --" + premium);		
		System.out.println("subtotal --" + subtotal);
		System.out.println("totalpremium --" + totalpremium);
		
		
			
	      String Base_Premium=XLS.getCellData(sheetName, "500 Deductible", 2);
	      String Total_Charges1 =XLS.getCellData(sheetName, "500 Deductible", 3);	      
	      String Total_Premium=XLS.getCellData(sheetName, "500 Deductible", 4);
	    
	      System.out.println("------from quote ----------");
	      
	  	System.out.println(premium);  
        System.out.println(subtotal);
        System.out.println(totalpremium);  
        
	      System.out.println("------from excel sheet----------");
            System.out.println(Base_Premium); 
            System.out.println(Total_Charges1); 
            System.out.println(Total_Premium);        
	      
	      if (premium.equalsIgnoreCase(Base_Premium) &&
	    		  subtotal.equalsIgnoreCase(Total_Charges1) &&
	    		  totalpremium.equalsIgnoreCase(Total_Premium)  )
	    	  
	      {	    	  
	    	  Assert.assertTrue("Premium calculation matches with the saved data ", true);
	      }else
	      {
	    	  Assert.assertTrue("Premium calculation matches with the saved data", true);

	      }  					
		
	}
	
	
	@Step
	public void save_verify_calculated_Premium(String Action) throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativePremium";
		Thread.sleep(1000);
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		String premium = getDriver().findElement(By.xpath(OR.getProperty("premium"))).getText();
		String subtotal = getDriver().findElement(By.xpath(OR.getProperty("subtotal"))).getText();
		String totalpremium = getDriver().findElement(By.xpath(OR.getProperty("totalpremium"))).getText();
				
		 if (Action.equalsIgnoreCase("verify"))
		{
			
	      String Base_Premium=XLS.getCellData(sheetName, "500 Deductible", 2);
	      String Total_Charges1 =XLS.getCellData(sheetName, "500 Deductible", 3);	      
	      String Total_Premium=XLS.getCellData(sheetName, "500 Deductible", 4);
	    
	      System.out.println("------from quote ----------");
	      
	  	System.out.println(premium);  
        System.out.println(subtotal);
        System.out.println(totalpremium);  
        
	      System.out.println("------from excel sheet----------");
            System.out.println(Base_Premium); 
            System.out.println(Total_Charges1); 
            System.out.println(Total_Premium);        
	      
	      if (premium.equalsIgnoreCase(Base_Premium) &&
	    		  subtotal.equalsIgnoreCase(Total_Charges1) &&
	    		  totalpremium.equalsIgnoreCase(Total_Premium)  )
	    	  
	      {	    	  
	    	  Assert.assertTrue("Premium calculation matches with the saved data ", true);
	      }else
	      {
	    	  Assert.assertTrue("Premium calculation matches with the saved data", true);

	      }  			
		}
		
		else
		{
			
		}
	}

	@Step
	public void save_calculated_Premium_ebix() throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativePremium";
		Thread.sleep(1000);
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
			List<WebElement> Base_Pemium =getDriver().findElements(By.xpath("(//*[text()='Total Base Premium:'])/..//span"));
			List<WebElement> Total_Charges =getDriver().findElements(By.xpath("(//td[text()='Total Premium Due:'])/..//span"));
	        List<WebElement> Total_Premium =getDriver().findElements(By.xpath("(//*[text()='Total Net Premium Payable To Chubb:'])/../..//td[2]//b"));

		Thread.sleep(500);
		System.out.println(Base_Pemium.get(0).getText());  
        System.out.println(Base_Pemium.get(1).getText());
		
		XLS.setCellData(sheetName, "500 Deductible ebix", 2, Base_Pemium.get(0).getText());
		XLS.setCellData(sheetName, "1000 Deductible ebix", 2, Base_Pemium.get(1).getText());
        

        try
        {
		Thread.sleep(500);
		XLS.setCellData(sheetName, "500 Deductible ebix", 3, Total_Charges.get(0).getText());
		XLS.setCellData(sheetName, "1000 Deductible ebix", 3, Total_Charges.get(1).getText());
        
        System.out.println(Total_Charges.get(0).getText());  
        System.out.println(Total_Charges.get(1).getText());
        }catch (Exception e)
        {
        	
        }
        
        try
        {     
		Thread.sleep(500);
		
		XLS.setCellData(sheetName, "500 Deductible ebix", 4, Total_Premium.get(0).getText());
		XLS.setCellData(sheetName, "1000 Deductible ebix", 4, Total_Premium.get(1).getText());
        System.out.println(Total_Premium.get(0).getText());  
        System.out.println(Total_Premium.get(1).getText());  
        }catch (Exception e)
        {
        	
        }
        
	}
	@Step
	public void verify_calculated_Premium_ebix_fullquote() throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativePremium";
		Thread.sleep(1000);
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

//			from the excel sheet
	      String Base_Premium_500=XLS.getCellData(sheetName, "500 Deductible ebix", 2);
	      String Total_Charges_500=XLS.getCellData(sheetName, "500 Deductible ebix", 3);	      
	      String Total_Premium_500=XLS.getCellData(sheetName, "500 Deductible ebix", 4);
	      
	      System.out.println(Base_Premium_500);  
          System.out.println(Total_Charges_500);  
          System.out.println(Total_Premium_500); 
          
//	      from the application
	      String premium = getDriver().findElement(By.xpath(OR.getProperty("premium"))).getText();
			String subtotal = getDriver().findElement(By.xpath(OR.getProperty("subtotal"))).getText();
			String totalpremium = getDriver().findElement(By.xpath(OR.getProperty("totalpremium"))).getText();
	    
			System.out.println(premium);  
            System.out.println(subtotal);  
            System.out.println(totalpremium); 

        
	      System.out.println("------INP----------");
            
      
	      
	      if (Base_Premium_500.equalsIgnoreCase(premium) &&
	    		  Total_Charges_500.equalsIgnoreCase(subtotal) &&
	    		  Total_Premium_500.equalsIgnoreCase(totalpremium)  )
	    	  
	      {
	    	  
	    	  Assert.assertTrue("Premium calculation matches with the saved data before Edit inidicative quote", true);

	    	  
	    	  
	      }else
	      {
	    	  Assert.assertTrue("Premium calculation matches with the saved data before Edit inidicative quote", true);

	      }
	}
}