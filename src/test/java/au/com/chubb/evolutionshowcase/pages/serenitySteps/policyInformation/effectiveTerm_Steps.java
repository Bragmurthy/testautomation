package au.com.chubb.evolutionshowcase.pages.serenitySteps.policyInformation;

import java.util.List;

import org.junit.runner.RunWith;
import org.openqa.selenium.Keys;

import au.com.chubb.evolutionshowcase.pages.objectModel.policyAdmin.policyAdminWelcomePage;
import au.com.chubb.evolutionshowcase.utilities.CommonFunctions;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class effectiveTerm_Steps extends PageObject {

	@Step
	public void modify_effective_date(String effectiveDate) throws InterruptedException {

		getDriver().findElement(By.id("effectiveDate")).clear();
		Thread.sleep(1000);
		getDriver().findElement(By.id("effectiveDate")).sendKeys(effectiveDate);
		getDriver().findElement(By.id("effectiveDate")).sendKeys(Keys.ESCAPE);
	}

	@Step
	public void modify_expiry_date(String expiryDate) throws InterruptedException {
		getDriver().findElement(By.id("expiryDate")).clear();
		Thread.sleep(1000);
		getDriver().findElement(By.id("expiryDate")).sendKeys(expiryDate);
		getDriver().findElement(By.id("expiryDate")).sendKeys(Keys.ESCAPE);

	}

	@Step
	public void modify_submission_received_date(String submissionReceivedDate) throws InterruptedException {
		getDriver().findElement(By.id("submissionDate")).clear();
		Thread.sleep(1000);
		getDriver().findElement(By.id("submissionDate")).sendKeys(submissionReceivedDate);
		getDriver().findElement(By.id("submissionDate")).sendKeys(Keys.ESCAPE);

	}

}