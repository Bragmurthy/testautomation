package au.com.chubb.evolutionshowcase.pages.objectModel.eBusinessPortal;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;

public class eBusinessPortalFindPolicyPage extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;
	String sheetName = "QuoteData";
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");

	public eBusinessPortalFindPolicyPage() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/eBusinessPortal.properties");
		OR.load(fo);
	}
	
	public void click_find_policy() throws InterruptedException {

		loop:
			for (int i=0;i<=100;i++)
			{
				try
				{
					getDriver().findElement(By.xpath(OR.getProperty("Find_Policy_Xpath"))).click();
					break loop;
				}catch (Exception e)
				{
					Thread.sleep(1000);
				}

			}
			Thread.sleep(2500);
	}
		
	@Step
	public void enter_policy_number(String policynumber) throws Throwable {
		getDriver().findElement(By.name("txtPolicyNo")).sendKeys(policynumber);
		Thread.sleep(10000);
		getDriver().findElement(By.name("btnSearch")).click();
	}

	@Step
	public void select_transaction_type(String transaction) throws Throwable {
		new Select(getDriver().findElement(By.name("selContractStage"))).selectByVisibleText(transaction);
	}
}