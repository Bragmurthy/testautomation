package au.com.chubb.evolutionshowcase.pages.stepDefinitions.policyInformation;
import au.com.chubb.evolutionshowcase.pages.serenitySteps.policyInformation.policyAddress_Steps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import net.serenitybdd.core.annotations.findby.By;
import net.thucydides.core.annotations.Steps;

public class policyAddress_SD {

	@Steps
	policyAddress_Steps policyAddress;
	
	@And("^modify policy address \"([^\"]*)\"$")
	public void modify_policy_address(String policy_Address) throws Throwable {
		policyAddress.modify_policy_address(policy_Address);
	}
	
	
	@And("^Verify address google search function enabled$")
	public void address_google_search_function_enabled(String policy_Address) throws Throwable {
		policyAddress.modify_policy_address(policy_Address);
		policyAddress.verifyAddressEdited(policy_Address);
	}
	
	@Given("^Enter Data in mandatory fields 'Year of Construction' as \"([^\"]*)\" , 'External Construction' as \"([^\"]*)\", 'Building Sum Insured' as \"([^\"]*)\" for Risk Location \"([^\"]*)\"$")
	public void enter_Data_in_mandatory_fields_Year_of_Construction_as_External_Construction_as_Building_Sum_Insured_as(String YearOfConstruction, String ExternalConstruction, String BuildingSumInsured,String locationNumber) throws InterruptedException {
	
		policyAddress.enterDataInMandatoryFields(YearOfConstruction,ExternalConstruction,BuildingSumInsured,locationNumber);
	}

	@Given("^verify all fields are blank in Policy information page for Risk Location \"([^\"]*)\"$")
	public void verify_all_fields_are_blank_in_Policy_information_page(String locationNumber) {
		policyAddress.verifyAllDefaultValues(locationNumber);
		
	}
	
	@Given("^Verify Data in mandatory fields 'Year of Construction' as \"([^\"]*)\" , 'External Construction' as \"([^\"]*)\", 'Building Sum Insured' as \"([^\"]*)\" for Risk Location \"([^\"]*)\"$")
	public void verify_Data_in_mandatory_fields_Year_of_Construction_as_External_Construction_as_Building_Sum_Insured_as(String YearOfConstruction, String ExternalConstruction, String BuildingSumInsured,String locationNumber) throws InterruptedException {
	
		policyAddress.verifyDataInMandatoryFields(YearOfConstruction,ExternalConstruction,BuildingSumInsured,locationNumber);
	}
	
	@And("^click on Continue Full Quote$")
	public void click_add_risk_details() throws Throwable {
		policyAddress.clickOnContinueFullQuote();
	}

}
