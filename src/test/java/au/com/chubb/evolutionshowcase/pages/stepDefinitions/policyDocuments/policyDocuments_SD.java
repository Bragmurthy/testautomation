package au.com.chubb.evolutionshowcase.pages.stepDefinitions.policyDocuments;


import au.com.chubb.evolutionshowcase.pages.serenitySteps.policyDocuments.policyDocuments_Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;


public class policyDocuments_SD {

	 policyDocuments_Steps policyDocuments_Steps;
	@And("^Select New Business Policy and Cover Letter checkbox$")
	public void select_PolcyAndCoverLetterCheckBox() throws Throwable {
		policyDocuments_Steps.select_PolcyAndCoverLetterCheckBox();
	}
	
	@And("^Select Quotation$")
	public void select_Quotation() throws Throwable{
		policyDocuments_Steps.select_Quotation();
	}
	
	@And("^Select Record of Answers$")
	public void select_record_of_answers_checkbox() throws Throwable{
		policyDocuments_Steps.select_Record_of_Answers();
	}
	
	@And("^Select Certificate of Currency radio button$")
	public void select_checkbox() throws Throwable {
		policyDocuments_Steps.select_CertificateOfCurrency();
	}
	
	@Given("^select policy documents checkbox and click generate documents$")
	public void select_Checkbox_policy_documents_click_genereate_documents() throws Throwable {
		policyDocuments_Steps.select_Checkbox_policy_documents_click_genereate_documents();
	}
	
	@And("^validate generate documents for quote offered for \"([^\"]*)\"$")
	public void validate_docs_Quote_offered(String testdata) throws Throwable {
		policyDocuments_Steps.validate_docs_Quote_offered(testdata);
	}
	
	@Given("^select revised Coverage Summary check box$")
	public void select_Checkbox_revised_coverage_summary_click_genereate_documents() throws Throwable {
		policyDocuments_Steps.select_Checkbox_revised_coverage_summary();
	}

	@And("^validate generate documents for revised Coverage Summary for \"([^\"]*)\"$")
	public void validate_docs_Revised_coverage_summary(String testdata) throws Throwable {
		policyDocuments_Steps.validate_docs_Revised_coverage_summary(testdata);
	}
	
	
}
