package au.com.chubb.evolutionshowcase.pages.serenitySteps.lossHistory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

import java.util.regex.*;

@RunWith(SerenityRunner.class)
public class lossHistory_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public lossHistory_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);

	}

	public void enter_lossHistory_details(String lossHistory_testData) throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		Thread.sleep(1000);

		String sheetName = "LossHistory";
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: for (int row = 2; row <= Row_Count; row++) {

			System.out.println("Liability test data --" + lossHistory_testData);
			if (lossHistory_testData.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				break innerloop;
			}
		}
		System.out.println("Value of row number " + rowNum);

		// Claim History
		List<WebElement> claimsHistoryFlag = getDriver().findElements(By.name("claimsHistoryFlag"));
		if (XLS.getCellData(sheetName, "ClaimHistory", rowNum).equalsIgnoreCase("Yes")) {
			claimsHistoryFlag.get(0).click();
			Thread.sleep(1000);
			String Claim_Count = XLS.getCellData(sheetName, "ClaimCount", rowNum);
			int claim_count = Integer.parseInt(Claim_Count);
			int locator=9;
			for (int i = 0; i < claim_count; i++) {

				String manualClaim = XLS.getCellData(sheetName, "ManualClaim", rowNum);
				// First click is only for UW view
				if(manualClaim.equalsIgnoreCase("Yes")){
				getDriver().findElement(By.id("addClaim")).click();
				Thread.sleep(1000);
			    }
				try {

					getDriver().findElement(By.id("claim_" + i + "_lossDate"))
							.sendKeys(XLS.getCellData(sheetName, "claim_" + i + "_lossDate", rowNum));
					Thread.sleep(500);
					getDriver().findElement(By.id("claim_" + i + "_lossDate")).sendKeys(Keys.ENTER);
					Thread.sleep(500);

				} catch (Exception e) {

				}

				try {

					getDriver().findElement(By.id("claim_" + i + "_lossDescription"))
							.sendKeys(XLS.getCellData(sheetName, "claim_" + i + "_lossDescription", rowNum));
					Thread.sleep(500);
				} catch (Exception e) {

				}

				try {

					new Select(getDriver().findElement(By.id("claim_" + i + "_LossType")))
							.selectByVisibleText(XLS.getCellData(sheetName, "claim_" + i + "_LossType", rowNum));
					Thread.sleep(1000);

				} catch (Exception e) {
					System.out.println(e);

				}

				try {
					getDriver().findElement(By.id("claim_" + i + "_paidAmount"))
							.sendKeys(XLS.getCellData(sheetName, "claim_" + i + "_paidAmount", rowNum));
					Thread.sleep(1000);
				} catch (Exception e) {

				}

				try {
					getDriver().findElement(By.id("claim_" + i + "_riskAddress")).click();
					Thread.sleep(1000);
					getDriver().findElement(By.id("claim_" + i + "_riskAddress"))
							.sendKeys(XLS.getCellData(sheetName, "claim_" + i + "_riskAddress", rowNum));
					Thread.sleep(1000);
				} catch (Exception e) {

				}

				if (XLS.getCellData(sheetName, "claim_" + i + "_exemptFromRating", rowNum).equalsIgnoreCase("Yes")) {
					
					try
					{
					
					getDriver().findElement(By.id("claim_" + i + "_exemptFromRating")).click();
					Thread.sleep(1000);
					Thread.sleep(1000);
					//XLS.getCellData(sheetName, "JustificationForExclusion_" + i, rowNum)
					new Select(getDriver().findElement(By.id("ko_unique_ "+locator))).selectByVisibleText("Accommodation");
					Thread.sleep(2000);
					getDriver().findElement(By.xpath(".//*[@id='lossHistoryForm']/div/div[1]/div/div[3]/table[1]/tbody/tr[2]/td/div/div/div/button[1]")).click();
					Thread.sleep(1000);
					
					}catch (Exception e)
					{
						
					}
					
					

				}
				locator=locator+2;

			}

		} else if (XLS.getCellData(sheetName, "ClaimHistory", rowNum).equalsIgnoreCase("No")) {
			claimsHistoryFlag.get(1).click();
			Thread.sleep(100);

		}

		// InsuranceRefusal
		List<WebElement> InsuranceRefusalFlag = getDriver().findElements(By.name("insuranceRefusalCancelRejectedFlag"));
		if (XLS.getCellData(sheetName, "InsuranceRefusal", rowNum).equalsIgnoreCase("Yes")) {
			InsuranceRefusalFlag.get(0).click();
			Thread.sleep(500);
			getDriver()
					.findElement(
							By.xpath(".//*[@id='lossHistoryForm']/div/div[2]/div/div/div/div[2]/div/span[2]/textarea"))
					.sendKeys(XLS.getCellData(sheetName, "InsuranceRefusalReason", rowNum));
			Thread.sleep(500);

		} else if (XLS.getCellData(sheetName, "InsuranceRefusal", rowNum).equalsIgnoreCase("No")) {
			InsuranceRefusalFlag.get(1).click();
			Thread.sleep(100);

		}
		
		  JavascriptExecutor jse = (JavascriptExecutor)getDriver();
		  jse.executeScript("window.scrollBy(0,450)", "");
		  Thread.sleep(3000);

		// Charge Convicted
		List<WebElement> ChargeConvictedFlag = getDriver().findElements(By.name("chargeConvictedFlag"));
		if (XLS.getCellData(sheetName, "ChargeConvicted", rowNum).equalsIgnoreCase("Yes")) {
			ChargeConvictedFlag.get(0).click();
			Thread.sleep(500);
			//getDriver().findElement(By.xpath(".//*[@id='lossHistoryForm']/div/div[3]/div/div/div/div[2]/div/span[2]/textarea"))
			getDriver().findElement(By.xpath("//span[contains(@data-bind,'chargeConvictedText')]")).sendKeys(XLS.getCellData(sheetName, "ChargeConvictedReason", rowNum));
			Thread.sleep(500);

		} else if (XLS.getCellData(sheetName, "ChargeConvicted", rowNum).equalsIgnoreCase("No")) {
			ChargeConvictedFlag.get(1).click();
			Thread.sleep(500);

		}

		
		  jse.executeScript("window.scrollBy(0,450)", "");
		  Thread.sleep(3000);
		

		// Bankruptcy
		List<WebElement> BankruptcyFlag = getDriver().findElements(By.name("bankruptcyFiledFlag"));
		if (XLS.getCellData(sheetName, "Bankruptcy", rowNum).equalsIgnoreCase("Yes")) {
			BankruptcyFlag.get(0).click();
			Thread.sleep(500);

			getDriver()
					.findElement(
							By.xpath(".//*[@id='lossHistoryForm']/div/div[4]/div/div/div/div[2]/div/span[2]/textarea"))
					.sendKeys(XLS.getCellData(sheetName, "BankruptcyReason", rowNum));
			Thread.sleep(500);

		} else if (XLS.getCellData(sheetName, "Bankruptcy", rowNum).equalsIgnoreCase("No")) {
			BankruptcyFlag.get(1).click();
			Thread.sleep(500);

		}

		try {
			getDriver().findElement(By.id("actionFooter")).click();
			Thread.sleep(3000);
			getDriver().findElement(By.id("btnSaveProgress")).click();
			Thread.sleep(7000);
		} catch (Exception e) {

		}

	}
	
	@Step
	public void click_proceed_with_Quote() throws InterruptedException {

		getDriver().findElement(By.xpath("//button[contains(.,'Proceed With Quote')]")).click();
		Thread.sleep(7000);

	}

	@Step
	public void verifyDefaultValue(String question, String defaultValue) {
		
		if(getDriver().findElement(By.xpath("//span[contains(@data-bind,'claimsHistoryFlag')]//input[parent::label[span[contains(text(),'"+defaultValue+"')]]]")).isSelected()){
		
			 Assert.assertTrue("The Default value selected for "+question +" is as dispalyed expected "+defaultValue, true);  
		   }
		 else {
			 Assert.assertTrue("The Default value selected for "+question +" is as not dispalyed expected is"+defaultValue, false); 

			}
	}
	@Step

	public void verifyNoDefaultValue(String question) {
		if(getDriver().findElement(By.xpath("//span[contains(@data-bind,'claimsHistoryFlag')]//input[parent::label[span[contains(text(),'Yes')]]]")).isSelected() && getDriver().findElement(By.xpath("//span[contains(@data-bind,'claimsHistoryFlag')]//input[parent::label[span[contains(text(),'No')]]]")).isSelected()){
			 Assert.assertTrue("The Default value selected for "+question +" is as selected value", false); 
		   }
		 else {
			
			 Assert.assertTrue("The Default value selected for "+question +" is as not selected any value as expected ", true);  
			}

}
@Step
	public void verifyNonPolicyChubbLossRow() {
		List<WebElement> elements=getDriver().findElements(By.xpath("//table[parent::div[contains(@data-bind,'showPrePolicyNonChubb')]]//tr"));
		int rowCount=elements.size();
		if(rowCount==3){
			 Assert.assertTrue("One Row for Non Policy Chubb Losses is dispalyed", true); 
		   }
		 else {
			 Assert.assertTrue("One Row for Non Policy Chubb Losses is not dispalyed", false); 
			}
	}

@Step
public void addClaimAndVerifyValues(String location,DataTable table) throws InterruptedException {
	Thread.sleep(10000);
	int addClaim=Integer.parseInt(location);
	addClaim=addClaim-1;
	List<List<String>> data = table.raw();
	String date=data.get(1).get(1);
	String description=data.get(2).get(1);
	String lossType=data.get(3).get(1);
	String amount=data.get(4).get(1);
	WebElement dateElement=getDriver().findElement(By.xpath("//input[@name='claim_"+addClaim+"_lossDate']"));
	dateElement.sendKeys(date);
	getDriver().switchTo().activeElement().sendKeys(Keys.TAB);
	WebElement lossDescElement=getDriver().findElement(By.xpath("//input[@name='claim_"+addClaim+"_lossDescription']"));
	lossDescElement.sendKeys(description);
	Select LossTypeElement=new Select(getDriver().findElement(By.xpath("//select[@name='claim_"+addClaim+"_LossType']")));
	LossTypeElement.selectByVisibleText(lossType);
	WebElement paidAmountElement=getDriver().findElement(By.xpath("//input[@name='claim_"+addClaim+"_paidAmount']"));
	paidAmountElement.sendKeys(amount);

	System.out.println("date values are"+dateElement.getAttribute("value").equals(date));
	System.out.println("loss type values are"+LossTypeElement.getFirstSelectedOption().getText());
	System.out.println("amount values are"+paidAmountElement.getAttribute("value").equals(amount));
	System.out.println("loss desc values are"+lossDescElement.getAttribute("value"));
	if(dateElement.getAttribute("value").equals(date) 
			&&lossDescElement.getAttribute("value").equals(description)
			&&LossTypeElement.getFirstSelectedOption().getText().equals(lossType)
			&& paidAmountElement.getAttribute("value").equals(amount)){
		Assert.assertTrue("Pre Policy Chubb losses fields are editable", true); 
	}
	else
		Assert.assertTrue("Pre Policy Chubb losses fields are not editable", false); 
}

@Step
public void selectOptionAndVerifyTextBoxDisplayed(String fieldName) throws InterruptedException {
	//WebElement element=getDriver().findElement(By.xpath("//span[contains(@data-bind,'"+fieldName+"')]//input[parent::label[strong[contains(text(),'Yes')]]]"));
	WebElement element=getDriver().findElement(By.xpath("//input[contains(@data-bind,'"+fieldName+"')][following-sibling::span[contains(text(),'Yes')]]"));
	((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
	element.click();
	Thread.sleep(8000);
	if(getDriver().findElement(By.xpath("//textarea[contains(@data-bind,'"+fieldName+"Text')]")).isDisplayed()){
		Assert.assertTrue(""+fieldName+" option Selected as 'Yes' and text box is dispalyed", true); 
	}
	else
		Assert.assertTrue(""+fieldName+" option Selected as 'Yes' and text box is not dispalyed", false); 
}
	
@Step
public void verifymandatoryFieldsErrorEmssages() throws InterruptedException {
	
	List<WebElement> errorMessageList=getDriver().findElements(By.xpath("//span[contains(text(),'This field is required.')]"));
	String errorMessage="This field is required.";
	int noOfErros=errorMessageList.size();
	boolean flag=true;
	String lossDateError=getDriver().findElement(By.xpath("//span[@id='claim_0_lossDate-error']")).getText();
	String lossDescError=getDriver().findElement(By.xpath("//span[@id='claim_0_lossDescription-error']")).getText();
	String lossTypeError=getDriver().findElement(By.xpath("//span[@id='claim_0_LossType-error']")).getText();
	String paidAmountError=getDriver().findElement(By.xpath("//span[@id='claim_0_paidAmount-error']")).getText();
	String insuranceRefusedError=getDriver().findElement(By.xpath("//span[@id='insuranceRefusalCancelRejectedFlag-error']")).getText();
	String chargeConvictedError=getDriver().findElement(By.xpath("//span[@id='chargeConvictedFlag-error']")).getText();
	String bankruptcyError=getDriver().findElement(By.xpath("//span[@id='bankruptcyFiledFlag-error']")).getText();
	
	if(!insuranceRefusedError.equalsIgnoreCase(errorMessage) 
			&&chargeConvictedError.equalsIgnoreCase(errorMessage) 
			&&bankruptcyError.equalsIgnoreCase(errorMessage) 
			&&lossDateError.equalsIgnoreCase(errorMessage)
			&&lossDescError.equalsIgnoreCase(errorMessage)
			&&lossTypeError.equalsIgnoreCase(errorMessage)
			&&paidAmountError.equalsIgnoreCase(errorMessage))
	{
		flag=false;
	}
	if(noOfErros==7 && flag)
	{
		Assert.assertTrue("Loss History field required error messages are displayed", true);
	}
	else
	 Assert.assertTrue("Loss History field required error messages are not displayed", false);
}

public void verifyfieldDisplayed(String fieldName) {
	if(getDriver().findElement(By.xpath("//*[contains(text(),'"+fieldName+"')]")).isDisplayed()){
		Assert.assertTrue(""+fieldName+" is dispalyed", true); 
	}
	else
		Assert.assertTrue(""+fieldName+" is not dispalyed", false); 	
}


@Step
public void verifyClaimValuesaresaved(String location, DataTable table) throws InterruptedException {
	Thread.sleep(10000);
	int addClaim=Integer.parseInt(location);
	addClaim=addClaim-1;
	List<List<String>> data = table.raw();
	String date=data.get(1).get(1);
	String description=data.get(2).get(1);
	String lossType=data.get(3).get(1);
	String amount=data.get(4).get(1);
	WebElement dateElement=getDriver().findElement(By.xpath("//span[contains(@data-bind,'dateText: lossDate')]"));
	

	WebElement lossDescElement=getDriver().findElement(By.xpath("//span[contains(@data-bind,'text: lossDescription')]"));
	WebElement LossTypeElement=getDriver().findElement(By.xpath("//span[contains(@data-bind,'text: preChubbLossTypeDescription')]"));

	WebElement paidAmountElement=getDriver().findElement(By.xpath("//span[contains(@data-bind,'currencyText: paidAmount')]"));
	
	System.out.println("date values are"+dateElement.getText().equals(date));
	System.out.println("loss type values are"+LossTypeElement.getText());
	System.out.println("amount values are"+paidAmountElement.getText().equals(amount));
	System.out.println("loss desc values are"+lossDescElement.getText().equals(lossDescElement));
	if(dateElement.getText().equals(date) 
			&&lossDescElement.getText().equals(description)
			&&LossTypeElement.getText().equals(lossType)
			&& paidAmountElement.getText().replace("$","").equals(amount)){
		Assert.assertTrue("Pre Policy Chubb losses fields are editable", true); 
	}
	else
		Assert.assertTrue("Pre Policy Chubb losses fields are not editable", false); 
}


@Step
public void verifyClaimValues(String location, DataTable table) throws InterruptedException {
	Thread.sleep(10000);
	int addClaim=Integer.parseInt(location);
	addClaim=addClaim-1;
	List<List<String>> data = table.raw();
	String date=data.get(1).get(1);
	String description=data.get(2).get(1);
	String lossType=data.get(3).get(1);
	String amount=data.get(4).get(1);
	WebElement dateElement=getDriver().findElement(By.xpath("//input[@name='claim_"+addClaim+"_lossDate']"));
	dateElement.sendKeys(date);
	getDriver().switchTo().activeElement().sendKeys(Keys.TAB);
	WebElement lossDescElement=getDriver().findElement(By.xpath("//input[@name='claim_"+addClaim+"_lossDescription']"));
	lossDescElement.sendKeys(description);
	Select LossTypeElement=new Select(getDriver().findElement(By.xpath("//select[@name='claim_"+addClaim+"_LossType']")));
	LossTypeElement.selectByVisibleText(lossType);
	WebElement paidAmountElement=getDriver().findElement(By.xpath("//input[@name='claim_"+addClaim+"_paidAmount']"));
	paidAmountElement.sendKeys(amount);

	System.out.println("date values are"+dateElement.getAttribute("value").equals(date));
	System.out.println("loss type values are"+LossTypeElement.getFirstSelectedOption().getText());
	System.out.println("amount values are"+paidAmountElement.getAttribute("value").equals(amount));
	System.out.println("loss desc values are"+lossDescElement.getAttribute("value"));
	if(dateElement.getAttribute("value").equals(date) 
			&&lossDescElement.getAttribute("value").equals(description)
			&&LossTypeElement.getFirstSelectedOption().getText().equals(lossType)
			&& paidAmountElement.getAttribute("value").equals(amount)){
		Assert.assertTrue("Pre Policy Chubb losses fields are editable", true); 
	}
	else
		Assert.assertTrue("Pre Policy Chubb losses fields are not editable", false); 
}
@Step
public void slectQuestionOption(String question, String optionValue) throws InterruptedException {
	//WebElement element=getDriver().findElement(By.xpath("//span[contains(@data-bind,'"+question+"')]//input[parent::label[strong[contains(text(),'"+optionValue+"')]]]"));
	WebElement element=getDriver().findElement(By.xpath("//input[contains(@data-bind,'"+question+"')][following-sibling::span[contains(text(),'"+optionValue+"')]]"));
	
	if(question.contains("claimsHistoryFlag")){
		element.click();}
	else{
	//input[contains(@data-bind,'insuranceRefusalCancelRejected')][following-sibling::span[contains(text(),'No')]]
	//((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
		element.click();
	}
	
	Thread.sleep(2000);
}
@Step
public void clickOnProceedWithReferrals(String fieldName) {
	try{
	if(getDriver().findElement(By.xpath("//button[contains(text(),'Proceed with referrals')]")).isDisplayed()){
		getDriver().findElement(By.xpath("//button[contains(text(),'Proceed with referrals')]")).click();
	}
	}
	catch(Exception e)
	{}
	
}

@Step
public void verifyReferralsMessage(String expectedText) {
	String actual=getDriver().findElement(By.xpath("//p[contains(text(),'This transaction has been referred for Underwriter review. You will be notified within 24 hours.')]")).getText(); 
	if(actual.equals(expectedText))
		Assert.assertTrue("Referals required message displayed", true); 
	
	else
		Assert.assertTrue("Referals required message not displayed as expected actual is "+actual, false);
	
}

@Step
public void verityQuoteValidationMessage(String expectedText) {
	String actual=getDriver().findElement(By.xpath("//p[contains(text(),'"+expectedText+"')]")).getText(); 
	if(actual.contains(expectedText))
		Assert.assertTrue("Quote Validation Message required message displayed", true); 
	
	else
		Assert.assertTrue("Quote Validation Message not displayed as expected actual is "+actual, false);
	
}

@Step
public void verifyUserOnLossHistoryPage() {
	try{
	if(getDriver().findElement(By.xpath("//h4[contains(text(),'Claim History')]")).isDisplayed()){
		Assert.assertTrue("User is on Loss History Page", true); 
	}
	}
	catch(Exception e)
	{
		Assert.assertTrue("User is not on Loss History Page", false);
		
	}
}

@Step
public void verifyClickHereTorReturnNotDsiplayed() {
	try{
		if(getDriver().findElement(By.xpath("//button[contains(text(),'Click here to return to client summary')]")).isDisplayed()){
			Assert.assertTrue("Click here to return to client summary is visible", false);
		}
		}
		catch(Exception e)
		{
			Assert.assertTrue("Click here to return to client summary is not visible", true);			
		}
	
}
@Step
public void clickButton(String buttonname) throws InterruptedException {
	
loop:
	
		for (int i=0;i<=100;i++)
		{
			try
			{
				if(buttonname.equalsIgnoreCase("Use Selected Client")){
					WebElement element=getDriver().findElement(By.xpath("//span[button[contains(text(),'"+buttonname+"')]]"));
					((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
					element.click();
					Thread.sleep(25000);
				}
				else{
				getDriver().findElement(By.xpath("//span[button[contains(text(),'"+buttonname+"')]]")).click();
				Thread.sleep(35000);}
				break loop;
			}catch (Exception e)
			{
				Thread.sleep(1000);
			}
		}
}
}

	