package au.com.chubb.evolutionshowcase.pages.stepDefinitions.riskAndCoverages;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.riskAndCoverages.addLocation_Steps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class addLocation_SD {

	@Steps
	addLocation_Steps addLocation;

	@And("^click add location$")
	public void add_location() throws Throwable {
		addLocation.add_location();
	}
	
	@And("^\"([^\"]*)\" risk location \"([^\"]*)\" and enter risk information details as per \"([^\"]*)\"$")
	public void enter_risk_information(String Action,String location, String TestData) throws Throwable {
		addLocation.add_edit_location_and_enter_risk_information(Action, location, TestData);
	}
	
	@Then("^field \"([^\"]*)\" should not be displayed$")
	public void field_should_not_be_displayed(String fieldName) throws Throwable {
		addLocation.verifyFieldnotDisplayed(fieldName);
	}
	 
	@Given("^display buttons 'Add Coverage', 'Edit' and 'Delete' with existing Evolution functionality$")
	public void display_buttons_Add_Coverage_Edit_and_Delete_with_existing_Evolution_functionality() throws Throwable {
		addLocation.verifyFiledsDisplayed();
	}
	
	@Given("^Verify field \"([^\"]*)\" button is enabled$")
	public void verify_field_button_is_enabled(String fieldName) throws Throwable {
		addLocation.verifyFieldEnabled(fieldName);
  }
	
	@Given("^verify footer fields in the page$")
	public void verify_footer_fields_in_the_page() throws Throwable {
		addLocation.verifyFooter();
	}
	
	
	@Given("^verify user is on Risks and Coverages page$")
	public void verifyUserRisksAndCoveragesPage()  {
		addLocation.verifyUserOnRisksAndCoveragesPage();
	}
	
	@And("^click on VAC category type$")
	public void click_on_VAC_type(){
		addLocation.click_on_VAC_type();
	}
	
	@And("^edit risk location \"([^\"]*)\" and enter risk information details as per \"([^\"]*)\" for MTA$")
	public void enter_risk_information_MTA(String location, String TestData) throws Throwable {
		addLocation.editrisk_information_MTA(location, TestData);
	}
	
	@And("^verify text Flood coverage not available text$")
	public void Flood_coverage_not_available() throws Throwable {
		addLocation.Flood_coverage_not_available();
	}
	
	@And("^verify Flood coverage Radio Buttons are disabled$")
	public void flood_coverage_RadioButton() throws Throwable {
		addLocation.flood_coverage_RadioButton();
	}
}
