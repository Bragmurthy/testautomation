package au.com.chubb.evolutionshowcase.pages.stepDefinitions.lossHistory;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.lossHistory.lossHistory_Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class lossHistory_SD {

	@Steps
	lossHistory_Steps lossHistory;

	@And("^enter details in loss History page as per \"([^\"]*)\"$")
	public void enter_lossHistory_details(String testData) throws Throwable {
		lossHistory.enter_lossHistory_details(testData);
	}

	@And("^click proceed with Quote in loss Histroy page$")
	public void click_proceed_with_Quote() throws Throwable {
		lossHistory.click_proceed_with_Quote();
	}

	@Then("^question \"([^\"]*)\" should default to \"([^\"]*)\"$")
	public void question_should_default_to(String question, String defaultValue){
		lossHistory.verifyDefaultValue(question,defaultValue);
	}

	@Then("^one row displayed for 'Pre Policy Non Chubb Losses' section$")
	public void one_row_displayed_for_Pre_Policy_Non_Chubb_Losses_section(){
		lossHistory.verifyNonPolicyChubbLossRow();
		
	}
	
	@Then("^the \"([^\"]*)\" insurance history questions should have no defaults$")
	public void the_insurance_history_questions_should_have_no_defaults(String question){
		lossHistory.verifyNoDefaultValue(question);
	}

	
	@Given("^Add claim \"([^\"]*)\" and verify values$")
	public void claim_and_verify_values(String location, DataTable table) throws InterruptedException{
		lossHistory.addClaimAndVerifyValues(location,table);
	}
	
	@Given("^select \"([^\"]*)\" as 'Yes' and verify textbox is displayed$")
	public void select_as_and_verify_textbox_is_displayed(String filedName) throws InterruptedException {
		lossHistory.selectOptionAndVerifyTextBoxDisplayed(filedName);
	}
	
		
	@Given("^verify mandatory fileds for Loss History page$")
	public void verifyMandatoryFieldsErrorMessages() throws InterruptedException {
		lossHistory.verifymandatoryFieldsErrorEmssages();
	}
	
	@Given("^verify \"([^\"]*)\" field displayed$")
	public void verify_field_displayed(String fieldName) throws Throwable {
		lossHistory.verifyfieldDisplayed(fieldName);
	}
	
	@Given("^verify claim values are saved as below for Claim \"([^\"]*)\"$")
	public void verify_claim_values_are_saved_as_below_for_Claim(String location, DataTable arg2) throws Throwable {
		lossHistory.verifyClaimValues(location,arg2);
	}
	
	@Given("^verify claim values are saved \"([^\"]*)\"$")
	public void verify_claim_values_are_saved(String location, DataTable arg2) throws Throwable {
		lossHistory.verifyClaimValuesaresaved(location,arg2);
	}
	
	@Given("^select \"([^\"]*)\" as \"([^\"]*)\"$")
	public void select_as_and_verify_textbox_is_displayed(String question, String optionValue) throws Throwable {
		lossHistory.slectQuestionOption(question, optionValue);
	}
	
	@Given("^click on \"([^\"]*)\" if exists$")
	public void click_on_if_exists(String buttonName)  {
		lossHistory.clickOnProceedWithReferrals(buttonName);
	}
	
	@Given("^verify referals message is displayed as \"([^\"]*)\"$")
	public void verifyReferralsMessage(String expectedText)  {
		lossHistory.verifyReferralsMessage(expectedText);
	}
	
	@Given("^verify Quote Validation message is displayed as \"([^\"]*)\"$")
	public void verityQuoteValidationMessage(String expectedText)  {
		lossHistory.verityQuoteValidationMessage(expectedText);
	}
	
	@Given("^verify user is on Loss History page$")
	public void verifyUserOnLossHistoryPage()  {
		lossHistory.verifyUserOnLossHistoryPage();
	}
	
	@Given("^verify Click here to return to client summary is not visible$")
	public void ClickHereTorReturnNotDsiplayed()  {
		lossHistory.verifyClickHereTorReturnNotDsiplayed();
	}
	
	@And("^click on \"([^\"]*)\" button in referal dialogbox$")
	public void click_Button(String buttonname) throws Throwable {
		lossHistory.clickButton(buttonname);
	}	
	
}
