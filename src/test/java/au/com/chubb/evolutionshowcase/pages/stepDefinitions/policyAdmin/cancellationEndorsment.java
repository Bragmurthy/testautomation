package au.com.chubb.evolutionshowcase.pages.stepDefinitions.policyAdmin;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote.riskLocation_calculatePremium_Steps;
import au.com.chubb.evolutionshowcase.pages.serenitySteps.policyAdmin.cancelEndorsementSteps;
import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import cucumber.api.java.en.And;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Steps;

public class cancellationEndorsment extends PageObject {

	@Steps
	cancelEndorsementSteps cancelEndorsement;
	DynamicSeleniumFunctions dynamicseleniumfunctions;
	riskLocation_calculatePremium_Steps  calculatePremium;

	
	@And("^cancel Endorsement$")
	public void createCancellationEndorsement() throws Throwable {
		cancelEndorsement.createCancellationEndorsement();
	}
	
	
	@And("^verify fields in Cancellation lightbox$")
	public void verifyFieldsinlightbox() throws Throwable {
		cancelEndorsement.verifyfieldsInCancellationEndorsementLightbox();
	}
	
	@And("^verify cancellation Reason$")
	public void verifycancellationReason() throws Throwable {
		cancelEndorsement.verifyOptionsInCancellationReason();
	}
		
	@And("^verify static text Please submit Manual Referral for Underwriter review is displayed$")
	public void verifyManualReferral() throws Throwable {
		cancelEndorsement.verifyManualReferral();
	}	
}
