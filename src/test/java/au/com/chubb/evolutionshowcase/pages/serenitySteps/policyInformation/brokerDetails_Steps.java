package au.com.chubb.evolutionshowcase.pages.serenitySteps.policyInformation;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class brokerDetails_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;
	DynamicSeleniumFunctions functions;

	public brokerDetails_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);
	}

	@Step
	public void add_broker(String testData) throws InterruptedException {

		JavascriptExecutor jse = (JavascriptExecutor) getDriver();
		jse.executeScript("window.scrollBy(0,450)", "");
		Thread.sleep(4000);
		
		jse.executeScript("window.scrollBy(0,450)", "");
		Thread.sleep(4000);

		getDriver().findElement(By.id("btnAddProducer")).click();
		Thread.sleep(3000);

	/*	Not for Broker view
		String sheetName = "AddBroker";
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		loop: for (int row = 2; row <= Row_Count; row++) {
			if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(testData)) {
				rowNum = row;
				break loop;
			}

		}

		new Select(getDriver().findElement(By.id("addProducer_Branch")))
				.selectByVisibleText(XLS.getCellData(sheetName, "ChubbBranch", rowNum));

		Thread.sleep(3000);
		getDriver().findElement(By.id("addProducer_Branch")).sendKeys(Keys.TAB);
		Thread.sleep(2000);
		WebElement ActiveElement = getDriver().switchTo().activeElement();
		ActiveElement.sendKeys(XLS.getCellData(sheetName, "BrokerName", rowNum));
		Thread.sleep(2000);
		ActiveElement.sendKeys(Keys.TAB);
		Thread.sleep(3000);
		*/

	}

	@Step
	public void search_broker_contact(String brokerName) throws InterruptedException {

		WebElement ActiveElement = getDriver().switchTo().activeElement();
		System.out.println("Active Element " + ActiveElement.isDisplayed());
		System.out.println("Broker Name " + brokerName);
		ActiveElement.sendKeys(brokerName);
		Thread.sleep(7000);
		ActiveElement.sendKeys(Keys.TAB);
		Thread.sleep(4000);
		getDriver().switchTo().activeElement();
		getDriver().findElement(By.xpath("//button[contains(.,'OK')]")).click();

	}

	@Step
	public void broker_contact(String action, String testData) throws InterruptedException {
		Thread.sleep(40000);
		if (action.equalsIgnoreCase("add")) {
			getDriver().findElement(By.xpath("//button[contains(text(),'Add New Broker Contact')]")).click();
			Thread.sleep(4000);
		} else if (action.equalsIgnoreCase("edit"))

		{
			Thread.sleep(1000);
			getDriver().findElement(By.id("btnEditProducerContact")).click();
			Thread.sleep(2000);

		}

		String sheetName = "AddBrokerContact";
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		loop: for (int row = 2; row <= Row_Count; row++) {
			if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(testData)) {
				rowNum = row;
				break loop;
			}

		}

		getDriver().findElement(By.id("editProducerContact_FirstName")).clear();
		getDriver().findElement(By.id("editProducerContact_FirstName"))
				.sendKeys(XLS.getCellData(sheetName, "FirstName", rowNum));
		getDriver().findElement(By.id("editProducerContact_LastName")).clear();
		getDriver().findElement(By.id("editProducerContact_LastName"))
				.sendKeys(XLS.getCellData(sheetName, "LastName", rowNum));
		getDriver().findElement(By.id("editProducerContact_Email")).clear();
		getDriver().findElement(By.id("editProducerContact_Email"))
				.sendKeys(XLS.getCellData(sheetName, "Email", rowNum));
		getDriver().findElement(By.id("editProducerContact_WorkPhone")).clear();
		getDriver().findElement(By.id("editProducerContact_WorkPhone"))
				.sendKeys(XLS.getCellData(sheetName, "WorkPhone", rowNum));
		getDriver().findElement(By.id("editProducerContact_Mobile")).clear();
		getDriver().findElement(By.id("editProducerContact_Mobile"))
				.sendKeys(XLS.getCellData(sheetName, "Mobile", rowNum));
		
		//Currently Effective Date is not visible for Broker
//		getDriver().findElement(By.id("editProducerContact_EffectiveDate")).clear();
//		getDriver().findElement(By.id("editProducerContact_EffectiveDate"))
//				.sendKeys(XLS.getCellData(sheetName, "EffectiveDate", rowNum));
//		getDriver().findElement(By.id("editProducerContact_EffectiveDate")).sendKeys(Keys.ENTER);
		
		
		/* Currently Expiry Date is not visible for Broker
		getDriver().findElement(By.id("editProducerContact_ExpiryDate")).clear();
		getDriver().findElement(By.id("editProducerContact_ExpiryDate"))
				.sendKeys(XLS.getCellData(sheetName, "ExpiryDate", rowNum));
		getDriver().findElement(By.id("editProducerContact_ExpiryDate")).sendKeys(Keys.TAB);
		Thread.sleep(1000);
		*/ 
		
//		getDriver().switchTo().activeElement().sendKeys(Keys.TAB);
//		Thread.sleep(1000);
//		getDriver().switchTo().activeElement().sendKeys(Keys.TAB);
//		WebElement ActiveElement = getDriver().switchTo().activeElement();
//		ActiveElement.click();
		Thread.sleep(2000);
		getDriver().findElement(By.xpath("(//*[text()='OK'])[2]")).click();
		Thread.sleep(2000);

		if (action.equalsIgnoreCase("add")) {
			Thread.sleep(2000);
			getDriver().switchTo().activeElement();
			getDriver().findElement(By.xpath("//button[contains(.,'OK')]")).click();
		}
		Thread.sleep(5000);

	}

	@Step
	public void chnage_broker(DataTable arg1) throws InterruptedException {

		getDriver().findElement(By.id("btnAddProducer")).click();
		Thread.sleep(2000);
		List<List<String>> data = arg1.raw();
		new Select(getDriver().findElement(By.id("addProducer_Branch"))).selectByVisibleText(data.get(1).get(1));
		Thread.sleep(1000);
		getDriver().findElement(By.id("addProducer_Branch")).sendKeys(Keys.TAB);
		Thread.sleep(1000);
		WebElement ActiveElement = getDriver().switchTo().activeElement();
		ActiveElement.sendKeys(data.get(2).get(1));
		Thread.sleep(1000);
		ActiveElement.sendKeys(Keys.TAB);
		Thread.sleep(1000);

	}

	public void verifyDefaultBrokerDetails(DataTable arg1) {
		
		List<List<String>> data = arg1.raw();
		
		String brokerOrg_App=getBrokerOrganizationName();
		String brokerAddress=getBrokerAddress();
		String brokerCountry_App=getBrokerCountry();
		if (brokerOrg_App.equalsIgnoreCase(data.get(1).get(1)) && brokerAddress.equalsIgnoreCase(data.get(2).get(1)) && brokerCountry_App.equalsIgnoreCase(data.get(3).get(1)))
		{
			Assert.assertTrue("Broker Address " +brokerAddress+" ,Broker Name " +brokerOrg_App+" and Country "+brokerCountry_App+"displayed with default values from indicative quote page", true);
		} else {
			Assert.assertTrue("Broker Address " +brokerAddress+" ,Broker Name " +brokerOrg_App+" and Country "+brokerCountry_App+" not displayed with default values from indicative quote page", false);
		} 
		
	}

	private String getBrokerCountry() {
		return getDriver().findElement(By.xpath("//td[span[contains(@data-bind,'countryName')]]")).getText();
	}

	private String getBrokerAddress() {
		return getDriver().findElement(By.xpath("//span[contains(@data-bind,'streetCityAddress')]")).getText();
	}

	private String getBrokerOrganizationName() {
		return getDriver().findElement(By.xpath("//span[contains(@data-bind,'organizationName')]")).getText();
	}

	public void verifyBrokerDetailsNotEditable() throws IOException {
		boolean brokerNameIsEditable=functions.verifyFieldNonEditable("BrokerName");
		boolean brokerAddressIsEditable=functions.verifyFieldNonEditable("Address");
		boolean brokerCountryIsEditable=functions.verifyFieldNonEditable("Country");
		if (brokerNameIsEditable &&brokerAddressIsEditable &&brokerCountryIsEditable)
		{
			Assert.assertTrue("Broker Address,Broker Name and Country are not editable", true);
		} else {
			Assert.assertTrue("Broker Address,Broker Name and Country are editable", false);
		} 
	}
@Step
	public void changeBrokerContact(DataTable contactName) throws InterruptedException {
		
		JavascriptExecutor jse = (JavascriptExecutor) getDriver();
		jse.executeScript("window.scrollBy(0,450)", "");
		Thread.sleep(4000);
		
		jse.executeScript("window.scrollBy(0,450)", "");
		Thread.sleep(4000);
	
		WebElement changeBroker = getDriver().findElement(By.id("btnAddProducer"));
		jse.executeScript("arguments[0].click();", changeBroker);
		Thread.sleep(10000);
		List<List<String>> data = contactName.raw();
		WebElement ActiveElement = getDriver().switchTo().activeElement();
		ActiveElement.sendKeys(Keys.TAB);
		getDriver().findElement(By.xpath("//div[contains(@data-bind,'model().producerId')]//input[contains(@style,'relative')]")).click();
		getDriver().findElement(By.xpath("//div[contains(@data-bind,'model().producerId')]//input[contains(@style,'relative')]")).sendKeys(Keys.BACK_SPACE);
		getDriver().findElement(By.xpath("//div[contains(@data-bind,'model().producerId')]//input[contains(@style,'relative')]")).sendKeys(data.get(1).get(1));

		//ActiveElement.sendKeys(data.get(1).get(1));
		Thread.sleep(6000);
		
		ActiveElement = getDriver().switchTo().activeElement();
		ActiveElement.sendKeys(Keys.ENTER);
		Thread.sleep(6000);
		WebElement Ok =  getDriver().findElement(By.xpath("//button[contains(text(),'OK')]"));
		jse.executeScript("arguments[0].click();", Ok);
		Thread.sleep(10000);
		
	}
@Step
public void verifybrokerdeatails(DataTable contactName) {
	
}

@Step
public void verifybrokerdeatailsInUWView(DataTable contactName) {
	
	}

	private String getBrokerName() {
		return getDriver().findElement(By.xpath("//span[contains(@data-bind,'firstname')]")).getText();
	}
	
	private String getBrokerEmail() {
		return getDriver().findElement(By.xpath("(//span[contains(@data-bind,'email')])[2]")).getText();
	}
	
	private String getBrokerOfficeNumber() {
		return getDriver().findElement(By.xpath("(//span[contains(@data-bind,'phoneNumber')])[2]")).getText();
	}
	
	private String getBrokerMobileNumber() {
		return getDriver().findElement(By.xpath("//span[contains(@data-bind,'mobileNumber')]")).getText();
	}
	public void verifyBrokerDetailsNameEmailOfficeAndMobileNumbers(String testData) throws IOException {


	String broker_name=getBrokerName();
	String brokerEmail=getBrokerEmail();
	String brokerOfficeNumber=getBrokerOfficeNumber();
	String brokerMobileNumber=getBrokerMobileNumber();	
	
	String sheetName = "AddBrokerContact";
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
	int Row_Count = XLS.getRowCount(sheetName);
	int rowNum = 0;
	loop: for (int row = 2; row <= Row_Count; row++) {
		if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(testData)) {
			rowNum = row;
			break loop;
		}

	}

	String firstName = XLS.getCellData(sheetName, "FirstName", rowNum);
	String lastName = XLS.getCellData(sheetName, "LastName", rowNum);
	String email = XLS.getCellData(sheetName, "Email", rowNum);
	String workPhone = XLS.getCellData(sheetName, "WorkPhone", rowNum);
	String mobile = XLS.getCellData(sheetName, "Mobile", rowNum);
	
	String temp = "+"+workPhone.substring(0, 2)+" (0) "+workPhone.substring(2,3)+" "+workPhone.substring(3,7)+" "+workPhone.substring(7);
	workPhone=temp;
	temp =null;
	temp = "+"+mobile.substring(0, 2)+" (0) "+mobile.substring(2,5)+" "+mobile.substring(5,8)+" "+mobile.substring(8);
	mobile=temp;
	
	
	if (broker_name.equalsIgnoreCase(firstName+" "+lastName) && brokerEmail.equalsIgnoreCase(email) && brokerOfficeNumber.equalsIgnoreCase(workPhone)  && brokerMobileNumber.equalsIgnoreCase(mobile))
	{
		Assert.assertTrue("Broker values are displayed as expected in indicative quote page", true);
	} else {
		Assert.assertTrue("Broker values are not displayed as expected in indicative quote page", false);
	}
	
}

@Step
public void broker_contact_In_Popup(String action, String testData) throws InterruptedException {

	if (action.equalsIgnoreCase("add")) {
		getDriver().findElement(By.xpath("//button[contains(.,'Add New Broker Contact')]")).click();
		Thread.sleep(4000);
	} else if (action.equalsIgnoreCase("edit"))

	{
		Thread.sleep(1000);
		getDriver().findElement(By.id("btnEditProducerContact")).click();
		Thread.sleep(2000);

	}

	String sheetName = "AddBrokerContact";
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
	int Row_Count = XLS.getRowCount(sheetName);
	int rowNum = 0;
	loop: for (int row = 2; row <= Row_Count; row++) {
		if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(testData)) {
			rowNum = row;
			break loop;
		}

	}

	getDriver().findElement(By.id("editProducerContact_FirstName")).clear();
	getDriver().findElement(By.id("editProducerContact_FirstName"))
			.sendKeys(XLS.getCellData(sheetName, "FirstName", rowNum));
	getDriver().findElement(By.id("editProducerContact_LastName")).clear();
	getDriver().findElement(By.id("editProducerContact_LastName"))
			.sendKeys(XLS.getCellData(sheetName, "LastName", rowNum));
	getDriver().findElement(By.id("editProducerContact_Email")).clear();
	getDriver().findElement(By.id("editProducerContact_Email"))
			.sendKeys(XLS.getCellData(sheetName, "Email", rowNum));
	getDriver().findElement(By.id("editProducerContact_WorkPhone")).clear();
	getDriver().findElement(By.id("editProducerContact_WorkPhone"))
			.sendKeys(XLS.getCellData(sheetName, "WorkPhone", rowNum));
	getDriver().findElement(By.id("editProducerContact_Mobile")).clear();
	getDriver().findElement(By.id("editProducerContact_Mobile"))
			.sendKeys(XLS.getCellData(sheetName, "Mobile", rowNum));
	
	
	Thread.sleep(2000);
	getDriver().findElement(By.xpath("(//*[text()='OK'])[2]")).click();
	Thread.sleep(2000);
}

public void broker_contact_in_popup(String testData) {

	String broker_contact =getDriver().findElement(By.xpath("//*[text()='Broker Contact: ']/../div")).getText();
			
			
	String sheetName = "AddBrokerContact";
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
	int Row_Count = XLS.getRowCount(sheetName);
	int rowNum = 0;
	loop: for (int row = 2; row <= Row_Count; row++) {
		if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(testData)) {
			rowNum = row;
			break loop;
		}

	}
	
	String firstName = XLS.getCellData(sheetName, "FirstName", rowNum);
	String lastName = XLS.getCellData(sheetName, "LastName", rowNum);
	String email = XLS.getCellData(sheetName, "Email", rowNum);
	String workPhone = XLS.getCellData(sheetName, "WorkPhone", rowNum);
	String mobile = XLS.getCellData(sheetName, "Mobile", rowNum);
		
	
	if (broker_contact.contains(firstName) &&
			broker_contact.contains(lastName) && broker_contact.contains(email) && broker_contact.contains(workPhone)  && broker_contact.contains(mobile))
	{
		Assert.assertTrue("Broker values are displayed as expected in dropdown", true);
	} else {
		Assert.assertTrue("Broker values are not displayed as expected in dropdown", false);
	}
	
}

@Step
public void verify_element_display_xpath(String text) {
	
	if(getDriver().findElement(By.xpath(OR.getProperty("CancelButtonInAddBrokerContact"))).isDisplayed())
		Assert.assertTrue("Element is displayed", true);
	
	else
		Assert.assertTrue("Element is not displayed", false);
}



	
}