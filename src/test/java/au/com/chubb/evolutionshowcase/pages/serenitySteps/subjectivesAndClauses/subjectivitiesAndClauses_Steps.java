package au.com.chubb.evolutionshowcase.pages.serenitySteps.subjectivesAndClauses;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.tools.ant.types.CommandlineJava.SysProperties;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

import java.util.regex.*;

@RunWith(SerenityRunner.class)
public class subjectivitiesAndClauses_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public subjectivitiesAndClauses_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);

	}

	@Step
	public void click_proceed_with_Quote() throws InterruptedException {

		getDriver().findElement(By.xpath("//button[contains(.,'Proceed With Quote')]")).click();
		Thread.sleep(7000);

	}

	@Step
	public void enter_subjectivitiesl_details(String action,String location, String testData) throws InterruptedException {

		int location_identifier = Integer.parseInt(location);
		getDriver().findElement(By.id("selectProperty_" + location_identifier)).click();
		Thread.sleep(5000);

		String sheetName = "Subjectivities&Clauses";

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		loop: for (int row = 2; row <= Row_Count; row++) {
			if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(testData)) {
				rowNum = row;
				break loop;
			}

		}		
		
		if (action.equals("add")){
			// Add Subjectivity
		getDriver()
				.findElement(
						By.xpath(".//*[@id='subSideNavigationMainBody']/div[2]/div[2]/div/div/div[1]/div/span/button"))
				.click();
		Thread.sleep(5000);
		
		}
		else if(action.equals("edit")){
			getDriver()
				.findElement(
						By.xpath(".//*[@id='subSideNavigationMainBody']/div[2]/div[2]/div/div/div[2]/span[contains(@data-bind,'editCondition')]"))
			.click();		
		}
		
		String SelectSubjectivity = XLS.getCellData(sheetName, "SelectSubjectivity", rowNum);
		System.out.println("Subjectivity -- " +SelectSubjectivity);

		try {
			new Select(getDriver().findElement(By.id("editSubjectivity_Template")))
					.selectByVisibleText(SelectSubjectivity);
			Thread.sleep(1000);
		} catch (Exception e) {

		}

		if (SelectSubjectivity.equalsIgnoreCase("Manuscript")) {
			try {

				getDriver().findElement(By.id("editSubjectivity_Title"))
						.sendKeys(XLS.getCellData(sheetName, "ManuscriptTitle" , rowNum));
				Thread.sleep(500);
				getDriver().findElement(By.id("editSubjectivity_Title")).sendKeys(Keys.TAB);
				Thread.sleep(1000);
				WebElement ManuscriptSubjectivity = getDriver().switchTo().activeElement();
				ManuscriptSubjectivity.clear();
				Thread.sleep(1000);
				ManuscriptSubjectivity.sendKeys(XLS.getCellData(sheetName, "ManuscriptSubjectivity" , rowNum));
				Thread.sleep(500);
			} catch (Exception e) {

			}

		}

		try {
			getDriver().findElement(By.id("editSubjectivity_RequiredBy"))
					.sendKeys(XLS.getCellData(sheetName, "RequiredByDate", rowNum));
			Thread.sleep(100);
			getDriver().findElement(By.id("editSubjectivity_RequiredBy")).sendKeys(Keys.ENTER);
			Thread.sleep(100);

		} catch (Exception e) {

		}

		try {
			getDriver().findElement(By.id("editSubjectivity_Completed"))
					.sendKeys(XLS.getCellData(sheetName, "DateCompleted", rowNum));
			Thread.sleep(100);
			getDriver().findElement(By.id("editSubjectivity_Completed")).sendKeys(Keys.ENTER);
			Thread.sleep(100);
			getDriver().findElement(By.id("editSubjectivity_Completed")).sendKeys(Keys.TAB);
			Thread.sleep(400);
			WebElement Cancel_Button = getDriver().switchTo().activeElement();
			Cancel_Button.sendKeys(Keys.TAB);
			Thread.sleep(400);
			WebElement OK_Button = getDriver().switchTo().activeElement();
			Thread.sleep(400);
			OK_Button.click();
			Thread.sleep(2000);

		} catch (Exception e) {

		}
		
		Thread.sleep(1000);
		getDriver().findElement(By.id("actionFooter")).click();
		
		getDriver().findElement(By.xpath("//*[contains(text(), 'Policy Subjectivities and Clauses')]"));
		Thread.sleep(2000);
		getDriver().findElement(By.id("btnSaveProgress")).click();

		Thread.sleep(10000);
	}
	
	@Step
	public void delete_subjectivities_details(String location) throws InterruptedException{
		try{
			
			int location_identifier = Integer.parseInt(location);
			getDriver().findElement(By.id("selectProperty_" + location_identifier)).click();
			Thread.sleep(5000);
			getDriver()
			//Delete Subjectivity
			.findElement(
					By.xpath(".//*[@id='subSideNavigationMainBody']/div[2]/div[2]/div/div/div[2]/div/table/thead/tr/th[2]/span/span[2]/a/i")).click();
			Thread.sleep(5000);	
			getDriver().findElement(By.xpath(".//*[@id='subSideNavigationMainBody']/div[2]/div[2]/div/div/div[2]/div/table/thead/tr/th[2]/span/span[2]/a/i")).isDisplayed();
		}
		catch (Exception e)
		{
			if(e.getClass().getName().contains("NoSuchElement")){
				Assert.assertTrue("Subjectivities deleted sucessfully", true);
			}
			else {
				System.out.println("test Failed ");
				Assert.assertTrue("Subjectivities not deleted sucessfully", false);
			}
		}		
	}
	
	@Step
	public void enter_clauses_details(String location, String testData) throws InterruptedException {

		int location_identifier = Integer.parseInt(location);
		int sub_identifier = location_identifier - 1;
		getDriver().findElement(By.id("selectProperty_" + sub_identifier)).click();
		Thread.sleep(5000);

		String sheetName = "Subjectivities&Clauses";

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		loop: for (int row = 2; row <= Row_Count; row++) {
			if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(testData)) {
				rowNum = row;
				break loop;
			}

		}
		// Add Subjectivity
		getDriver()
				.findElement(
						By.xpath(".//*[@id='subSideNavigationMainBody']/div[2]/div[2]/div/div/div[1]/div/span/button"))
				.click();
		Thread.sleep(5000);
		String SelectSubjectivity = XLS.getCellData(sheetName, "SelectSubjectivity", rowNum);
		System.out.println("Subjectivity -- " +SelectSubjectivity);

		try {
			new Select(getDriver().findElement(By.id("editSubjectivity_Template")))
					.selectByVisibleText(SelectSubjectivity);
			Thread.sleep(1000);
		} catch (Exception e) {

		}

		if (SelectSubjectivity.equalsIgnoreCase("Manuscript")) {
			try {

				getDriver().findElement(By.id("editSubjectivity_Title"))
						.sendKeys(XLS.getCellData(sheetName, "ManuscriptTitle" , rowNum));
				Thread.sleep(500);
				getDriver().findElement(By.id("editSubjectivity_Title")).sendKeys(Keys.TAB);
				Thread.sleep(1000);
				WebElement ManuscriptSubjectivity = getDriver().switchTo().activeElement();
				ManuscriptSubjectivity.clear();
				Thread.sleep(1000);
				ManuscriptSubjectivity.sendKeys(XLS.getCellData(sheetName, "ManuscriptSubjectivity" , rowNum));
				Thread.sleep(500);
			} catch (Exception e) {

			}

		}

		try {
			getDriver().findElement(By.id("editSubjectivity_RequiredBy"))
					.sendKeys(XLS.getCellData(sheetName, "RequiredByDate", rowNum));
			Thread.sleep(100);
			getDriver().findElement(By.id("editSubjectivity_RequiredBy")).sendKeys(Keys.ENTER);
			Thread.sleep(100);

		} catch (Exception e) {

		}

		try {
			getDriver().findElement(By.id("editSubjectivity_Completed"))
					.sendKeys(XLS.getCellData(sheetName, "DateCompleted", rowNum));
			Thread.sleep(100);
			getDriver().findElement(By.id("editSubjectivity_Completed")).sendKeys(Keys.ENTER);
			Thread.sleep(100);
			getDriver().findElement(By.id("editSubjectivity_Completed")).sendKeys(Keys.TAB);
			Thread.sleep(400);
			WebElement Cancel_Button = getDriver().switchTo().activeElement();
			Cancel_Button.sendKeys(Keys.TAB);
			Thread.sleep(400);
			WebElement OK_Button = getDriver().switchTo().activeElement();
			Thread.sleep(400);
			OK_Button.click();
			Thread.sleep(2000);

		} catch (Exception e) {

		}

		Thread.sleep(1000);
		getDriver().findElement(By.id("actionFooter")).click();

		Thread.sleep(2000);
		getDriver().findElement(By.id("btnSaveProgress")).click();

		Thread.sleep(10000);

	}

	@Step
	public void delete_clause_details(String location) throws InterruptedException{
		try{			
			
			int location_identifier = Integer.parseInt(location);
			getDriver().findElement(By.id("selectProperty_" + location_identifier)).click();
			Thread.sleep(5000);
			//Delete Clause
			getDriver()
			.findElement(
					By.xpath(".//*[@id='subSideNavigationMainBody']/div[2]/div[3]/div/div/div[2]/div/table/thead/tr/th[2]/span/span[2]/a/i"))
			.click();
			Thread.sleep(5000);	
			getDriver()
			.findElement(
					By.xpath(".//*[@id='subSideNavigationMainBody']/div[2]/div[3]/div/div/div[2]/div/table/thead/tr/th[2]/span/span[2]/a/i")).isDisplayed();
		}
		catch (Exception e)
		{
			if(e.getClass().getName().contains("NoSuchElement")){
				Assert.assertTrue("Clause deleted sucessfully", true);
			}
			else {
				System.out.println("test Failed ");
				Assert.assertTrue("Clause not deleted sucessfully", false);
			}
		}	
	}
	
	public void enter_clause_details(String action, String location, String testData) throws InterruptedException {
		int location_identifier = Integer.parseInt(location);
		int sub_identifier = location_identifier;
		
		getDriver().findElement(By.id("selectProperty_" + sub_identifier)).click();
		Thread.sleep(5000);

		String sheetName = "Subjectivities&Clauses";

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		loop: for (int row = 2; row <= Row_Count; row++) {
			if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(testData)) {
				rowNum = row;
				break loop;
			}

		}
		if (action.equals("add")){
			// Add Clause
			WebElement AddClause= getDriver().findElement(By.xpath("//button[contains(@data-bind,'addClause')]"));
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", AddClause);
            Thread.sleep(2000);
			getDriver().findElement(By.xpath("//button[contains(@data-bind,'addClause')]")).click();
			Thread.sleep(5000);
		}
		else if(action.equals("edit")){
			WebElement Editclause= getDriver()
					.findElement(
							By.xpath(".//*[@id='subSideNavigationMainBody']/div[2]/div[3]/div/div/div[2]/span[contains(@data-bind,'editCondition')]"));
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", Editclause);
            Thread.sleep(2000);
			getDriver()
				.findElement(
						By.xpath(".//*[@id='subSideNavigationMainBody']/div[2]/div[3]/div/div/div[2]/span[contains(@data-bind,'editCondition')]"))
			.click();		
		}
		
		String SelectSubjectivity = XLS.getCellData(sheetName, "Clause", rowNum);
		System.out.println("Subjectivity -- " +SelectSubjectivity);

		try {
			new Select(getDriver().findElement(By.id("editClause_Template"))).selectByVisibleText(SelectSubjectivity);
			Thread.sleep(1000);
		} catch (Exception e) {

		}

		if (SelectSubjectivity.equalsIgnoreCase("Manuscript")) {
			try {

				getDriver().findElement(By.id("editClause_Title"))
						.sendKeys(XLS.getCellData(sheetName, "ManuscriptTitle" , rowNum));
				Thread.sleep(500);
				getDriver().findElement(By.id("editClause_Title")).sendKeys(Keys.TAB);
				Thread.sleep(1000);
				WebElement ManuscriptSubjectivity = getDriver().switchTo().activeElement();
				ManuscriptSubjectivity.clear();
				Thread.sleep(1000);
				ManuscriptSubjectivity.sendKeys(XLS.getCellData(sheetName, "ManuscriptSubjectivity" , rowNum));
				Thread.sleep(500);
			} catch (Exception e) {

			}

		}

		try {
			getDriver().findElement(By.id("editClause_RequiredBy"))
					.sendKeys(XLS.getCellData(sheetName, "RequiredByDate", rowNum));
			Thread.sleep(100);
			getDriver().findElement(By.id("editClause_RequiredBy")).sendKeys(Keys.ENTER);
			Thread.sleep(100);

		} catch (Exception e) {

		}

		try {
			getDriver().findElement(By.id("editClause_Completed"))
					.sendKeys(XLS.getCellData(sheetName, "DateCompleted", rowNum));
			Thread.sleep(100);
			getDriver().findElement(By.id("editClause_Completed")).sendKeys(Keys.ENTER);
			Thread.sleep(100);
			getDriver().findElement(By.id("editClause_Completed")).sendKeys(Keys.TAB);
			Thread.sleep(400);
			WebElement Cancel_Button = getDriver().switchTo().activeElement();
			Cancel_Button.sendKeys(Keys.TAB);
			Thread.sleep(400);
			WebElement OK_Button = getDriver().switchTo().activeElement();
			Thread.sleep(400);
			OK_Button.click();
			Thread.sleep(2000);

		} catch (Exception e) {

		}

		Thread.sleep(1000);
		getDriver().findElement(By.id("actionFooter")).click();

		Thread.sleep(2000);
		getDriver().findElement(By.id("btnSaveProgress")).click();

		Thread.sleep(10000);
	}
	
}
