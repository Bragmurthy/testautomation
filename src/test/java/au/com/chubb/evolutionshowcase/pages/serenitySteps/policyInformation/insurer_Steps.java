package au.com.chubb.evolutionshowcase.pages.serenitySteps.policyInformation;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.pages.objectModel.policyAdmin.policyAdminWelcomePage;
import au.com.chubb.evolutionshowcase.utilities.CommonFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class insurer_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;
	String sheetName = "QuoteData";
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");

	public insurer_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);

	}

	@Step
	public void modify_policy_address(String policy_Address) throws InterruptedException {

		getDriver().findElement(By.id("addressEditor_0_Search")).sendKeys(policy_Address);
		Thread.sleep(500);
		getDriver().findElement(By.id("addressEditor_0_Search")).sendKeys(Keys.DOWN);
		getDriver().findElement(By.id("addressEditor_0_Search")).sendKeys(Keys.TAB);
		Thread.sleep(500);

	}

	@Step
	public void select_current_insurer(String insurer_name) throws InterruptedException {
		
		JavascriptExecutor jse = (JavascriptExecutor)getDriver();
		jse.executeScript("window.scrollBy(0,250)", "");
		
		Thread.sleep(500);
		new Select(getDriver().findElement(By.id("insurer"))).selectByVisibleText(insurer_name);
		Thread.sleep(500);

		jse.executeScript("window.scrollBy(0,450)", "");
		Thread.sleep(500);
	}

	@Step
	public void edit_named_insured(String insurer_type, DataTable arg1) throws InterruptedException {

		Thread.sleep(1000);
		WebElement Pencil = getDriver().findElement(By.xpath(OR.getProperty("edit_namedInsured_Xpath")));
		JavascriptExecutor executor = (JavascriptExecutor) getDriver();
		executor.executeScript("arguments[0].click();", Pencil);
		Thread.sleep(1000);

		List<List<String>> data = arg1.raw();
		if (insurer_type.equalsIgnoreCase("Individual"))

		{
			getDriver().findElement(By.id("niPersonSalutation")).sendKeys(data.get(1).get(1));
			Thread.sleep(500);
			getDriver().findElement(By.id("niPersonFirstName")).clear();
			getDriver().findElement(By.id("niPersonFirstName")).sendKeys(data.get(2).get(1));
			Thread.sleep(500);
			getDriver().findElement(By.id("niPersonMiddleName")).clear();
			getDriver().findElement(By.id("niPersonMiddleName")).sendKeys(data.get(3).get(1));
			Thread.sleep(500);
			getDriver().findElement(By.id("niPersonLastName")).clear();
			getDriver().findElement(By.id("niPersonLastName")).sendKeys(data.get(4).get(1));
			Thread.sleep(500);
			getDriver().findElement(By.id("niPersonDob")).clear();
			Thread.sleep(500);
			getDriver().findElement(By.id("niPersonDob")).sendKeys(data.get(5).get(1));
			Thread.sleep(500);
			getDriver().findElement(By.id("niPersonDob")).sendKeys(Keys.TAB);
			Thread.sleep(500);
			
			// Retired

			// industry

			WebElement Industry_Locator = getDriver().findElement(By.xpath("//select[starts-with(@id, 'ko_unique_')]"));
			Thread.sleep(500);
			new Select(Industry_Locator).selectByVisibleText(data.get(6).get(1));
			Industry_Locator.sendKeys(Keys.TAB);
			Thread.sleep(500);

			// occupation
			WebElement Occupation_Locator = getDriver().switchTo().activeElement();
			Thread.sleep(500);
			new Select(Occupation_Locator).selectByVisibleText(data.get(7).get(1));
			Occupation_Locator.sendKeys(Keys.TAB);
			Thread.sleep(500);
			
			// employer
			WebElement Employer_Locator = getDriver().switchTo().activeElement();
			Thread.sleep(500);
			Employer_Locator.clear();
			Employer_Locator.sendKeys(data.get(8).get(1));
			Employer_Locator.sendKeys(Keys.TAB);
			Thread.sleep(500);
			
			// phone
			WebElement Phone_Locator = getDriver().switchTo().activeElement();
			Thread.sleep(500);
			Phone_Locator.sendKeys(data.get(9).get(1));
			Phone_Locator.sendKeys(Keys.TAB);
			Thread.sleep(500);

			// email
			WebElement Email_Locator = getDriver().switchTo().activeElement();
			Thread.sleep(500);
			Email_Locator.clear();
			Email_Locator.sendKeys(data.get(10).get(1));
			Thread.sleep(500);

		}

		Thread.sleep(500);
		getDriver().findElement(By.xpath(("//button[contains(text(), 'OK')]"))).click();

	}

	@Step
	public void add_name_to_document(String document_name) {

		getDriver().findElement(org.openqa.selenium.By.id("namesToDisplayOnDocumentsText")).sendKeys(document_name);

	}

	@Step
	public void delete_existing_named_insured() throws InterruptedException {

		WebElement crossmark = getDriver().findElement(By.xpath(OR.getProperty("delete_namedInsured_Xpath")));
		JavascriptExecutor executor = (JavascriptExecutor) getDriver();
		executor.executeScript("arguments[0].click();", crossmark);
		Thread.sleep(500);

	}

	@Step
	public void search_and_add_named_insured(String namedInsured_name) throws InterruptedException {

		Thread.sleep(500);
		WebElement AddNamedInsured = getDriver()
				.findElement(By.xpath(OR.getProperty("add_named_Insured_button_Xpath")));
		JavascriptExecutor executor = (JavascriptExecutor) getDriver();
		executor.executeScript("arguments[0].click();", AddNamedInsured);

		Thread.sleep(500);
		getDriver().findElement(By.id("namedInsuredSearchInput")).sendKeys("krishna");
		Thread.sleep(500);
		getDriver().findElement(By.xpath(("//button[contains(text(),'OK')]"))).click();

		loop: for (int i = 1; i <= 25; i++) {
			String NamedInsured_APP = getDriver()
					.findElement(By.xpath(".//*[@id='namedInsuredSearchResultTable']/tbody/tr[" + i + "]/td[2]"))
					.getText();
			if (NamedInsured_APP.equalsIgnoreCase(namedInsured_name)) {
				getDriver().findElement(By.xpath(".//*[@id='namedInsuredSearchResultTable']/tbody/tr[" + i + "]/td[1]"))
						.click();
				break loop;
			}

		}
		Thread.sleep(500);
		getDriver().findElement(By.id("btnCreateNewNamedInsured")).click();
		Thread.sleep(500);

	}
	
	@Step
	public void verify_named_insured_default_values() {
		String email=XLS.getCellData(sheetName, "Email", 3);
		String firstName=XLS.getCellData(sheetName, "Firstname", 3);
		//XLS.setCellData(sheetName, "Middlename", 3,"MiddleEbix");
		String lastName=XLS.getCellData(sheetName, "Lastname", 3);
		
		String fullName=firstName +" "+lastName;
		XLS.getCellData(sheetName, "Email", 3);	
		String name_App=getDriver().findElement(By.xpath("//span[contains(@data-bind,'fullName')]")).getText();
		String retired_App=getDriver().findElement(By.xpath("//span[contains(@data-bind,'retired')]")).getText();
		String email_App=getDriver().findElement(By.xpath("//span[contains(@data-bind,'Contact.Email')]")).getText();
		String datecreated_App=getDriver().findElement(By.xpath("//span[contains(@data-bind,'createDate')]")).getText();

		
		//Changing date created field need to modify after duplicate client issue fixed
		//data.get(9).get(1)= y;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = sdf.format(cal.getTime());
		System.out.println("Current date in String Format: "+strDate);
		
		if (name_App.equalsIgnoreCase(fullName) && 
				retired_App.equalsIgnoreCase("No") &&
				email_App.equalsIgnoreCase(email) &&
				datecreated_App.equalsIgnoreCase(strDate))
		{
			Assert.assertTrue("Name " +name_App+" ,Retired "+retired_App + "Email" +email_App+" Date Created " +datecreated_App+"  displayed with default values from indicative quote page", true);
		} else {
			Assert.assertTrue("Name " +name_App+" ,Retired "+retired_App+" Email" +email_App+" Date Created " +datecreated_App+"  displayed with default values from indicative quote page", false);		} 
		
	}

	@Step
	public void search_address_policy_information(String address) throws InterruptedException {
		
		    getDriver().findElement(By.id("addressEditor_0_Search")).sendKeys(address);
			Thread.sleep(2000);
			getDriver().findElement(By.id("addressEditor_0_Search")).sendKeys(Keys.DOWN);
			Thread.sleep(2000);
			getDriver().findElement(By.id("addressEditor_0_Search")).sendKeys(Keys.ENTER);
		
	}

	@Step
	public void addNamedInsured(String action, String testflow) throws InterruptedException {
		Thread.sleep(1000);
		WebElement Pencil = getDriver().findElement(By.xpath("//button[contains(text(),' Add Named Insured')]"));
		JavascriptExecutor executor = (JavascriptExecutor) getDriver();
		executor.executeScript("arguments[0].click();", Pencil);
		Thread.sleep(10000);
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "NamedInsured";
		Thread.sleep(1000);
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

			if (testflow.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				System.out.println("test data --" + testflow);
				System.out.println("try to come out of loop "+rowNum);
				break innerloop;
			}
		}
		String insurer_type=XLS.getCellData(sheetName, "InsurerType", rowNum);
		
		if (insurer_type.equalsIgnoreCase("Individual"))
		{
			String salutation=XLS.getCellData(sheetName, "Title", rowNum);
			getDriver().findElement(By.id("niPersonSalutation")).sendKeys(salutation);
			Thread.sleep(500);
			getDriver().findElement(By.id("niPersonFirstName")).clear();
			getDriver().findElement(By.id("niPersonFirstName")).sendKeys(XLS.getCellData(sheetName, "FirstName", rowNum));
			Thread.sleep(500);
			getDriver().findElement(By.id("niPersonMiddleName")).clear();
			getDriver().findElement(By.id("niPersonMiddleName")).sendKeys(XLS.getCellData(sheetName, "MiddleName", rowNum));
			Thread.sleep(500);
			getDriver().findElement(By.id("niPersonLastName")).clear();
			getDriver().findElement(By.id("niPersonLastName")).sendKeys(XLS.getCellData(sheetName, "LastName", rowNum));
			Thread.sleep(500);
			getDriver().findElement(By.id("niPersonDob")).clear();
			Thread.sleep(500);
			
			getDriver().findElement(By.id("niPersonDob")).sendKeys(XLS.getCellData(sheetName, "DateOfBirth", rowNum));
			Thread.sleep(500);
			getDriver().findElement(By.id("niPersonDob")).sendKeys(Keys.TAB);
			Thread.sleep(500);
			
			// Retired

			// industry

			WebElement Industry_Locator = getDriver().findElement(By.xpath("//select[contains(@data-bind,'industryId')]"));
			Thread.sleep(500);
			new Select(Industry_Locator).selectByVisibleText(XLS.getCellData(sheetName, "Industry", rowNum));
			Industry_Locator.sendKeys(Keys.TAB);
			Thread.sleep(500);

			// occupation
			WebElement Occupation_Locator = getDriver().switchTo().activeElement();
			Thread.sleep(500);
			new Select(Occupation_Locator).selectByVisibleText(XLS.getCellData(sheetName, "Occupation", rowNum));
			Occupation_Locator.sendKeys(Keys.TAB);
			Thread.sleep(500);
			
			// employer
			WebElement Employer_Locator = getDriver().switchTo().activeElement();
			Thread.sleep(500);
			Employer_Locator.clear();
			Employer_Locator.sendKeys(XLS.getCellData(sheetName, "Employer", rowNum));
			Employer_Locator.sendKeys(Keys.TAB);
			Thread.sleep(500);
			
			// phone
			WebElement Phone_Locator = getDriver().switchTo().activeElement();
			Thread.sleep(500);
			Phone_Locator.sendKeys(XLS.getCellData(sheetName, "WorkPhone", rowNum));
			Phone_Locator.sendKeys(Keys.TAB);
			Thread.sleep(500);

			// email
			getDriver().findElement(By.xpath("//input[contains(@data-bind,'contact.email')]")).clear();
			getDriver().findElement(By.xpath("//input[contains(@data-bind,'contact.email')]")).sendKeys(XLS.getCellData(sheetName, "Email", rowNum));
			Thread.sleep(500);
			
			getDriver().findElement(By.xpath("//button[contains(text(),'OK')]")).click();
			Thread.sleep(10000);
		}
	}
@Step
	public void verifyFieldsInPage(String namedInsured) {
		if(getDriver().findElement(By.xpath("//input[@name='niPersonFirstName']")).isDisplayed()
				&&getDriver().findElement(By.xpath("//input[@id='niPersonLastName']")).isDisplayed()
				&&getDriver().findElement(By.xpath("//input[contains(@data-bind,'employerName')]")).isDisplayed()
				&&getDriver().findElement(By.xpath("//input[@id='niPersonDob']")).isDisplayed()
				&&getDriver().findElement(By.xpath("//select[contains(@data-bind,'industryId')]")).isDisplayed()
				&&getDriver().findElement(By.xpath("//select[contains(@data-bind,'occupationId')]")).isDisplayed()
				&&getDriver().findElement(By.xpath("//input[contains(@data-bind,'phoneNumber')]")).isDisplayed()
				&&getDriver().findElement(By.xpath("//input[contains(@data-bind,'contact.email')]")).isDisplayed()
				&&getDriver().findElement(By.xpath("//div[contains(@class,'niPersonRetired')]")).isDisplayed()
				&&getDriver().findElement(By.xpath("//select[contains(@data-bind,'salutationId')]")).isDisplayed())
			Assert.assertTrue("All expected fields in named insured page are displayed", true);
	 else {
			Assert.assertTrue("All expected fields in named insured page are not displayed", false);
		
	}
		
	}
@Step
public void verifyNamedInsuredAddedSuccesfully(String testflow) throws InterruptedException {
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
	String sheetName = "NamedInsured";
	Thread.sleep(1000);
	
	int Row_Count = XLS.getRowCount(sheetName);
	int rowNum = 0;
	
	innerloop: 
		for (int row = 2; row <= Row_Count; row++) {

		if (testflow.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
			rowNum = row;
			System.out.println("test data --" + testflow);
			System.out.println("try to come out of loop "+rowNum);
			break innerloop;
		}
	}
	
		//String salutation=XLS.getCellData(sheetName, "Title", rowNum);
		String firstName=XLS.getCellData(sheetName, "FirstName", rowNum);
		//String middleName=XLS.getCellData(sheetName, "MiddleName", rowNum);
		String lastName=XLS.getCellData(sheetName, "LastName", rowNum);
	    //String fullname=firstName+" "+middleName+" "+lastName;
		String fullname=firstName+" "+lastName;
		String dateOfBirth=XLS.getCellData(sheetName, "DateOfBirth", rowNum);
		String industry=XLS.getCellData(sheetName, "Industry", rowNum);
    	String occupation=XLS.getCellData(sheetName, "Occupation", rowNum);
		String employer=XLS.getCellData(sheetName, "Employer", rowNum);
		String workPhone=XLS.getCellData(sheetName, "WorkPhone", rowNum);
		workPhone="+"+workPhone.substring(0, 2) + " (0) " +workPhone.substring(2, 3)+" "+workPhone.substring(3, 7)+" "+workPhone.substring(7, 11);
		String email=XLS.getCellData(sheetName, "Email", rowNum);
		
		if(getDriver().findElement(By.xpath("//span[contains(text(),'"+dateOfBirth+"')]")).isDisplayed()
				&& getDriver().findElement(By.xpath("//span[contains(text(),'"+industry+"')]")).isDisplayed()
				&& getDriver().findElement(By.xpath("//span[contains(text(),'"+occupation+"')]")).isDisplayed()
				&& getDriver().findElement(By.xpath("//span[contains(text(),'"+employer+"')]")).isDisplayed()
				&& getDriver().findElement(By.xpath("//span[contains(text(),'"+workPhone+"')]")).isDisplayed()
				&& getDriver().findElement(By.xpath("//span[contains(text(),'"+email+"')]")).isDisplayed()
				&& getDriver().findElement(By.xpath("//span[contains(text(),'"+fullname+"')]")).isDisplayed())
			Assert.assertTrue("Named Insured Added succesfully", true);
			else
			Assert.assertTrue("Named Insured values not saved as specfied in creation", false);
	}
@Step
public void edit_named_insured(String position, String testflow) throws InterruptedException
{
	Thread.sleep(5000);
	
	int namedInsuredToEdit=Integer.parseInt(position)-1;
	List<WebElement> Pencil = getDriver().findElements(By.xpath("//div[contains(@data-bind,'app.models.Person')]"));
	WebElement element=Pencil.get(namedInsuredToEdit).findElement(By.xpath("//a[contains(@data-bind,'editNamedInsured')]"));
	JavascriptExecutor executor = (JavascriptExecutor) getDriver();
	executor.executeScript("arguments[0].click();", element);
	element.click();
	
	Thread.sleep(10000);
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
	String sheetName = "NamedInsured";
	Thread.sleep(1000);
	
	int Row_Count = XLS.getRowCount(sheetName);
	int rowNum = 0;
	
	innerloop: 
		for (int row = 2; row <= Row_Count; row++) {

		if (testflow.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
			rowNum = row;
			System.out.println("test data --" + testflow);
			System.out.println("try to come out of loop "+rowNum);
			break innerloop;
		}
	}
	String insurer_type=XLS.getCellData(sheetName, "InsurerType", rowNum);
	
	if (insurer_type.equalsIgnoreCase("Individual"))
	{
		String salutation=XLS.getCellData(sheetName, "Title", rowNum);
		getDriver().findElement(By.id("niPersonSalutation")).sendKeys(salutation);
		Thread.sleep(500);
		getDriver().findElement(By.id("niPersonFirstName")).clear();
		getDriver().findElement(By.id("niPersonFirstName")).sendKeys(XLS.getCellData(sheetName, "FirstName", rowNum));
		Thread.sleep(500);
		getDriver().findElement(By.id("niPersonMiddleName")).clear();
		getDriver().findElement(By.id("niPersonMiddleName")).sendKeys(XLS.getCellData(sheetName, "MiddleName", rowNum));
		Thread.sleep(500);
		getDriver().findElement(By.id("niPersonLastName")).clear();
		getDriver().findElement(By.id("niPersonLastName")).sendKeys(XLS.getCellData(sheetName, "LastName", rowNum));
		Thread.sleep(500);
		getDriver().findElement(By.id("niPersonDob")).clear();
		Thread.sleep(500);
		
		getDriver().findElement(By.id("niPersonDob")).sendKeys(XLS.getCellData(sheetName, "DateOfBirth", rowNum));
		Thread.sleep(500);
		getDriver().findElement(By.id("niPersonDob")).sendKeys(Keys.TAB);
		Thread.sleep(500);
		
		// Retired

		// industry

		WebElement Industry_Locator = getDriver().findElement(By.xpath("//select[contains(@data-bind,'industryId')]"));
		Thread.sleep(500);
		new Select(Industry_Locator).selectByVisibleText(XLS.getCellData(sheetName, "Industry", rowNum));
		Industry_Locator.sendKeys(Keys.TAB);
		Thread.sleep(500);

		// occupation
		WebElement Occupation_Locator = getDriver().switchTo().activeElement();
		Thread.sleep(500);
		new Select(Occupation_Locator).selectByVisibleText(XLS.getCellData(sheetName, "Occupation", rowNum));
		Occupation_Locator.sendKeys(Keys.TAB);
		Thread.sleep(500);
		
		// employer
		WebElement Employer_Locator = getDriver().switchTo().activeElement();
		Thread.sleep(500);
		Employer_Locator.clear();
		Employer_Locator.sendKeys(XLS.getCellData(sheetName, "Employer", rowNum));
		Employer_Locator.sendKeys(Keys.TAB);
		Thread.sleep(500);
		
		// phone
		WebElement Phone_Locator = getDriver().switchTo().activeElement();
		Thread.sleep(500);
		Phone_Locator.sendKeys(XLS.getCellData(sheetName, "WorkPhone", rowNum));
		Phone_Locator.sendKeys(Keys.TAB);
		Thread.sleep(500);

		// email
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'contact.email')]")).clear();
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'contact.email')]")).sendKeys(XLS.getCellData(sheetName, "Email", rowNum));
		Thread.sleep(500);
		
		getDriver().findElement(By.xpath("//button[contains(text(),'OK')]")).click();
		Thread.sleep(5000);
	}
	
}

public void changeInsurer(DataTable insurerName) throws InterruptedException {
	List<List<String>> data = insurerName.raw();
	JavascriptExecutor jse = (JavascriptExecutor)getDriver();
	jse.executeScript("window.scrollBy(0,250)", "");
	
	Thread.sleep(500);
	new Select(getDriver().findElement(By.id("insurer"))).selectByVisibleText(data.get(1).get(1));
	Thread.sleep(500);

	jse.executeScript("window.scrollBy(0,350)", "");
	Thread.sleep(500);
}

@Step
public void verifyInsurer(DataTable insurerName) {
	List<List<String>> data = insurerName.raw();
	String Current_Insurer_App = getDriver().findElement(By.id("insurer")).getText();
	
	if (Current_Insurer_App.equalsIgnoreCase(data.get(1).get(1)))
	{
		Assert.assertTrue("Current insurer change is reflected in policy information  " +Current_Insurer_App, true);

	}
	else
	{
		Assert.assertTrue("Current insurer changed in Ebix is not reflected in Policy Admin",  false);
	}
	
}

@Step
public void verifyInsurerSectionNotEditable() throws InterruptedException {
	select_current_insurer("NRMA");
    String Current_Insurer_App = new Select(getDriver().findElement(By.id("insurer"))).getFirstSelectedOption().getText();
	if (!Current_Insurer_App.equalsIgnoreCase("NRMA"))
	{
		Assert.assertTrue("Current Insurer is not editable" +Current_Insurer_App, true);

	}
	else
	{
		Assert.assertTrue("Current insurer is editable",  false);
	}
	
}
@Step
public void Remove_named_Insured() {
	getDriver().findElement(By.xpath("//a[contains(@data-bind,'removeNamedInsured')]")).click();
	
}
	
}

