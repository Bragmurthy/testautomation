package au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;


import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class riskLocation_coverageInformation_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public riskLocation_coverageInformation_Steps() throws IOException {
		/*
		 * OR = new Properties(); FileInputStream fo = new FileInputStream(root +
		 * "/ObjectRepository/landingPage.properties"); OR.load(fo);
		 */
	}

	@Step
	public void verify_building_sum_insured_value(String float_value, String location, String input_value)
			throws InterruptedException {

		int k = 0;
		k = Integer.valueOf(location);
		getDriver().findElement(By.id("RiskLocation_" + k + "_BuildingSumInsured")).sendKeys(float_value);
		String building_sum_insured_APP = getDriver().findElement(By.id("RiskLocation_" + k + "_BuildingSumInsured"))
				.getAttribute("value");
		System.out.println("Building Sum Insured APP-- " + building_sum_insured_APP);

		if (building_sum_insured_APP.equalsIgnoreCase(input_value))

		{
			Assert.assertTrue("Building Sum Insured accepts only interger value " + building_sum_insured_APP, true);
		} else {
			Assert.assertTrue("Building Sum Insured accepts only interger value " + building_sum_insured_APP, false);

		}

	}

	@Step
	public void verify_content_sum_insured_default(String building_sum_insured, String contents_sum_insured,
			String location) throws InterruptedException {

		int k = 0;
		k = Integer.valueOf(location);
		new Select(getDriver().findElement(By.id("property_" + k + "_ResidenceType")))
				.selectByVisibleText("Primary Residence");
		Thread.sleep(2000);		
		System.out.println("Value of count " + k);
		getDriver().findElement(By.id("RiskLocation_" + k + "_BuildingSumInsured")).clear();
		getDriver().findElement(By.id("RiskLocation_" + k + "_BuildingSumInsured")).sendKeys(building_sum_insured);
		Thread.sleep(1000);
		getDriver().findElement(By.id("RiskLocation_" + k + "_ContentsSumInsured")).click();
		Thread.sleep(1000);
		String content_sum_insured_APP = getDriver().findElement(By.id("RiskLocation_" + k + "_ContentsSumInsured"))
				.getAttribute("value");

		if (content_sum_insured_APP.equalsIgnoreCase(contents_sum_insured))

		{
			Assert.assertTrue("Content Sum Insured is 20% of Building Sum Insured Value " + content_sum_insured_APP,
					true);
		} else {
			Assert.assertTrue("Content Sum Insured is 20% of Building Sum Insured Value " + content_sum_insured_APP,
					false);

		}

	}

	@Step
	public void verify_building_contents_deductible_default(String building_contents_default,
			String building_sum_insured, String residence_type, String location) throws InterruptedException {

		int k = 0;
		k = Integer.valueOf(location);
		new Select(getDriver().findElement(By.id("property_" + k + "_ResidenceType")))
				.selectByVisibleText(residence_type);
		Thread.sleep(2000);

		getDriver().findElement(By.id("RiskLocation_" + k + "_BuildingSumInsured")).sendKeys(building_sum_insured);
		Thread.sleep(1000);
		getDriver().findElement(By.id("RiskLocation_" + k + "_ContentsSumInsured")).click();
		Thread.sleep(2000);

		int BuildingSumInsured = Integer.parseInt(building_sum_insured);
		String contents_deductible_APP = getDriver().findElement(By.id("RiskLocation_" + k + "_Deductible"))
				.getAttribute("value");
		System.out.println("contents deductible app -- " + contents_deductible_APP);

		if (BuildingSumInsured > 2000000 && residence_type.equalsIgnoreCase("Primary Residence")) {

			if (contents_deductible_APP.equalsIgnoreCase(building_contents_default)) {
				Assert.assertTrue("Contents Deductible is defaulted to correct value $" + building_contents_default,
						true);
			} else {
				Assert.assertTrue("Contents Deductible is defaulted to correct value $" + building_contents_default,
						false);

			}

		} else if (BuildingSumInsured < 2000000 && residence_type.equalsIgnoreCase("Primary Residence")) {

			if (contents_deductible_APP.equalsIgnoreCase(building_contents_default)) {
				Assert.assertTrue("Contents Deductible is defaulted to correct value $" + building_contents_default,
						true);
			} else {
				Assert.assertTrue("Contents Deductible is defaulted to correct value $" + building_contents_default,
						false);

			}

		} else if (!residence_type.equalsIgnoreCase("Primary Residence")) {

			if (contents_deductible_APP.equalsIgnoreCase(building_contents_default)) {
				Assert.assertTrue("Contents Deductible is defaulted to correct value $" + building_contents_default,
						true);
			} else {
				Assert.assertTrue("Contents Deductible is defaulted to correct value $" + building_contents_default,
						false);

			}

		} else {

		}

	}

	@Step
	public void verify_label_change(String property_type, String location, String label_name)
			throws InterruptedException {

		int location_count = 0;
		location_count = Integer.valueOf(location);
		System.out.println("Value of count " + location_count);
		int k = location_count;
		new Select(getDriver().findElement(By.id("property_" + k + "_PropertyType")))
				.selectByVisibleText(property_type);

		// String label_APP=getDriver().findElement(By.xpath(xpathExpression))

	}

	@Step
	public void verify_contents_sum_insured_pop_up_error_message(String pop_up_error, String building_sum_insured_value,
			String content_sum_insured_value, String location) throws InterruptedException {

		int k = 0;
		k = Integer.valueOf(location);
		Thread.sleep(2000);
		getDriver().findElement(By.id("RiskLocation_" + k + "_BuildingSumInsured"))
				.sendKeys(building_sum_insured_value);
		Thread.sleep(500);
		getDriver().findElement(By.id("RiskLocation_" + k + "_ContentsSumInsured")).click();
		Thread.sleep(500);
		
		getDriver().findElement(By.id("RiskLocation_" + k + "_ContentsSumInsured")).sendKeys("10");
		Thread.sleep(500);
				
		JavascriptExecutor jse = (JavascriptExecutor) getDriver();
		jse.executeScript("window.scrollBy(0,250)", "");
		Thread.sleep(2000);

		try {
			List<WebElement> VAC_Coverage = getDriver().findElements(By.name("VacFlag_" + k));
			Thread.sleep(500);
			VAC_Coverage.get(0).click();
			Thread.sleep(500);

		} catch (Exception e) {

		}

		getDriver().findElement(By.id("RiskLocation_" + k + "_ContentsSumInsured")).click();
		Thread.sleep(500);
		getDriver().findElement(By.id("RiskLocation_" + k + "_ContentsSumInsured")).clear();
		Thread.sleep(500);
		getDriver().findElement(By.id("RiskLocation_" + k + "_ContentsSumInsured")).sendKeys(content_sum_insured_value);
		Thread.sleep(500);
		getDriver().findElement(By.id("RiskLocation_" + k + "_BuildingSumInsured")).click();
		Thread.sleep(3000);
		String pop_up_error_message_APP = getDriver()
				.findElement(By.xpath("//*[starts-with(@id,'modalDialog_')]/div[2]/p")).getText();
		System.out.println("APP " + pop_up_error_message_APP);
		System.out.println("INP " + pop_up_error);

		if (pop_up_error_message_APP.equalsIgnoreCase(pop_up_error)) {
			Assert.assertTrue("Pop up error message is displayed when contents sum insured is $0", true);
		} else {
			Assert.assertTrue("Pop up error message is displayed when contents sum insured is $0", false);

		}

	}

	@Step
	public void verify_valuable_category_values(String building_sum_insured_value, String location, DataTable arg1)
			throws InterruptedException {

		int k = 0;
		k = Integer.valueOf(location);
		Thread.sleep(2000);
		getDriver().findElement(By.id("RiskLocation_" + k + "_BuildingSumInsured")).sendKeys(building_sum_insured_value);
		Thread.sleep(500);
//		getDriver().findElement(By.id("RiskLocation_" + k + "_ContentsSumInsured")).click();
//		Thread.sleep(500);
		
		getDriver().findElement(By.id("RiskLocation_" + k + "_ContentsSumInsured")).sendKeys("50000");
//		Thread.sleep(500);

		JavascriptExecutor jse = (JavascriptExecutor) getDriver();
		jse.executeScript("window.scrollBy(0,250)", "");
		Thread.sleep(2000);

		try {
			List<WebElement> VAC_Coverage = getDriver().findElements(By.name("VacFlag_" + k));
			Thread.sleep(500);
			VAC_Coverage.get(0).click();
			Thread.sleep(500);

		} catch (Exception e) {

		}

		List<List<String>> data = arg1.raw();
		int drop_down_counter = 1;
		for (int i = 1; i <= data.size(); i++) {

			try {
				int j = i - 1;
				String valuable_category = data.get(i).get(0).toString().replace("[", "").replace("]", "");
				new Select(getDriver().findElement(By.id("Property_" + k + "_VacCategory_" + j)))
						.selectByVisibleText(valuable_category);
				Thread.sleep(100);
				String total_sum_insured = data.get(i).get(1).toString().replace("[", "").replace("]", "");
				getDriver().findElement(By.id("Property_" + k + "_VacCat_" + j + "_SumInsured"))
						.sendKeys(total_sum_insured);
				drop_down_counter++;

				if (i < data.size()-1) {
					List<WebElement> VAC_Coverage = getDriver().findElements(By.id("vac_AddVacCategory"));
					Thread.sleep(100);
					VAC_Coverage.get(k).click();
					Thread.sleep(100);
					jse.executeScript("window.scrollBy(0,50)", "");
					Thread.sleep(100);
				}

			} catch (Exception e) {
			}

		}

		if (data.size() == drop_down_counter) {
			Assert.assertTrue("Valuable Category drop down values are selected as per input", true);
		} else {
			Assert.assertTrue("Valuable Category drop down values are selected as per input", false);

		}
		System.out.println("input-- " + data.size());
		System.out.println("output-- " + drop_down_counter);

	}

	@Step
	public void delete_valuable_category_values(String valuable_category_count, String location)
			throws InterruptedException {

		int k;
		boolean flag=false;
		int VAC_COUNT = Integer.parseInt(valuable_category_count);
		k = Integer.valueOf(location);
		Thread.sleep(2000);
		JavascriptExecutor jse = (JavascriptExecutor) getDriver();
		int delete_counter = 0;
		for (int i = VAC_COUNT; i >= 1; i--) {

			try {
				int j = i - 1;

				//getDriver().findElement(By.xpath(".//*[@id='vacEdit_Items_" + j + "_Remove']/i")).click();
				getDriver().findElement(By.xpath(".//*[@id='vacEdit_Items_" + j + "_Remove']")).click();
				Thread.sleep(300);
				jse.executeScript("window.scrollBy(0,-50)", "");
				Thread.sleep(300);
				delete_counter++;

			} catch (Exception e) {

			}
		}
		
		try{
			if(getDriver().findElement(By.xpath("//button[contains(text(),'Property_"+k+"+_VacCat_"+k+"_SumInsured')]")).isDisplayed()){}
		}

		catch(Exception e1)
		{
			if(e1.getClass().getName().contains("NoSuchElement")){
				flag=true;
			}	
		}
		
//		if (getDriver().findElements(By.xpath("//table[contains(@data-bind,'UICoverageGroups()')]//tr")).size()==2) {
//			Assert.assertTrue("Delete function works in Valuable category section ", true);
//		} else {
//			Assert.assertTrue("Delete function not works in Valuable category section", false);
//
//		}
		if (flag) {
			Assert.assertTrue("Delete function works in Valuable category section ", true);
		} else {
			Assert.assertTrue("Delete function not works in Valuable category section", false);
		}
		System.out.println("input-- " + VAC_COUNT);
		System.out.println("output-- " + delete_counter);

	}

	@Step
	public void verify_contents_sum_insured(String property_type, String location, String contents_sum_insured)
			throws InterruptedException {

		int k = Integer.valueOf(location);
		new Select(getDriver().findElement(By.id("property_" + k + "_PropertyType")))
				.selectByVisibleText(property_type);
		Thread.sleep(1000);
		String Contents_Sum_Insured_APP = getDriver().findElement(By.id("RiskLocation_" + k + "_ContentsSumInsured"))
				.getAttribute("value");
		System.out.println(Contents_Sum_Insured_APP);

		if (Contents_Sum_Insured_APP.equalsIgnoreCase(contents_sum_insured)) {
			Assert.assertTrue("Contents Sum Insured defaults to " + Contents_Sum_Insured_APP + "for Unit", true);
		} else {
			Assert.assertTrue("Contents Sum Insured defaults to " + Contents_Sum_Insured_APP + "for Unit", false);

		}

	}

	@Step

	public void verify_coverage_information_section(String riskLocation_identifier, String propertyType,
			DataTable arg1) throws InterruptedException {

		int locationIdentifier = Integer.parseInt(riskLocation_identifier);
		List<List<String>> data = arg1.raw();
		String ContentsSumInsured_APP = null;
		
		// Scrolling because the values are not visible in screen
		
		JavascriptExecutor jse = (JavascriptExecutor) getDriver();
		jse.executeScript("window.scrollBy(0,450)", "");
		Thread.sleep(4000);
		
		String BuildingSumInsured_APP = getDriver().findElement(By.id("RiskLocation_" + locationIdentifier + "_BuildingSumInsured")).getAttribute("value");
		System.out.println("BuildingSumInsured_APP -- " + BuildingSumInsured_APP);
		System.out.println("BuildingSumInsured_INP -- " + data.get(1).get(1));

		if (propertyType.equalsIgnoreCase("House") || propertyType.equalsIgnoreCase("Other")) {
			ContentsSumInsured_APP = getDriver()
					.findElement(By.id("RiskLocation_" + locationIdentifier + "_ContentsSumInsured"))
					.getAttribute("value");
			System.out.println("ContentsSumInsured_APP -- " + ContentsSumInsured_APP);
			System.out.println("ContentsSumInsured_INP -- " + data.get(2).get(1));
		}

		String Deductible_APP = getDriver().findElement(By.id("RiskLocation_" + locationIdentifier + "_Deductible"))
				.getAttribute("value");
		System.out.println("Deductible_APP -- " + Deductible_APP);
		System.out.println("Deductible_INP -- " + data.get(3).get(1));
		

		if (propertyType.equalsIgnoreCase("House") || propertyType.equalsIgnoreCase("Other")) {

			if (BuildingSumInsured_APP.equalsIgnoreCase(data.get(1).get(1))
					&& ContentsSumInsured_APP.equalsIgnoreCase(data.get(2).get(1))
					&& Deductible_APP.equalsIgnoreCase(data.get(3).get(1))) {
				Assert.assertTrue("Coverage Information " + riskLocation_identifier
						+ "details are auto populated after Edit Quote", true);
			} else {
				Assert.assertTrue("Coverage Information " + riskLocation_identifier
						+ "details are not auto populated after Edit Quote", false);

			}
		} else {
			if (BuildingSumInsured_APP.equalsIgnoreCase(data.get(1).get(1))
					&& Deductible_APP.equalsIgnoreCase(data.get(3).get(1))) {
				Assert.assertTrue("Coverage Information " + riskLocation_identifier
						+ "details are auto populated after Edit Quote", true);
			} else {
				Assert.assertTrue("Coverage Information " + riskLocation_identifier
						+ "details are not auto populated after Edit Quote", false);

			}
		}
	}
	
	@Step
	public void enter_building_sum_insured(String test_data) throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(10000);
		System.out.println("control reached enter risk location details" );
	
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

			if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				System.out.println("test data --" + test_data);
				System.out.println("try to come out of loop "+rowNum);
				break innerloop;
			}
		}
		
		String no_of_loacations =XLS.getCellData(sheetName, "RiskLocation_Count", rowNum);


		
		for (int i=0;i<Integer.parseInt(no_of_loacations);i++)
		{
			try
			{

				String building_sum_insured = XLS.getCellData(sheetName, "BuildingSumInsured_"+(i+1), rowNum);
				System.out.println("RiskLocation_"+i+"_BuildingSumInsured");
				getDriver().findElement(By.id("RiskLocation_"+i+"_BuildingSumInsured")).clear();
				getDriver().findElement(By.id("RiskLocation_"+i+"_BuildingSumInsured")).sendKeys(building_sum_insured);
				getDriver().findElement(By.id("RiskLocation_"+i+"_BuildingSumInsured")).sendKeys(Keys.TAB);
				Thread.sleep(5000);
				
			}catch (Exception e)
			{
				Thread.sleep(1000);
			}
		}	
	}
	
	@Step
	public void verify_building_sum_insured_default(String test_data) throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(10000);
		System.out.println("control reached enter risk location details" );
	
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

			if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				System.out.println("test data --" + test_data);
				System.out.println("try to come out of loop "+rowNum);
				break innerloop;
			}
		}
		
		String no_of_loacations =XLS.getCellData(sheetName, "RiskLocation_Count", rowNum);
		String building_sum_insured_string;
		int building_sum_insured;
		if (no_of_loacations.equalsIgnoreCase("2"))
		{
			building_sum_insured_string = XLS.getCellData(sheetName, "BuildingSumInsured_1", rowNum);
			String building_sum_insured1_string = XLS.getCellData(sheetName, "BuildingSumInsured_2", rowNum);
			building_sum_insured = Integer.parseInt(building_sum_insured_string);
			int building_sum_insured1 = Integer.parseInt(building_sum_insured1_string);
			if(building_sum_insured<building_sum_insured1)
				building_sum_insured=building_sum_insured1;			
		}
		else
		{
			building_sum_insured_string = XLS.getCellData(sheetName, "BuildingSumInsured_1", rowNum);
			building_sum_insured = Integer.parseInt(building_sum_insured_string);
		}
		
		for (int i=0;i<Integer.parseInt(no_of_loacations);i++)
		{
			try
			{
				String contentText = getDriver().findElement(By.name("RiskLocation_"+i+"_Deductible")).getAttribute("value");
				String content;
				if(contentText.contains(",")){
					String[] words = contentText.split(",");
					content= words[0]+words[1];			
				}
				else
					content = contentText;
				System.out.println("content is "+content);
				Thread.sleep(1000);
				if(building_sum_insured>=2000000 )
				{
					if(Integer.parseInt(content)==1000)
						Assert.assertTrue("Content sum insured is as expected", true);
					else
						Assert.assertTrue("Content sum insured is as expected", true);
					
				}
				else if(building_sum_insured<2000000 )
				{
					if(Integer.parseInt(content)==500)
						Assert.assertTrue("Content sum insured is as expected", true);
					else
						Assert.assertTrue("Content sum insured is as expected", true);
					
				}
				
			}catch (Exception e)
			{
				Thread.sleep(1000);
			}
		}		
		
	}




}