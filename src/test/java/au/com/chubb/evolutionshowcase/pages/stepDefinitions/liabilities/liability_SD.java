package au.com.chubb.evolutionshowcase.pages.stepDefinitions.liabilities;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.liabilities.liability_Steps;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

public class liability_SD {

	@Steps
	liability_Steps liability;

	@And("^enter details in liability page as per \"([^\"]*)\"$")
	public void enter_liability_details(String testData) throws Throwable {
		liability.enter_liability_details(testData);
	}

	@And("^click proceed with Quote in liability page$")
	public void click_proceed_with_Quote() throws Throwable {
		liability.click_proceed_with_Quote();
	}

	@And("^verify default values in Liability and sum insured is \"([^\"]*)\"$")
	public void verify_liability_default_values(String sum_insured) throws Throwable {
		liability.verify_liability_default_values(sum_insured);
	}
	 
}
