package au.com.chubb.evolutionshowcase.pages.serenitySteps.closing;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import org.junit.runner.RunWith;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Keyboard;

import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class closingSteps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	@Step
	public void close_quote_as_uw(String test_data) throws InterruptedException,IOException{
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

				if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
					rowNum = row;
					System.out.println("test data --" + test_data);
					System.out.println("try to come out of loop "+rowNum);
					break innerloop;
				}
			}
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'closingReferenceId')]")).click();
    	getDriver().findElement(By.xpath("//input[contains(@data-bind,'closingReferenceId')]")).sendKeys(XLS.getCellData(sheetName, "Quote_Reference_Number", rowNum));
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = sdf.format(cal.getTime());
		System.out.println("Current date in String Format: "+strDate);
		
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'closingIssueDate')]")).sendKeys(strDate);
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'closingIssueDate')]")).sendKeys(Keys.TAB);
		
		String premium_App = getDriver().findElement(By.xpath("//td[contains(@data-bind,'premium.premiumPayableToChubbAmount')]")).getText();
		premium_App=premium_App .replace("$", "");
		premium_App=premium_App .replace(",", "");
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'closingPremiumPayableToChubbAmount')]")).sendKeys(premium_App);
	}

}