package au.com.chubb.evolutionshowcase.pages.stepDefinitions.notes;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.notes.*;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Steps;

public class addNotesStepDefinition extends PageObject {

	@Steps
	addNotesSteps addNotesSteps;

	@And("^Add Notes and verify$")
	public void addNotes() throws Throwable {
		addNotesSteps.addNotesandverifyNotes();
		Thread.sleep(1000);
	}

	
}