package au.com.chubb.evolutionshowcase.pages.serenitySteps.landingPage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
//import org.sikuli.script.FindFailed;
//import org.sikuli.script.Pattern;
//import org.sikuli.script.Screen;

import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class landingPage_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public landingPage_Steps() throws IOException {
		OR = new Properties();
		// FileInputStream fo = new FileInputStream(root +
		// "/ObjectRepository/landingPage.properties");
		// OR.load(fo);

	}

	@Step
	public void click_indicative_quote() throws InterruptedException {
		loop: for (int i = 1; i < 100; i++) {
			try {
				//getDriver().findElement(By.id("btnIndicative")).isDisplayed();
				Thread.sleep(5000);

				//WebElement elmnt = getDriver().findElement(By.id("btnIndicative"));
				//JavascriptExecutor js = (JavascriptExecutor) getDriver();
				//js.executeScript("arguments[0].click();", elmnt);
				 getDriver().findElement(By.id("btnIndicative")).click();

				break loop;
			} catch (Exception e) {
				Thread.sleep(1000);
			}
		}

		getDriver().findElement(By.id("btnContinue")).click();
		WebDriverWait wait = new WebDriverWait(getDriver(), 100);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("ClientFirstName")));
				
		Thread.sleep(10000);
	}

	@Step
	public void click_full_quote() throws InterruptedException {

		loop: for (int i = 1; i < 100; i++) {
			try {

				WebElement elmnt = getDriver().findElement(By.id("btnFull"));
				JavascriptExecutor js = (JavascriptExecutor) getDriver();
				js.executeScript("arguments[0].click();", elmnt);
				// getDriver().findElement(By.id("btnFull")).click();
				break loop;
			} catch (Exception e) {
				Thread.sleep(1000);
			}
		}

		WebElement elmnt = getDriver().findElement(By.id("btnContinue"));
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("arguments[0].click();", elmnt);
		Thread.sleep(50000);
		
		// getDriver().findElement(By.id("btnContinue")).click();
		// getDriver().findElement(By.xpath("//button[contains(text(),'OK')]")).click();
		// Thread.sleep(10000);
		
		// WebDriverWait wait = new WebDriverWait(getDriver(), 300);
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Update Client')]")));
		// Thread.sleep(1000);
	}

	@Step
	public void verify_navigate_URL(String URL, String testflow) throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		System.out.println("control reached enter risk location details");

		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		String Current_URL = getDriver().getCurrentUrl();
		System.out.println(Current_URL);
		if (Current_URL.contains(URL)) {
			Assert.assertTrue("Page Navigation successful ", true);
		} else {

			innerloop: for (int row = 2; row <= Row_Count; row++) {

				if (testflow.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {

					XLS.setCellData(sheetName, "STATUS", row, "FAIL");
					break innerloop;
				}
			}

			Assert.assertTrue("Page Navigation successful ", false);

		}
	}

	@Step
	public void verify_footer_URL(String footer_link, String URL) throws InterruptedException {

		/*
		 * JavascriptExecutor jse = (JavascriptExecutor)getDriver();
		 * jse.executeScript("window.scrollBy(0,450)", ""); Thread.sleep(2000);
		 */
		getDriver().findElement(By.xpath("//a[contains(text(),('" + footer_link + "'))]")).click();
		Thread.sleep(9000);

		ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());
		getDriver().switchTo().window(tabs.get(1));
		Thread.sleep(3000);
		String Clicked_URL = getDriver().getCurrentUrl();
		System.out.println("Clicked URL " + Clicked_URL);

		if (Clicked_URL.contains(URL)) {
			Assert.assertTrue("Page Navigation successful ", true);
		} else {
			Assert.assertTrue("Page Navigation successful ", false);

		}

		getDriver().close();
		getDriver().switchTo().window(tabs.get(0));
	}

	/*
	 * Created by: Premchand
	 * 
	 * Method:click_On_Quick_Indication_Button
	 * 
	 * Description: clicking on Quick Indication quote
	 * 
	 * verifying:After clickng on Quick Indication Quote, checking Indicative
	 * Quote page is displaying or not.
	 * 
	 * arguments: NA
	 */

	@Step
	public void click_On_Quick_Indication_Button() throws InterruptedException {
		try {
			Thread.sleep(5000);
			getDriver().findElement(By.id("btnIndicative")).click();
		} catch (NoSuchElementException e) {
			System.out.println("Element is not found:" + e.getMessage());
		}
		Thread.sleep(3000);
		getDriver().findElement(By.id("btnContinue")).click();
		Thread.sleep(3000);
		Assert.assertEquals("Policy Admin: Chubb Masterpiece", getDriver().getTitle());
	}

	
	/*
	 * Created by: Premchand
	 *  
	 * Method:click_privacy_link_verify_url
	 *
	 * Description:clicking on privacy link in the footer and trying to open pdf
	 * file
	 *
	 * arguments: NA
	 */

//	@Step
//	public void click_privacy_link_openPdf() throws InterruptedException, FindFailed {
//		try {
//			getDriver().findElement(By.linkText("Privacy Statement")).click();
//			Thread.sleep(2000);
//		} catch (NoSuchElementException e) {
//			System.out.println("Privacy Statement Link not found:" + e.getMessage());
//		}
//		// Creating Object for Screen class using Sikuli
//		Screen scr = new Screen();
//
//		// creating Object for open Button using Pattern class under Sikuili
//		Pattern open_button = new Pattern(
//				System.getProperty("user.dir") + "/src/test/java/au/com/chubb/evolutionshowcase/Images/openButton.PNG");
//
//		// click on Open button
//		scr.click(open_button);
//	}
//
//	public void click_duty_of_disclousure()throws InterruptedException, FindFailed{
//		// TODO Auto-generated method stub
//		try{
//			getDriver().findElement(By.linkText("Duty of Disclosure")).click();
//			Thread.sleep(2000);
//		} catch (NoSuchElementException e) {
//			System.out.println("Privacy Statement Link not found:" + e.getMessage());
//		}
//		// Creating Object for Screen class using Sikuli
//				Screen scr = new Screen();
//
//				// creating Object for open Button using Pattern class under Sikuili
//				Pattern open_button = new Pattern(
//						System.getProperty("user.dir") + "/src/test/java/au/com/chubb/evolutionshowcase/Images/openButton.PNG");
//
//				// click on Open button
//				scr.click(open_button);
//	}

}