package au.com.chubb.evolutionshowcase.pages.serenitySteps.liabilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

import java.util.regex.*;

@RunWith(SerenityRunner.class)
public class liability_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public liability_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);

	}

	public void enter_liability_details(String liability_testData) throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		Thread.sleep(1000);

		String sheetName = "Liability";
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: for (int row = 2; row <= Row_Count; row++) {

			System.out.println("Liability test data --" + liability_testData);
			if (liability_testData.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				break innerloop;
			}
		}
		System.out.println("Value of row number " + rowNum);

		// Include Personal Liability
		List<WebElement> IncludePersonalLiability = getDriver().findElements(By.name("personalLiabilityFlag"));
		if (XLS.getCellData(sheetName, "IncludePersonalLiability", rowNum).equalsIgnoreCase("Yes")) {
			Thread.sleep(2000);
			IncludePersonalLiability.get(0).click();
			Thread.sleep(100);

		} else if (XLS.getCellData(sheetName, "IncludePersonalLiability", rowNum).equalsIgnoreCase("No")) {
			Thread.sleep(2000);
			IncludePersonalLiability.get(1).click();
			Thread.sleep(100);

		}

		String EnterSumInsured = XLS.getCellData(sheetName, "EnterSumInsured", rowNum);

		if (XLS.getCellData(sheetName, "IncludePersonalLiability", rowNum).equalsIgnoreCase("Yes")) {
			
			getDriver().findElement(By.id("personalLiabilitySumInsured")).clear();
			Thread.sleep(500);
			getDriver().findElement(By.id("personalLiabilitySumInsured")).sendKeys(EnterSumInsured);
			Thread.sleep(100);

		} else {

			
		}
		
		Thread.sleep(2000);

		// Include Family Protection
		List<WebElement> IncludeFamilyProtection = getDriver().findElements(By.name("familyProtectionFlag"));
		if (XLS.getCellData(sheetName, "IncludeFamilyProtection", rowNum).equalsIgnoreCase("Yes")) {
			IncludeFamilyProtection.get(0).click();
			Thread.sleep(2000);

		} else if (XLS.getCellData(sheetName, "IncludeFamilyProtection", rowNum).equalsIgnoreCase("No")) {
			IncludeFamilyProtection.get(1).click();
			Thread.sleep(100);

		}

		try {

			getDriver().findElement(By.id("btnSaveProgress")).click();
			Thread.sleep(8000);
		} catch (Exception e) {

		}

	}

	@Step
	public void click_proceed_with_Quote() throws InterruptedException {

		getDriver().findElement(By.xpath("//button[contains(.,'Proceed With Quote')]")).click();
		Thread.sleep(8000);

	}

	public void verify_liability_default_values(String sum_insured) {
		// TODO Auto-generated method stub
		int match_counter=0;

		List<WebElement> IncludePersonalLiability = getDriver().findElements(By.name("personalLiabilityFlag"));
		List<WebElement> IncludeFamilyProtection = getDriver().findElements(By.name("familyProtectionFlag"));
		if (IncludePersonalLiability.get(0).getAttribute("checked").equals("true"))
		{
			match_counter++;
			System.out.println("IncludePersonalLiability" +match_counter);
		}
		
		String sum_insured_APP=getDriver().findElement(By.id("personalLiabilitySumInsured")).getAttribute("value");
		
		if (sum_insured_APP.equalsIgnoreCase(sum_insured))
		{
			match_counter++;
			System.out.println("sum insured" +match_counter);
		}
		
		if (IncludeFamilyProtection.get(0).getAttribute("checked").equals("true"))
		{
			match_counter++;
			System.out.println("IncludeFamilyProtection" +match_counter);
		}
		
		if (match_counter==3)
		{
			Assert.assertTrue("Default values in Liability pages matches for Underwriter View", true);
		}
		else
		{
			Assert.assertTrue("Default values in Liability pages matches for Underwriter View", false);

		}
		
	}

}
