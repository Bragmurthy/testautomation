package au.com.chubb.evolutionshowcase.pages.serenitySteps.eBusinessPortal;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import au.com.chubb.evolutionshowcase.pages.objectModel.eBusinessPortal.eBusinessPortalAddNewQuotePage;
import au.com.chubb.evolutionshowcase.utilities.CommonFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class eBusinessPortalFindQuoteSteps extends PageObject {
	String root = System.getProperty("user.dir");
	static eBusinessPortalAddNewQuotePage ebusinessportaladdNewQuotepage;
	static CommonFunctions common_functions;

		
	@Step
	public void click_find_quote() throws InterruptedException {

		ebusinessportaladdNewQuotepage.click_find_quote();

	}
	
	@Step	
	public void verify_quote_status(String test_data, String quoteStatus) throws InterruptedException {
    	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop: for (int row = 2; row <= Row_Count; row++) {

			System.out.println("test data --" + test_data);
			if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				break innerloop;
			}
		}
		
		String quoteReferenceNumber_excel=XLS.getCellData(sheetName, "Quote_Reference_Number", rowNum);
		System.out.println("Quote Reference Excel -- " +quoteReferenceNumber_excel );
		
			try
			{
				getDriver().findElement(By.name("txtInsurerRef")).sendKeys(quoteReferenceNumber_excel);
                //getDriver().findElement(By.xpath("html/body/table/tbody/tr[2]/td[2]/form/table[2]/tbody/tr[2]/td[4]/input")).sendKeys(quoteReferenceNumber_excel);
                Thread.sleep(5000);
                getDriver().findElement(By.id("Search")).click();
                Thread.sleep(5000);
                WebElement Element = getDriver().findElement(By.xpath("//form[@name='frmGIQteFind']/table[3]/tbody/tr[3]"));
                if(Element.getText().contains(quoteStatus))
                {
                      Assert.assertTrue("Quote Status  as expected", true);
                }
                else
                {
                      Assert.assertTrue("Quote Status as not expected Actual is "+Element.getText(), false);                      
                }
              		
			}catch (Exception e)
			{
				Thread.sleep(1000);

			}
	
	}
	
	@Step	
	public void verify_quote_status_by_passing_clientId(String test_data, String quoteStatus) throws InterruptedException {
    	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "QuoteData";
		Thread.sleep(1000);
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop: for (int row = 2; row <= Row_Count; row++) {

			System.out.println("test data --" + test_data);
			if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				break innerloop;
			}
		}
		
		String InsuredId=XLS.getCellData(sheetName, "InsuredId", rowNum);
		System.out.println("Quote Reference Excel -- " +InsuredId );
		
			try
			{
				getDriver().findElement(By.name("txtClientId")).sendKeys(InsuredId);
                //getDriver().findElement(By.xpath("html/body/table/tbody/tr[2]/td[2]/form/table[2]/tbody/tr[2]/td[4]/input")).sendKeys(quoteReferenceNumber_excel);
                Thread.sleep(1000);
                getDriver().findElement(By.id("Search")).click();
                Thread.sleep(5000);
                WebElement Element = getDriver().findElement(By.xpath("//form[@name='frmGIQteFind']/table[3]/tbody/tr[3]/td[5]"));
                if(Element.getText().contains(quoteStatus))
                {
                      Assert.assertTrue("Quote Status  as expected", true);
                }
                else
                {
                      Assert.assertTrue("Quote Status as not expected", false);                      
                }
                	
			}catch (Exception e)
			{
				Thread.sleep(1000);

			}
		
		}

@Step
	public void searchQuoteIneBix(String testData) throws InterruptedException {
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop: for (int row = 2; row <= Row_Count; row++) {
	
			System.out.println("test data --" + testData);
			if (testData.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				break innerloop;
			}
		}
		
		String quoteReferenceNumber_excel=XLS.getCellData(sheetName, "Quote_Reference_Number", rowNum);
		System.out.println("Quote Reference Excel -- " +quoteReferenceNumber_excel );
		
		
		loop:
		for (int i=0;i<100;i++)
		{
			try
			{
				getDriver().findElement(By.name("txtInsurerRef")).sendKeys(quoteReferenceNumber_excel);
	            //getDriver().findElement(By.xpath("html/body/table/tbody/tr[2]/td[2]/form/table[2]/tbody/tr[2]/td[4]/input")).sendKeys(quoteReferenceNumber_excel);
	            Thread.sleep(1000);
	            getDriver().findElement(By.id("Search")).click();
	            Thread.sleep(8000);
	          
	            break loop;
	
				
			}catch (Exception e)
			{
				Thread.sleep(1000);
	
			}
		
		}
	
	
	}

@Step
public void searchPolicyIneBix(String testData) throws InterruptedException {
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
	String sheetName = "IndicativeQuoteRiskLocation";
	Thread.sleep(1000);
	
	int Row_Count = XLS.getRowCount(sheetName);
	int rowNum = 0;
	
	innerloop: for (int row = 2; row <= Row_Count; row++) {

		System.out.println("test data --" + testData);
		if (testData.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
			rowNum = row;
			break innerloop;
		}
	}
	
	String policy_excel=XLS.getCellData(sheetName, "PolicyNumber", rowNum).replace("'","");
	System.out.println("Quote Reference Excel -- " +policy_excel );
	
	
	loop:
	for (int i=0;i<100;i++)
	{
		try
		{
			getDriver().findElement(By.name("txtPolicyNo")).sendKeys(policy_excel);
            //getDriver().findElement(By.xpath("html/body/table/tbody/tr[2]/td[2]/form/table[2]/tbody/tr[2]/td[4]/input")).sendKeys(quoteReferenceNumber_excel);
            Thread.sleep(1000);
            getDriver().findElement(By.id("Search")).click();
            Thread.sleep(10000);
          
            break loop;

			
		}catch (Exception e)
		{
			Thread.sleep(1000);

		}
	
	}


}


	@Step
	public void searchQuoteIneBixOurRef(String testData) throws InterruptedException {
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop: for (int row = 2; row <= Row_Count; row++) {
	
			System.out.println("test data --" + testData);
			if (testData.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				break innerloop;
			}
		}
		
		String OurRef=XLS.getCellData(sheetName, "OurRef", rowNum);
		System.out.println("Our Reference Excel -- " +OurRef );
		
		
		loop:
		for (int i=0;i<100;i++)
		{
			try
			{
				getDriver().findElement(By.name("txtOurRef")).sendKeys(OurRef);
	            Thread.sleep(1000);
	            getDriver().findElement(By.id("Search")).click();
	            Thread.sleep(8000);
	          
	            break loop;
	
				
			}catch (Exception e)
			{
				Thread.sleep(1000);
	
			}
		
		}
	}
}
