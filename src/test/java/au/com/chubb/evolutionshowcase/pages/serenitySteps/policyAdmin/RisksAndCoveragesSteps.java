package au.com.chubb.evolutionshowcase.pages.serenitySteps.policyAdmin;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.utilities.CommonFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;

public class RisksAndCoveragesSteps extends PageObject {

	CommonFunctions common_functions;
	String root = System.getProperty("user.dir");
	public static Properties OR = null;
	
	@Step
	public void changeELIASrating(String test_data) throws InterruptedException, IOException {
		getDriver().get("http://apevodweb121:1111/WorkManager");
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//a[contains(text(),'Global Search')]")).click();	
		Thread.sleep(10000);
		
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		System.out.println("control reached enter risk location details" );

		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;

		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

				if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
					rowNum = row;
					System.out.println("test data --" + test_data);
					System.out.println("try to come out of loop "+rowNum);
					break innerloop;
				}
			}

		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyAdmin.properties");
		OR.load(fo);
//		getDriver().findElement(By.xpath("//input[@id='SearchPolicyNum']")).sendKeys(XLS.getCellData(sheetName, "Policy_Number", rowNum));
		


		getDriver().findElement(By.xpath("//input[@id='SearchPolicyNum']")).sendKeys(XLS.getCellData(sheetName, "Quote_Reference_Number", rowNum));
		
    	getDriver().findElement(By.xpath("//button[@id='Search']")).click();
    	
    	Thread.sleep(10000);
    	getDriver().findElement(By.xpath("//td[contains(text(),'Referral')]")).click();

    	Thread.sleep(5000);
    	
    	String WorkManagerwindow= getDriver().getWindowHandle();
    	System.out.println("present window is" +WorkManagerwindow);

    	WebElement editInPolicy=getDriver().findElement(By.xpath("//input[@id='btnEditPolicy']"));
		
    	((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", editInPolicy);
		editInPolicy.click();
		Thread.sleep(5000);
		
		Set handles=getDriver().getWindowHandles();
		System.out.println("present window is" +handles);
		
		for( String policyAdminwindow:getDriver().getWindowHandles())
		{
			getDriver().switchTo().window(policyAdminwindow);
		}
		
		Thread.sleep(5000);
		getDriver().manage().window().maximize();
//		clicking on Risks and coverages
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//a[contains(text(),'Risks / Coverages')]")).click();
		System.out.println("clicked on Risks and coverages");
    	Thread.sleep(5000);
    	getDriver().findElement(By.xpath("//div[@id='divEliasRead']/span/button")).click();
		System.out.println("clicked on edit of the ELIAS");
		Thread.sleep(5000);
		
//		Selecting option from rating from the popup
		WebElement ratingElement = getDriver().findElement(By.xpath("//select[@id='override-flood-rating-id']"));
		Select sel = new Select(ratingElement);
		sel.selectByIndex(4);
		System.out.println("option is selected from rating dropdown");
//		Selecting option from reason from the popup		
		WebElement reasonElement = getDriver().findElement(By.xpath("//select[@id='override-flood-rating-reasonid']"));
		Select sel1 = new Select(reasonElement);
		sel1.selectByIndex(3);
		System.out.println("option is selected from reason dropdown");
		
//		clicking on save button from the popup		
		getDriver().findElement(By.xpath("//button[text()='Save']")).click();
		System.out.println("Clicked on save button from the popup");
		Thread.sleep(5000);
//		clicking on Save Progress button 		
		getDriver().findElement(By.xpath("//button[text()='Save Progress']")).click();		
		System.out.println("Clicked on save progress button");
		Thread.sleep(5000);
		
		getDriver().close();
//		getDriver().switchTo().defaultContent();
		getDriver().switchTo().window(WorkManagerwindow);
		System.out.println("switched to default driver ");
	}
}
