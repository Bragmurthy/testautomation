package au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class riskLocation_enterDetails_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public riskLocation_enterDetails_Steps() throws IOException {
		/*
		 * OR = new Properties(); FileInputStream fo = new FileInputStream(root +
		 * "/ObjectRepository/landingPage.properties"); OR.load(fo);
		 */
	}

	@Step
	public void enter_risk_location_details(String test_data) throws InterruptedException {
				
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		System.out.println("control reached enter risk location details" );
	
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

			if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				System.out.println("test data --" + test_data);
				System.out.println("try to come out of loop "+rowNum);
				break innerloop;
			}
		}
		
		System.out.println("came out of for loop");
		String Location_Count=XLS.getCellData(sheetName, "RiskLocation_Count", rowNum);
		System.out.println("Location_Count "+Location_Count);
        int count =0;
		count=Integer.valueOf(Location_Count);
		System.out.println("Value of count " + count);

		for (int i=1;i<=count;i++)
		{ 					System.out.println("insider for loop");

			int k=i-1;
			if (i==2)
			{
				System.out.println("Try to click add risk location");
				Thread.sleep(2000);	
				WebElement AddNextRiskLoaction=getDriver().findElement(By.xpath("//*[contains(text(),('Add Next Risk Location'))]"));
				((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", AddNextRiskLoaction);
	            Thread.sleep(2000);
				getDriver().findElement(By.xpath("//*[contains(text(),('Add Next Risk Location'))]")).click();
	            Thread.sleep(2000);

			}
						
			loop:
				for (int j=1;j<100;j++)
				{
				try
				{
					 System.out.println("Address selection " +k);
					 System.out.println("value of i " +i);
					    getDriver().findElement(By.id("addressEditor_"+k+"_Search")).sendKeys(XLS.getCellData(sheetName, "Address_"+i, rowNum));
						Thread.sleep(2000);
						getDriver().findElement(By.id("addressEditor_"+k+"_Search")).sendKeys(Keys.DOWN);
						Thread.sleep(2000);
						getDriver().findElement(By.id("addressEditor_"+k+"_Search")).sendKeys(Keys.ENTER);
						
					break loop;
				}catch (Exception e)
				{
					Thread.sleep(1000);
				}
				}
			
			Thread.sleep(2000);
			new Select(getDriver().findElement(By.id("property_"+k+"_OwnershipType"))).selectByVisibleText(XLS.getCellData(sheetName, "PropertyOwnership_"+i, rowNum));
			new Select(getDriver().findElement(By.id("property_"+k+"_ResidenceType"))).selectByVisibleText(XLS.getCellData(sheetName, "TypeOfResidence_"+i, rowNum));
			Thread.sleep(1000);

             String TypeofResidence=XLS.getCellData(sheetName, "TypeOfResidence_"+i, rowNum);
             if (TypeofResidence.equalsIgnoreCase("Other"))
             {
            	 try
         		{
         			List<WebElement> TypeOfResidenceOther = getDriver().findElements(By.id("OtherResidenceTypeText"));
         			if (k==0) {
         				TypeOfResidenceOther.get(0).sendKeys(XLS.getCellData(sheetName, "TypeOfResidenceOther_"+i, rowNum));
         				Thread.sleep(100);
         			} else if (k==1) {
         				TypeOfResidenceOther.get(1).sendKeys(XLS.getCellData(sheetName, "TypeOfResidenceOther_"+i, rowNum));
         				Thread.sleep(100);
         			}
         			else
         			{
         				
         			}
         			
         		} catch (Exception e)       
          		{
         			
          		}
                  	 
             }
             
     		try
    		{
    			List<WebElement> YearOfConstruction = getDriver().findElements(By.id("YearOfConstruction"));
    			if (k==0) {
    				YearOfConstruction.get(0).sendKeys(XLS.getCellData(sheetName, "YearOfConsturction_"+i, rowNum));
    				Thread.sleep(100);
    			} else if (k==1) {
    				YearOfConstruction.get(1).sendKeys(XLS.getCellData(sheetName, "YearOfConsturction_"+i, rowNum));
    				Thread.sleep(100);
    			}
    			else
    			{
    				
    			}
    			
    		} catch (Exception e)       
     		{
    			
     		}
     		
			new Select(getDriver().findElement(By.id("property_"+k+"_PropertyType"))).selectByVisibleText(XLS.getCellData(sheetName, "PropertyType_"+i, rowNum));
            String PropertyType=XLS.getCellData(sheetName, "PropertyType_"+i, rowNum);
			Thread.sleep(1000);

            if (PropertyType.equalsIgnoreCase("Other"))
            {
           	 try
        		{
        			List<WebElement> PropertyTypeOther = getDriver().findElements(By.id("OtherPropertyTypeText"));
        			if (k==0) {
        				PropertyTypeOther.get(0).sendKeys(XLS.getCellData(sheetName, "PropertyTypeOther_"+i, rowNum));
        				Thread.sleep(100);
        			} else if (k==1) {
        				PropertyTypeOther.get(1).sendKeys(XLS.getCellData(sheetName, "PropertyTypeOther_"+i, rowNum));
        				Thread.sleep(100);
        			}
        			else
        			{
        				
        			}
        			
        		} catch (Exception e)       
         		{
        			
         		}
                 	 
            }
            
            new Select(getDriver().findElement(By.id("property_"+k+"_BurglarAlarmDetection"))).selectByVisibleText(XLS.getCellData(sheetName, "BurglarAlarm_"+i, rowNum));
			new Select(getDriver().findElement(By.id("property_"+k+"_ExternalConstruction"))).selectByVisibleText(XLS.getCellData(sheetName, "ExternalConstruction_"+i, rowNum));
            String ExternalConstruction=XLS.getCellData(sheetName, "ExternalConstruction_"+i, rowNum);
			Thread.sleep(1000);

			 if (PropertyType.equalsIgnoreCase("Other"))
	            {
	           	 try
	        		{
	        			List<WebElement> ExternalConstructionOther = getDriver().findElements(By.id("OtherExternalConstructionMaterialText"));
	        			if (k==0) {
	        				ExternalConstructionOther.get(0).sendKeys(XLS.getCellData(sheetName, "ExternalConstructionOther_"+i, rowNum));
	        				Thread.sleep(100);
	        			} else if (k==1) {
	        				ExternalConstructionOther.get(1).sendKeys(XLS.getCellData(sheetName, "ExternalConstructionOther_"+i, rowNum));
	        				Thread.sleep(100);
	        			}
	        			else
	        			{
	        				
	        			}
	        			
	        		} catch (Exception e)       
	         		{
	        			
	         		}
	                 	 
	            }
			 
			 JavascriptExecutor jse = (JavascriptExecutor) getDriver();
			 jse.executeScript("window.scrollBy(0,100)", "");
			 Thread.sleep(2000);
			 
			 if (!XLS.getCellData(sheetName, "PropertyType_"+i, rowNum).equals("Unit")){
					getDriver().findElement(By.id("RiskLocation_"+k+"_BuildingSumInsured")).sendKeys(XLS.getCellData(sheetName, "BuildingSumInsured_"+i, rowNum));
					Thread.sleep(100); 
			 }
			 
			 getDriver().findElement(By.id("RiskLocation_"+k+"_ContentsSumInsured")).click();
			 Thread.sleep(500);
			 getDriver().findElement(By.id("RiskLocation_"+k+"_ContentsSumInsured")).clear();
			 Thread.sleep(3000);
			 getDriver().findElement(By.id("RiskLocation_"+k+"_ContentsSumInsured")).sendKeys(XLS.getCellData(sheetName, "ContentsSumInsured_"+i, rowNum));
			 Thread.sleep(1000);
			 
			String VACFlag=XLS.getCellData(sheetName, "VAC_Coverage_"+i, rowNum);
			 if (VACFlag.equalsIgnoreCase("Yes"))
	            {
	           	 try
	        		{
	        			List<WebElement> VAC_Coverage = getDriver().findElements(By.name("VacFlag_"+k));
	        			Thread.sleep(500);
	        			VAC_Coverage.get(0).click();
	        			Thread.sleep(500);
        			
	        		} catch (Exception e)       
	         		{
	        			
	         		}
	            }

			 String FloodCoverageFlag=XLS.getCellData(sheetName, "Flood_Coverage_"+i, rowNum);
			 if (FloodCoverageFlag.equalsIgnoreCase("Yes"))
	            {
	           	 try
	        		{
	        			List<WebElement> Flood_Coverage = getDriver().findElements(By.name("FloodCoverageFlag_"+k));
	        			Thread.sleep(500);
	        			Flood_Coverage.get(0).click();
	        			Thread.sleep(500);
        			
	        		} catch (Exception e)       
	         		{
	        			
	         		}
	            }
			 System.out.println(i);
			 System.out.println(XLS.getCellData(sheetName, "NumberofLosses_"+i, rowNum));
			 String numberOfLosses=XLS.getCellData(sheetName, "NumberofLosses_"+i, rowNum);
			 int losses=Integer.parseInt(numberOfLosses);
			 if (losses>=0)
	            {
	           	 try
	        		{
	        		new Select(getDriver().findElement(By.xpath("//select[contains(@data-bind,'NumberOfClaims')]"))).selectByVisibleText(Integer.toString(losses));
	        		} catch (Exception e)       
	         		{
	        			
	         		}
	            }
			 try
     		{
			 String Quote_Reference_Number=getDriver().findElement(By.xpath(".//*[@id='indicativeQuote']/div/div[10]/div[2]/span/span")).getText();
			 System.out.println(Quote_Reference_Number);
			 XLS.setCellData(sheetName, "Quote_Reference_Number", rowNum, Quote_Reference_Number);
			 XLS.setCellData(sheetName, "QuoteNumber", rowNum, Quote_Reference_Number);
     		}catch(Exception e)
			 {
     			
			 }
	 
			 
		}
	}

	@Step
	public void verify_default_value(String field_locator) {
		
		String default_drop_down_value=getDriver().findElement(By.id(field_locator)).getAttribute("Value");
		if (default_drop_down_value==null)
		{
			Assert.assertTrue("Default drop down value for field "+field_locator+ " is Empty", true);
		}
		else
		{
			Assert.assertTrue("Default drop down value for field "+field_locator+ " is Empty", false);

		}
		
	}

	@Step
	public void enterClientSurName() {
		String ALPHA_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		Random random = new Random();	     
	    String randResult = "";
	     for (int i = 0; i < 4; i++) {
	        int index = random.nextInt(ALPHA_STRING.length());
	        randResult += ALPHA_STRING.charAt(index);
	    }
		getDriver().findElement(By.name("ClientLastName")).sendKeys(randResult);
			
	}
	
@Step
	public void enter_risklocation_details(String test_data) throws InterruptedException {
	
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		System.out.println("control reached enter risk location details" );
	
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

			if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				System.out.println("test data --" + test_data);
				System.out.println("try to come out of loop "+rowNum);
				break innerloop;
			}
		}
		
		System.out.println("came out of for loop");
		String Location_Count=XLS.getCellData(sheetName, "RiskLocation_Count", rowNum);
		System.out.println("Location_Count "+Location_Count);
        int count =0;
		count=Integer.valueOf(Location_Count);
		System.out.println("Value of count " + count);

		for (int i=1;i<=count;i++)
		{ 					System.out.println("insider for loop");

			int k=i-1;
			if (i==2)
			{
				System.out.println("Try to click add risk location");
				Thread.sleep(2000);	
				WebElement AddNextRiskLoaction=getDriver().findElement(By.xpath("//*[contains(text(),('Add Next Risk Location'))]"));
				((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", AddNextRiskLoaction);
	            Thread.sleep(2000);
				getDriver().findElement(By.xpath("//*[contains(text(),('Add Next Risk Location'))]")).click();
	            Thread.sleep(2000);

			}
						
			loop:
				for (int j=1;j<100;j++)
				{
				try
				{
					 System.out.println("Address selection " +k);
					 System.out.println("value of i " +i);
					  	getDriver().findElement(By.id("addressEditor_"+k+"_UnitNo")).sendKeys("");
						getDriver().findElement(By.id("addressEditor_"+k+"_StreetNo")).sendKeys("45");
						getDriver().findElement(By.id("addressEditor_"+k+"_StreetName")).sendKeys("Ferozpuru Road");
						getDriver().findElement(By.id("addressEditor_"+k+"_BuildingName")).sendKeys("");
						getDriver().findElement(By.id("addressEditor_"+k+"_City")).sendKeys("Ludhianaaa");
						getDriver().findElement(By.id("addressEditor_"+k+"_PostCode")).sendKeys("1422");
						new Select(getDriver().findElement(By.id("addressEditor_"+k+"_Country"))).selectByVisibleText("Australia");
						new Select(getDriver().findElement(By.id("addressEditor_"+k+"_State"))).selectByVisibleText("Victoria");
						
						
					break loop;
				}catch (Exception e)
				{
					Thread.sleep(1000);
				}
				}
			
			Thread.sleep(2000);
			new Select(getDriver().findElement(By.id("property_"+k+"_OwnershipType"))).selectByVisibleText(XLS.getCellData(sheetName, "PropertyOwnership_"+i, rowNum));
			new Select(getDriver().findElement(By.id("property_"+k+"_ResidenceType"))).selectByVisibleText(XLS.getCellData(sheetName, "TypeOfResidence_"+i, rowNum));
			Thread.sleep(1000);

             String TypeofResidence=XLS.getCellData(sheetName, "TypeOfResidence_"+i, rowNum);
             if (TypeofResidence.equalsIgnoreCase("Other"))
             {
            	 try
         		{
         			List<WebElement> TypeOfResidenceOther = getDriver().findElements(By.id("OtherResidenceTypeText"));
         			if (k==0) {
         				TypeOfResidenceOther.get(0).sendKeys(XLS.getCellData(sheetName, "TypeOfResidenceOther_"+i, rowNum));
         				Thread.sleep(100);
         			} else if (k==1) {
         				TypeOfResidenceOther.get(1).sendKeys(XLS.getCellData(sheetName, "TypeOfResidenceOther_"+i, rowNum));
         				Thread.sleep(100);
         			}
         			else
         			{
         				
         			}
         			
         		} catch (Exception e)       
          		{
         			
          		}
                  	 
             }
             
     		try
    		{
    			List<WebElement> YearOfConstruction = getDriver().findElements(By.id("YearOfConstruction"));
    			if (k==0) {
    				YearOfConstruction.get(0).sendKeys(XLS.getCellData(sheetName, "YearOfConsturction_"+i, rowNum));
    				Thread.sleep(100);
    			} else if (k==1) {
    				YearOfConstruction.get(1).sendKeys(XLS.getCellData(sheetName, "YearOfConsturction_"+i, rowNum));
    				Thread.sleep(100);
    			}
    			else
    			{
    				
    			}
    			
    		} catch (Exception e)       
     		{
    			
     		}
     		
			new Select(getDriver().findElement(By.id("property_"+k+"_PropertyType"))).selectByVisibleText(XLS.getCellData(sheetName, "PropertyType_"+i, rowNum));
            String PropertyType=XLS.getCellData(sheetName, "PropertyType_"+i, rowNum);
			Thread.sleep(1000);

            if (PropertyType.equalsIgnoreCase("Other"))
            {
           	 try
        		{
        			List<WebElement> PropertyTypeOther = getDriver().findElements(By.id("OtherPropertyTypeText"));
        			if (k==0) {
        				PropertyTypeOther.get(0).sendKeys(XLS.getCellData(sheetName, "PropertyTypeOther_"+i, rowNum));
        				Thread.sleep(100);
        			} else if (k==1) {
        				PropertyTypeOther.get(1).sendKeys(XLS.getCellData(sheetName, "PropertyTypeOther_"+i, rowNum));
        				Thread.sleep(100);
        			}
        			else
        			{
        				
        			}
        			
        		} catch (Exception e)       
         		{
        			
         		}
                 	 
            }
            
            new Select(getDriver().findElement(By.id("property_"+k+"_BurglarAlarmDetection"))).selectByVisibleText(XLS.getCellData(sheetName, "BurglarAlarm_"+i, rowNum));
			new Select(getDriver().findElement(By.id("property_"+k+"_ExternalConstruction"))).selectByVisibleText(XLS.getCellData(sheetName, "ExternalConstruction_"+i, rowNum));
            String ExternalConstruction=XLS.getCellData(sheetName, "ExternalConstruction_"+i, rowNum);
			Thread.sleep(1000);

			 if (PropertyType.equalsIgnoreCase("Other"))
	            {
	           	 try
	        		{
	        			List<WebElement> ExternalConstructionOther = getDriver().findElements(By.id("OtherExternalConstructionMaterialText"));
	        			if (k==0) {
	        				ExternalConstructionOther.get(0).sendKeys(XLS.getCellData(sheetName, "ExternalConstructionOther_"+i, rowNum));
	        				Thread.sleep(100);
	        			} else if (k==1) {
	        				ExternalConstructionOther.get(1).sendKeys(XLS.getCellData(sheetName, "ExternalConstructionOther_"+i, rowNum));
	        				Thread.sleep(100);
	        			}
	        			else
	        			{
	        				
	        			}
	        			
	        		} catch (Exception e)       
	         		{
	        			
	         		}
	                 	 
	            }
			 
			 JavascriptExecutor jse = (JavascriptExecutor) getDriver();
			 jse.executeScript("window.scrollBy(0,100)", "");
			 Thread.sleep(2000);
			 
			 if (!XLS.getCellData(sheetName, "PropertyType_"+i, rowNum).equals("Unit")){
					getDriver().findElement(By.id("RiskLocation_"+k+"_BuildingSumInsured")).sendKeys(XLS.getCellData(sheetName, "BuildingSumInsured_"+i, rowNum));
					Thread.sleep(100); 
			 }
			 
			 getDriver().findElement(By.id("RiskLocation_"+k+"_ContentsSumInsured")).click();
			 Thread.sleep(500);
			 getDriver().findElement(By.id("RiskLocation_"+k+"_ContentsSumInsured")).clear();
			 Thread.sleep(3000);
			 getDriver().findElement(By.id("RiskLocation_"+k+"_ContentsSumInsured")).sendKeys(XLS.getCellData(sheetName, "ContentsSumInsured_"+i, rowNum));
			 Thread.sleep(1000);
			 
			String VACFlag=XLS.getCellData(sheetName, "VAC_Coverage_"+i, rowNum);
			 if (VACFlag.equalsIgnoreCase("Yes"))
	            {
	           	 try
	        		{
	        			List<WebElement> VAC_Coverage = getDriver().findElements(By.name("VacFlag_"+k));
	        			Thread.sleep(500);
	        			VAC_Coverage.get(0).click();
	        			Thread.sleep(500);
        			
	        		} catch (Exception e)       
	         		{
	        			
	         		}
	            }

			 String FloodCoverageFlag=XLS.getCellData(sheetName, "Flood_Coverage_"+i, rowNum);
			 if (FloodCoverageFlag.equalsIgnoreCase("Yes"))
	            {
	           	 try
	        		{
	        			List<WebElement> Flood_Coverage = getDriver().findElements(By.name("FloodCoverageFlag_"+k));
	        			Thread.sleep(500);
	        			Flood_Coverage.get(0).click();
	        			Thread.sleep(500);
        			
	        		} catch (Exception e)       
	         		{
	        			
	         		}
	            }
			 
			 String numberOfLosses=XLS.getCellData(sheetName, "NumberofLosses_"+i, rowNum);
			 System.out.println(numberOfLosses);
			 int losses=Integer.parseInt(numberOfLosses);
			 if (losses>=0)
	            {
	           	 try
	        		{
	        		new Select(getDriver().findElement(By.xpath("//select[contains(@data-bind,'NumberOfClaims')]"))).selectByVisibleText(Integer.toString(losses));
	        		} catch (Exception e)       
	         		{
	        			
	         		}
	            }
			 try
     		{
			 String Quote_Reference_Number=getDriver().findElement(By.xpath(".//*[@id='indicativeQuote']/div/div[10]/div[2]/span/span")).getText();
			 XLS.setCellData(sheetName, "Quote_Reference_Number", rowNum, Quote_Reference_Number);
     		}catch(Exception e)
			 {
     			
			 }
	 
			 
		}
	}    		
     		

@Step
	public void select_type_of_residence_only(String test_data) throws InterruptedException {
		
	
	
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(10000);
		System.out.println("control reached enter risk location details" );
	
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

			if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				System.out.println("test data --" + test_data);
				System.out.println("try to come out of loop "+rowNum);
				break innerloop;
			}
		}
		String no_of_loacations =XLS.getCellData(sheetName, "RiskLocation_Count", rowNum);
		
		if (no_of_loacations.equalsIgnoreCase("2"))
		{
			
			
			WebElement AddNextRiskLoaction=getDriver().findElement(By.xpath("//*[contains(text(),('Add Next Risk Location'))]"));
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", AddNextRiskLoaction);
	        Thread.sleep(2000);
			getDriver().findElement(By.xpath("//*[contains(text(),('Add Next Risk Location'))]")).click();
			
		}
		
		for (int i=0;i<Integer.parseInt(no_of_loacations);i++)
		{
			try
			{
				int locaiton_for_excel = i+1;
				new Select(getDriver().findElement(By.id("property_"+i+"_ResidenceType"))).selectByVisibleText(XLS.getCellData(sheetName, "TypeOfResidence_"+locaiton_for_excel, rowNum));
				
			}catch (Exception e)
			{
				Thread.sleep(1000);
			}
		}

		} 

	@Step
	public void change_pincode_risklocation(String pincode) throws InterruptedException {
		getDriver().findElement(By.id("addressEditor_0_PostCode")).clear();
	getDriver().findElement(By.id("addressEditor_0_PostCode")).sendKeys(pincode);
	System.out.println("pin code changed from risk location");
	}


@Step
public void enter_DeclinePostcodeRisklocation_details(String test_data) throws InterruptedException {

	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
	String sheetName = "IndicativeQuoteRiskLocation";
	Thread.sleep(1000);
	System.out.println("control reached enter risk location details" );

	
	int Row_Count = XLS.getRowCount(sheetName);
	int rowNum = 0;
	
	innerloop: 
		for (int row = 2; row <= Row_Count; row++) {

		if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
			rowNum = row;
			System.out.println("test data --" + test_data);
			System.out.println("try to come out of loop "+rowNum);
			break innerloop;
		}
	}
	
	System.out.println("came out of for loop");
	String Location_Count=XLS.getCellData(sheetName, "RiskLocation_Count", rowNum);
	System.out.println("Location_Count "+Location_Count);
    int count =0;
	count=Integer.valueOf(Location_Count);
	System.out.println("Value of count " + count);

	for (int i=1;i<=count;i++)
	{ 					System.out.println("insider for loop");

		int k=i-1;
		if (i==2)
		{
			System.out.println("Try to click add risk location");
			Thread.sleep(2000);	
			WebElement AddNextRiskLoaction=getDriver().findElement(By.xpath("//*[contains(text(),('Add Next Risk Location'))]"));
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", AddNextRiskLoaction);
            Thread.sleep(2000);
			getDriver().findElement(By.xpath("//*[contains(text(),('Add Next Risk Location'))]")).click();
            Thread.sleep(2000);

		}
					
		loop:
			for (int j=1;j<100;j++)
			{
			try
			{
				 System.out.println("Address selection " +k);
				 System.out.println("value of i " +i);
				  	getDriver().findElement(By.id("addressEditor_"+k+"_UnitNo")).sendKeys("");
					getDriver().findElement(By.id("addressEditor_"+k+"_StreetNo")).sendKeys("9");
					getDriver().findElement(By.id("addressEditor_"+k+"_StreetName")).sendKeys("Main St");
					getDriver().findElement(By.id("addressEditor_"+k+"_BuildingName")).sendKeys("");
					getDriver().findElement(By.id("addressEditor_"+k+"_City")).sendKeys("Augathella");
					getDriver().findElement(By.id("addressEditor_"+k+"_PostCode")).sendKeys("4477");
					new Select(getDriver().findElement(By.id("addressEditor_"+k+"_Country"))).selectByVisibleText("Australia");
					new Select(getDriver().findElement(By.id("addressEditor_"+k+"_State"))).selectByVisibleText("Queensland");
					
					
				break loop;
			}catch (Exception e)
			{
				Thread.sleep(1000);
			}
			}
		
		Thread.sleep(2000);
		new Select(getDriver().findElement(By.id("property_"+k+"_OwnershipType"))).selectByVisibleText(XLS.getCellData(sheetName, "PropertyOwnership_"+i, rowNum));
		new Select(getDriver().findElement(By.id("property_"+k+"_ResidenceType"))).selectByVisibleText(XLS.getCellData(sheetName, "TypeOfResidence_"+i, rowNum));
		Thread.sleep(1000);

         String TypeofResidence=XLS.getCellData(sheetName, "TypeOfResidence_"+i, rowNum);
         if (TypeofResidence.equalsIgnoreCase("Other"))
         {
        	 try
     		{
     			List<WebElement> TypeOfResidenceOther = getDriver().findElements(By.id("OtherResidenceTypeText"));
     			if (k==0) {
     				TypeOfResidenceOther.get(0).sendKeys(XLS.getCellData(sheetName, "TypeOfResidenceOther_"+i, rowNum));
     				Thread.sleep(100);
     			} else if (k==1) {
     				TypeOfResidenceOther.get(1).sendKeys(XLS.getCellData(sheetName, "TypeOfResidenceOther_"+i, rowNum));
     				Thread.sleep(100);
     			}
     			else
     			{
     				
     			}
     			
     		} catch (Exception e)       
      		{
     			
      		}
              	 
         }
         
 		try
		{
			List<WebElement> YearOfConstruction = getDriver().findElements(By.id("YearOfConstruction"));
			if (k==0) {
				YearOfConstruction.get(0).sendKeys(XLS.getCellData(sheetName, "YearOfConsturction_"+i, rowNum));
				Thread.sleep(100);
			} else if (k==1) {
				YearOfConstruction.get(1).sendKeys(XLS.getCellData(sheetName, "YearOfConsturction_"+i, rowNum));
				Thread.sleep(100);
			}
			else
			{
				
			}
			
		} catch (Exception e)       
 		{
			
 		}
 		
		new Select(getDriver().findElement(By.id("property_"+k+"_PropertyType"))).selectByVisibleText(XLS.getCellData(sheetName, "PropertyType_"+i, rowNum));
        String PropertyType=XLS.getCellData(sheetName, "PropertyType_"+i, rowNum);
		Thread.sleep(1000);

        if (PropertyType.equalsIgnoreCase("Other"))
        {
       	 try
    		{
    			List<WebElement> PropertyTypeOther = getDriver().findElements(By.id("OtherPropertyTypeText"));
    			if (k==0) {
    				PropertyTypeOther.get(0).sendKeys(XLS.getCellData(sheetName, "PropertyTypeOther_"+i, rowNum));
    				Thread.sleep(100);
    			} else if (k==1) {
    				PropertyTypeOther.get(1).sendKeys(XLS.getCellData(sheetName, "PropertyTypeOther_"+i, rowNum));
    				Thread.sleep(100);
    			}
    			else
    			{
    				
    			}
    			
    		} catch (Exception e)       
     		{
    			
     		}
             	 
        }
        
        new Select(getDriver().findElement(By.id("property_"+k+"_BurglarAlarmDetection"))).selectByVisibleText(XLS.getCellData(sheetName, "BurglarAlarm_"+i, rowNum));
		new Select(getDriver().findElement(By.id("property_"+k+"_ExternalConstruction"))).selectByVisibleText(XLS.getCellData(sheetName, "ExternalConstruction_"+i, rowNum));
        String ExternalConstruction=XLS.getCellData(sheetName, "ExternalConstruction_"+i, rowNum);
		Thread.sleep(1000);

		 if (PropertyType.equalsIgnoreCase("Other"))
            {
           	 try
        		{
        			List<WebElement> ExternalConstructionOther = getDriver().findElements(By.id("OtherExternalConstructionMaterialText"));
        			if (k==0) {
        				ExternalConstructionOther.get(0).sendKeys(XLS.getCellData(sheetName, "ExternalConstructionOther_"+i, rowNum));
        				Thread.sleep(100);
        			} else if (k==1) {
        				ExternalConstructionOther.get(1).sendKeys(XLS.getCellData(sheetName, "ExternalConstructionOther_"+i, rowNum));
        				Thread.sleep(100);
        			}
        			else
        			{
        				
        			}
        			
        		} catch (Exception e)       
         		{
        			
         		}
                 	 
            }
		 
		 JavascriptExecutor jse = (JavascriptExecutor) getDriver();
		 jse.executeScript("window.scrollBy(0,100)", "");
		 Thread.sleep(2000);
		 
		 if (!XLS.getCellData(sheetName, "PropertyType_"+i, rowNum).equals("Unit")){
				getDriver().findElement(By.id("RiskLocation_"+k+"_BuildingSumInsured")).sendKeys(XLS.getCellData(sheetName, "BuildingSumInsured_"+i, rowNum));
				Thread.sleep(100); 
		 }
		 
		 getDriver().findElement(By.id("RiskLocation_"+k+"_ContentsSumInsured")).click();
		 Thread.sleep(500);
		 getDriver().findElement(By.id("RiskLocation_"+k+"_ContentsSumInsured")).clear();
		 Thread.sleep(3000);
		 getDriver().findElement(By.id("RiskLocation_"+k+"_ContentsSumInsured")).sendKeys(XLS.getCellData(sheetName, "ContentsSumInsured_"+i, rowNum));
		 Thread.sleep(1000);
		 
		String VACFlag=XLS.getCellData(sheetName, "VAC_Coverage_"+i, rowNum);
		 if (VACFlag.equalsIgnoreCase("Yes"))
            {
           	 try
        		{
        			List<WebElement> VAC_Coverage = getDriver().findElements(By.name("VacFlag_"+k));
        			Thread.sleep(500);
        			VAC_Coverage.get(0).click();
        			Thread.sleep(500);
    			
        		} catch (Exception e)       
         		{
        			
         		}
            }

		 String FloodCoverageFlag=XLS.getCellData(sheetName, "Flood_Coverage_"+i, rowNum);
		 if (FloodCoverageFlag.equalsIgnoreCase("Yes"))
            {
           	 try
        		{
        			List<WebElement> Flood_Coverage = getDriver().findElements(By.name("FloodCoverageFlag_"+k));
        			Thread.sleep(500);
        			Flood_Coverage.get(0).click();
        			Thread.sleep(500);
    			
        		} catch (Exception e)       
         		{
        			
         		}
            }
		 
		 String numberOfLosses=XLS.getCellData(sheetName, "NumberofLosses_"+i, rowNum);
		 System.out.println(numberOfLosses);
		 int losses=Integer.parseInt(numberOfLosses);
		 if (losses>=0)
            {
           	 try
        		{
        		new Select(getDriver().findElement(By.xpath("//select[contains(@data-bind,'NumberOfClaims')]"))).selectByVisibleText(Integer.toString(losses));
        		} catch (Exception e)       
         		{
        			
         		}
            }
		 try
 		{
		 String Quote_Reference_Number=getDriver().findElement(By.xpath(".//*[@id='indicativeQuote']/div/div[10]/div[2]/span/span")).getText();
		 XLS.setCellData(sheetName, "Quote_Reference_Number", rowNum, Quote_Reference_Number);
 		}catch(Exception e)
		 {
 			
		 } 
	}
}
	
@Step
public void enter_ReferPostcoderisklocation_details(String test_data) throws InterruptedException {
	
		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "IndicativeQuoteRiskLocation";
		Thread.sleep(1000);
		System.out.println("control reached enter risk location details" );
	
		
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		
		innerloop: 
			for (int row = 2; row <= Row_Count; row++) {

			if (test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				rowNum = row;
				System.out.println("test data --" + test_data);
				System.out.println("try to come out of loop "+rowNum);
				break innerloop;
			}
		}
		
		System.out.println("came out of for loop");
		String Location_Count=XLS.getCellData(sheetName, "RiskLocation_Count", rowNum);
		System.out.println("Location_Count "+Location_Count);
        int count =0;
		count=Integer.valueOf(Location_Count);
		System.out.println("Value of count " + count);

		for (int i=1;i<=count;i++)
		{ 					System.out.println("insider for loop");

			int k=i-1;
			if (i==2)
			{
				System.out.println("Try to click add risk location");
				Thread.sleep(2000);	
				WebElement AddNextRiskLoaction=getDriver().findElement(By.xpath("//*[contains(text(),('Add Next Risk Location'))]"));
				((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", AddNextRiskLoaction);
	            Thread.sleep(2000);
				getDriver().findElement(By.xpath("//*[contains(text(),('Add Next Risk Location'))]")).click();
	            Thread.sleep(2000);

			}
						
			loop:
				for (int j=1;j<100;j++)
				{
				try
				{
					 System.out.println("Address selection " +k);
					 System.out.println("value of i " +i);
					  	getDriver().findElement(By.id("addressEditor_"+k+"_UnitNo")).sendKeys("");
						getDriver().findElement(By.id("addressEditor_"+k+"_StreetNo")).sendKeys("48");
						getDriver().findElement(By.id("addressEditor_"+k+"_StreetName")).sendKeys("Boundary St");
						getDriver().findElement(By.id("addressEditor_"+k+"_BuildingName")).sendKeys("");
						getDriver().findElement(By.id("addressEditor_"+k+"_City")).sendKeys("Spring Hill");
						getDriver().findElement(By.id("addressEditor_"+k+"_PostCode")).sendKeys("4004");
						new Select(getDriver().findElement(By.id("addressEditor_"+k+"_Country"))).selectByVisibleText("Australia");
						new Select(getDriver().findElement(By.id("addressEditor_"+k+"_State"))).selectByVisibleText("Queensland");
						
						
					break loop;
				}catch (Exception e)
				{
					Thread.sleep(1000);
				}
				}
			
			Thread.sleep(2000);
			new Select(getDriver().findElement(By.id("property_"+k+"_OwnershipType"))).selectByVisibleText(XLS.getCellData(sheetName, "PropertyOwnership_"+i, rowNum));
			new Select(getDriver().findElement(By.id("property_"+k+"_ResidenceType"))).selectByVisibleText(XLS.getCellData(sheetName, "TypeOfResidence_"+i, rowNum));
			Thread.sleep(1000);

             String TypeofResidence=XLS.getCellData(sheetName, "TypeOfResidence_"+i, rowNum);
             if (TypeofResidence.equalsIgnoreCase("Other"))
             {
            	 try
         		{
         			List<WebElement> TypeOfResidenceOther = getDriver().findElements(By.id("OtherResidenceTypeText"));
         			if (k==0) {
         				TypeOfResidenceOther.get(0).sendKeys(XLS.getCellData(sheetName, "TypeOfResidenceOther_"+i, rowNum));
         				Thread.sleep(100);
         			} else if (k==1) {
         				TypeOfResidenceOther.get(1).sendKeys(XLS.getCellData(sheetName, "TypeOfResidenceOther_"+i, rowNum));
         				Thread.sleep(100);
         			}
         			else
         			{
         				
         			}
         			
         		} catch (Exception e)       
          		{
         			
          		}
                  	 
             }
             
     		try
    		{
    			List<WebElement> YearOfConstruction = getDriver().findElements(By.id("YearOfConstruction"));
    			if (k==0) {
    				YearOfConstruction.get(0).sendKeys(XLS.getCellData(sheetName, "YearOfConsturction_"+i, rowNum));
    				Thread.sleep(100);
    			} else if (k==1) {
    				YearOfConstruction.get(1).sendKeys(XLS.getCellData(sheetName, "YearOfConsturction_"+i, rowNum));
    				Thread.sleep(100);
    			}
    			else
    			{
    				
    			}
    			
    		} catch (Exception e)       
     		{
    			
     		}
     		
			new Select(getDriver().findElement(By.id("property_"+k+"_PropertyType"))).selectByVisibleText(XLS.getCellData(sheetName, "PropertyType_"+i, rowNum));
            String PropertyType=XLS.getCellData(sheetName, "PropertyType_"+i, rowNum);
			Thread.sleep(1000);

            if (PropertyType.equalsIgnoreCase("Other"))
            {
           	 try
        		{
        			List<WebElement> PropertyTypeOther = getDriver().findElements(By.id("OtherPropertyTypeText"));
        			if (k==0) {
        				PropertyTypeOther.get(0).sendKeys(XLS.getCellData(sheetName, "PropertyTypeOther_"+i, rowNum));
        				Thread.sleep(100);
        			} else if (k==1) {
        				PropertyTypeOther.get(1).sendKeys(XLS.getCellData(sheetName, "PropertyTypeOther_"+i, rowNum));
        				Thread.sleep(100);
        			}
        			else
        			{
        				
        			}
        			
        		} catch (Exception e)       
         		{
        			
         		}
                 	 
            }
            
            new Select(getDriver().findElement(By.id("property_"+k+"_BurglarAlarmDetection"))).selectByVisibleText(XLS.getCellData(sheetName, "BurglarAlarm_"+i, rowNum));
			new Select(getDriver().findElement(By.id("property_"+k+"_ExternalConstruction"))).selectByVisibleText(XLS.getCellData(sheetName, "ExternalConstruction_"+i, rowNum));
            String ExternalConstruction=XLS.getCellData(sheetName, "ExternalConstruction_"+i, rowNum);
			Thread.sleep(1000);

			 if (PropertyType.equalsIgnoreCase("Other"))
	            {
	           	 try
	        		{
	        			List<WebElement> ExternalConstructionOther = getDriver().findElements(By.id("OtherExternalConstructionMaterialText"));
	        			if (k==0) {
	        				ExternalConstructionOther.get(0).sendKeys(XLS.getCellData(sheetName, "ExternalConstructionOther_"+i, rowNum));
	        				Thread.sleep(100);
	        			} else if (k==1) {
	        				ExternalConstructionOther.get(1).sendKeys(XLS.getCellData(sheetName, "ExternalConstructionOther_"+i, rowNum));
	        				Thread.sleep(100);
	        			}
	        			else
	        			{
	        				
	        			}
	        			
	        		} catch (Exception e)       
	         		{
	        			
	         		}
	                 	 
	            }
			 
			 JavascriptExecutor jse = (JavascriptExecutor) getDriver();
			 jse.executeScript("window.scrollBy(0,100)", "");
			 Thread.sleep(2000);
			 
			 if (!XLS.getCellData(sheetName, "PropertyType_"+i, rowNum).equals("Unit")){
					getDriver().findElement(By.id("RiskLocation_"+k+"_BuildingSumInsured")).sendKeys(XLS.getCellData(sheetName, "BuildingSumInsured_"+i, rowNum));
					Thread.sleep(100); 
			 }
			 
			 getDriver().findElement(By.id("RiskLocation_"+k+"_ContentsSumInsured")).click();
			 Thread.sleep(500);
			 getDriver().findElement(By.id("RiskLocation_"+k+"_ContentsSumInsured")).clear();
			 Thread.sleep(3000);
			 getDriver().findElement(By.id("RiskLocation_"+k+"_ContentsSumInsured")).sendKeys(XLS.getCellData(sheetName, "ContentsSumInsured_"+i, rowNum));
			 Thread.sleep(1000);
			 
			String VACFlag=XLS.getCellData(sheetName, "VAC_Coverage_"+i, rowNum);
			 if (VACFlag.equalsIgnoreCase("Yes"))
	            {
	           	 try
	        		{
	        			List<WebElement> VAC_Coverage = getDriver().findElements(By.name("VacFlag_"+k));
	        			Thread.sleep(500);
	        			VAC_Coverage.get(0).click();
	        			Thread.sleep(500);
        			
	        		} catch (Exception e)       
	         		{
	        			
	         		}
	            }

			 String FloodCoverageFlag=XLS.getCellData(sheetName, "Flood_Coverage_"+i, rowNum);
			 if (FloodCoverageFlag.equalsIgnoreCase("Yes"))
	            {
	           	 try
	        		{
	        			List<WebElement> Flood_Coverage = getDriver().findElements(By.name("FloodCoverageFlag_"+k));
	        			Thread.sleep(500);
	        			Flood_Coverage.get(0).click();
	        			Thread.sleep(500);
        			
	        		} catch (Exception e)       
	         		{
	        			
	         		}
	            }
			 
			 String numberOfLosses=XLS.getCellData(sheetName, "NumberofLosses_"+i, rowNum);
			 System.out.println(numberOfLosses);
			 int losses=Integer.parseInt(numberOfLosses);
			 if (losses>=0)
	            {
	           	 try
	        		{
	        		new Select(getDriver().findElement(By.xpath("//select[contains(@data-bind,'NumberOfClaims')]"))).selectByVisibleText(Integer.toString(losses));
	        		} catch (Exception e)       
	         		{
	        			
	         		}
	            }
			 try
     		{
			 String Quote_Reference_Number=getDriver().findElement(By.xpath(".//*[@id='indicativeQuote']/div/div[10]/div[2]/span/span")).getText();
			 XLS.setCellData(sheetName, "Quote_Reference_Number", rowNum, Quote_Reference_Number);
     		}catch(Exception e)
			 {
     			
			 }			 
		}
	}    		     		
}

 		

     
