package au.com.chubb.evolutionshowcase.pages.objectModel.policyAdmin;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.pages.PageObject;

public class policyAdminWelcomePage extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public policyAdminWelcomePage() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyAdmin.properties");
		OR.load(fo);

	}

	public void select_search_type(String searchType) throws InterruptedException {

		getDriver().findElement(By.xpath(OR.getProperty("searchType_Xpath"))).click();
		Thread.sleep(1000);
		loop: for (int i = 1; i <= 5; i++) {
		   
			String selector_APP = getDriver().findElement(By.xpath(".//*[@id='globalSearch']/div/div[1]/ul/li[" + i + "]/a")).getText();
			if (selector_APP.equalsIgnoreCase(searchType)) {
				getDriver().findElement(By.xpath(".//*[@id='globalSearch']/div/div[1]/ul/li[" + i + "]/a")).click();
				break loop;
			}

		}

	}

	public void enter_search_value(String searchValue) throws InterruptedException {

		getDriver().findElement(By.xpath(OR.getProperty("searchInput_Xpath"))).sendKeys(searchValue);
		Thread.sleep(7000);

	}

	public void click_search() throws InterruptedException {

		getDriver().findElement(By.id(OR.getProperty("searchButton_Id"))).click();
		Thread.sleep(2000);

	}

}