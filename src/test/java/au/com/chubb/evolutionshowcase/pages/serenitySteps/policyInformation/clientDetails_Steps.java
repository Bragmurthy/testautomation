package au.com.chubb.evolutionshowcase.pages.serenitySteps.policyInformation;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class clientDetails_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	defaultSection_nameDate_Steps defaultSection_nameDate;
	DynamicSeleniumFunctions functions;
	public static Properties OR = null;
	String sheetName = "ClientDetails";
	String ClientDetailsSheet="QuoteData";
	XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");

	public clientDetails_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);

	}

	@Step
	public void default_clientname() throws InterruptedException, IOException {
		String firstName=XLS.getCellData(ClientDetailsSheet, "Firstname", 3);
		//XLS.setCellData(sheetName, "Middlename", 3,"MiddleEbix");
		String lastName=XLS.getCellData(ClientDetailsSheet, "Lastname", 3);
		String fullName=firstName +" "+lastName;
		String fullName_APP=getDriver().findElement(By.xpath("//span[contains(@data-bind,'Client.Item.FullName')]")).getText();
		
		if (fullName_APP.equalsIgnoreCase(fullName))
		{
			Assert.assertTrue("Ful Name successful displayed in indicative quote page" +fullName_APP+" ", true);
		} else {
			Assert.assertTrue("Full Name doesnt match "+fullName_APP+"", false);

		}
		functions.verifyFieldNonEditable("ClientName");
	}
@Step
	public void default_clientDetails(String custID) throws InterruptedException {
	
	String clientDetails[]=getClientDetails(custID);
	//String expectedDateOfBirth=clientDetails[2];
	String expectedEmail=clientDetails[3];
	//String expectedPhoneNumber=clientDetails[4];
	//String dateOfBirth_APP=getDriver().findElement(By.xpath("//span[contains(@data-bind,'Client.Item.FullName')]")).getText();
	String email_APP=getDriver().findElement(By.xpath("//span[contains(@data-bind,'Client.Item.FullName')]")).getText();
	//String phoneNumber_APP=getDriver().findElement(By.xpath("//span[contains(@data-bind,'Client.Item.FullName')]")).getText();
	//if (expectedDateOfBirth.equals(dateOfBirth_APP) &&email_APP.equals(expectedEmail) &&expectedPhoneNumber.equals(phoneNumber_APP))
	if (expectedEmail.equals(email_APP))
	{
		Assert.assertTrue("Client Date of Birth,Email,Phone Number dispalyed as expected "+expectedEmail+"", true);
	} else {
		Assert.assertTrue("Client Date of Birth,Email,Phone Number not dispalyed as expected "+email_APP+"", false);
 }
	}

private String[] getClientDetails(String fullname) throws InterruptedException {
	
	String clientDetails[] = new String[10];
	getDriver().findElement(By.xpath("//a[contains(text(),'Find customer')]")).click();
	Thread.sleep(5000);
	getDriver().findElement(By.xpath("//input[@name='txtEntityId']")).sendKeys(fullname);
	getDriver().findElement(By.xpath("//input[@name='btnSearch']")).click();
	Thread.sleep(5000);
	getDriver().findElement(By.xpath("//a[contains(text(),'View')]")).click();
	Thread.sleep(5000);
	getDriver().findElement(By.xpath("//input[@value='Full Customer Details']")).click();
	Thread.sleep(5000);
	
	clientDetails[0]=getDriver().findElement(By.xpath("//td[input[@name='txtFirstName']]")).getText();
	clientDetails[1]=getDriver().findElement(By.xpath("//td[input[@name='txtLastName']]")).getText();
	clientDetails[2]=getDriver().findElement(By.xpath("//td[input[@name='txtBirthday']]")).getText();
	clientDetails[3]=getDriver().findElement(By.xpath("//td[input[@name='txtEmail']]")).getText();
	clientDetails[4]=getDriver().findElement(By.xpath("//td[input[@name='txtMobile']]")).getText();
	return clientDetails;
}

public void verifyClientDetailMandatoryFields(String dateofBirth, String retired, String industry,String occupation, String employer){ }

@Step
public void editClientDetails() throws InterruptedException {
/*	String lastName="FirstName"+randomAlpha(3);
	getDriver().findElement(By.xpath("//input[@id='individualLastName']")).clear();
	getDriver().findElement(By.xpath("//input[@id='individualLastName']")).sendKeys(lastName);
    XLS.setCellData(sheetName, "LastName", 2,lastName);
	
	String middleName="Middle"+randomAlpha(3);
	getDriver().findElement(By.xpath("//input[@id='individualMiddleName']")).clear();
	getDriver().findElement(By.xpath("//input[@id='individualMiddleName']")).sendKeys(middleName);*/
	//XLS.setCellData(sheetName, "LastName", 2,lastName);
	//new Select(getDriver().findElement(By.xpath("//select[@id='individualSalutationId']"))).selectByIndex(2);

	new Select(getDriver().findElement(By.xpath("//select[@id='individualIndustryId']"))).selectByIndex(2);
	XLS.setCellData(sheetName, "Industry", 2,new Select(getDriver().findElement(By.xpath("//select[@id='individualIndustryId']"))).getFirstSelectedOption().getText());
	
	new Select(getDriver().findElement(By.xpath("//select[@id='individualOccupationId']"))).selectByIndex(2);
	XLS.setCellData(sheetName, "Occupation", 2,new Select(getDriver().findElement(By.xpath("//select[@id='individualIndustryId']"))).getFirstSelectedOption().getText());

	String dateOfBirth="12/11/1985";
	getDriver().findElement(By.xpath("//input[@id='individualDateOfBirth']")).clear();
	getDriver().findElement(By.xpath("//input[@id='individualDateOfBirth']")).sendKeys(dateOfBirth);
	XLS.setCellData(sheetName, "dateOfBirth", 2,dateOfBirth);
	//input[@id='individualDateOfBirth']

	getDriver().switchTo().activeElement().sendKeys(Keys.TAB);
	String employer="employer"+randomAlpha(3);
	
	getDriver().findElement(By.xpath("//input[@id='individualEmployer']")).clear();
	getDriver().findElement(By.xpath("//input[@id='individualEmployer']")).sendKeys(employer);
	XLS.setCellData(sheetName, "Employer", 2,employer);
	
	//String phoneNumber=randomAlphaNumeric(10);
	//getDriver().findElement(By.xpath("//input[@id='individualAddressTelephoneNumber']")).sendKeys(phoneNumber);
	//XLS.setCellData(sheetName, "TelephoneNumber", 2,phoneNumber);
	
	//getDriver().findElement(By.id("addressEditor_Ind_Search")).sendKeys("36 Hobbs Street, Seddon VIC, Australia");
	getDriver().findElement(By.id("addressEditor_Ind_StreetNo")).clear();
	getDriver().findElement(By.id("addressEditor_Ind_StreetNo")).sendKeys("36");
	
	getDriver().findElement(By.id("addressEditor_Ind_StreetName")).clear();
	getDriver().findElement(By.id("addressEditor_Ind_StreetName")).sendKeys("Briggs Street");
	
	getDriver().findElement(By.id("addressEditor_Ind_City")).clear();
	getDriver().findElement(By.id("addressEditor_Ind_City")).sendKeys("Mount Waverly");
	
	getDriver().findElement(By.id("addressEditor_Ind_PostCode")).clear();
	getDriver().findElement(By.id("addressEditor_Ind_PostCode")).sendKeys("3142");
	new Select(getDriver().findElement(By.id("addressEditor_Ind_Country"))).selectByVisibleText("Australia");
	new Select(getDriver().findElement(By.id("addressEditor_Ind_State"))).selectByVisibleText("Victoria");
	
//	getDriver().findElement(By.id("addressEditor_Ind_Search")).sendKeys(Keys.DOWN);
//	Thread.sleep(4000);
//	getDriver().findElement(By.id("addressEditor_Ind_Search")).sendKeys(Keys.TAB);
//	Thread.sleep(4000);
	getDriver().findElement(By.xpath("//*[@id='individualAddressTelephoneNumber']")).clear();
	getDriver().findElement(By.xpath("//*[@id='individualAddressTelephoneNumber']")).sendKeys("12345678901");
	Thread.sleep(2000);
	getDriver().findElement(By.xpath("//*[@id='individualAddressMobileNumber']")).clear();
	getDriver().findElement(By.xpath("//*[@id='individualAddressMobileNumber']")).sendKeys("98766356361");
	Thread.sleep(2000);
//	getDriver().findElement(By.xpath("//*[@id='individualAddressEmail']")).clear();
//	getDriver().findElement(By.xpath("//*[@id='individualAddressEmail']")).sendKeys("test@testssmail.com");
//	Thread.sleep(10000);
}


public static String randomNumeric(int count) {
final String ALPHA_NUMERIC_STRING = "0123456789";
StringBuilder builder = new StringBuilder();
while (count-- != 0) {
int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
builder.append(ALPHA_NUMERIC_STRING.charAt(character));
}
return builder.toString();
}
public static String randomAlpha(int count) {
final String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstuvwxyz";
StringBuilder builder = new StringBuilder();
while (count-- != 0) {
int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
builder.append(ALPHA_NUMERIC_STRING.charAt(character));
}
return builder.toString();
}
@Step
public void verifyClientDetails() {
	
	//String firstName=XLS.getCellData(sheetName, "FirstName", 2);
	//String lastName=XLS.getCellData(sheetName, "LastName", 2);
	String Industry=XLS.getCellData(sheetName, "Industry", 2);
	//sString telephoneNumber=XLS.getCellData(sheetName, "Address", 2);
	String occupation=XLS.getCellData(sheetName, "Occupation", 2);
	String dateofBirth=XLS.getCellData(sheetName, "dateOfBirth", 2);
	String employer=XLS.getCellData(sheetName, "Employer", 2);
	//String clientName=firstName+ " "+lastName;
	
	//String clientNameName_APP=getDriver().findElement(By.xpath("//span[contains(@data-bind,'Item.FullName')]")).getText();
	String Industry_APP=getDriver().findElement(By.xpath("//span[contains(@data-bind,'IndustryText')]")).getText();
	//String telephoneNumber_APP=getDriver().findElement(By.xpath("//span[contains(@data-bind,'Contact.PhoneNumber')]")).getText();
	String occupation_App=getDriver().findElement(By.xpath("//span[contains(@data-bind,'OccupationText')]")).getText();
	String dateofBirth_App=getDriver().findElement(By.xpath("//span[contains(@data-bind,'DateOfBirth')]")).getText();
	String employer_App=getDriver().findElement(By.xpath("//span[contains(@data-bind,'Item.EmployerName')]")).getText();
	
	if(Industry.equals(Industry_APP)
            && occupation.equals(occupation_App)
            &&dateofBirth.equals(dateofBirth_App)
            &&employer.equals(employer_App))
		Assert.assertTrue("Client Date of Birth,Industry,Employer,Occupation are edited succesfully", true);
	
	else
		Assert.assertTrue("Client Date of Birth,Industry,Employer,Occupation not edited expected values are "+Industry+","+occupation+", "+dateofBirth+" ,"+employer_App+" ", true);
		
	}

@Step
public void verifyClientDetailsInUWView() {
	
	//String firstName=XLS.getCellData(sheetName, "FirstName", 2);
	//String lastName=XLS.getCellData(sheetName, "LastName", 2);
	String Industry=XLS.getCellData(sheetName, "Industry", 2);
	//sString telephoneNumber=XLS.getCellData(sheetName, "Address", 2);
	String occupation=XLS.getCellData(sheetName, "Occupation", 2);
	String dateofBirth=XLS.getCellData(sheetName, "dateOfBirth", 2);
	String employer=XLS.getCellData(sheetName, "Employer", 2);
	//String clientName=firstName+ " "+lastName;
	
	//String clientNameName_APP=getDriver().findElement(By.xpath("//span[contains(@data-bind,'Item.FullName')]")).getText();
	String Industry_APP=getDriver().findElement(By.xpath("//span[contains(@data-bind,'IndustryText')]")).getText();
	//String telephoneNumber_APP=getDriver().findElement(By.xpath("//span[contains(@data-bind,'Contact.PhoneNumber')]")).getText();
	String occupation_App=getDriver().findElement(By.xpath("//span[contains(@data-bind,'OccupationText')]")).getText();
	String dateofBirth_App=getDriver().findElement(By.xpath("//span[contains(@data-bind,'DateOfBirth')]")).getText();
	String employer_App=getDriver().findElement(By.xpath("//span[contains(@data-bind,'Item.EmployerName')]")).getText();
	
	if(Industry.equals(Industry_APP)
            && occupation.equals(occupation_App)
            &&dateofBirth.equals(dateofBirth_App)
            &&employer.equals(employer_App))
		Assert.assertTrue("Client Date of Birth,Industry,Employer,Occupation are edited succesfully", true);
	
	else
		Assert.assertTrue("Client Date of Birth,Industry,Employer,Occupation not edited expected values are "+Industry+","+occupation+", "+dateofBirth+" ,"+employer_App+" ", false);
		
	}

@Step
public void clickOnUpdateClient() throws InterruptedException {
	Thread.sleep(5000);
	WebElement elmnt =getDriver().findElement(By.xpath("//a[contains(text(),'Update Client')]"));  
	Actions builder = new Actions(getDriver());   
	builder.moveToElement(elmnt, +0, +0).click().build().perform();
	WebDriverWait wait=new WebDriverWait(getDriver(), 300);
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='individualFirstName']")));
	Thread.sleep(3000);
}

@Step
public void verify_Field_Required() {
	
	if(getDriver().findElement(By.xpath("//span[contains(text(),'This field is required.')]")).isDisplayed())
		Assert.assertTrue("Mandatory fields error message displayed", true);
	
	else
		Assert.assertTrue("Mandatory fields error message displayed", false);
}

@Step
public void verifyUpdateClient() {
	
	if(getDriver().findElement(By.xpath("//a[contains(text(),'Update Client')]")).isDisplayed())
		Assert.assertTrue("Update client button displayed", true);
	
	else
		Assert.assertTrue("Update client button not displayed", false);
}

@Step
public void verify_field_must_displayed(String text) {
	
	if(getDriver().findElement(By.xpath("//*[contains(text(),'"+text+"')]")).isDisplayed())
		Assert.assertTrue("Element is displayed", true);
	
	else
		Assert.assertTrue("Element is not displayed", false);
}

@Step
public void verify_field_must_displayed_full_text(String text) {
	
	if(getDriver().findElement(By.xpath("//*[text()='"+text+"']")).isDisplayed())
		Assert.assertTrue("Element is displayed", true);
	
	else
		Assert.assertTrue("Element is not displayed", false);
}


@Step
public void verify_broker_contact_message() {
	String actual = (getDriver().findElement(By.xpath("//*[@id='brokerContactRequired-error']")).getText());
	if(actual.contains("Please select a broker contact"))
		Assert.assertTrue("Element is displayed", true);
	
	else
		Assert.assertTrue("Element is not displayed", false);
}

@Step
public void verify_field_must_mandatory(String text) {
	
	String actual = (getDriver().findElement(By.xpath("//*[contains(text(),'"+text+"')]/span")).getText());
	if(actual.contains("*"))
		Assert.assertTrue("Element is mandatory field", true);
	
	else
		Assert.assertTrue("Element is not mandatory field", false);
}

@Step
public void verify_field_must_optional(String text) {
	try{
		String actual = (getDriver().findElement(By.xpath("//*[contains(text(),'"+text+"')]/span")).getText());
		if(actual.contains("*"))
			Assert.assertFalse("Element is not optional field", true);
		
		else
			Assert.assertFalse("Element is optional field", false);
	}
	catch(Exception e)
	{
		System.out.println("Element is optional");
	}
		
}

@Step
public void verify_field_must_readonly(String text) {
	
	if(getDriver().findElement(By.xpath("//*[@id='"+text+"']")).isEnabled())
		Assert.assertFalse("Element is read only", true);
	
	else
		Assert.assertFalse("Element is not read only", false);
}

@Step
public void verify_class_field_must_readonly(String text) {
	
	if(getDriver().findElement(By.xpath("//*[@class='"+text+"']")).isEnabled())
		Assert.assertFalse("Element is not read only", true);
	
	else
		Assert.assertFalse("Element is read only", false);
}

@Step
public void verify_class_field_must_not_readonly(String text) {
	
	if(getDriver().findElement(By.xpath("//*[@class='"+text+"']")).isEnabled())
		Assert.assertTrue("Element is not read only", true);
	
	else
		Assert.assertTrue("Element is read only", false);
}


@Step
public void verify_field_must_not_readonly(String text) {
	if(getDriver().findElement(By.xpath("//*[text()='"+text+"']")).isEnabled())
//	if(getDriver().findElement(By.xpath("//*[contains(text(),'"+text+"')]")).isEnabled())
		Assert.assertTrue("Element is not read only", true);
	
	else
		Assert.assertTrue("Element is read only", false);
}


@Step
public void verifydefaultValuesFromSunrise() throws IOException {
	String email=XLS.getCellData(ClientDetailsSheet, "Email", 3);
	String email_App=getDriver().findElement(By.xpath("//td[span[contains(@data-bind,'Contact.Email')]]")).getText();
	String retired_App=getDriver().findElement(By.xpath("//td[span[contains(@data-bind,'Item.Retired')]]")).getText();
	if ( retired_App.equalsIgnoreCase("No") &&email_App.equalsIgnoreCase(email)) 
		{
		Assert.assertTrue("Retired "+retired_App + "Email" +email_App+" displayed with default values from sunrise", true);
	} else {
		Assert.assertTrue("Retired "+retired_App + "Email" +email_App+" not displayed with default values from sunrise", false);
		functions.verifyFieldNonEditable("Email");
		functions.verifyFieldNonEditable("Retired");
}

}


@Step
public void verifyUpdateClientDetails(DataTable arg1) throws InterruptedException {

	Thread.sleep(5000);
	List<List<String>> data = arg1.raw();
	String StreetNo_APP=getDriver().findElement(By.xpath("//input[@name='addressEditor_Ind_StreetNo']")).getAttribute("value");
	String StreetName_APP=getDriver().findElement(By.xpath("//input[@name='addressEditor_Ind_StreetName']")).getAttribute("value");
	String City_APP=getDriver().findElement(By.xpath("//input[@name='addressEditor_Ind_City']")).getAttribute("value");
	String PostCode_APP=getDriver().findElement(By.xpath("//input[@name='addressEditor_Ind_PostCode']")).getAttribute("value");
	String Country_APP=new Select(getDriver().findElement(By.xpath("//select[@name='addressEditor_Ind_Country']"))).getFirstSelectedOption().getText();
	String State_APP=new Select(getDriver().findElement(By.xpath("//select[@name='addressEditor_Ind_State']"))).getFirstSelectedOption().getText();

	
    System.out.println("UnitNo_INP -- " +data.get(1).get(1));

	
	System.out.println("StreetNo_APP-- " +StreetNo_APP);
	System.out.println("StreetNo_INP -- " +data.get(2).get(1));

	System.out.println("StreetName_APP-- " +StreetName_APP);
	System.out.println("StreetName_INP -- " +data.get(3).get(1));

	System.out.println("BuildingName_INP -- " +data.get(4).get(1));

	System.out.println("City_APP -- " +City_APP);
	System.out.println("City_INP -- " +data.get(5).get(1));

	System.out.println("PostCode_APP -- " +PostCode_APP);
	System.out.println("PostCode_INP -- " +data.get(6).get(1));

	System.out.println("Country_APP -- " +Country_APP);
	System.out.println("Country_INP -- " +data.get(7).get(1));

	System.out.println("State_APP -- " +State_APP);
	System.out.println("State_INP -- " +data.get(8).get(1));

			
	if (StreetNo_APP.equals(data.get(2).get(1)) &&
					StreetName_APP.equals(data.get(3).get(1)) &&
									City_APP.equals(data.get(5).get(1)) &&
											PostCode_APP.equals(data.get(6).get(1)) &&
													Country_APP.equals(data.get(7).get(1)) &&
															State_APP.equals(data.get(8).get(1)))
															{
		                                                            Assert.assertTrue("Updated client details displayed as expected", true);
															} else {
																Assert.assertTrue("Updated client details displayed as expected", false);

															}
															}

}