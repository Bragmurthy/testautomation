package au.com.chubb.evolutionshowcase.pages.serenitySteps.interestedParties;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

import java.util.regex.*;

@RunWith(SerenityRunner.class)
public class interestedParties_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public interestedParties_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);

	}

	public void add_interested_party(String interested_party_count, String Location_Identifier, DataTable arg1)
			throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		int Location_Number = Integer.parseInt(Location_Identifier);
		int interestedparty_count = Integer.parseInt(interested_party_count);
		Thread.sleep(1000);
		if (interestedparty_count > 0)
		{
			getDriver().findElement(By.id("p_"+Location_Number)).click();
			Thread.sleep(3000);

			for (int i = 1; i <= interestedparty_count; i++) {

				String sheetName = "InterestedParties";
				int Row_Count = XLS.getRowCount(sheetName);
				int rowNum = 0;

				List<List<String>> data = arg1.raw();

				innerloop: for (int row = 2; row <= Row_Count; row++) {
					String vac_test_data = data.get(i).toString().replace("[", "").replace("]", "").trim();

					System.out.println("Interested Parties test data --" + vac_test_data);
					if (vac_test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
						rowNum = row;
						break innerloop;
					}
				}
				System.out.println("Value of row number " + rowNum);

				try {
					// Add Interested Party
					WebElement Add_Interested_Party = getDriver()
							.findElement(By.xpath(".//*[@id='subSideNavigationHeader']/div/span/button"));
					((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", Add_Interested_Party);

					Thread.sleep(2000);
					System.out.println("Clicked - Add Interested Party Button");
				} catch (Exception e) {
				}

				try {

					Thread.sleep(500);

					String Role = XLS.getCellData(sheetName, "Role", rowNum);
					getDriver().switchTo().defaultContent();
					// Select Role***********************************//
					Thread.sleep(500);

					if (Role.equalsIgnoreCase("Interested Party"))

					{

						System.out.println("Role -- -" + Role);

						Thread.sleep(500);

						// Enter Interested Party Name
						// ************************//
						getDriver().findElement(By.id("addInterestedParty_Name"))
						.sendKeys(XLS.getCellData(sheetName, "InterestedPartyName", rowNum));

						Thread.sleep(500);

						// Enter Description of Interest
						// ************************//
						getDriver().findElement(By.id("addInterestedParty_InterestDesc"))
						.sendKeys(XLS.getCellData(sheetName, "DescriptionOfInterest", rowNum));

						Thread.sleep(500);

						getDriver().findElement(By.id("addInterestedParty_InterestDesc")).sendKeys(Keys.TAB);
						Thread.sleep(1000);

					} else if (Role.equalsIgnoreCase("Mortgagee")) {

						getDriver().switchTo().activeElement().sendKeys(Keys.DOWN);
						Thread.sleep(2000);

						new Select(getDriver().findElement(By.id("addInterestedParty_FinancialInstitution")))
						.selectByVisibleText(XLS.getCellData(sheetName, "FinancialInstitutionName", rowNum));
						Thread.sleep(500);

						new Select(getDriver().findElement(By.id("addInterestedParty_Rank")))
						.selectByVisibleText(XLS.getCellData(sheetName, "Rank", rowNum));
						Thread.sleep(500);

						getDriver().findElement(By.id("addInterestedParty_Rank")).sendKeys(Keys.TAB);
						Thread.sleep(1000);

					}

					WebElement Cancel_Button = getDriver().switchTo().activeElement();
					Thread.sleep(1000);
					Cancel_Button.sendKeys(Keys.TAB);
					WebElement AddParty_Button = getDriver().switchTo().activeElement();
					Thread.sleep(1000);
					AddParty_Button.click();
					Thread.sleep(3000);

				} catch (Exception e) {

				}

			}

			
		}

	}

	@Step
	public void click_proceed_with_Quote() throws InterruptedException {

		getDriver().findElement(By.xpath("//button[contains(.,'Proceed With Quote')]")).click();
		Thread.sleep(9000);

	}

	public void verify_interested_party(String interested_party_count, String Location_Identifier, DataTable arg1)
			throws InterruptedException {

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		String sheetName = "InterestedParties";
		int Location_Number = Integer.parseInt(Location_Identifier);
		int Row_Count = XLS.getRowCount(sheetName);
		List<List<String>> data = arg1.raw();
		for (int row = 2; row <= Row_Count; row++) {
			String vac_test_data = data.get(1).get(0).toString().replace("[", "").replace("]", "").trim();

			System.out.println("Interested Parties test data --" + vac_test_data);
			if (vac_test_data.equalsIgnoreCase(XLS.getCellData(sheetName, "TestDataFlows", row))) {
				Location_Number = row;
				break ;		
		}
		}
	
  	    String role = XLS.getCellData(sheetName, "Role", Location_Number);
  		String financialInstituteName=XLS.getCellData(sheetName, "FinancialInstitutionName", Location_Number);
		String rank=XLS.getCellData(sheetName, "Rank", Location_Number);

		System.out.println("Excel values are "+role +financialInstituteName +rank);
		String role_APP=getDriver().findElement(By.xpath("//a[contains(@data-bind,'root.selectInterestedParty')]")).getText().split(" ")[1];
		role_APP=role_APP.replaceAll("\\(", "");
		role_APP=role_APP.replaceAll("\\)", "");
		System.out.println("After trim"+role_APP);
		String financialInstituteName_APP=getDriver().findElement(By.xpath("//p[@data-bind='text: name']")).getText();
		String rank_APP=getDriver().findElement(By.xpath("//span[contains(@data-bind,'mortgageeRank')]")).getText();
		System.out.println("App values are " +financialInstituteName_APP +rank_APP);
		
		if(role_APP.contains(role) 
			&&financialInstituteName.equals(financialInstituteName_APP) && rank.equals(rank_APP)){
			Assert.assertTrue("Inerested Party is Added Succesfully", true);
		}
		else
			Assert.assertTrue("Inerested Party is not Added", false);
	} 
		
	
	public void verify_Role_DropdownOptions(String optionName)
			throws InterruptedException {
		
		List<WebElement> elments=getDriver().findElements(By.xpath("//select[@id='addInterestedParty_Type']/option"));
		if(elments.size()==1)
			if(elments.get(0).getText().equals(optionName))
			{
				Assert.assertTrue("only option is displayed in Role lis i.e "+optionName, true);	
		}
		else
			Assert.assertTrue("only option is not displayed in Role lis i.e", false);
		
	}

	public void click_checkbox_InterestedParty() throws InterruptedException {
		getDriver().findElement(By.xpath("//input[contains(@data-bind,'root.selectProperty.bind')]")).click();
		Thread.sleep(6000);
		
	}

	public void click_Cancel() throws InterruptedException {
		WebElement element=getDriver().findElement(By.xpath("//span[button[contains(text(),'Cancel')]]"));
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
		element.click();
		Thread.sleep(6000);
		
	}

	public void verifyAllMandatoryFiledsErrorMessages() {
		List<WebElement> errorMessageList=getDriver().findElements(By.xpath("//span[contains(text(),'This field is required.')]"));
		String errorMessage="This field is required.";
		int noOfErros=errorMessageList.size();
		boolean flag=true;
		String financialInsititueError=getDriver().findElement(By.xpath("//span[@id='addInterestedParty_FinancialInstitution-error']")).getText();
		String roleError=getDriver().findElement(By.xpath("//span[@id='addInterestedParty_Rank-error']")).getText();
			
		if(!financialInsititueError.equalsIgnoreCase(errorMessage) 
				&&roleError.equalsIgnoreCase(errorMessage) 
				)
		{
			flag=false;
		}
		if(noOfErros==2 && flag)
		{
			Assert.assertTrue("Interested Party field required error messages are displayed", true);
		}
		else
		 Assert.assertTrue("Interested Party field required error messages are displayed", false);
	}
		
	} 




