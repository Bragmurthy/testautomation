package au.com.chubb.evolutionshowcase.pages.stepDefinitions.policyInformation;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.policyInformation.effectiveTerm_Steps;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

public class effectiveTerm_SD {

	@Steps
	effectiveTerm_Steps effectiveTerm;

	@And("^modify effective date \"([^\"]*)\"$")
	public void modify_effective_date(String effectiveDate) throws Throwable {
		effectiveTerm.modify_effective_date(effectiveDate);
		Thread.sleep(1000);
	}

	@And("^modify expiry date \"([^\"]*)\"$")
	public void modify_expiry_date(String expiryDate) throws Throwable {
		effectiveTerm.modify_expiry_date(expiryDate);
		Thread.sleep(1000);
	}

	@And("^modify submission received date \"([^\"]*)\"$")
	public void enter_expiry_date(String submissionReceivedDate) throws Throwable {
		effectiveTerm.modify_submission_received_date(submissionReceivedDate);
		Thread.sleep(1000);
	}

}
