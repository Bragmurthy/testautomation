package au.com.chubb.evolutionshowcase.pages.stepDefinitions.eBusinessPortal;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.eBusinessPortal.eBusinessPortalPremiumSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class eBusinessPortalpremiumDetailsStepDefinition {

	@Steps
	eBusinessPortalPremiumSteps eBusiness_potal_premium_steps;
	
	
	@Then("^validate premium details for indicative quote$")
	public void validate_premium_details() throws Throwable {
		eBusiness_potal_premium_steps.validate_premium_details();
	}
	
//	@Then("^\"([^\"]*)\" premium details for indicative quote$")
//	public void save_verify_calculated_Premium_indicativequote(String test_data) throws Throwable {
//		eBusiness_potal_premium_steps.save_verify_calculated_Premium(test_data);
//	}
	
	
	//working fine below 2
	@Then("^save premium details from ebix$")
	public void save_calculated_Premium_ebix() throws Throwable {
		eBusiness_potal_premium_steps.save_calculated_Premium_ebix();
	}
	
	
	@Then("^verify premium details from ebix$")
	public void verify_calculated_Premium_ebix_fullquote() throws Throwable {
		eBusiness_potal_premium_steps.verify_calculated_Premium_ebix_fullquote();
	}
	

}