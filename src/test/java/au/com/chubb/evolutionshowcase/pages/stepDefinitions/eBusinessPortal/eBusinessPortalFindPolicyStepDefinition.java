package au.com.chubb.evolutionshowcase.pages.stepDefinitions.eBusinessPortal;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.eBusinessPortal.eBusinessPortalFindPolicySteps;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

public class eBusinessPortalFindPolicyStepDefinition {

	@Steps
	eBusinessPortalFindPolicySteps find_policy;

	
	@And("^click on find policy$")
	public void click_find_policy() throws Throwable {
		find_policy.click_find_policy();
	}

	@And("^enter policy number saved in \"([^\"]*)\" and search$")
	public void enter_policy_number(String policynumber) throws Throwable {
		find_policy.enter_policy_number(policynumber);
	}
	
	@And("^select transaction state as \"([^\"]*)\"$")
	public void select_transaction_type(String transaction) throws Throwable {
		find_policy.enter_policy_number(transaction);
	}
	
	@And("^search for policy number stored in \"([^\"]*)\"$")
	public void searchQuoteIneBix(String testData) throws Throwable {
		find_policy.searchPolicy(testData);
	}
	
	@And("^click View in Ebix policy page$")
	public void click_View_Ebix () throws Throwable {
		find_policy.click_View_Ebix();
	}

//	@And("^search for quote reference number from \"([^\"]*)\" and verify status of quote is \"([^\"]*)\"$")
//	public void verify_quote_status(String testData, String quoteStatus) throws Throwable {
//		find_quote.verify_quote_status(testData,quoteStatus );
//	}
//	
//	@And("^search for quote reference number from \"([^\"]*)\" and verify status of quote is \"([^\"]*)\" by passing Client ID in ebusinenss porlrta$")
//	public void verify_quote_status_by_passing_clientId(String testData, String quoteStatus) throws Throwable {
//		find_quote.verify_quote_status_by_passing_clientId (testData,quoteStatus );
//	}
	
	
}