package au.com.chubb.evolutionshowcase.pages.stepDefinitions.policyInformation;

import java.io.IOException;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote.riskLocation_calculatePremium_Steps;
import au.com.chubb.evolutionshowcase.pages.serenitySteps.policyInformation.clientDetails_Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class clientDetails_SD {

	@Steps
	clientDetails_Steps cleintDetails;
	riskLocation_calculatePremium_Steps steps;

	@Then("^Client Name First and Surname should default from Sunrise$")
	public void default_clientname() throws InterruptedException, IOException{
		cleintDetails.default_clientname();
	}
	
	@And("^Date of Birth, Email address, Phone Number should default from Sunrise, if available for \"([^\"]*)\"$")
	public void default_clientDetails(String custID) throws InterruptedException  {
		cleintDetails.default_clientDetails(custID);
	}
	
	@And("^'Update Client' button enabled for selection$")
	public void Update_ClientbuttonEnabled() throws InterruptedException  {
		steps.click_button("Update Client");
	}
	
	@Then("^Email,Retired fields should be mandatory and fields are non editable$")
	public void verifydefaultValuesFromSunrise() throws IOException  {
		cleintDetails.verifydefaultValuesFromSunrise();
	}
	@When("^There are no validation error message when the data is saved in Edit Client page$")
	public void there_are_no_validation_error_message_when_the_data_is_saved_in_Edit_Client_page() {
	  
	}
	
	@When("^client details are edited$")
	public void editClientDetails() throws InterruptedException {
		cleintDetails.editClientDetails();
	}
	
	@When("^all client details should be saved$")
	public void verifyClientDetails() {
		cleintDetails.verifyClientDetails();
	}
	
	@When("^all client details should be saved in UW view$")
	public void verifyClientDetailsInUWView() {
		cleintDetails.verifyClientDetailsInUWView();
	}
	
	
	@Then("^click on update client button in Policy Information page$")
	public void fields_should_be_mandatory_and_fields_are_non_editable() throws InterruptedException  {
		cleintDetails.clickOnUpdateClient();
	}
	
	
	@And("^\"([^\"]*)\" field should be displayed$")
	public void verifyUpdateClient(String button_name) throws Throwable {
		cleintDetails.verifyUpdateClient();
	
	}
	
	@And("^\"([^\"]*)\" field must be displayed$")
	public void verify_field_must_displayed(String text) throws Throwable {
		cleintDetails.verify_field_must_displayed(text);
	
	}
	
	@And("^validate \"([^\"]*)\" field must be displayed$")
	public void verify_field_must_displayed_full_text(String text) throws Throwable {
		cleintDetails.verify_field_must_displayed_full_text(text);
	
	}	
	
	@And("^validate broker contact required message from Add broker contact$")
	public void verify_broker_contact_message() throws Throwable {
		cleintDetails.verify_broker_contact_message();
	
	}
	
	@And("^\"([^\"]*)\" id field must be read only$")
	public void verify_field_must_readonly(String text) throws Throwable {
		cleintDetails.verify_field_must_readonly(text);
	
	}
	
	@And("^\"([^\"]*)\" class field must be read only$")
	public void verify_class_field_must_readonly(String text) throws Throwable {
		cleintDetails.verify_class_field_must_readonly(text);
	
	}
	
	@And("^verify field required error message$")
	public void verify_Field_Required() throws Throwable {
		cleintDetails.verify_Field_Required();
	
	}
	
	@And("^\"([^\"]*)\" class field must not read only$")
	public void verify_class_field_must_not_readonly(String text) throws Throwable {
		cleintDetails.verify_class_field_must_not_readonly(text);
	
	}
	
	@And("^\"([^\"]*)\" field must not read only$")
	public void verify_field_must_not_readonly(String text) throws Throwable {
		cleintDetails.verify_field_must_not_readonly(text);
	
	}
	
	@And("^\"([^\"]*)\" field must be mandatory$")
	public void verify_field_must_mandatory(String text) throws Throwable {
		cleintDetails.verify_field_must_mandatory(text);
	
	}
	
	@And("^\"([^\"]*)\" field must be optional$")
	public void verify_field_must_optional(String text) throws Throwable {
		cleintDetails.verify_field_must_optional(text);
	
	}
	
	@And("^verify below details are populated in update client$")
	public void address_data_auto_populate(DataTable arg1) throws Throwable {
		cleintDetails.verifyUpdateClientDetails(arg1);
	}
	
	
}
