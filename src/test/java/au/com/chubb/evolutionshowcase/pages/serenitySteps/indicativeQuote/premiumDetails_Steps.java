package au.com.chubb.evolutionshowcase.pages.serenitySteps.indicativeQuote;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

@RunWith(SerenityRunner.class)
public class premiumDetails_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public premiumDetails_Steps() throws IOException {
	}

	public void savePremiumetails(String scenario) {
		// TODO Auto-generated method stub
		
	}
	
	@Step
	public void verifyWorkersCompensationText(String expectedText) throws InterruptedException {
	Thread.sleep(10000);
	String CompensationText_APP=getDriver().findElement(By.xpath("//label[contains(text(),'Compensation coverage included')]")).getText();
	if(CompensationText_APP.equals(expectedText) )
	{
	System.out.println("test Passed ");
	Assert.assertTrue("worker's compensation text is displayed successfully ", true);
	}
	else
	{
	System.out.println("test Failed ");
	Assert.assertTrue("worker's compensation text is displayed successfully ", false);
	}
	}
}