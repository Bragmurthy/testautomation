package au.com.chubb.evolutionshowcase.pages.serenitySteps.appraisals;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.tools.ant.types.CommandlineJava.SysProperties;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import au.com.chubb.evolutionshowcase.utilities.DynamicSeleniumFunctions;
import au.com.chubb.evolutionshowcase.utilities.XLSReader;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;

import java.util.regex.*;

@RunWith(SerenityRunner.class)
public class appraisals_Steps extends PageObject {
	String root = System.getProperty("user.dir");
	public static Properties OR = null;

	public appraisals_Steps() throws IOException {
		OR = new Properties();
		FileInputStream fo = new FileInputStream(root + "/ObjectRepository/policyInformation.properties");
		OR.load(fo);

	}

	@Step
	public void click_proceed_with_Quote() throws InterruptedException {

		getDriver().findElement(By.xpath("//button[contains(.,'Proceed With Quote')]")).click();
		Thread.sleep(7000);

	}

	@Step
	public void enter_appraisal_details(String location, String testData) throws InterruptedException {

		int location_identifier = Integer.parseInt(location);
		getDriver().findElement(By.id("selectProperty_" + location_identifier)).click();
		Thread.sleep(3000);

		String sheetName = "Appraisals";

		XLSReader XLS = new XLSReader(root + "/testdata/RiskandCoveragesDetails.xlsx");
		int Row_Count = XLS.getRowCount(sheetName);
		int rowNum = 0;
		loop: for (int row = 2; row <= Row_Count; row++) {
			if (XLS.getCellData(sheetName, "TestDataFlows", row).equalsIgnoreCase(testData)) {
				rowNum = row;
				break loop;
			}

		}

		String AppraisalType = XLS.getCellData(sheetName, "AppraisalType_" + location_identifier, rowNum);
		try {
			new Select(getDriver().findElement(By.id("property_" + location_identifier + "_AppraisalType")))
					.selectByVisibleText(XLS.getCellData(sheetName, "AppraisalType_" + location_identifier, rowNum));
			Thread.sleep(100);
		} catch (Exception e) {

		}
		if (AppraisalType.equalsIgnoreCase("Exterior")) {

		} else {
			try {
				new Select(getDriver().findElement(By.id("property_" + location_identifier + "_AppraisalTypeChangeReason")))
						.selectByVisibleText(
								XLS.getCellData(sheetName, "JustificationForAmendments_" + location_identifier, rowNum));
				Thread.sleep(100);
			} catch (Exception e) {

			}
		}

		try {

			getDriver().findElement(By.id("property_" + location_identifier + "_AppraisalTypeSpecialInstructions"))
					.sendKeys(XLS.getCellData(sheetName, "SpecialInstructions_" + location_identifier, rowNum));
			Thread.sleep(100);
		} catch (Exception e) {

		}

		try {
			getDriver().findElement(By.id("property_" + location_identifier + "_AppraisalAmount"))
					.sendKeys(XLS.getCellData(sheetName, "AppraisalAmount_" + location_identifier, rowNum));
			Thread.sleep(500);
		} catch (Exception e) {

		}

		try {
			getDriver().findElement(By.id("property_" + location_identifier + "_AppraisalDate")).click();
			Thread.sleep(500);
			getDriver().findElement(By.id("property_" + location_identifier + "_AppraisalDate"))
					.sendKeys(XLS.getCellData(sheetName, "AppraisalDate_" + location_identifier, rowNum));
			Thread.sleep(500);
		} catch (Exception e) {

		}

		try {
			getDriver().findElement(By.id("property_" + location_identifier + "_AppraisalDate")).sendKeys(Keys.ENTER);
			Thread.sleep(300);
		} catch (Exception e) {

		}

		try {
			getDriver().findElement(By.id("property_" + location_identifier + "_IndexedAppraisalAmount"))
					.sendKeys(XLS.getCellData(sheetName, "IndexedAppraisalAmount_" + location_identifier, rowNum));
			Thread.sleep(100);
		} catch (Exception e) {

		}

	}

	public void verify_appraisal_type(String location, String appraisalType) throws InterruptedException {
		int location_identifier = Integer.parseInt(location);
		getDriver().findElement(By.id("selectProperty_" + location_identifier)).click();
		Thread.sleep(3000);
		Select appraisal_type_dropdown=new Select(getDriver().findElement(By.id("property_" + location_identifier + "_AppraisalType")));
		String apprisalType_APP=appraisal_type_dropdown.getFirstSelectedOption().getText();;
		System.out.println("displayed  value " +apprisalType_APP);
		if (apprisalType_APP.equalsIgnoreCase(appraisalType))
		{
			Assert.assertTrue("Default value in Appraisal Type drop down is " +apprisalType_APP, true);
			
		}
		else
		{
			Assert.assertTrue("Default value in Appraisal Type drop down is " +apprisalType_APP, false);

		}
		
		
		
	}

}
