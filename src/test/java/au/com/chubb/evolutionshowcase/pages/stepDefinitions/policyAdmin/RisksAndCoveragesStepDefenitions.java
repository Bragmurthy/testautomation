package au.com.chubb.evolutionshowcase.pages.stepDefinitions.policyAdmin;

import au.com.chubb.evolutionshowcase.pages.serenitySteps.policyAdmin.RisksAndCoveragesSteps;
import au.com.chubb.evolutionshowcase.utilities.CommonFunctions;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

public class RisksAndCoveragesStepDefenitions {
	@Steps
	RisksAndCoveragesSteps risksAndCoveragesSteps;
	CommonFunctions common_functions;
	
	
	@And("^change Elias rating from Admin Policy for \"([^\"]*)\"$")
	public void ApproveQuote(String testdata) throws Throwable {
		risksAndCoveragesSteps.changeELIASrating(testdata);
	}
	

}